/* 
 * File:   RessourceContainer.cpp
 * Author: Kevin
 * 
 * Created on 1. Oktober 2015, 11:58
 */

#include "RessourceContainer.h"

#include "LoaderLws.h"
#include "App.h"
#include "LoaderLzo.h"

#include <climits>
#include <iostream>

std::deque<RessourceContainer::Load> RessourceContainer::s_loadList;

RessourceContainer::RessourceContainer() {
}

RessourceContainer::~RessourceContainer() {
}

void RessourceContainer::loadMesh(Mesh** pointer, const std::string& name) {
    s_loadList.push_back(Load());
    
    s_loadList.back().file = name;
    s_loadList.back().type = TYPE_MESH;
    s_loadList.back().ptrMesh = pointer;
}

void RessourceContainer::loadShape(Mesh** pointer, const std::string& name) {
   s_loadList.push_back(Load());
    
    s_loadList.back().file = name;
    s_loadList.back().type = TYPE_SHAPE;
    s_loadList.back().ptrMesh = pointer;
}

void RessourceContainer::LoadActivity(Activity** pointer, const std::string& name) {
    s_loadList.push_back(Load());
    
    s_loadList.back().file = name;
    s_loadList.back().type = TYPE_ACTIVITY;
    s_loadList.back().ptrActivity = pointer;
}

void RessourceContainer::loadTexture(Texture** pointer, const std::string& name) {
    s_loadList.push_back(Load());
    
    s_loadList.back().file = name;
    s_loadList.back().type = TYPE_TEXTURE;
    s_loadList.back().ptrTexture = pointer;
}

void RessourceContainer::loadFont(Font** pointer, const std::string& name) {
    s_loadList.push_back(Load());
    
    s_loadList.back().file = name;
    s_loadList.back().type = TYPE_FONT;
    s_loadList.back().ptrFont = pointer;
}

void RessourceContainer::loadIcon(MenuIcon* pointer, const std::string& name) {
    const std::string path = "Interface\\Icons\\";
    
    loadTexture(&((*pointer).normal)  , path       + name + ".bmp");
    loadTexture(&((*pointer).pushed)  , path + "P" + name + ".bmp");
    loadTexture(&((*pointer).disabled), path + "N" + name + ".bmp");
}

void RessourceContainer::loadMenuIcon(MenuIcon* pointer, const std::string& name) {
    const std::string path = "Interface\\Menus\\";
    
    loadTexture(&((*pointer).normal)  , path       + name + ".bmp");
    loadTexture(&((*pointer).pushed)  , path + "P" + name + ".bmp");
    loadTexture(&((*pointer).disabled), path + "N" + name + ".bmp");
}


bool RessourceContainer::loadNext() {
    if (s_loadList.size()){
        Load load = s_loadList.front();

        switch(load.type){
            case TYPE_ACTIVITY:
                loadActivity(load);
                break;
            case TYPE_FONT:
                loadFont(load);
                break;
            case TYPE_MESH:
                loadMesh(load);
                break;
            case TYPE_TEXTURE:
                loadTexture(load);
                break;
            case TYPE_SHAPE:
                loadShape(load);
                break;
        }

        s_loadList.pop_front();
        
        return true;
    }else{
        return false;
    }
}

void RessourceContainer::load(int count) {
    while (count > 0 && loadNext()){
        count--;
    }
}

void RessourceContainer::loadAll() {
    load(INT_MAX);
}

int RessourceContainer::getCount() {
    return s_loadList.size();
}

void RessourceContainer::loadActivity(Load& load) {
    *(load.ptrActivity) = App::getApp()->getFileLoader()->loadActivity(load.file);
}

void RessourceContainer::loadFont(Load& load) {
    (*(load.ptrFont)) = App::getApp()->getFileLoader()->loadFont(load.file);
}

void RessourceContainer::loadMesh(Load& load) {
    LoaderLws lwsLoader;
    
    lwsLoader.load(load.file);
    *(load.ptrMesh) = lwsLoader.createMesh();
    App::getApp()->getEngine3D()->addMesh(*(load.ptrMesh));
}

void RessourceContainer::loadTexture(Load& load) {
    Texture* texture = App::getApp()->getFileLoader()->loadTexture(load.file);
    
    if (texture == nullptr){
        std::cout << "[ERROR] couldn't load \"" << load.file << "\"" << std::endl;
    }
    
    *(load.ptrTexture) = texture;
}

void RessourceContainer::loadShape(Load& load) {
    //std::cout << "Load Shape: \"" << load.file << "\"" << std::endl;
     LoaderLzo lzoLoader;
    
    if (lzoLoader.load(load.file)){
        Mesh* mesh = new Mesh();

        int groupId = mesh->addGroup();

        for (int a = 0; a < lzoLoader.getNumShapes(); a++){
            mesh->addShape(groupId, lzoLoader.getShape(a));
        }

        App::getApp()->getEngine3D()->addMesh(mesh);

        *(load.ptrMesh) = mesh;
    }else{
        *(load.ptrMesh) = nullptr;
    }
}

