/* 
 * File:   ObjectInfo.cpp
 * Author: Kevin
 * 
 * Created on 8. September 2015, 12:08
 */

#include "ObjectInfo.h"
#include <algorithm>

ObjectInfo::ObjectInfo() {
}

ObjectInfo::~ObjectInfo() {
}

bool ObjectInfo::isBuilding() {
    return getNumTiles() > 0;
}

int ObjectInfo::getNumTiles() {
    return m_tiles.size();
}

glm::ivec2 ObjectInfo::getTilePos(int id) {
    return m_tiles[id].pos;
}

ObjectInfo::TileType ObjectInfo::getTileType(int id) {
    return m_tiles[id].type;
}

void ObjectInfo::addTile(const glm::ivec2& pos, TileType type) {
    m_tiles.push_back(Tile{pos, type});
}


void ObjectInfo::addActivity(ActivityNew* activity) {
    //name has to be lower case
    std::transform(activity->name.begin(), activity->name.end(), activity->name.begin(), ::tolower); 
    
    m_activities.push_back(activity);
}

int ObjectInfo::countActivity() {
    return m_activities.size();
}

ObjectInfo::ActivityNew* ObjectInfo::getActivity(int id) {
    return m_activities[id];
}

ObjectInfo::ActivityNew* ObjectInfo::getActivity(const std::string& name) {
    std::string lName = name;
    std::transform(lName.begin(), lName.end(), lName.begin(), ::tolower);
    
    for (int a = 0; a < m_activities.size(); a++){
        if (m_activities[a]->name == lName){
            return m_activities[a];
        }
    }
}

void ObjectInfo::addUpgrade(Upgrade* upgrade) {
    m_upgrades.push_back(upgrade);
}

int ObjectInfo::countUpgrades(std::bitset<4> upgrades) {
    int count = 0;
    
    for (auto up: m_upgrades){
        if (up->upgrades == upgrades){
            count++;
        }
    }
    
    return count;
}

ObjectInfo::Upgrade* ObjectInfo::getUpgrade(std::bitset<4> upgrades, int id) {
    for (auto up: m_upgrades){
        if (up->upgrades == upgrades){
            if (id-- <= 0){
                return up;
            }
        }
    }
    
    return nullptr;
}

void ObjectInfo::addAnimInfo(AnimInfo* info) {
    m_animInfos.push_back(info);
}

AnimInfo* ObjectInfo::getAnimInfo(int id) {
    if (m_animInfos.size() > 0){
        return m_animInfos[id];
    }
    
    return nullptr;
}

int ObjectInfo::getNumAnimInfo() {
    return m_animInfos.size();
}


