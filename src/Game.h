/* 
 * File:   Game.h
 * Author: Kevin
 *
 * Created on 13. August 2015, 15:55
 */

#ifndef GAME_H
#define	GAME_H

#include <vector>

#include <glm/vec2.hpp>

#include "GameMap.h"
#include "TaskManager.h"
#include "ObjectInfo.h"

class GameListener;

class Game {
public:
    enum ObjectType{
        //material
        OBJ_ORE,
        OBJ_CRYSTAL,
        OBJ_BRICK,
        OBJ_DYNAMITE,
        
        //units
        OBJ_FIGURE,
        
        //vehicles
        OBJ_HOVERSCOUT,
        OBJ_SMALLDRILL,
        OBJ_SMALLTRANSPORTER,
        OBJ_RAPIDRIDER,
        OBJ_GRANITEGRINDER,
        OBJ_LOADERDOZER,
        OBJ_CHROMECRUSHER,
        OBJ_TUNNELTRANSPORT,
        
        //buildings
        OBJ_SMALLTELPORTER,
        OBJ_LARGETELEPORTER,
        OBJ_TOOLSTATION,
        OBJ_FENCE,
        OBJ_POWERSTATION,
        OBJ_UPGRADE,
        OBJ_DOCKS,
        OBJ_GUNSTATION,
        OBJ_OREREFINERY,
        OBJ_SUPPLY,
        
        //enemies
        OBJ_ROCKMONSTER,
        OBJ_TINYROCKMONSTER,
        OBJ_SLIMYSLUG,
        OBJ_BATS,
        OBJ_SPIDER,
    };
    
    enum PriotyType{
        PRIO_BUILD,
        PRIO_DRILL,
        PRIO_CLEAN,
        PRIO_ORE,
        PRIO_CRYSTAL,
        PRIO_REPAIR,
        PRIO_DRIVE,
        PRIO_REINFORCE,
        PRIO_RECHARGE
    };
    
    Game();
    Game(const Game& orig) = delete;
    ~Game();
    
    void createTestObjects();
    
    void addObjectInfo(ObjectInfo* obj);
    ObjectInfo* getObjectInfo(int id);
    
    void addListener(GameListener* listener);
    void removeListener(GameListener* listener);
    
    int registerObject(MapObject* object);
    void removeObject(MapObject* object);
    
    void selectObject(uint32_t objId, bool state);
    void selectTile(glm::ivec2 tile);
    void deselectTile();
    
    glm::vec2 getRessDest();
    void setRessDest(const glm::vec2& pos);
    
    void setGameMap(GameMap* gameMap);
    GameMap* getGameMap();
    
    TaskManager* getTaskManager();
    
    glm::ivec2 getSelectedTile();
    
    void update();
    
    int getOre();
    int getCrystal();
    int getBricks();
    
    void setOre(int ore);
    void setCrystal(int crystal);
    int setBricks(int bricks);
    
    void notifyRessChange();
private:
    GameMap* m_gameMap = nullptr;
    TaskManager m_taskManager;
    
    std::vector<MapObject*> m_object;
    std::vector<GameListener*> m_listener;
    
    bool m_tileSelected = false;
    glm::ivec2 m_selectedTile;
    
    glm::vec2 m_ressDest;
    
    int m_ore = 0;
    int m_crystal = 0;
    int m_bricks = 0;
    
    std::vector<ObjectInfo*> m_objectInfo;
};

#endif	/* GAME_H */

