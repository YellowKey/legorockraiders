/* 
 * File:   WidgetRightPanel.h
 * Author: Kevin
 *
 * Created on 22. Juni 2015, 17:59
 */

#ifndef WIDGETRIGHTPANEL_H
#define	WIDGETRIGHTPANEL_H

#include "Widget.h"
#include "WidgetText.h"
#include "Texture.h"
#include "Font.h"

#include "InterfaceRessorces.h"

class WidgetRightPanel: public Widget{
public:
    WidgetRightPanel();
    WidgetRightPanel(const WidgetRightPanel& orig) = delete;
    virtual ~WidgetRightPanel();
    
    void setTextures(const InterfaceRessources* ress);
    
    void setTextureMain(Texture* texture);
    
    void setTextureCrystal(Texture* texture);
    void setTextureCrystalEmpty(Texture* texture);
    void setTextureCrystalUsed(Texture* texture);
    
    void setTextureOre(Texture* texture);
    
    void setFont(Font* font);
    
    void setCrystalGoal(int count);
    void setCrystalCount(int count);
    void setCrystalUsedCount(int count);
    void setOreCount(int count);
    
    virtual bool isBeeingHovered() override;
protected:
    virtual void onPaint(Renderer2D* renderer) override;
    virtual void onUpdate() override;

    float getOreBarHeight();
private:
    Texture* m_textureMain = nullptr;
    
    Texture* m_textureCrystal      = nullptr;
    Texture* m_textureCrystalEmpty = nullptr;
    Texture* m_textureCrystalUsed  = nullptr;
    
    Texture* m_textureOre = nullptr;
    
    int m_crystalGoal  = 0;
    int m_crystalUsed  = 0;
    int m_crystalCount = 0;
    int m_oreCount     = 0;
    
    WidgetText m_widgetOre;
    WidgetText m_widgetCrystal;
};

#endif	/* WIDGETRIGHTPANEL_H */

