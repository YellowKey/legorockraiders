/* 
 * File:   SlotManager.cpp
 * Author: Kevin
 * 
 * Created on 17. November 2014, 21:03
 */

#include "SlotManager.h"


int SlotManager::reserveSlot(void* obj) {
    int id = getSlot(obj); 
    
    //Object already bound to a slot
    if (id >= 0){
        m_slots[id].trueBound = true;
        return SLOT_OLD;
    }
    
    //Free Slots left
    if (m_slots.size() + 1 < m_maxSlots){
        Slot slot;
        slot.obj       = obj;
        slot.trueBound = true;
        
        m_slots.push_back(slot);
        
        //return id
        return m_slots.size() - 1;
    }else{
        //Find a Slot that is not in use
        for (int id = 0; id < m_slots.size(); id++){
            auto& slot = m_slots[id];
            
            if (slot.trueBound == false){
                slot.obj = this;
                slot.trueBound = true;
                return id;
            }
        }
    }
    return SLOT_ERROR;
}

int SlotManager::getSlot(void* obj) {
    for (int id = 0; id < m_slots.size(); id++){
        auto& slot = m_slots[id];
        
        if (slot.obj == obj){
            return id;
        }
    }
    
    return SLOT_ERROR;
}


void SlotManager::freeSlot(void* obj) {
    for (auto& slot: m_slots){
        if (slot.obj == obj){
            slot.trueBound = false;
        }
    }
}

void SlotManager::clearSlot(int id) {
    m_slots[id].obj = nullptr;
    m_slots[id].trueBound = false;
}

void SlotManager::setMaxSlots(int max) {
    m_maxSlots = max;
}

int SlotManager::getMaxSlots() {
    return m_maxSlots;
}


int SlotManager::getUsedSlot() {
    int num = 0;
    
    for (auto slot: m_slots){
        if (slot.trueBound == true){
            ++num;
        }
    }
    
    return num;
}

bool SlotManager::isTrueBound(void* obj) {
    for (auto slot: m_slots){
        if (slot.obj == obj){
            return slot.trueBound;
        }
    }
    
    return false;
}


