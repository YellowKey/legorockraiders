/* 
 * File:   LoaderLzo.h
 * Author: Kevin
 *
 * Created on 11. Mai 2015, 21:06
 */


#ifndef LZOLOADER_H
#define	LZOLOADER_H

#include <string>
#include <vector>

#include "Shape.h"

class LoaderLzo {
public:
    LoaderLzo();
    LoaderLzo(const LoaderLzo& orig) = delete;
    virtual ~LoaderLzo();
    
    bool load(std::string filename);
    
    Shape* getShape(int id);
    int    getNumShapes();
private:
    struct chunkHandler{
        std::string name;
        void (LoaderLzo::*handler)(int);
    };
    
    struct Vertex{
        glm::vec3 pos;
        glm::vec2 uv;
        glm::vec3 normal;
        int triCount;
    };
    struct Triangle{
        glm::vec3 normal[3];
        int vertex[3];
        int surfaceId;
    };
    struct Surface{
        glm::vec3 color;
        Texture* texture;
        glm::vec3 size;
        glm::vec3 center;
        int axis;
        bool additiveBlending;
        bool doubleSided;
        bool smoothShading;
        std::string name;
        float luminance;
    };
    
    void parseData(const void* data, int size, bool autoRelease);
    void clearData();
    char* createBuffer(int size);
    
    void DebugChunk(int layer, std::string name, int size);
    bool isDebugging();
    
    std::string readString();
    std::string readString(int length);
    uint32_t    readUInt32();
    int32_t     readInt32();
    uint16_t    readUInt16();
    int16_t     readInt16();
    uint8_t     readUInt8();
    int8_t      readInt8();
    char        readChar();
    float       readFloat();
    void        readMem(void* mem, int size);
    
    intptr_t  getDataOffset();
    void setDataOffset(intptr_t pos);
    
    void parseChunk(const std::vector<chunkHandler>& handlerList, const std::string& name, int size);
    
    void readTags(int size);
    void readSurfaceList(int size);
    void readSurface(int size);
    void readVertices(int size);
    void readPolygons(int size);    
    
    void readColor(int size);
    void readLuminance(int size);
    void readDiffuse(int size);
    void readFlags(int size);
    void readColorTexture(int size);
    void readTextureImage(int size);
    void readTextureColor(int size);
    void readTextureSize(int size);
    void readTextureCenter(int size);
    void readTextureSequence(int size);
    void readTextureFlags(int size);
    void readTextureWrap(int size);
    
    void     computeNormals();
    Shape*   createShape(int id);
    Surface* getSurface(int id);
    
    //Handler
    std::vector<chunkHandler> m_chunkHandler = {
        chunkHandler{"TAGS", &LoaderLzo::readTags}, 
        chunkHandler{"SRFS", &LoaderLzo::readSurfaceList},
        chunkHandler{"PNTS", &LoaderLzo::readVertices},
        chunkHandler{"POLS", &LoaderLzo::readPolygons},
        chunkHandler{"SURF", &LoaderLzo::readSurface},
    };
    std::vector<chunkHandler> m_surfHandler = {
        {"COLR", &LoaderLzo::readColor},
        {"FLAG", &LoaderLzo::readFlags}, 
        {"VLUM", &LoaderLzo::readLuminance},
        {"VDIF", &LoaderLzo::readDiffuse},
        {"CTEX", &LoaderLzo::readColorTexture},
        {"TIMG", &LoaderLzo::readTextureImage},
        {"TCLR", &LoaderLzo::readTextureColor},
        {"TSIZ", &LoaderLzo::readTextureSize},
        {"TCTR", &LoaderLzo::readTextureCenter},
        {"TFLG", &LoaderLzo::readTextureFlags},
        {"IMSQ", &LoaderLzo::readTextureSequence},
        {"TWRP", &LoaderLzo::readTextureWrap},
    };
    
    //Data
    const char* m_data = nullptr;
    const char* m_dataPos = nullptr;
    int   m_dataSize;
    bool  m_ownData;
    
    std::string m_path;
    
    int m_currentSurface;
    
    std::vector <Shape*> m_shapes;
    
    std::vector <Vertex> m_vertices;
    std::vector <Triangle> m_triangles;
    std::vector <Surface> m_surfaces;
};

#endif	/* LZOLOADER_H */

