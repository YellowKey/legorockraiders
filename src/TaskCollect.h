/* 
 * File:   TaskCollect.h
 * Author: Kevin
 *
 * Created on 29. August 2015, 15:16
 */

#ifndef TASKPICKUP_H
#define	TASKPICKUP_H

#include "Task.h"

class Ressource;

class TaskCollect: public Task{
public:
    TaskCollect(Ressource* m_ressource);
    TaskCollect(const TaskCollect& orig) = delete ;
    virtual ~TaskCollect();
    
    virtual glm::vec2 getPos() override;
    virtual Task::TaskType getTaskType() override;

    virtual bool assignUnit(Unit* unit) override;
    virtual bool isAssigned() override;

    virtual void unitCanceled() override;
    
    virtual bool operator==(const Task& task) override;
private:
    Ressource* m_ressource = nullptr;
    Unit* m_unit = nullptr;;
    
    bool m_assigned = false;
};

#endif	/* TASKPICKUP_H */

