/* 
 * File:   MapObject.h
 * Author: Kevin
 *
 * Created on 2. Juni 2015, 22:12
 */

#ifndef MAPOBJECT_H
#define	MAPOBJECT_H

#include <vector>
#include <glm/vec3.hpp>

class EngineObject;
class GameMap;

class MapObject {
public:
    MapObject();
    virtual ~MapObject();
    
    virtual void update(); 
    
    void attachTo(MapObject* obj, int slotId);
    void remove();
    
    virtual void setPos(const glm::vec3& pos);
    virtual void setRot(const glm::vec3& rot);
    virtual void setScale(const glm::vec3& scale);
    
    glm::vec3 getPos();
    glm::vec3 getRot();
    glm::vec3 getScale();
    
    virtual MapObject* getSlot(int slotId);
    virtual EngineObject* getSlotEngineObject(int slotId);
    virtual int getSlotGroupId(int slotId);
    virtual glm::vec3 getSlotPos(int slotId);
protected:
    virtual void onRTSChange();
    virtual void onAttachTo(MapObject* target, int slotId);
    virtual void onRemoved();
    
    virtual void changeSlots();
private:
    friend class GameMap;
    
    GameMap* m_gameMap = nullptr;
    
    MapObject* m_target;
    std::vector<MapObject*> m_slots;
    
    glm::vec3 m_pos = {0.0f, 0.0f, 0.0f};
    glm::vec3 m_rot = {0.0f, 0.0f, 0.0f};
    glm::vec3 m_scale = {1.0f, 1.0f, 1.0f};
};

#endif	/* MAPOBJECT_H */

