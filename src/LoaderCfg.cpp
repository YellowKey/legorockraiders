/* 
 * File:   CfgLoader.cpp
 * Author: Kevin
 * 
 * Created on 13. Juni 2015, 15:06
 */

#include "LoaderCfg.h"

#include <cassert>
#include <cstdlib>
#include <iostream>
#include <algorithm>

LoaderCfg::LoaderCfg() {
}

LoaderCfg::~LoaderCfg() {
}


LoaderCfg::Group* LoaderCfg::getMainGroup() {
    return m_mainGroup;
}

void LoaderCfg::disableAutoDelete() {
    m_autoDelete = false;
}

void LoaderCfg::load(void* mem, int size) {
    deleteMainGroup();
    
    m_fileData = m_filePos = (char*)mem;
    m_fileSize = size;
    m_autoDelete = false;
    
    m_mainGroup = loadGroup();
}

LoaderCfg::Group* LoaderCfg::loadGroup() {
    std::string text;
    
    Group* group = nullptr;
    
    while(!group){
        text = readString();
        
        if (text != ""){
            if (text == "{"){
                //ERROR
                nextLine();
            }else if(text == "}"){
                return nullptr;
            }else if(text == ";"){
                nextLine();
            }else{
                group = new Group(text);
            }
        }else{
            nextLine();
        }
    }
    
    text = readString();
    
    if (text == "{"){
        for(;;){
            Group* subGroup = loadGroup();
            
            if (subGroup){
                group->addGroup(subGroup);
            }else{
                break;
            }
        }
    }else{
        group->setValue(text);
    }
    
    return group;
}

void LoaderCfg::deleteMainGroup() {
    if (m_autoDelete){
        delete m_mainGroup;
    }
}




std::string LoaderCfg::readString() {
    std::string text; 
    
    while (isWhitespace(peekChar())){
        pollChar();
    }
    
    if (isControl(peekChar())){
        return text += pollChar();
    }
    
    while (isLetter(peekChar())){
        text += pollChar();
    };
    
    return text;
}

char LoaderCfg::pollChar() {
    if (!eof()){
        return *(m_filePos++);
    }else{
        return '\n';
    }
}

char LoaderCfg::peekChar() {
    if (!eof()){
        return *(m_filePos);
    }else{
        return '\n';
    }
}



void LoaderCfg::nextLine() {
    while (!isNewline(pollChar())){}
}


bool LoaderCfg::isWhitespace(char character) {
    if (character != ' ' && character != '\t'){
        return false;
    }
    
    return true;
}

bool LoaderCfg::isControl(char character) {
    if (character != '{' && character != ';' && character != '}'){
        return false;
    }
    
    return true;
}

bool LoaderCfg::isNewline(char character) {
    if (character != 10 && character != 13){
        return false;
    }
    
    return true;
}

bool LoaderCfg::isLetter(char character) {
    if (!isWhitespace(character) && !isControl(character) && !isNewline(character)){
        return true;
    }
    
    return false;
}



bool LoaderCfg::eof() {
    if (getFilePos() < m_fileSize){
        return false;
    }else{
        return true;
    }
}

int LoaderCfg::getFilePos() {
    return (intptr_t)(m_filePos - m_fileData);
}

void LoaderCfg::setFilePos(int pos) {
    m_filePos = m_filePos + pos;
}



//LoaderCfg::Group

LoaderCfg::Group::Group(const std::string& name):m_name(name){
    std::transform(m_name.begin(), m_name.end(), m_name.begin(), ::tolower);
}

LoaderCfg::Group::~Group() {
    for (auto& child : m_children){
        delete child;
    }
}

std::string LoaderCfg::Group::getString() const{
    return m_value;
}

std::string LoaderCfg::Group::getString(const std::string& name) const {
    auto group = getGroup(name);
    
    if (group){
        return group->getString();
    }
    
    return "";
}


int LoaderCfg::Group::getInt() const{
    return atoi(getString().c_str());
}

int LoaderCfg::Group::getInt(const std::string& name) const {
    auto group = getGroup(name);
    
    if (group){
        return group->getInt();
    }
    
    return 0;
}


float LoaderCfg::Group::getFloat() const{
    return atof(getString().c_str());
}

float LoaderCfg::Group::getFloat(const std::string& name) const {
    auto group = getGroup(name);
    
    if (group){
        return group->getFloat();
    }
    
    return 0.0f;
}


std::string LoaderCfg::Group::getFieldString(char seperator, int index) const{
    int startPos = 0;
    int endPos   = 0;
    
    for (int a = 0; a < index; a++){
        startPos = m_value.find(seperator, startPos);
        
        if (startPos >= 0){ //don't increment if npos (-1))
            startPos++;
        }
    }
    
    if (startPos == std::string::npos){
        return "";
    }
    
    endPos = m_value.find(seperator, startPos);
    
    size_t len = endPos - startPos;
    
    if (endPos == std::string::npos){
        len = std::string::npos;
    }
  
    
    return m_value.substr(startPos, len);
}

std::string LoaderCfg::Group::getFieldString(const std::string& name, char seperator, int index) const {
    auto group = getGroup(name);
    
    if (group){
        return group->getFieldString(seperator, index);
    }
    
    return "";
}


int LoaderCfg::Group::getFieldInt(char seperator, int index) const{
     return atoi(getFieldString(seperator, index).c_str());
}

int LoaderCfg::Group::getFieldInt(const std::string& name, char seperator, int index) const {
    auto group = getGroup(name);
    
    if (group){
        return group->getFieldInt(seperator, index);
    }
    
    return 0;
}

float LoaderCfg::Group::getFieldFloat(char seperator, int index) const{
     return atof(getFieldString(seperator, index).c_str());
}

float LoaderCfg::Group::getFieldFloat(const std::string& name, char seperator, int index) const {
    auto group = getGroup(name);
    
    if (group){
        return group->getFieldFloat(seperator, index);
    }
    
    return 0.0f;
}

std::string LoaderCfg::Group::getName() const{
    return m_name;
}

void LoaderCfg::Group::setValue(const std::string value) {
    m_value = value;
}

int LoaderCfg::Group::getNumChildren() const{
    return m_children.size();
}

const LoaderCfg::Group* LoaderCfg::Group::getChild(int id) const{
    assert(id < m_children.size());
    
    return m_children[id];
}

void LoaderCfg::Group::addGroup(const Group* group) {
    m_children.push_back(group);
} 

const LoaderCfg::Group* LoaderCfg::Group::getGroup(const std::string& name) const{
    std::string lName = name;
    
    std::transform(lName.begin(), lName.end(), lName.begin(), ::tolower);
    
    for (auto& child : m_children){
        if (child->getName() == lName){
            return child;
        }
    }
    
    return nullptr;
}






