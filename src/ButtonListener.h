/* 
 * File:   ButtonListener.h
 * Author: Kevin
 *
 * Created on 14. August 2015, 10:29
 */

#ifndef BUTTONLISTENER_H
#define	BUTTONLISTENER_H

#include "WidgetButton.h"

#include <vector>


class WidgetButton;

class ButtonListener {
public:
    ButtonListener();
    ButtonListener(const ButtonListener& orig) = delete;
    virtual ~ButtonListener();
    
    virtual void onPush(int buttonId, int mouseButton);
    virtual void onRelease(int buttonId, int mouseButton);
    virtual void onClick(int buttonId, int mouseButton);
    virtual void onHover(int buttonId, bool state);
private:
    friend class WidgetButton;
    
    void addButton(WidgetButton* button);
    void removeButton(WidgetButton* button);
    
    std::vector<WidgetButton*> m_buttons;
};

#endif	/* BUTTONLISTENER_H */

