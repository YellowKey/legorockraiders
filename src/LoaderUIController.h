/* 
 * File:   LoaderUIController.h
 * Author: Kevin
 *
 * Created on 1. Oktober 2015, 12:15
 */

#ifndef LOADERUICONTROLLER_H
#define	LOADERUICONTROLLER_H

#include "WidgetText.h"
#include "WidgetImage.h"
#include "WidgetContainer.h"
#include "LoaderRessources.h"

#include <glm/vec2.hpp>

class LoaderUIController {
public:
    LoaderUIController(LoaderRessources& ress);
    LoaderUIController(const LoaderUIController& orig) = delete;
    virtual ~LoaderUIController();
    
    void scale(glm::vec2 size);
    void update();
    void render(Renderer2D* renderer);
    
    bool isDone();
private:
    float getProgress();
    
    WidgetContainer m_widgetScreen;
    WidgetImage m_widgetBackground;
    WidgetImage m_widgetBar;
    WidgetText  m_widgetText;
    
    LoaderRessources& m_ress;
    
    glm::vec2 m_barSize;
    
    int m_count = 0;
    int m_current = 0;
    int m_loadCount = 10;
    
    bool m_done = false;
};

#endif	/* LOADERUICONTROLLER_H */

