/* 
 * File:   UnitFigure.cpp
 * Author: Kevin
 * 
 * Created on 2. Juni 2015, 21:59
 */

#include "UnitFigure.h"

#include "App.h"

#include <iostream>

UnitFigure::UnitFigure() {
    m_activity = App::getApp()->getGameRessource()->pilot;
    
    setStatus(STAND);
}


UnitFigure::~UnitFigure() {
}

void UnitFigure::update() {
    Unit::update();
}


void UnitFigure::onStatusChange(Status status) {
    Mesh* mesh = nullptr;
    bool loop = true;
    
    switch(status){
        case WALK:
            if (isCarring()){
                mesh = m_activity->getAnimMesh(Activity::ANIM_CARRY);
            }else{
                mesh = m_activity->getAnimMesh(Activity::ANIM_ROUTE);
            }
            break;
        case STAND:
            if (isCarring()){
                mesh = m_activity->getAnimMesh(Activity::ANIM_CARRYSTAND);
            }else{
                mesh = m_activity->getAnimMesh(Activity::ANIM_STAND);
            }
            break;
        case DRILL:
            mesh = m_activity->getAnimMesh(Activity::ANIM_DRILL);
            break;
        case CLEAR:
            mesh = m_activity->getAnimMesh(Activity::ANIM_CLEAR);
            break;
        case COLLECT:
            mesh = m_activity->getAnimMesh(Activity::ANIM_COLLECT);
            loop = false;
            break;
        case PLACE:
            mesh = m_activity->getAnimMesh(Activity::ANIM_PLACE);
            loop = false;
            break;
        default:
            mesh = m_activity->getAnimMesh(Activity::ANIM_STAND);
            break;
    }
    
    if (m_engineObject.getMesh() != mesh){
        m_engineObject.setMesh(mesh);
        m_engineObject.playAnimation(loop);
        changeSlots();
    }
}

EngineObject* UnitFigure::getSlotEngineObject(int slotId) {
    return &m_engineObject;
}

int UnitFigure::getSlotGroupId(int slotId) {
    return m_engineObject.getMesh()->getGroup("item_null");
}

