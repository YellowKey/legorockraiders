/* 
 * File:   Building.cpp
 * Author: Kevin
 * 
 * Created on 13. Juni 2015, 14:43
 */

#include "Building.h"

Building::Building(Activity* activity, glm::vec3 pos) {
    m_activity = activity;
    m_engineObject.setPos(pos);
    m_pos = pos;
}


Building::Building(const Building& orig) {
}

Building::~Building() {
}

void Building::update() {
    Mesh* mesh;
    switch(m_state){
        case STATE_TELEPORT:
            mesh = m_activity->getAnimMesh(Activity::ANIM_TELEPORT);
            
            if (m_engineObject.getMesh() == mesh){
                if (!m_engineObject.isAnimRunning()){
                    m_state = STATE_NORMAL;
                }
            }else{
                m_engineObject.setMesh(mesh);
                m_engineObject.playAnimation(false);
            }
            break;
        case STATE_NORMAL:
            mesh = m_activity->getAnimMesh(Activity::ANIM_STAND);
            if (m_engineObject.getMesh() != mesh){
                m_engineObject.setMesh(mesh);
                m_engineObject.playAnimation(true);
            }
            break;
    }
}

void Building::setState(State state) {
    m_state = state;
}
