/* 
 * File:   ActionClear.h
 * Author: Kevin
 *
 * Created on 18. August 2015, 15:50
 */

#ifndef ACTIONCLEAR_H
#define	ACTIONCLEAR_H

#include "Action.h"

class ActionClear: public Action{
public:
    ActionClear(const glm::ivec2& tilePos);
    ActionClear(const ActionClear& orig) = delete;
    virtual ~ActionClear();
    
    virtual void start() override;
    virtual void updateUnit() override;
    virtual void cancel() override;
private:
    glm::ivec2 m_tilePos;
    
    float m_cleanTime = 7.16f;
    float m_startTime;
};

#endif	/* ACTIONCLEAR_H */

