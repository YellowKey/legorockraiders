/* 
 * File:   glShader.h
 * Author: Kevin
 *
 * Created on 22. November 2014, 14:46
 */

#ifndef GLSHADER_H
#define	GLSHADER_H

#include <GL/glew.h>
#include <string>

using std::string;

class glShader {
public:
    enum class Type{
        FRAGMENT,
        VERTEX,
        COMPUTE,
        TESS_CONTROL,
        TESS_EVALUATION,
        GEOMETRY,
    };
    
    glShader(Type type);
    glShader(Type type, const string& fileName);
    ~glShader();
    
    bool load(const string& fileName);
    Type getType() const;
    
    int getOpenGlType() const;
    GLuint getHandle() const;
    
    static const string& setShaderFolder(const string& folder);
    static string getShaderFolder();
protected:
    Type m_type;
    GLuint m_handle;
    
    static string s_shaderFolder;
    
private:
    friend class glProgram;
};

#endif	/* GLSHADER_H */

