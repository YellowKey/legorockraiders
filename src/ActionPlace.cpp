/* 
 * File:   ActionPlace.cpp
 * Author: Kevin
 * 
 * Created on 28. August 2015, 14:15
 */

#include "ActionPlace.h"
#include "App.h"
#include "Ressource.h"

ActionPlace::ActionPlace(Place place):m_place(place){
}

ActionPlace::~ActionPlace() {
}

void ActionPlace::start() {
    m_startTime = App::getApp()->getGameTime();
    getUnit()->setStatus(Unit::PLACE);
}

void ActionPlace::updateUnit() {
    if (App::getApp()->getGameTime() - m_startTime > m_time){
        finish();
        MapObject* obj = getUnit()->drop();
        Ressource* ress = dynamic_cast<Ressource*>(obj);
        
        if (m_place == PLACE_STORE){
            Game* game = App::getApp()->getGame();
            if (ress){
                switch(ress->GetType()){
                    case Ressource::TYPE_ORE:
                        game->setOre(game->getOre() + 1);
                        break;
                    case Ressource::TYPE_CRYSTAL:
                        game->setCrystal(game->getCrystal() + 1);
                        break;
                    case Ressource::TYPE_BRICK:
                        game->setBricks(game->getBricks() + 1);
                        break;
                }
            }
                    
            delete obj;
        }
    }
}

void ActionPlace::cancel() {

}


