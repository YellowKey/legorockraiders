/* 
 * File:   Game.cpp
 * Author: Kevin
 * 
 * Created on 13. August 2015, 15:55
 */

#include "Game.h"
#include "GameListener.h"

#include <algorithm>
#include <iostream>

#include "util.h"
#include "Action.h"
#include "App.h"

Game::Game() {
    m_gameMap = App::getApp()->getGameMap();
}

Game::~Game() {
    
}

void Game::addListener(GameListener* listener) {
    listener->setGame(this);
    
    m_listener.push_back(listener);
}

void Game::removeListener(GameListener* listener) {
    listener->setGame(nullptr);
    
    m_listener.erase(std::remove(m_listener.begin(), m_listener.end(), listener), m_listener.end());
}

void Game::deselectTile() {
    if (m_tileSelected){
        m_tileSelected = false;
        
        m_gameMap->setTileMark(m_selectedTile.x, m_selectedTile.y, GameMap::MARK_SELECTED, false);
        
        for (auto listener: m_listener){
            listener->onTileDeselect();
        }
    }
}

void Game::selectTile(glm::ivec2 tile) {
    if (m_tileSelected == false || m_selectedTile != tile){
        m_tileSelected = true;
        m_selectedTile = tile;
        
        m_gameMap->setTileMark(tile.x, tile.y, GameMap::MARK_SELECTED, true);
        
        for (auto listener: m_listener){
            listener->onTileSelect(tile);
        }
    }
}

glm::ivec2 Game::getSelectedTile() {
    return m_selectedTile;
}


TaskManager* Game::getTaskManager() {
    return &m_taskManager;
}

void Game::update() {
    for (int a = 0; a < m_gameMap->getNumObjects(); a++){
        auto obj = m_gameMap->getObject(a);
        
        Unit* unit = dynamic_cast<Unit*>(obj);
        
        if (unit){
            if (unit->isIdle()){
                m_taskManager.findTaskFor(unit);
            }
        }
    }
}

glm::vec2 Game::getRessDest() {
    return m_ressDest;
}

void Game::setRessDest(const glm::vec2& pos) {
    m_ressDest = pos;
}

int Game::getOre() {
    return m_ore;
}

int Game::getCrystal() {
    return m_crystal;
}

int Game::getBricks() {
    return m_bricks;
}

void Game::setOre(int ore) {
    m_ore = ore;
    
    notifyRessChange();
}

void Game::setCrystal(int crystal) {
    m_crystal = crystal;
    
    notifyRessChange();
}

int Game::setBricks(int bricks) {
    m_bricks = bricks;
    
    notifyRessChange();
}

void Game::notifyRessChange() {
    for (auto listener: m_listener){
        listener->onGameRessChange(getCrystal(), getOre(), getBricks());
    }
}

void Game::addObjectInfo(ObjectInfo* obj) {
    m_objectInfo.push_back(obj);
}

ObjectInfo* Game::getObjectInfo(int id) {
    for (ObjectInfo* obj: m_objectInfo){
        if (obj->id == id){
            return obj;
        }
    }
    
    return nullptr;
}

void Game::setGameMap(GameMap* gameMap) {
    m_gameMap = gameMap;
}

GameMap* Game::getGameMap() {
    return m_gameMap;
}

void Game::createTestObjects() {
    GameRessource* ress = App::getApp()->getGameRessource();
    
    ObjectInfo* smallTeleport = new ObjectInfo();
    smallTeleport->id = OBJ_SMALLTELPORTER;
    smallTeleport->activity = ress->smallTeleport;
    smallTeleport->addTile(glm::ivec2(0, 0), ObjectInfo::TILE_BUILDING);
    smallTeleport->addTile(glm::ivec2(0, -1), ObjectInfo::TILE_POWER);
    addObjectInfo(smallTeleport);
    
    ObjectInfo* supply = new ObjectInfo();
    supply->id = OBJ_SUPPLY;
    supply->activity = ress->geodome;
    supply->addTile(glm::ivec2(0, 0), ObjectInfo::TILE_BUILDING);
    supply->addTile(glm::ivec2(0, -1), ObjectInfo::TILE_POWER);
    addObjectInfo(supply);
    
    ObjectInfo* powerStation = new ObjectInfo();
    powerStation->id = OBJ_POWERSTATION;
    powerStation->activity = ress->powerstation;
    powerStation->addTile(glm::ivec2(0, 0), ObjectInfo::TILE_BUILDING);
    powerStation->addTile(glm::ivec2(1, 0), ObjectInfo::TILE_BUILDING);
    powerStation->addTile(glm::ivec2(0, -1), ObjectInfo::TILE_POWER);
    powerStation->addTile(glm::ivec2(1, -1), ObjectInfo::TILE_POWER);
    addObjectInfo(powerStation);
    
    /*ObjectInfo* smallTruck = new ObjectInfo();
    smallTruck->id = OBJ_SMALLTRANSPORTER;
    smallTruck->activity = ress->smallTruck;
    addObjectInfo(smallTruck);*/
}
