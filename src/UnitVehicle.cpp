/* 
 * File:   UnitVehicle.cpp
 * Author: Kevin
 * 
 * Created on 25. September 2015, 13:13
 */

#include "UnitVehicle.h"
#include "App.h"

#include <iostream>

UnitVehicle::UnitVehicle(ObjectInfo* obj):m_info(obj) {
    auto ress = App::getApp()->getGameRessource();
    
    std::bitset<4> upgrades(0);
    
    for (int a = 0; a < obj->countUpgrades(upgrades); a++){
        auto upgrade = obj->getUpgrade(upgrades, a);
        addAttachment(upgrade->mesh, upgrade->null, upgrade->slot);
        
        //std::cout << "Add: " << upgrade->mesh << " at " << upgrade->null << ", " << upgrade->slot << std::endl;
    }
    /*addAttachment(ress->smallBucket, "normal_bucket_null", 0);
    addAttachment(ress->smallDash, "dash_null", 0);
    addAttachment(ress->standardRear, "upgrade_rear_null", 0);*/
    
    //addWheels(ress->smallWheel, "tyre_null");
}

UnitVehicle::~UnitVehicle() {
    for (auto& a: m_attachments){
        delete a.obj;
    }
}

void UnitVehicle::update() {
    Unit::update();
}

void UnitVehicle::onStatusChange(Status status) {
    Mesh* mesh = nullptr;
    bool loop = true;
    
    switch(status){
        case WALK:
            //mesh = m_info->activity->getAnimMesh(Activity::ANIM_ROUTE);
            mesh = m_info->getActivity("Activity_Route")->mesh;
            break;
        case STAND:
            //mesh = m_info->activity->getAnimMesh(Activity::ANIM_STAND);
            mesh = m_info->getActivity("Activity_Stand")->mesh;
            break;
        default:
            //mesh = m_info->activity->getAnimMesh(Activity::ANIM_STAND);
            mesh = m_info->getActivity("Activity_Stand")->mesh;
            break;
    }
    
    if (m_engineObject.getMesh() != mesh){
        m_engineObject.setMesh(mesh);
        m_engineObject.playAnimation(loop);
        updateAttachments();
        changeSlots();
    }
}

EngineObject* UnitVehicle::getSlotEngineObject(int slotId) {
    return &m_engineObject;
}

int UnitVehicle::getSlotGroupId(int slotId) {
    return m_engineObject.getMesh()->getGroup("item_null");
}

void UnitVehicle::addAttachment(Mesh* mesh, const std::string& null, int index, bool update) {
    m_attachments.emplace_back();
    
    m_attachments.back().obj    = new EngineObject();
    m_attachments.back().index  = index;
    m_attachments.back().null   = null;
    m_attachments.back().update = update;
    
    m_attachments.back().obj->setMesh(mesh);
}

void UnitVehicle::addWheels(Mesh* mesh, const std::string& null) {
    //int count = m_info->activity->getAnimMesh(Activity::ANIM_STAND)->countGroup(null);
    int count = m_info->getActivity("Activity_Stand")->mesh->countGroup(null);
    
    for (int a = 0; a < count; a++){
        addAttachment(mesh, null, a, false);
    }
}

void UnitVehicle::clearUpdateAttachments() {
    
}

void UnitVehicle::updateAttachments() {
    int count = 0;
    for (auto& a: m_attachments){
        int group = m_engineObject.getMesh()->getGroup(a.null, a.index);
        a.obj->attachTo(&m_engineObject, group);
    }
}

bool UnitVehicle::canMove() {
    return true;
}

int UnitVehicle::canCarry() {
    return 4;
}

bool UnitVehicle::canClean() {
    return false;
}

bool UnitVehicle::canDrill() {
    return false;
}


