/* 
 * File:   MapObject.cpp
 * Author: Kevin
 * 
 * Created on 2. Juni 2015, 22:12
 */

#include "MapObject.h"
#include "Action.h"
#include "EngineObject.h"
#include "GameMap.h"

#include <algorithm>

MapObject::MapObject() {
}


MapObject::~MapObject() {
    if (m_gameMap){
        m_gameMap->removeObject(this);
    }
}

void MapObject::update() {

}

void MapObject::setPos(const glm::vec3& pos) {
    m_pos = pos;
    
    onRTSChange();
}

void MapObject::setRot(const glm::vec3& rot) {
    m_rot = rot;
    
    onRTSChange();
}

void MapObject::setScale(const glm::vec3& scale) {
    m_scale = scale;
    
    onRTSChange();
}

glm::vec3 MapObject::getPos() {
    return m_pos;
}

glm::vec3 MapObject::getRot() {
    return m_rot;
}

glm::vec3 MapObject::getScale() {
    return m_scale;
}

MapObject* MapObject::getSlot(int slotId) {
    if (slotId < m_slots.size()){
        return m_slots[slotId];
    }else{
        return nullptr;
    }
}

EngineObject* MapObject::getSlotEngineObject(int slotId) {
    return nullptr;
}

int MapObject::getSlotGroupId(int slotId) {
    return 0;
}


void MapObject::attachTo(MapObject* obj, int slotId) {
    m_target = obj;
    
    if (m_target->m_slots.size() <= slotId){
        m_target->m_slots.resize(slotId + 1);
    }
    
    auto& slot = m_target->m_slots[slotId];
    
    if (slot){
        slot->remove();
    }
    
    slot = this;
    
    onAttachTo(obj, slotId);
}

void MapObject::remove() {
    if (m_target){
        auto& s = m_target->m_slots;
        s.erase(std::remove(s.begin(), s.end(), this), s.end());
        
        onRemoved();
        
        m_target = nullptr;
    }
}


void MapObject::onRTSChange() {

}

void MapObject::onRemoved() {

}

void MapObject::onAttachTo(MapObject* target, int slotId) {

}

/*void MapObject::changeSlot(int slotId) {
    if (slotId < m_slots.size()){
        auto& slot = m_slots[slotId];
        
        if (slot){
            slot->onAttachTo(this, slotId);
        }
    }
}*/

void MapObject::changeSlots() {
    for (int slotId = 0; slotId < m_slots.size(); slotId++){
        const auto& slot = m_slots[slotId];
        if (slot){
            slot->onRemoved();
            slot->onAttachTo(this, slotId);
        }
    }
}

glm::vec3 MapObject::getSlotPos(int slotId) {
    EngineObject* obj = getSlotEngineObject(slotId);
    int group = getSlotGroupId(slotId);
    
    glm::mat4 model = obj->getMatrix();
    
    model = model * obj->getMesh()->getMatrix(group, obj->getAnimTime());
    
    glm::vec4 slotPos = model * glm::vec4(0, 0, 0, 1);
    
    return glm::vec3(slotPos.x, slotPos.y, slotPos.z);
}
