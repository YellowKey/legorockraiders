/* 
 * File:   Texture.h
 * Author: Kevin
 *
 * Created on 26. April 2015, 15:33
 */

#ifndef TEXTURE_H
#define	TEXTURE_H

#include <string>
#include <glm/glm.hpp>

#include "glTexture.h"

class Texture {
public:
    enum Format{
        RGB8,
        RGBA8,
    };
    
    enum Wrap{
        WRAP_BLACK = 0,
        WRAP_CLAMP = 1,
        WRAP_REPEAT = 2,
        WRAP_MIRROR = 3,
    };
    
    Texture();
    Texture(const void* data, int width, int height, Format format = RGB8);
    Texture(const Texture& orig) = delete;
    virtual ~Texture();
    
    void load(const void* data, int widht, int height, Format format = RGB8);
    
    bool isAntiAliased() const;
    bool isAnimated() const;
    
    int getWidth() const;
    int getHeight() const;
    glm::ivec2 getSize() const;
    
    void setTransColor(const glm::vec4& color);
    glm::vec4 getTransColor() const;
    
    glTexture* getGlTexture() const;    
private:
    glTexture* createGlTexture(const void* data, int width, int height, Format format = RGB8);
    
    int16_t m_wrapX = WRAP_REPEAT;
    int16_t m_wrapY = WRAP_REPEAT;
    
    float m_antiAliasing = 1.0f;
    float m_alpha = 1.0f;
    
    glm::vec4 m_transColor = glm::vec4(0);
    
    glTexture* m_texture;
    
    int m_width;
    int m_height;
};

#endif	/* TEXTURE_H */

