/* 
 * File:   Pathfinder.cpp
 * Author: Kevin
 * 
 * Created on 8. Juni 2015, 14:46
 */

#include "Pathfinder.h"
#include "App.h"

#include "util.h"

#include <limits>
#include <iostream>

Pathfinder::Pathfinder() {
    clear();
}

Pathfinder::~Pathfinder() {
    
}

void Pathfinder::setStartPoint(const glm::ivec2& pos) {
    m_startPos = pos;
}

void Pathfinder::setEndPoint(const glm::ivec2& end) {
    m_endPos = end;
}


void Pathfinder::setGroundSpeed(float speed) {
    m_groundSpeed = speed;
}

void Pathfinder::setGravelSpeed(float speed) {
    m_gravelSpeed = speed;
}

void Pathfinder::setPathSpeed(float speed) {
    m_pathSpeed = speed;
}

void Pathfinder::setWaterSpeed(float speed) {
    m_waterSpeed = speed;
}

void Pathfinder::clear() {
    setGroundSpeed(0);
    setGravelSpeed(0);
    setPathSpeed(0);
    setWaterSpeed(0);
}



void Pathfinder::setMap(GameMap* map) {
    delete[] m_tileData;
    
    m_map    = map;
    m_width  = m_map->getWidth();
    m_height = m_map->getHeight();
    
    m_tileData = new TileData[m_width * m_height];
}

void Pathfinder::pathfind() {
    initPathfinding();
    mainPathfinding();
}

void Pathfinder::initPathfinding() {
    for (int y = 0; y < m_height; y++){
        for (int x = 0; x < m_width; x++){
            tile(x, y).distance = std::numeric_limits<float>::infinity();
        }
    }
    
    m_nodes.clear();
    
    addNode({0, m_startPos});
}

void Pathfinder::mainPathfinding() {
    while(!isFinished()){
        checkNode(getNextNode());
    }
}

float Pathfinder::getTileSpeed(const glm::ivec2& pos) {
    auto type = m_map->getTileType(pos);
    
    if (type == GameMap::TILE_GROUND || type == GameMap::TILE_SLUGHOLE){
        return m_groundSpeed;
    }else if(type == GameMap::TILE_GRAVEL){
        return m_gravelSpeed;
    }else if(type == GameMap::TILE_WATER){
        return m_waterSpeed;
    }else if (type == GameMap::TILE_PATH || type == GameMap::TILE_BUILDWALK)
        return m_pathSpeed;
    return 0.0f;
}

void Pathfinder::addNode(const Node& data) {
    auto iter = m_nodes.begin();
    
    while(iter != m_nodes.end() && iter->dist < data.dist){
        iter++;
    }
    
    m_nodes.insert(iter, data);
    
    tile(data.pos).distance = data.dist;
}

Pathfinder::Node Pathfinder::getNextNode() {
    Node node = m_nodes.front();
    
    m_nodes.pop_front();
    
    return node;
}

bool Pathfinder::isFinished() {
    if (m_nodes.size() > 0 && tile(m_endPos).distance == std::numeric_limits<float>::infinity()){
        return false;
    }else{
        return true;
    }
}


void Pathfinder::checkNode(const Node& data) {
    for (const auto& test: m_tests){
        glm::ivec2 pos = data.pos + test.offset;
        float dist = data.dist + test.distance / getTileSpeed(pos);
        
        if (dist < tile(pos).distance){
            addNode({dist, pos});
        }
    }
}

Path Pathfinder::getPath() {
    glm::ivec2 pos = m_endPos;
    Path       path;
    
    if (tile(pos).distance == std::numeric_limits<float>::infinity()){
        return path;
    }
    
    
    path.addNode(pos);
    
    while (pos != m_startPos){
        float minDist = std::numeric_limits<float>::infinity();
        
        glm::ivec2 nextPos = pos;
        
        for (const auto& test:m_tests){
            glm::ivec2 testPos = pos + test.offset;
            float dist = tile(testPos).distance;
            
            if (dist < minDist){
                minDist = dist;
                nextPos = testPos;
            }
        }
        
        if (pos == nextPos){
            return Path();
        }
        
        pos = nextPos;
        
        if (pos != m_startPos){
            path.addNode(nextPos);
        }
    }
    
    return path;
}




