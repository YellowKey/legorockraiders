/* 
 * File:   Renderer2D.cpp
 * Author: Kevin
 * 
 * Created on 2. Mai 2015, 14:36
 */

#include <vector>
#include <glm/detail/func_common.hpp>
#include <fstream>

#include "Renderer2D.h"
#include "glVertexArray.h"
#include "glProgram.h"
#include "glBuffer.h"
#include "Texture.h"
#include "App.h"

//constexpr int Renderer2D::MAXTEXTURES;
constexpr int Renderer2D::BUFFERVERTICES;

Renderer2D::Renderer2D() {
    setupOpenGl();
}

Renderer2D::~Renderer2D() {
}

void Renderer2D::setupOpenGl() {
    m_vertexArray.bind();
    
    m_shader.load("shader/shader2D");
    
    //Max Texture Units
    int maxTextures;
    glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS, &maxTextures);
    //if (maxTextures > MAXTEXTURES) maxTextures = MAXTEXTURES;
    if (!GLEW_ARB_gpu_shader5) maxTextures = 1;
    
    for (int a = 0; a < maxTextures; a++){
        const std::string indexName = "[" + std::to_string(a) + "]";
        m_uniTexture.push_back(TextureUniform());
        m_uniTexture.back().texture    = m_shader.getUniform("colorTexture" + indexName);
        m_uniTexture.back().transColor = m_shader.getUniform("transColor" + indexName);
    }
    
    m_textures.resize(maxTextures);
    
    m_uniScaleFactor = m_shader.getUniform("scaleFactor");
    m_uniScreenOffset = m_shader.getUniform("screenOffset");
    
    m_vertexArray.vertexAttribFormat(0, 2, GL_FLOAT, false, (void*)offsetof(Vertex, pos));
    m_vertexArray.vertexAttribFormat(1, 2, GL_FLOAT, false, (void*)offsetof(Vertex, uv));
    m_vertexArray.vertexAttribFormat(2, 4, GL_FLOAT, false, (void*)offsetof(Vertex, color));
    m_vertexArray.vertexAttribIFormat(3, 1, GL_INT, (void*)offsetof(Vertex, textureId));
    
    m_vertexArray.bindVertexBuffer(0, &m_vertexBuffer, 0, sizeof(Vertex));
    m_vertexArray.vertexAttribBinding(0, 0);
    m_vertexArray.vertexAttribBinding(1, 0);
    m_vertexArray.vertexAttribBinding(2, 0);
    m_vertexArray.vertexAttribBinding(3, 0);
    
    m_vertexArray.enableVertexAttribArray(0);
    m_vertexArray.enableVertexAttribArray(1);
    m_vertexArray.enableVertexAttribArray(2);
    m_vertexArray.enableVertexAttribArray(3);
    
    newBuffer();
}

void Renderer2D::newBuffer() {
    if (m_vertexBuffer.getMapping()){
        m_vertexBuffer.unmapBuffer();
    }
    int size = sizeof(Vertex[BUFFERVERTICES]);
    
    m_vertexBuffer.BufferData(size, nullptr);
    m_vertex = (Vertex*)m_vertexBuffer.mapBufferRange(0, size, GL_MAP_UNSYNCHRONIZED_BIT|GL_MAP_WRITE_BIT);
}


void Renderer2D::flush() {   
    glViewport(0, 0, m_windowSize.x, m_windowSize.y);
    
    m_vertexArray.bind();
    
    m_shader.use();
    
    for (int a = 0; a < m_textures.size(); a++){
        if (m_textures[a]){
            m_uniTexture[a].texture = m_textures[a]->getGlTexture()->bindTexture(a);
            m_uniTexture[a].transColor = m_textures[a]->getTransColor();
        }
    }
    
    m_vertexBuffer.unmapBuffer();
    
    m_uniScaleFactor  =  glm::vec2(2) / m_screenSize;
    m_uniScreenOffset = glm::vec2(-1,-1);
    
    glDrawArrays(GL_TRIANGLES, 0, m_numVertices);
    
    newBuffer();
    
    for (int a = 0; a < m_textures.size(); a++){
        if (m_textures[a]){
            //m_textures[a]->unbindTexture();
        }
        
        if (a != m_currentTextureId){ //TODO: find a better way to do this
            m_textures[a] = nullptr;
        }
    }
    
    m_numVertices = 0;
}

void Renderer2D::addVertex(glm::vec2 pos, glm::vec2 uv) {
    if (m_numVertices + 1 >= BUFFERVERTICES){
        flush();
    }
    
    Vertex& vertex = m_vertex[m_numVertices];
    
    vertex.pos = pos;
    vertex.uv = glm::mix(m_textureStart, m_textureEnd, uv);
    vertex.color = m_color;
    vertex.textureId = m_currentTextureId;
    
    ++m_numVertices;
}

void Renderer2D::setScreenSize(int width, int height) {
    flush();
    m_screenSize = glm::vec2(width, height);
}

void Renderer2D::setWindowSize(int width, int height) {
    m_windowSize = glm::vec2(width, height);
}


void Renderer2D::render(const glm::vec2& pos, const glm::vec2& size) {
    glm::vec2 p0 = pos;
    glm::vec2 p1 = pos + glm::vec2(size.x, 0);
    glm::vec2 p2 = pos + glm::vec2(size.x, size.y);
    glm::vec2 p3 = pos + glm::vec2(0, size.y);
    
    addVertex(p0, glm::vec2(0,0));
    addVertex(p1, glm::vec2(1,0));
    addVertex(p2, glm::vec2(1,1));
    
    addVertex(p2, glm::vec2(1,1));
    addVertex(p3, glm::vec2(0,1));
    addVertex(p0, glm::vec2(0,0));
}

void Renderer2D::renderTexture(const Texture* texture, const glm::vec2& pos, const glm::vec2& size) {
    setTexture(texture);
    render(pos, size);
}

void Renderer2D::setClipRegion(const glm::vec2& start, const glm::vec2& end) {
    flush();
    
    m_clipStart = start;
    m_clipEnd   = end;
}

void Renderer2D::setTexture(const Texture* texture) {
    if (texture == nullptr){
        setTexture(App::getApp()->getDefaultTexture());
        
        return;
    }
    
    setTextureClipping(glm::vec2(0,0), glm::vec2(1,1));
    setColor(glm::vec4(1));
    
    for (int a = 0; a < m_textures.size(); a++){
        if (m_textures[a] == texture){
            m_currentTextureId = a;
            return;
        }
    }
    
    for (int a = 0; a < m_textures.size(); a++){
        if (m_textures[a] == nullptr){
            m_textures[a] = texture;
            m_currentTextureId = a;
            return;
        }
    }
    
    //flush and retry
    flush();
    
    m_textures[m_currentTextureId] = nullptr; //current texture is no longer needed
    
    setTexture(texture);
}

void Renderer2D::setTextureClipping(const glm::vec2& start, const glm::vec2& end) {
    m_textureStart = start;
    m_textureEnd   = end;
}

void Renderer2D::renderText(const std::string& text, const glm::vec2& pos, Align mode) {
    int width = m_font->getWidth(text);
    
    glm::vec2 textPos = pos;
    
    if (mode == RIGHT){
        textPos.x -= width;
    }else if(mode == CENTER){
        textPos.x -= width / 2;
    }
    
    for (int a = 0; a < text.size(); a++){
        textPos = renderChar(text[a], textPos);
    }
}


glm::vec2 Renderer2D::renderChar(int charId, const glm::vec2& pos) {
    const glm::vec2 charSize = m_font->getCharSize(charId) * m_fontScale;

    /*glm::vec2 start = m_font->getCharStart(charId) * glm::vec2(380, 817);
    glm::vec2 end   = m_font->getCharEnd(charId) * glm::vec2(380, 817);
    std::cout << start << " - " << end << std::endl;*/
    
    setTextureClipping(m_font->getCharStart(charId), m_font->getCharEnd(charId));
    //setTextureClipping(glm::vec2(0), glm::vec2(0.5f));
    render(pos, charSize);
    
    return pos + glm::vec2(charSize.x, 0);
}


void Renderer2D::setFont(const Font* font) {
    m_font = font;
    setFontScale(1.0f);
    setTexture(font);
}

void Renderer2D::setFontScale(float size) {
    m_fontScale = size;
}

void Renderer2D::setColor(int red, int green, int blue) {
    setColor((float)red * (1.0f / 255), (float)green * (1.0f / 255), (float)blue * (1.0f / 255));
}

void Renderer2D::setColor(float red, float green, float blue) {
    setColor(glm::vec4(red, green, blue, 1));
}

void Renderer2D::setColor(const glm::vec4 color) {
    m_color = color;
}

