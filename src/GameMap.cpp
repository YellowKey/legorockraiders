/* 
 * File:   GameMap.cpp
 * Author: Kevin
 * 
 * Created on 25. Mai 2015, 17:16
 */

#include "GameMap.h"

#include "App.h"
#include "Ressource.h"

#include <iostream>
#include <glm/gtc/matrix_transform.hpp>
#include <cstring>

#define M_PI		3.14159265358979323846
#define M_PI_2		1.57079632679489661923
#define M_PI_4		0.78539816339744830962

GameMap::GameMap() {
}

GameMap::~GameMap() {
    delete[] m_tiles;
    delete m_ressource;
}

int GameMap::getWidth() {
    return m_width;
}

int GameMap::getHeight() {
    return m_height;
}

void GameMap::loadGameLevel(int id) {
    //"Levels\\GameLevels\\Level14\\Surf_14.map"
    
    std::string number = std::to_string(id);
    
    if (id < 10){
        number = "0" + number;
    }
    
    load("Levels\\GameLevels\\Level" + number + "\\", number);
}

void GameMap::load(const std::string& name, const std::string& id) {
    if (m_ressource == nullptr){
        m_ressource = new MapRessource("Rock");
        m_ressource->load();
    }
    
    m_width  = 0; //Reset height to indicate new map
    m_height = 0;
    
    uint16_t* surfMap = loadMapData(name + "Surf_" + id + ".map");
    uint16_t* duggMap = loadMapData(name + "Dugg_" + id + ".map");
    uint16_t* crorMap = loadMapData(name + "Cror_" + id + ".map");
    
    delete[] m_tiles;
    m_tiles = new Tile[m_width * m_height];
    
    for (int y = 0; y < m_height; y++){
        for (int x = 0; x < m_width; x++){
            uint16_t surf = surfMap[x + m_width * y];
            uint16_t dugg = duggMap[x + m_width * y];
            uint16_t cror = crorMap[x + m_width * y];
            
            if (dugg == 4){ //Slimy Slug hole fix
                dugg = 2;
                surf = TILE_SLUGHOLE;
            }else if (dugg == 3){
                dugg = 1;
                surf = TILE_SLUGHOLE;
            }
            
            if (dugg == 0 && !isTrueWall((TileType)surf)){ //Wall fix
                surf = TILE_DIRT;
            }else if (dugg && isTrueWall((TileType)surf)){
                surf = TILE_GROUND;
            }
            
            
            setTileType(x, y, (TileType)surf);
            setTileVisib(x, y, (TileVisib)dugg);
            setTileRess(x, y, (TileRess)cror);
        }
    }
    
    delete[] surfMap;
    delete[] duggMap;
    delete[] crorMap;
    
    /*for (int y = 0; y < m_height; y++){
        for (int x = 0; x < m_width; x++){
            updateEngineObject(x, y);
        }
    }*/
    updateAll();
}

uint16_t* GameMap::loadMapData(const std::string& name) {
    uint16_t* mapData;
    
    auto* loader = App::getApp()->getDataLoader0();
    
    int fileId = loader->getFileId(name);
    
    if (fileId == -1){
        std::cout << "File \"" << name << "\" not found" << std::endl;
        if (m_width == 0 ||m_height == 0){
            exit(1);
        }
        
        return new uint16_t[m_width * m_height];
    }
    
    char* data = (char*)loader->loadFileData(fileId);
    
    std::string header = std::string(data, 4);
    
    int fileSize = *((int*)(data + 4));
    int width    = *((int*)(data + 8));
    int height   = *((int*)(data + 12));
    
    if (width == 0 || height == 0){
        std::cout << "width or height of map is zero (" << name << ")" << std::endl;
        exit(1);
    }
    if (m_width == 0 && m_height == 0){
        m_width  = width;
        m_height = height;
    }else if (m_width != width || m_height != height){
        std::cout << "width or height changed (" << name << ")" << std::endl;
        exit(1);
    }
    
    mapData = new uint16_t[m_width * m_height];
    memcpy(mapData, data + 16, m_width * m_height * 2);
    
    loader->freeFileData(data);
    
    return mapData;
}


void GameMap::debugMap() {
    std::cout << std::hex;
    
    std::cout << "[Surf]" << std::endl;
    for (int y = 0; y < m_height; y++){
        for (int x = 0; x < m_width; x++){
            std::cout << getTileType(x, y) << " ";
            //std::cout << getHeightmap(x, y) << " ";
        }
        std::cout << std::endl;
    }
    
    std::cout << "[Dugg]" << std::endl;
    for (int y = 0; y < m_height; y++){
        for (int x = 0; x < m_width; x++){
            std::cout << getTileVisib(x, y) << " ";
            //std::cout << getHeightmap(x, y) << " ";
        }
        std::cout << std::endl;
    }
    
    std::cout << "[Ress]" << std::endl;
    for (int y = 0; y < m_height; y++){
        for (int x = 0; x < m_width; x++){
            std::cout << getTileRess(x, y) << " ";
            //std::cout << getHeightmap(x, y) << " ";
        }
        std::cout << std::endl;
    }
    
    std::cout << std::dec;
}

bool GameMap::onMap(int x, int y) {
    if (x >= 0 && x < m_width && y >= 0 && y < m_height){
        return true;
    }else{
        return false;
    }
}

GameMap::Tile& GameMap::tile(int x, int y) {
    return m_tiles[x + y * m_width];
}


GameMap::TileType GameMap::getTileType(int x, int y) {
    if (onMap(x, y)){
        return tile(x, y).type;
    }else{
        return TILE_SOLID;
    }
}

GameMap::TileType GameMap::getTileType(const glm::ivec2& pos) {
    return getTileType(pos.x, pos.y);
}


void GameMap::setTileType(int x, int y, TileType type) {
    if (onMap(x, y)){
        tile(x, y).type = type;
    }
}

GameMap::TileVisib GameMap::getTileVisib(int x, int y) {
    if (onMap(x, y)){
        return tile(x, y).visible;
    }else{
        return VISIB_WALL;
    }
}

GameMap::TileVisib GameMap::getTileVisib(const glm::ivec2& pos) {
    return getTileVisib(pos.x, pos.y);
}


void GameMap::setTileVisib(int x, int y, TileVisib visible) {
    if (onMap(x, y)){
        tile(x, y).visible = visible;
    }
}

GameMap::TileRess GameMap::getTileRess(int x, int y) {
    if (onMap(x, y)){
        return tile(x, y).ress;
    }else{
        return RESS_NONE;
    }
}

GameMap::TileRess GameMap::getTileRess(const glm::ivec2& pos) {
    return getTileRess(pos.x, pos.y);
}


void GameMap::setTileRess(int x, int y, TileRess ress) {
    if (onMap(x,y)){
        tile(x, y).ress = ress;
    }
}

bool GameMap::getTileMark(const glm::ivec2& pos, TileMark mark) {
    return getTileMark(pos.x, pos.y, mark);
}

bool GameMap::getTileMark(int x, int y, TileMark mark) {
    if (onMap(x, y)){  
        return tile(x, y).marks[mark];
    }
    
    return false;
}

void GameMap::setTileMark(int x, int y, TileMark mark, bool state) {
    if (onMap(x, y)){
        if (tile(x,y).marks[mark] != state){
            tile(x, y).marks[mark] = state;
            onTileMarkChange(x, y);
        }
    }
}

void GameMap::setTileColor(int x, int y, const glm::vec4& color) {
    tile(x, y).obj.setColor(color);
}




bool GameMap::isWall(int x, int y) {
    if (isTrueWall(x, y) || getTileVisib(x, y) == VISIB_FALSE){
        return true;
    }else{
        return false;
    }
}

bool GameMap::isTrueWall(TileType type) {
    switch(type){
        case TILE_DIRT:
        case TILE_HARD:
        case TILE_LOOSE:
        case TILE_ORESEAM:
        case TILE_RECHARGE:
        case TILE_CRYSTALSEAM:
        case TILE_SOLID:
            return true;
        default:
            return false;
    }
}


bool GameMap::isTrueWall(int x, int y) {
    return isTrueWall(getTileType(x, y));
}


int GameMap::getHeightmap(int x, int y) {
    int heightmap = 0;
    
    if (!isWall(x,y)){
        return 0;
    }
    
    if (isWall(x - 1, y + 0) && isWall(x - 1, y - 1) && isWall(x + 0, y - 1)){
        heightmap |= 0b0001;
    }
    
    if (isWall(x + 1, y + 0) && isWall(x + 1, y - 1) && isWall(x + 0, y - 1)){
        heightmap |= 0b0010;
    }
    
    if (isWall(x + 1, y + 0) && isWall(x + 1, y + 1) && isWall(x + 0, y + 1)){
        heightmap |= 0b0100;
    }
    
    if (isWall(x - 1, y + 0) && isWall(x - 1, y + 1) && isWall(x + 0, y + 1)){
        heightmap |= 0b1000;
    }
    
    return heightmap;
}

void GameMap::updateEngineObject(int x, int y) {
    int heightmap = getHeightmap(x, y);
    TileType type = getTileType(x, y);
    int shapeId = 0;
    float m_rot = 0;
    
    switch (heightmap){
        case 0b0000:
            shapeId = 0;
            break;
        
        case 0b1001:
            m_rot += M_PI_2;
        case 0b0011:
            m_rot += M_PI_2;
        case 0b0110:
            m_rot += M_PI_2;
        case 0b1100:
            shapeId = 1;
            break;
            
        case 0b0001:
            m_rot += M_PI_2;
        case 0b0010:
            m_rot += M_PI_2;
        case 0b0100:
            m_rot += M_PI_2;
        case 0b1000:
            shapeId = 51;
            break;
            
        case 0b1011:
            m_rot += M_PI_2;
        case 0b0111:
            m_rot += M_PI_2;
        case 0b1110:
            m_rot += M_PI_2;
        case 0b1101:
            shapeId = 31;
            break;
            
        case 0b1111:
            shapeId = 70;
            break;
            
        case 0b1010:
        case 0b0101:
            break;
    }
    
    switch (type){
        case TILE_DIRT:
            shapeId += 1;
            break;
        case TILE_LOOSE:
            shapeId += 2;
            break;
        case TILE_HARD:
            shapeId += 3;
            break;
        case TILE_SOLID:
            shapeId += 4;
            break;
        case TILE_GRAVEL:
            shapeId = 10;
            break;
        case TILE_LAVA:
            shapeId = 46;
            break;
        case TILE_WATER:
            shapeId = 45;
            break;
        case TILE_ORESEAM:
            shapeId = 40;
            break;
        case TILE_CRYSTALSEAM:
            shapeId = 20;
            break;
        case TILE_RECHARGE:
            shapeId = 67;
            break;
        case TILE_SLUGHOLE:
            shapeId = 30;
            break;
        case TILE_BUILD:
        case TILE_BUILDWALK:
            shapeId = 66;
            break;
        case TILE_PATH:
            shapeId = 71;
            break;
    }
    
    if (getHeightmap(x, y) == 0b1111){
        shapeId = 70;
    }else if (getHeightmap(x, y) == 0b1010){
        shapeId = 77;
    }else if (getHeightmap(x, y) == 0b0101){
        m_rot = M_PI_2;
        shapeId = 77;
    }
    
    tile(x, y).obj.setPos(glm::vec3(x * getTileSize(), 0, y * getTileSize()));
    tile(x, y).obj.setRot(glm::vec3(m_rot, 0, 0));
    tile(x, y).obj.setMesh(m_ressource->getShape(shapeId));
    //Engine3D* engine = App::getApp()->getEngine3D();
    //engine->renderShape(m_ressource->getShape(shapeId), model);
}

void GameMap::update(int x, int y) {
    if (onMap(x,y)){
        onUpdate(x, y);
        //updateEngineObject(x, y);
    } 
}

void GameMap::updateNeighbours(int x, int y) {
    update(x + 1, y);
    update(x - 1, y);
    update(x, y + 1);
    update(x, y - 1);
    
    update(x + 1, y + 1);
    update(x + 1, y - 1);
    update(x - 1, y - 1);
    update(x - 1, y + 1);
}

void GameMap::updateAll() {
    for (int y = 0; y < m_height; y++){
        for(int x = 0; x < m_width; x++){
            update(x, y);
        }
    }
}


void GameMap::onUpdate(int x, int y) {
    if (isTrueWall(x, y) && getHeightmap(x, y) == 0){
        destroy(x, y);
    }else if (!isTrueWall(x, y) && getTileVisib(x,y) == 2){
        if (getTileVisib(x + 1, y) == VISIB_TRUE ||
            getTileVisib(x - 1, y) == VISIB_TRUE ||
            getTileVisib(x, y + 1) == VISIB_TRUE ||
            getTileVisib(x, y - 1) == VISIB_TRUE){
            setTileVisib(x, y, VISIB_TRUE);
            updateNeighbours(x, y);
        }
    }
}


void GameMap::destroy(int x, int y) {
    if (isTrueWall(x, y)){
        setTileType(x, y, TILE_GRAVEL);
        setTileVisib(x, y, VISIB_TRUE);
        spawnRessources(x, y);
        update(x, y);
        updateNeighbours(x, y);
    }
}



void GameMap::addObject(MapObject* object) {
    object->m_gameMap = this;
    
    m_objects.push_back(object);
}

void GameMap::removeObject(MapObject* object) {
    object->m_gameMap = nullptr;
    
    for (int a = 0; a < m_objects.size(); a++){
        if (m_objects[a] == object){
            m_objects.erase(m_objects.begin() + a);
            --a;
        }
    }
}

void GameMap::updateObjects() {
    for (int a = 0; a < m_objects.size(); a++){
        m_objects[a]->update();
    }
}

int GameMap::getNumObjects() {
    return m_objects.size();
}

MapObject* GameMap::getObject(int id) {
    return m_objects[id];
}





int GameMap::getTileSize() {
    return 40;
}

int GameMap::getTileHeight() {
    return 60;
}


void GameMap::spawnRessources(int x, int y) {
    if (onMap(x, y)){
        Ressource::Type type;
        int count = 0;

        switch (getTileRess(x, y)){
            case RESS_CRYSTAL_1:
                type = Ressource::TYPE_CRYSTAL;
                count = 1;
            break;
            case RESS_ORE_1:
                type = Ressource::TYPE_ORE;
                count = 1;
            break;    
            case RESS_CRYSTAL_3:
                type = Ressource::TYPE_CRYSTAL;
                count = 3;
            break;
            case RESS_ORE_3:
                type = Ressource::TYPE_ORE;
                count = 3;
            break; 
            case RESS_CRYSTAL_5:
                type = Ressource::TYPE_CRYSTAL;
                count = 5;
            break;
            case RESS_ORE_5:
                type = Ressource::TYPE_ORE;
                count = 5;
            break; 
            case RESS_CRYSTAL_11:
                type = Ressource::TYPE_CRYSTAL;
                count = 11;
            break;
            case RESS_ORE_11:
                type = Ressource::TYPE_ORE;
                count = 11;
            break; 
            case RESS_CRYSTAL_25:
                type = Ressource::TYPE_CRYSTAL;
                count = 25;
            break;
            case RESS_ORE_25:
                type = Ressource::TYPE_ORE;
                count = 25;
            break; 
        }
        
        for (int a = 0; a < count; a++){
            Ressource* res = new Ressource(type);
            
            int s = getTileSize();
            int px = x * s + rand() % s - s/2;
            int py = y * s + rand() % s - s/2;
            
            res->setPosMap(glm::vec2(px, py));
            addObject(res);
        }
    }
}

glm::ivec2 GameMap::posToTile(const glm::vec2& pos) {
    const glm::vec2 size = glm::vec2(getTileSize());
    
    return (pos + size * 0.5f) / size;
}

glm::vec2 GameMap::tileToPos(const glm::ivec2& pos) {
    const glm::vec2 size = glm::vec2(getTileSize());
    
    return glm::vec2(pos) * size;
}

void GameMap::onTileMarkChange(int x, int y) {
    auto& t = tile(x, y);
    
    if (t.marks[MARK_BUILD]){
        setTileColor(x, y, glm::vec4(0.0f, 1.0f, 0.0f, 1.0f));
    }else if (t.marks[MARK_BUILDPATH]){
        setTileColor(x, y, glm::vec4(1.0f, 1.0f, 0.0f, 1.0f));
    }else if (t.marks[MARK_SELECTED]){
        setTileColor(x, y, glm::vec4(0.6f, 0.6f, 1.0f, 1.0f));
    }else if (t.marks[MARK_DYNAMITE]){
        setTileColor(x, y, glm::vec4(1.0f, 0.8f, 0.8f, 1.0f));
    }else if (t.marks[MARK_DRILL]){
        setTileColor(x, y, glm::vec4(0.7f, 0.7f, 0.7f, 1.0f));
    }else if(t.marks[MARK_CLEANUP]){
        setTileColor(x, y, glm::vec4(0.7f, 0.7f, 0.7f, 1.0f));
    }else{
        setTileColor(x, y, EngineObject::DEFAULTCOLOR);
    }
}
