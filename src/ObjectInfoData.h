/* 
 * File:   ObjectInfoData.h
 * Author: Kevin
 *
 * Created on 8. September 2015, 12:15
 */

#ifndef OBJECTINFODATA_H
#define	OBJECTINFODATA_H

#include <vector>
#include <string>

class ObjectInfo;

template <class T>
class ObjectInfoData {
public:
    ObjectInfoData();
    ObjectInfoData(const ObjectInfoData& orig);
    virtual ~ObjectInfoData();
    
    T get(int level);
    void set(int level, T data);
    void readData(const std::string& data);
private:
    const ObjectInfo* m_parent;
    
    std::vector<T> m_data;
};

#endif	/* OBJECTINFODATA_H */

