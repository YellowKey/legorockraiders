/* 
 * File:   TaskClear.h
 * Author: Kevin
 *
 * Created on 19. August 2015, 14:35
 */

#ifndef TASKCLEAR_H
#define	TASKCLEAR_H

#include "Task.h"

#include <glm/vec2.hpp>

class TaskClear: public Task{
public:
    TaskClear(const glm::ivec2 &tile);
    TaskClear(const TaskClear& orig) = delete;
    virtual ~TaskClear();
    
    virtual glm::vec2 getPos() override;
    virtual Task::TaskType getTaskType() override;

    virtual bool assignUnit(Unit* unit) override;
    virtual bool isAssigned() override;

    virtual void unitCanceled() override;
    

    virtual bool operator==(const Task& task) override;

private:
    const glm::ivec2 m_tile;
    
    bool m_assigned = false;
};

#endif	/* TASKCLEAR_H */

