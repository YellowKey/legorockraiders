/* 
 * File:   Shape.cpp
 * Author: Kevin
 * 
 * Created on 13. Mai 2015, 18:18
 */

#include <string.h>

#include "Shape.h"

#include <iostream>
#include "util.h"

Shape::Shape() {
    for (int a = 0; a < NUM_TEXTURE_SLOTS; a++){
        m_surfaces[a].texture = nullptr;
    }
}

Shape::~Shape() {
}

void Shape::setColor(const glm::vec3& color) {
    m_color = color;
}

glm::vec3 Shape::getColor() {
    return m_color;
}

void Shape::setTexture(TextureSlot slot, Texture* texture) {
    if (slot < NUM_TEXTURE_SLOTS){
        m_surfaces[slot].texture = texture;
    }
}

Texture* Shape::getTexture(TextureSlot slot) {
    if (slot < NUM_TEXTURE_SLOTS){
        return m_surfaces[slot].texture;
    }
    
    return nullptr;
}




void Shape::addVertex(const glm::vec3& pos, const glm::vec2& uv, const glm::vec3& normal) {
    m_vertices.push_back({pos, uv, normal});
}

int Shape::getNumVertices() {
    return m_vertices.size();
}

void Shape::clearLocalData() {
    m_vertices.clear();
}

int Shape::getVertexSize() {
    return sizeof(Vertex);
}

int Shape::getDataSize() {
    return sizeof(Vertex[getNumVertices()]);
}



void Shape::writeDataTo(void* data) {
    memcpy(data, m_vertices.data(), sizeof(Vertex[m_vertices.size()]));
}

const void* Shape::getData() {
    return m_vertices.data();
}


void Shape::setBuffer(glBuffer* buffer, int offset) {
    m_buffer = buffer;
    m_bufferOffset = offset;
}

glBuffer* Shape::getBuffer() {
    return m_buffer;
}

int Shape::getBufferOffset() {
    return m_bufferOffset;
}

bool Shape::getAdditiveBlending() {
    return m_additiveBlending;
}

void Shape::setAdditiveBlending(bool state) {
    m_additiveBlending = state;
}

void Shape::setLuminance(float lumi) {
    m_luminance = lumi;
}

float Shape::getLuminance() {
    return m_luminance;
}

