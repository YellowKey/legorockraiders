/* 
 * File:   GameRessource.h
 * Author: kevin
 *
 * Created on June 3, 2015, 10:07 AM
 */

#ifndef GAMERESSOURCE_H
#define	GAMERESSOURCE_H

#include "Mesh.h"
#include "Activity.h"
#include "Action.h"
#include "RessourceContainer.h"
#include "LoaderCfg.h"

#include "ObjectInfo.h"
#include "Game.h"

#include "AnimInfo.h"

#include <map>

class GameRessource: public RessourceContainer{
public:
    GameRessource();
    GameRessource(const GameRessource& orig) = delete;
    virtual ~GameRessource();
    
    Mesh* manStand;
    Mesh* manWalk;
    Mesh* manRun;
    
    Mesh* crystal;
    Mesh* crystalEmpty;
    
    Mesh* ore;
    
    Activity* pilot;
    
    Activity* barracks;
    Activity* bigTeleport;
    Activity* smallTeleport;
    Activity* docks;
    Activity* fence;
    Activity* geodome;
    Activity* orerefinery;
    Activity* gunstation;
    Activity* powerstation;
    Activity* toolstation;
    Activity* upgrade;
    
    Activity* smallTruck;
    
    Mesh* smallWheel;
    Mesh* smallBucket;
    Mesh* smallDash;
    Mesh* standardRear;
    
    void load();
    void postLoad();
private:
    void loadUpgrades();
    void loadActivities(const std::string& groupName, ObjectInfo::ObjType type);
    void loadActivity(ObjectInfo* obj, const std::string& path);
    void loadStats();
    
    AnimInfo* loadAnimInfo(const std::string& file);
    
    void addInfos(Game* game);
    void addInfo(Game* game, const std::string& name, Game::ObjectType type);
    
    std::string rEM(const std::string& text); //removeExclamationMark
    
    ObjectInfo* getInfo(const std::string& internalName);
    
    std::map<std::string, std::string> m_upgrades;
    std::map<std::string, AnimInfo*> m_upgradesAI;
    std::vector<ObjectInfo*> m_objInfo;
    
    LoaderCfg m_cfgLoader;
};

#endif	/* GAMERESSOURCE_H */

