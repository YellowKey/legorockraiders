/* 
 * File:   WidgetImage.cpp
 * Author: Kevin
 * 
 * Created on 21. Juni 2015, 16:02
 */

#include "WidgetImage.h"

WidgetImage::WidgetImage(const Texture* texture):m_texture(texture){
}

WidgetImage::~WidgetImage() {
}

void WidgetImage::onPaint(Renderer2D* renderer) {
    renderer->renderTexture(m_texture, getAbsPos(), getSize());
}

void WidgetImage::setTexture(const Texture* texture) {
    m_texture = texture;
    
    setBaseSize(texture->getSize());
}

