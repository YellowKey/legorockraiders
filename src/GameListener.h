/* 
 * File:   GameListener.h
 * Author: Kevin
 *
 * Created on 13. August 2015, 15:56
 */

#ifndef GAMELISTENER_H
#define	GAMELISTENER_H

#include <glm/vec2.hpp>

#include "Game.h"

class GameListener {
public:
    GameListener() = default;
    GameListener(const GameListener& orig) = delete;
    virtual ~GameListener();
    
    virtual void onNewObject(uint32_t objId, Game::ObjectType type);
    virtual void onObjectSelect(uint32_t objId, bool state);
    virtual void onObjectRotate(uint32_t objId, glm::vec3 rotation);
    virtual void onObjectMove(uint32_t objId, glm::vec3 pos);
    virtual void onObjectAttach(uint32_t objId, uint32_t targetId, uint32_t slotId);
    virtual void onObjectRemove(uint32_t objId, uint32_t targetId, uint32_t slotId);
    virtual void onObjectAnim(uint32_t objId, uint32_t animId, float time);
    
    virtual void onOrderCancel(uint32_t objId);
    virtual void onOrderSend(uint32_t objId, glm::vec2 pos);
    virtual void onOrderPickup(uint32_t objId, uint32_t targetId);
    virtual void onOrderEnter(uint32_t objId, uint32_t targetId);
    virtual void onOrderLeave(uint32_t objId);
    virtual void onOrderDrop(uint32_t objId, uint32_t slotId);
    virtual void onOrderDrill(uint32_t objId, glm::ivec2 pos);
    virtual void onOrderCleanup(uint32_t objId, glm::ivec2 pos);
    virtual void onOrderDestroy(uint32_t objId);
    
    virtual void onAddBuildingBuild(uint32_t objId, Game::ObjectType type);
    
    virtual void onTileSelect(glm::ivec2 pos);
    virtual void onTileDeselect();
    virtual void onTileMarkDrill(glm::ivec2 pos, bool state);
    virtual void onTileMarkCleanup(glm::ivec2 pos, bool state);
    virtual void onTileChange(glm::uint32_t tileId);
    
    virtual void onPrioChange(Game::PriotyType, uint32_t priority);
    
    virtual void onGamePause(bool state);
    virtual void onGameSpeedChange(float speed);
    virtual void onGameRessChange(int crystal, int ore, int bricks);
    Game* getGame();
private:
    friend class Game;
    
    Game* m_game = nullptr;
    void setGame(Game* game);
};

#endif	/* GAMELISTENER_H */

