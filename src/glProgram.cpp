/* 
 * File:   glProgram.cpp
 * Author: Kevin
 * 
 * Created on 22. November 2014, 14:46
 */

#include "glProgram.h"

#include <GL/glew.h>
#include <iostream>

glProgram::glProgram() {
    m_handle = glCreateProgram();
}

glProgram::glProgram(string name, bool tesselation, bool geometry)
    :glProgram() {
    load(name, tesselation, geometry);
}

glProgram::~glProgram() {
    glDeleteProgram(m_handle);
}

void glProgram::attachShader(const glShader& shader) {
    glAttachShader(m_handle, shader.getHandle());
}

void glProgram::link() {
    GLint result = GL_FALSE;
    int   length = 0;
    
    glLinkProgram(m_handle);

    glGetProgramiv(m_handle, GL_LINK_STATUS,     &result);
    glGetProgramiv(m_handle, GL_INFO_LOG_LENGTH, &length);

    char message[length];
    glGetProgramInfoLog(m_handle, length, NULL, message);

    if (result == 0){
        std::cerr << "Error while linking: " + string(message) << std::endl;
    }
}

void glProgram::load(string name, bool tesselation, bool geometry) {
    attachShader(glShader(glShader::Type::VERTEX, name + ".vert"));
    attachShader(glShader(glShader::Type::FRAGMENT, name + ".frag"));
    
    if (tesselation){
        attachShader(glShader(glShader::Type::TESS_CONTROL, name + ".tesc"));
        attachShader(glShader(glShader::Type::TESS_EVALUATION, name + ".tese"));
    }
    
    if (geometry){
        attachShader(glShader(glShader::Type::GEOMETRY, name + "geom"));
    }
    
    link();
    
}

void glProgram::use() {
    glUseProgram(m_handle);
}

glUniform glProgram::getUniform(const string& name) {
    GLint id = glGetUniformLocation(m_handle, name.c_str());
    
    if (id == -1){
        std::cerr <<  "couldn't get " + name << std::endl;
        fflush(stderr);
    }
    
    return glUniform(this, id);
}

GLuint glProgram::getHandle() const {
    return m_handle;
}

GLint glProgram::getAttribLocation(const string& name) {
    return glGetAttribLocation(m_handle, name.c_str());
}
