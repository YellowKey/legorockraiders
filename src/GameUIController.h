/* 
 * File:   UIController.h
 * Author: Kevin
 *
 * Created on 14. August 2015, 11:11
 */

#ifndef UICONTROLLER_H
#define	UICONTROLLER_H

#include "Engine3D.h"
#include "Renderer2D.h"
#include "GameMap.h"
//#include "Game.h"

#include "Widget.h"
#include "WidgetContainer.h"
#include "WidgetGameView.h"

#include "ButtonListener.h"
#include "WidgetIconPanel.h"

#include "GameListener.h"

class App;

class WidgetRightPanel;

class GameUIController: private ButtonListener, GameListener{
public:
    enum SideMenu{
        SIDE_MAIN,
        SIDE_TRAIN,
        SIDE_BUILDINGS,
        SIDE_SMALLVEHICLES,
        SIDE_LARGEVEHICLES,
        SIDE_UNIT,
        SIDE_VEHICLE,
        SIDE_UPGRADE,
        SIDE_TOOLS,
        SIDE_WALL,
        SIDE_GROUND,
    };
    enum ButtonId{
        BUT_NONE,
        
        //main side menu
        BUT_TELEPORT,
        BUT_BUILDINGS,
        BUT_SMALLVEHICLES,
        BUT_LARGEVEHICLES,
        
        //wall side menu
        BUT_DRILL,
        BUT_REINFORCE,
        BUT_DYNAMITE,
        BUT_CANCELDRILL,
        
        //ground size menu
        BUT_PATH,
        BUT_CLEARRUBBLE,
        BUT_DESTROYPATH,
        BUT_REMOVELAVA,
        BUT_FENCE,
        
        //unit side menu
        BUT_EAT,
        BUT_DROP,
        BUT_PICKUP,
        BUT_TOOLS,
        BUT_SONICPLASTER, 
        BUT_UPGRADE,
        BUT_TRAIN,
        BUT_FIRSTPERSON,
        BUT_SHOULDER,
        BUT_TELEPORTBACK,
        
        //additional for vehicle
        BUT_UNLOADVEHICLE,
        BUT_LOADVEHICLE,
        BUT_UPGRADEVEHICLE,
        BUT_GETDRIVER,
        //first Person
        //2nd Person
        BUT_TELEPORTVEHICLE,
        
        //training side menu
        BUT_TRAIN_DRIVER,
        BUT_TRAIN_REPAIR,
        BUT_TRAIN_GEOLOGIST,
        BUT_TRAIN_PILOT,
        BUT_TRAIN_SAILOR,
        BUT_TRAIN_DYNAMITE,
        
        //building side menu
        BUT_BUILD_TOOLSTORE,
        BUT_BUILD_SMALLTELEPORT,
        BUT_BUILD_DOCKS,
        BUT_BUILD_POWERPLANT,
        BUT_BUILD_SUPPORTSTATION,
        BUT_BUILD_UPGRADESTATION,
        BUT_BUILD_GEOLOGICCENTER,
        BUT_BUILD_OREREFINERY,
        BUT_BUILD_LASER,
        BUT_BUILD_LARGETELEPORT,
        
        //small vehiles side menu
        BUT_BUILD_HOVERSCOUT,
        BUT_BUILD_SMALLDIGGER,
        BUT_BUILD_TRANSPORTVEHICLE,
        BUT_BUILD_RAPIDRIDER,
        BUT_BUILD_SMALLLASER,
        BUT_BUILD_TUNNELSCOUT,
        
        //large vehicles side menu
        BUT_BUILD_LOADERDOZER,
        BUT_BUILD_GRANITEGRINDER,
        BUT_BUILD_LARGEMOBILELASER,
        BUT_BUILD_CARGOCARRIER,
        BUT_BUILD_CHROMECRUSHER,
        BUT_BUILD_TUNNELTRANSPORT,
        
        //equipment side menu
        BUT_TAKE_DRILL,
        BUT_TAKE_SPADE,
        BUT_TAKE_HAMMER,
        BUT_TAKE_SPANNER,
        BUT_TAKE_LASER,
        BUT_TAKE_FREEZE,
        BUT_TAKE_PUSH,
        BUT_TAKE_SONIC,
        
        //back buttons
        BUT_BACK_MAIN,
        BUT_BACK_UNIT,
        BUT_BACK_DESELTILE,
        
        //Priority Panel
        BUT_PRIO_0,
        BUT_PRIO_1,
        BUT_PRIO_2,
        BUT_PRIO_3,
        BUT_PRIO_4,
        BUT_PRIO_5,
        BUT_PRIO_6,
        BUT_PRIO_7,
        BUT_PRIO_8,
    };
    GameUIController();
    GameUIController(const GameUIController& orig) = delete;
    virtual ~GameUIController();
    
    //void attachToGame(Game* game);
    //void attachToWindow() //TODO: Add WindowListener
    
    void scale(glm::vec2 size); //TODO: Make this private
    void update();
    void render(Renderer2D* renderer);
    
    Widget* GetScreenWidget();
    WidgetGameView* getGameWidget();
    
    GameMap* getGameMap() const;
private:
    virtual void onTileSelect(glm::ivec2 pos) override;
    virtual void onTileDeselect() override;
    virtual void onGameRessChange(int crystal, int ore, int bricks) override;
    
    virtual void onClick(int buttonId, int mouseButton) override;
    
    WidgetIconPanel* createPanel(int count, ButtonId backButton);
    
    void openMainSideMenu();
    void openBuildingsSideMenu();
    void openSmallVehiclesSideMenu();
    void openLargeVehiclesSideMenu();
    void openTrainMenu();
    void openUnitMenu();
    void openToolMenu();
    void openWallMenu();
    void openGroundMenu();
    
    Widget m_screenWidget;
    WidgetContainer m_widgetRight;
    WidgetContainer m_widgetLeft;
    WidgetContainer m_sideMenuContainer;
    WidgetGameView m_widgetGame;
    
    Widget* m_sideMenu = nullptr;
    
    App*     m_app     = nullptr;
    Game*    m_game    = nullptr;
    GameMap* m_gameMap = nullptr;
    
    SideMenu m_currentSideMenu;
    SideMenu m_nextSideMenu;
    
    WidgetRightPanel* m_rightPanel;
};

#endif	/* UICONTROLLER_H */

