/* 
 * File:   WidgetImage.h
 * Author: Kevin
 *
 * Created on 21. Juni 2015, 16:02
 */

#ifndef WIDGETIMAGE_H
#define	WIDGETIMAGE_H

#include "Texture.h"
#include "Widget.h"

class WidgetImage: public Widget{
public:
    WidgetImage(const Texture* texture = nullptr);
    virtual ~WidgetImage();
    
    void setTexture(const Texture* texture);
    
protected:
    virtual void onPaint(Renderer2D* renderer) override;
    
private:
    const Texture* m_texture = nullptr;
};

#endif	/* WIDGETIMAGE_H */

