/* 
 * File:   Action.h
 * Author: kevin
 *
 * Created on July 9, 2015, 1:29 PM
 */

#ifndef ACTION_H
#define	ACTION_H

#include "Unit.h"
#include "Task.h"

class Action {
public:
    Action();
    Action(const Action& orig) = delete;
    virtual ~Action();
    
    virtual void start();
    virtual void updateUnit();
    virtual void cancel();
    
    void markLocked(); //Action cannot be canceled
    bool isLocked();
    
    void setUnit(Unit* unit);
    
    Unit* getUnit();
    
    bool isFinished();
    
    void setTask(Task* task);
protected:
    Task* getTask();
    
    void finish();
    
    Unit* m_unit;
private:
    bool m_finished = false;
    bool m_locked   = false;
    
    Task* m_task = nullptr;
};

#endif	/* ACTION_H */

