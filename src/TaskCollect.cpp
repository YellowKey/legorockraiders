/* 
 * File:   TaskCollect.cpp
 * Author: Kevin
 * 
 * Created on 29. August 2015, 15:16
 */

#include "TaskCollect.h"
#include "Ressource.h"
#include "Task.h"
#include "Unit.h"
#include "ActionWalk.h"
#include "ActionCollect.h"
#include "App.h"
#include "ActionPlace.h"

TaskCollect::TaskCollect(Ressource* ressoure):m_ressource(ressoure){
    
}

TaskCollect::~TaskCollect() {
    
}

glm::vec2 TaskCollect::getPos() {
    glm::vec3 pos = m_ressource->getPos();
            
    return glm::vec2(pos.x, pos.z);
}

Task::TaskType TaskCollect::getTaskType() {
    if (m_ressource->GetType() == Ressource::TYPE_CRYSTAL){
        return TASK_CRYSTAL;
    }else{
        return TASK_ORE;
    }
}

bool TaskCollect::assignUnit(Unit* unit) {
    if (!(unit->isCarring()) && unit->canCarry()){
        unit->addAction(new ActionWalk(getPos()));
        unit->addAction(new ActionCollect(m_ressource));
        unit->addAction(new ActionWalk(App::getApp()->getGame()->getRessDest()));
        unit->addAction(addAction(new ActionPlace(ActionPlace::PLACE_STORE)));

        m_assigned = true;
        
        return true;
    }
    
    return false;
}

bool TaskCollect::isAssigned() {
    return m_assigned;
}

void TaskCollect::unitCanceled() {
    m_assigned = false;
}

bool TaskCollect::operator==(const Task& task) {
    const TaskCollect* taskPickup = dynamic_cast<const TaskCollect*>(&task);
    
    if (taskPickup && m_ressource == taskPickup->m_ressource){
        return true;
    }
    
    return false;
}





