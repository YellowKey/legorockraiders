/* 
 * File:   LoaderBmp.cpp
 * Author: Kevin
 * 
 * Created on 28. April 2015, 15:54
 */

#include "LoaderBmp.h"

#include <cstring>

#include <iostream>
#include <iomanip>

//#include <IL/il.h>
//#include <IL/ilu.h>

constexpr int LoaderBmp::MAXCOLORS;

LoaderBmp::LoaderBmp() {
    
}

LoaderBmp::~LoaderBmp() {
    freeData();
}

void LoaderBmp::load(const void* mem, int size) {
    char* buffer = createBuffer(size);
    
    memcpy(buffer, mem, size);
    
    setData(buffer, size);
    readBitmap();
}

void LoaderBmp::load(const std::string& filename){
    FILE* file = fopen(filename.c_str(), "rb");
    
    load(file);
    
    fclose(file);
}

void LoaderBmp::load(FILE* file){
    fseek(file, 0L, SEEK_END);
    int size = ftell(file);
    fseek(file, 0L, SEEK_SET);
    
    char* buffer = createBuffer(size);
    
    fread(buffer, 1, size, file);
    
    setData(buffer, size);
    readBitmap();
}

void LoaderBmp::readBitmap() {
    /*static bool ilReady = false;
    
    if (!ilReady){
        ilInit();
        iluInit();
    }
    
    ILuint image = ilGenImage();
    
    ilLoadL(IL_BMP, m_fileData, m_fileSize);
    
    m_imageWidth = ilGetInteger(IL_IMAGE_WIDTH);
    m_imageHeight = ilGetInteger(IL_IMAGE_HEIGHT);
    
    m_pixelBits = 24;
    m_numColors = 0;
    
    m_imageData = new char[m_imageWidth * m_imageHeight * getPixelBytes()];
    
    //iluFlipImage();
    ilCopyPixels(0, 0, 0, m_imageWidth, m_imageHeight, 1, IL_BGR, IL_UNSIGNED_BYTE, m_imageData);
    
    ilDeleteImage(image);
    
    std::cout << "devil Load" << std::endl;*/
    
    if (isDebugging()){
        std::cout << "-------------------------" << std::endl;
        std::cout << "        Load Bitmap      " << std::endl;
        std::cout << "-------------------------" << std::endl;
    }
    
    readBmpHeader();
    readDibHeader();
    
    if (m_pixelBits <= 8){
        readColorTable();
    }
    
    readPixelArray();
    
    if (isDebugging()){
        std::cout << std::endl;
        std::cout.flush();
    }
}

void LoaderBmp::readBmpHeader() {
    std::string header = readString(2);
    
    int size = read32i();
    int reserved1 = read16i();
    int reserved2 = read16i();
    
    m_pixelDataOffset = read32i();
    
    if (isDebugging()){
        std::cout << "[BMP HEADER]" << std::endl;
        std::cout << "Header:    " << header << std::endl;
        std::cout << "Size:      " << size << std::endl;
        std::cout << "Reserved2: " << reserved1 << std::endl;
        std::cout << "Reserved1: " << reserved2 << std::endl;
        std::cout << "PixelOff:  " << m_pixelDataOffset << std::endl;
    }
}

void LoaderBmp::readDibHeader() {
    int headerStart = getFilePos();
    
    int headerSize = read32i();
    
    if (headerSize < 40){
        //TODO Report Error
    }
    
    m_imageWidth = read32i();
    m_imageHeight = read32i();
    int numColorPanes = read16i();
    
    if (numColorPanes != 1){
        //TODO Report Error
    }
    
    m_pixelBits = read16i();
    int compression    = read32i();
    int rawImageSize   = read32i();
    int resolutionHori = read32i();
    int resolutionVert = read32i();
    m_numColors        = read32i();
    int numImportColor = read32i();
    
    if (m_numColors == 0){
        m_numColors = 256;
    }
    if (m_numColors > MAXCOLORS){
        //TODO: Report Error
    }
    
    setFilePos(headerStart + headerSize);
    
    if (isDebugging()){
        std::cout << "[DIP HEADER]   " << std::endl;
        std::cout << "HeaderSize:   " << headerSize << std::endl;
        std::cout << "ImageWidth:   " << m_imageWidth << std::endl;
        std::cout << "ImageHeight:  " << m_imageHeight << std::endl;
        std::cout << "ColorPanes:   " << numColorPanes << std::endl;
        std::cout << "PixelBits:    " << m_pixelBits << std::endl;
        std::cout << "Compression:  " << compression << std::endl;
        std::cout << "RawImageSize: " << rawImageSize << std::endl;
        std::cout << "ResoHori:     " << resolutionHori << std::endl;
        std::cout << "ResoVert:     " << resolutionVert << std::endl;
        std::cout << "NumColors:    " << m_numColors << std::endl;
        std::cout << "ImportantCol: " << numImportColor << std::endl;
    }
}

void LoaderBmp::readColorTable() {
    for (int a = 0; a < m_numColors && a < MAXCOLORS; a++){
        m_colorPalet[a] = read32i();
    }
    
    if (isDebugging() && false){
        std::cout << "[COLOR TABLE]" << std::endl;
        
        for (int a = 0; a < m_numColors && a < MAXCOLORS; a++){
            const uint8_t* byte = (uint8_t*)&m_colorPalet[a];
            
            std::cout << std::right << std::setw(3) << a << "(" << std::setw(3) << (int)byte[0] << ", " << std::setw(3) << (int)byte[1] << ", " << std::setw(3) << (int)byte[2] << ", " << std::setw(3) << (int)byte[3] << ")" << std::endl;
        }
        
        std::cout << std::endl;
    }
        
}

void LoaderBmp::readPixelArray() {
    setFilePos(m_pixelDataOffset);
    
    delete[] m_imageData;
    
    if (getPixelBits() % 8){
        //TODO report Error
    }
    
    int lineSize = getWidth() * getPixelBytes();
    
    if (lineSize % 4){
        lineSize += 4 - lineSize % 4;
    }
    
    
    m_imageData = new char[lineSize * m_imageHeight];
    
    
    char* line = m_imageData;
    
    for (int a = 0; a < m_imageHeight; a++){
        if (!readMem(line, lineSize)){
            return;
        }
        
        //setFilePos(getFilePos() + offset);
        
        line+= lineSize;
        
        //pad(4);
    }
}

char* LoaderBmp::createBuffer(int size) {
    return new char[size];
}

void LoaderBmp::setData(void* data, int size) {
    freeData();
    
    m_fileData = (char*)data;
    m_filePos  = m_fileData;
    m_fileSize = size;
}



void LoaderBmp::freeData() {
    if (m_fileData){
        delete[] m_fileData;
        delete[] m_imageData;
        
        m_fileData  = nullptr;
        m_filePos   = nullptr;
        m_imageData = nullptr;
    }
}

bool LoaderBmp::readMem(void* data, int size) {
    if (readTest(size)){
        memcpy(data, m_filePos, size);
        
        m_filePos += size;
        
        return true;
    }else{
        markEof();
        
        return false;
    }
}


template<class T>
T LoaderBmp::read() {
    T value;
    
    readMem(&value, sizeof(T));
    
    return value;
}


int8_t LoaderBmp::read8i() {
    return read<int8_t>();
}

int16_t LoaderBmp::read16i() {
    return read<int16_t>();
}

int32_t LoaderBmp::read32i() {
    return read<int32_t>();
}


std::string LoaderBmp::readString(int length) {
    std::string text;
    
    for (int a = 0; a < length; a++){
        text += read<char>();
    }
    
    return text;
}

void LoaderBmp::pad(int numBytes) {
    int offset = getFilePos() % numBytes;
    
    if (readTest(offset)){
        m_filePos += offset;
    }else{
        markEof();
    }
}

void LoaderBmp::setFilePos(int offset) {
    m_filePos = m_fileData + offset;
}


bool LoaderBmp::readTest(int numBytes) {
    if (getFilePos() + numBytes <= getFileSize()){
        return true;
    }else{
        return false;
    }
}

bool LoaderBmp::eof() {
    if (m_eof || getFilePos() >= getFileSize()){
        return true;
    }else{
        return false;
    }
}

void LoaderBmp::markEof() {
    m_eof = true;
}

intptr_t LoaderBmp::getFilePos() {
    return (intptr_t)(m_filePos - m_fileData);
}

int LoaderBmp::getFileSize() {
    return m_fileSize;
}

const void* LoaderBmp::getData() {
    return m_imageData;
}

int LoaderBmp::getHeight() {
    return m_imageHeight;
}

int LoaderBmp::getWidth() {
    return m_imageWidth;
}

const int32_t* LoaderBmp::getPalet() {
    return m_colorPalet;
}

int LoaderBmp::getPixelBits() {
    return m_pixelBits;
}

int LoaderBmp::getPixelBytes() {
    int bytes = m_pixelBits / 8;
    
    if (m_pixelBits % 8){
        bytes += 1;
    }
    
    return bytes;
}

int LoaderBmp::getNumChannels() {
    if (hasPalett()){
        return 4;
    }else if (getPixelBits() == 24){
        return 3;
    }else if (getPixelBits() == 32){
        return 4;
    }
    
    return 0;
}

bool LoaderBmp::hasPalett() {
    if (getPixelBits() <= 8){
        return true;
    }else{
        return false;
    }
}

void LoaderBmp::convertToRGB(){
    if (getPixelBits() == 24){
        return;
    }
    
    int pixelSize = 3;
    int lineSize = pixelSize * getWidth();
    
    if (lineSize % 4){
        lineSize += 4 - lineSize % 4;
    }
    
    char* newImage = new char[getHeight() * lineSize];
    
    for (int y = 0; y < getHeight(); y++){
        for (int x = 0; x < getWidth(); x++){
            char* pixelPointer = newImage + x * pixelSize + y * lineSize;
            
            int32_t color = getColor(x, y);
            
            memcpy(pixelPointer, &color, 3);
        }
    }
    
    delete[] m_imageData;
    m_imageData = newImage;
    m_pixelBits = 24;
}

void LoaderBmp::convertToRGBA() {
    if (getPixelBits() == 32){
        return;
    }
    
    char* newImage = new char[getWidth() * getHeight() * 4];
    
    for (int y = 0; y < getHeight(); y++){
        for (int x = 0; x < getWidth(); x++){
            int32_t* pixelPointer = ((int32_t*)newImage) + x + y * getWidth();
            
            *pixelPointer = getColor(x, y);
        }
    }
    
    delete[] m_imageData;
    m_imageData = newImage;
    m_pixelBits = 32;
}

int32_t LoaderBmp::getColor(int x, int y) {
    int pixelSize = getPixelBytes();
    int lineSize  = pixelSize * getWidth();
    
    if (lineSize % 4){
        lineSize += 4 - lineSize % 4;
    }
    
    char* pixelPointer = (m_imageData + x * pixelSize + y * lineSize);
    
    if (hasPalett()){
        uint8_t colorIndex = *((uint8_t*)pixelPointer);
        return m_colorPalet[colorIndex] | 0xFF000000; // with Alpha == 255
    }
    
    int32_t color = 0xFF000000;

    memcpy(pixelPointer, &color, getPixelBytes());
    
    return color;
}

bool LoaderBmp::isDebugging() {
    return false;
}
