/* 
 * File:   Container.cpp
 * Author: Kevin
 * 
 * Created on 26. September 2015, 12:23
 */

#include "Container.h"
#include <algorithm>

ContainerBase::ContainerBase() {

}

ContainerBase::~ContainerBase() {

}


int ContainerBase::size() {
    return m_list.size();
}

ContainerItem*& ContainerBase::get(int index) {
    m_list[index];
}

void ContainerBase::addContainerItem(ContainerItem* item) {
    bool found = false;
    
    for (ContainerItem* currentItem: m_list){
        if (currentItem == item){
            found = true;
        }
    }
    
    if (!found){
        item->addToContainer(this);
        
        m_list.push_back(item);
    }
}

void ContainerBase::removeContainerItem(ContainerItem* item) {
    item->removeFromContainer(this);
    
    m_list.erase(std::remove(m_list.begin(), m_list.end(), item), m_list.end());
}

ContainerBase::baseIterator ContainerBase::ContainerBaseBegin() {
    m_list.begin();
}

ContainerBase::baseIterator ContainerBase::ContainerBaseEnd() {
    m_list.end();
}


