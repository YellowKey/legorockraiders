/* 
 * File:   LoaderBmp.h
 * Author: Kevin
 *
 * Created on 28. April 2015, 15:54
 */

#ifndef BMPLOADER_H
#define	BMPLOADER_H

#include <string>
#include <stdio.h>

class LoaderBmp {
public:
    //TODO: Add support for 4 and 1 Bit
    LoaderBmp();
    LoaderBmp(const LoaderBmp& orig) = delete;
    virtual ~LoaderBmp();
    
    void load(const void* mem, int size);
    void load(const std::string& filename);
    void load(FILE* file);
    
    int getWidth();
    int getHeight();
    int getNumChannels();
    int getPixelBits();
    int getPixelBytes();
    
    bool hasPalett();
    
    void convertToRGB();
    void convertToRGBA();
    
    const int32_t* getPalet();
    const void*    getData();
    
    int32_t getColor(int x, int y);
    
    void freeData();
private:
    static constexpr int MAXCOLORS = 256;
    
    char*    m_fileData = nullptr;
    char*    m_filePos  = nullptr;
    char*    m_imageData = nullptr;
    int32_t m_colorPalet[MAXCOLORS];
    
    int m_imageWidth;
    int m_imageHeight;
    int m_numChannels;
    int m_pixelBits;
    int m_numColors;
    int m_fileSize = 0;
    int m_pixelDataOffset;
    bool m_eof = false;
    
    void readBitmap();
    void readBmpHeader();
    void readDibHeader();
    void readColorTable();
    void readPixelArray();
    
    int32_t read32i();
    int16_t read16i();
    int8_t  read8i();
    bool    readMem(void* data, int size);
    template <class T>
    T read();
    std::string readString(int length);
    void pad(int numBytes);
    void setFilePos(int offset);
    
    bool      eof();
    intptr_t  getFilePos();
    int       getFileSize();
    
    void markEof();
    bool readTest(int numBytes);
    
    char* createBuffer(int size);
    void setData(void* data, int size);
    
    bool isDebugging();
};

#endif	/* BMPLOADER_H */

