/* 
 * File:   glTexture.h
 * Author: Kevin
 *
 * Created on 17. November 2014, 21:02
 */

#ifndef GLTEXTURE_H
#define	GLTEXTURE_H

#include "SlotManager.h"
#include "glObject.h"

#include <GL/glew.h>

#include <map>

using std::map;

class glTexture:public glObject{
public:
    glTexture(GLenum type);
    glTexture(const glTexture& orig);
    ~glTexture();
    
    GLint bindTexture(GLint id = 0);
    //void  unbindTexture();
    //bool  isTextureBound();
    
    bool  isImmutable();
    
    GLenum getGlTextureType();
    GLuint getTextureHandle();
    //GLint  getTextureSlot();
    
    void textureParameters(GLenum target, GLfloat param);
    void textureParameters(GLenum target, GLint param);
    void genMipmaps(int levels);
    
    void texStorage1D(GLint levels, GLint internalformat, GLsizei width);
    void texStorage2D(GLint levels, GLint internalformat, GLsizei width, GLsizei height);
    void texStorage3D(GLint levels, GLint internalformat, GLsizei width, GLsizei height, GLsizei depth);
    
    void texImage1D(GLint level, GLint internalformat, GLsizei width, GLenum format, GLenum type, const void* data);
    void texImage2D(GLint level, GLint internalformat, GLsizei width, GLsizei height, GLenum format, GLenum type, const void* data);
    void texImage3D(GLint level, GLint internalformat, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLenum type, const void* data);
    
    void texSubImage1D(GLint level, GLint xOffset, GLsizei width, GLenum format, GLenum type, const GLvoid* data);
    void texSubImage2D(GLint level, GLint xOffset, GLint yOffset, GLsizei width, GLsizei height, GLenum format, GLenum type, const GLvoid* data);
    void texSubImage3D(GLint level, GLint xOffset, GLint yOffset, GLint zOffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLenum type, const GLvoid* data);
    
    /*class TempBinder{
    public:
        TempBinder(glTexture*, bool active = false);
        TempBinder(const TempBinder&) = delete;
        ~TempBinder();
    private:
        bool m_wasBound;
        glTexture* const m_texture;
    };*/
    
    virtual glObject::Type getObjectType() override;
protected:
    void markImmutable();
    void bindCurrent();
    
    static void activeTexture(int id);
private:
    GLuint m_handle;
    GLenum m_type;
    GLint  m_boundSlot = -1;
    bool   m_bound     = false;
    bool   m_immutable = false;
    
    static GLint s_activeSlot;
    static int   s_maxTextureSlots; //GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS
    
    //static map <GLenum, SlotManager> s_slotManager;
    
    static int getFormatSize(GLint format);
};

#endif	/* GLTEXTURE_H */

