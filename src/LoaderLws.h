/* 
 * File:   LoaderLws.h
 * Author: Kevin
 *
 * Created on 16. Mai 2015, 17:51
 */

#ifndef LWSLOADER_H
#define	LWSLOADER_H

#include <string>
#include <vector>

#include <glm/vec3.hpp>

#include "Mesh.h"

class LoaderLws {
public:
    LoaderLws();
    LoaderLws(const LoaderLws& orig);
    virtual ~LoaderLws();
    
    void load(const std::string& name);
    
    Mesh* createMesh();
private:
    struct KeyFrame{
        glm::vec3 pos;
        glm::vec3 rot;
        glm::vec3 scale;
        
        int frame;
    };
    struct Object{
        std::string name;
        
        bool nullObject;
        
        std::vector<KeyFrame> motion;
        
        bool motionLoop;
        int endBehavior;
        
        glm::vec3 pivotPoint;
        
        int parent;
    };
    
    void parseData(const void* data, int size);
    
    std::string readString();
    int         readInt();
    float       readFloat();
    char        readChar();
    
    void nextLine();
    void ignoreNewLines(bool status);
    void markEof();
    
    bool isWhitespace();
    bool isNewLine();
    bool eof();
    
    void parseCommands();
    void parseMotion();
    
    void addObject(const std::string& name, bool nullObject = false);
    void addNullObject(const std::string& name);
    void addKeyFrame(const KeyFrame& frame);
    void setParent(int parentId);
    void setEndBehavior(int behavior);
    void setPivotPoint(const glm::vec3& pivotPoint);
    
    void clearData();
    
    bool isDebugging();
    
    std::string m_path;
    
    const char* m_data;
    const char* m_dataPos;
    
    int m_dataSize;
    
    bool m_ignoreNewLines;
    bool m_eof;
    
    int   m_firstFrame;
    int   m_lastFrame;
    float m_frameRate;
    std::vector<Object> m_objects;
    
};

#endif	/* LWSLOADER_H */

