/* 
 * File:   WidgetRightPanel.cpp
 * Author: Kevin
 * 
 * Created on 22. Juni 2015, 17:59
 */

#include "WidgetRightPanel.h"
#include "util.h"
#include "cmath"

WidgetRightPanel::WidgetRightPanel() {
    addChild(&m_widgetCrystal);
    addChild(&m_widgetOre);
}

WidgetRightPanel::~WidgetRightPanel() {
}

void WidgetRightPanel::setTextures(const InterfaceRessources* ress) {
    setTextureMain(ress->crystalBar);
    setTextureCrystal(ress->crystalIcon);
    setTextureCrystalUsed(ress->crystalIconUsed);
    setTextureCrystalEmpty(ress->crystalIconEmpty);
    setTextureOre(ress->oreBar);
    setFont(ress->fontMenuSmall);
}


void WidgetRightPanel::setTextureMain(Texture* texture) {
    m_textureMain = texture;
    
    setBaseSize(texture->getSize());
}

void WidgetRightPanel::setTextureCrystal(Texture* texture) {
    m_textureCrystal = texture;
}

void WidgetRightPanel::setTextureCrystalEmpty(Texture* texture) {
    m_textureCrystalEmpty = texture;
}

void WidgetRightPanel::setTextureCrystalUsed(Texture* texture) {
    m_textureCrystalUsed = texture;
}

void WidgetRightPanel::setTextureOre(Texture* texture) {
    m_textureOre = texture;
}

void WidgetRightPanel::setFont(Font* font) {
    m_widgetCrystal.setFont(font);
    m_widgetOre.setFont(font);
}

void WidgetRightPanel::setCrystalGoal(int count) {
    m_crystalGoal = count;
}

void WidgetRightPanel::setCrystalCount(int count) {
    m_crystalCount = count;
    
    sendUpdate();
}

void WidgetRightPanel::setCrystalUsedCount(int count) {
    m_crystalUsed = count;
}


void WidgetRightPanel::setOreCount(int count) {
    m_oreCount = count;
    
    sendUpdate();
}

float WidgetRightPanel::getOreBarHeight() {
    float x = clamp(((float) m_oreCount * (1.0f / 50)), 0.0f, 1.0f);
    
    //1 - (1 - x)^2.0;
    x = 1 - pow(1-x, 1.8);
    
    return x;
}


void WidgetRightPanel::onPaint(Renderer2D* renderer) {
    renderer->renderTexture(m_textureMain, getAbsPos(), getSize());
    
    const int maxCount = 26;
    int count = maxCount;
    
    if (m_crystalGoal && m_crystalGoal < maxCount){
        count = m_crystalGoal;
    }
    
    glm::vec2 basePos = getAbsPos() + getSize();
    
    for (int a = 0; a < count; a++){
        Texture* texture = m_textureCrystalEmpty;
        if (a < m_crystalUsed){
            texture = m_textureCrystalUsed;
        }else if (a < m_crystalCount){
            texture = m_textureCrystal;
        }
        
        glm::vec2 crystSize = glm::vec2(texture->getSize()) * getScale();
        glm::vec2 pos = basePos - glm::vec2(crystSize.x, crystSize.y * (maxCount - a));
        
        renderer->renderTexture(texture , pos, crystSize);
    }
    
    glm::vec2 oreSize = glm::vec2(8, 422 * getOreBarHeight()) * getScale();
    glm::vec2 orePos = glm::vec2(31, 46) * getScale();
    renderer->renderTexture(m_textureOre, getAbsPos() + orePos, oreSize);
}

void WidgetRightPanel::onUpdate() {
    m_widgetOre.setRelPos(glm::vec2(8, 0));
    m_widgetOre.setBaseSize(glm::vec2(16, 13));
    //m_widgetOre.setScale(getScale());
    m_widgetOre.setText(std::to_string(m_oreCount));
    
    m_widgetCrystal.setRelPos(glm::vec2(31, 0));
    m_widgetCrystal.setBaseSize(glm::vec2(20 ,13));
    //m_widgetCrystal.setScale(getScale());
    m_widgetCrystal.setText(std::to_string(m_crystalCount));
}

bool WidgetRightPanel::isBeeingHovered() {
    return false; //TODO: improve collision check
}
