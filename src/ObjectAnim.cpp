/* 
 * File:   ObjectAnim.cpp
 * Author: Kevin
 * 
 * Created on 19. Oktober 2015, 09:26
 */

#include "ObjectAnim.h"

#include <algorithm>
#include <iostream>

ObjectAnim::ObjectAnim():m_pos(0), m_rot(0), m_scale(1){
    
}

ObjectAnim::ObjectAnim(AnimInfo* info, std::string& anim):ObjectAnim(){
    setAnimInfo(info);
    setAnim(anim);
}

ObjectAnim::~ObjectAnim() {
    remove();
    clearEngineObjects();
}

void ObjectAnim::setPos(const glm::vec3& pos) {
    m_pos = pos;
    
    m_object.setPos(pos);
}

void ObjectAnim::setRot(const glm::vec3& rot) {
    m_rot = rot;
    
    m_object.setRot(rot);
}

void ObjectAnim::setScale(const glm::vec3& scale) {
    m_scale = scale;
    
    m_object.setScale(scale);
}

glm::vec3 ObjectAnim::getPos() {
    return m_pos;
}

glm::vec3 ObjectAnim::getRot() {
    return m_rot;
}

glm::vec3 ObjectAnim::getScale() {
    return m_scale;
}

void ObjectAnim::setAnimInfo(AnimInfo* info) {
    m_anim = info;
    
    setAnim("");
}

void ObjectAnim::clearEngineObjects() {
    for (auto& upgrade: m_upgrades){
        delete upgrade;
    }
    for (auto& wheel: m_wheels){
        delete wheel;
    }
    
    m_upgrades.clear();
    m_wheels.clear();
    /*for (auto obj: m_engineObjects){
        delete obj;
    }
    
    m_engineObjects.clear();*/
}

void ObjectAnim::createEngineObjects(std::string animName) {
    clearEngineObjects();
    
    //Add Main Object
    AnimInfo::Activity* act = m_anim->getActivity(animName);
    m_object.setMesh(act->mesh);
    
    //Add Wheels
    if  (m_anim->wheel){
        int numWheels = act->mesh->countGroup(m_anim->wheelNull);
        for (int i = 0; i < numWheels; i++){
            ObjectAnim* anim = new ObjectAnim(m_anim->wheel, animName);
            anim->attachTo(this, m_anim->wheelNull, i);
            m_wheels.push_back(anim);
        }
    }
    
    //Add Upgrades
    int numUpgrades = m_anim->countUpgrades(m_upgradesInstalled);
    for (int i = 0; i < numUpgrades; i++){
        auto upgrade = m_anim->getUpgrade(m_upgradesInstalled, i);
        ObjectAnim* anim = new ObjectAnim(upgrade->mesh, animName);
        anim->attachTo(this, upgrade->null, upgrade->slot);
        m_upgrades.push_back(anim);
    }
}

void ObjectAnim::attachToObject(EngineObject* target, int groupId) {
    m_object.attachTo(target, groupId);
}

void ObjectAnim::removeFromObject() {
    m_object.remove();
}

void ObjectAnim::attachTo(ObjectAnim* target, std::string slotName, int slotId) {
    remove(); //removes Object if attached to other object
    
    m_target       = target;
    m_targetSlot   = slotName;
    m_targetSlotId = slotId;
    
    target->m_childs.push_back(this);
    target->updateChilds();
}

void ObjectAnim::remove() {
    if (m_target != nullptr){
        auto& c = m_target->m_childs;

        c.erase(std::remove(c.begin(), c.end(), this), c.end());

        m_target->updateChilds();
        m_target = nullptr;

        removeFromObject();
    }
}

void ObjectAnim::updateChilds() {
    EngineObject* obj = getMainObject();
    
    for (auto& child: m_childs){
        int group = obj->getMesh()->getGroup(child->m_targetSlot, child->m_targetSlotId);
        
        child->attachToObject(obj, group);
    }
}

EngineObject* ObjectAnim::getMainObject() {
    return &m_object;
}

bool ObjectAnim::isAnimRunning() {
    return getMainObject()->isAnimRunning();
}

float ObjectAnim::getAnimTime() {
    return getMainObject()->getAnimTime();
}

void ObjectAnim::playAnimation(bool loop) {
    m_object.playAnimation(loop);
    
    for (auto& upgrade: m_upgrades){
        upgrade->playAnimation(loop);
    }
    for (auto& wheel: m_wheels){
        wheel->playAnimation(loop);
    }
}

void ObjectAnim::pauseAnimation() {
    m_object.pauseAnimation();
    
    for (auto& upgrade: m_upgrades){
        upgrade->pauseAnimation();
    }
    for (auto& wheel: m_wheels){
        wheel->pauseAnimation();
    }
    
}

void ObjectAnim::stopAnimation() {
    m_object.stopAnimation();
    
    for (auto& upgrade: m_upgrades){
        upgrade->stopAnimation();
    }
    for (auto& wheel: m_wheels){
        wheel->stopAnimation();
    }
}

AnimInfo::Activity* ObjectAnim::setAnim(const std::string& name) {
    if (m_anim){
        m_activity = m_anim->getActivity(name);

        createEngineObjects(name);
    }
}

AnimInfo* ObjectAnim::getAnimInfo() {
    return m_anim;
}





