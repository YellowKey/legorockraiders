/* 
 * File:   WidgetGameView.cpp
 * Author: kevin
 * 
 * Created on July 1, 2015, 10:20 AM
 */

#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "WidgetGameView.h"
#include "Unit.h"
#include "UnitFigure.h"
#include "ObjectInfo.h"

#include <iostream>

WidgetGameView::WidgetGameView() {
}

WidgetGameView::~WidgetGameView() {
}

void WidgetGameView::setEngine3D(Engine3D* engine) {
    m_engine = engine;
}

void WidgetGameView::setGameMap(GameMap* map) {
    m_map = map;
}

void WidgetGameView::setGame(Game* game) {
    m_game = game;
}

void WidgetGameView::setRendererMap(RendererMap* renderer) {
    m_rendererMap = renderer;
}


void WidgetGameView::enableDebugControl(bool state) {
    m_debugControls = state;
}

void WidgetGameView::setCameraFov(float fov) {
    m_cameraFov = fov;
}

void WidgetGameView::setCameraPos(const glm::vec3 pos) {
    m_cameraPos = pos;
}

void WidgetGameView::setCameraRot(const glm::vec3 rot) {
    m_cameraRot = rot;
} 


glm::vec3 WidgetGameView::getCameraPos() {
    return m_cameraPos;
}

glm::vec3 WidgetGameView::getCameraRot() {
    return m_cameraRot;
}


float WidgetGameView::getCameraFov() {
    return m_cameraFov;
}

void WidgetGameView::engineSetCamera() {
    const glm::vec2 size = getSize();
    
     //m_engine->setCameraPos(m_cameraPos);
    m_engine->setCameraRot(m_cameraRot);
    //TODO: setCameraFov
    m_engine->setAspectRatio(size.x / size.y);
    m_engine->setRenderArea(getAbsPos(), size);
}


#include <iostream>
#include "util.h"
#include "Game.h"
#include "Building.h"
#include "App.h"

void WidgetGameView::onPaint(Renderer2D* renderer) {
    renderer->flush();
    
    engineSetCamera();
    
    m_engine->render();
    
    if (m_selecting){
        glm::vec2 start = m_selectionStart + getAbsPos();
        glm::vec2 size  = m_selectionEnd   + getAbsPos() - start;
        
        renderer->setTexture(nullptr);
        renderer->setColor(0.0f, 1.0f, 0.0f);
        renderer->render(start, glm::vec2(size.x, 1));
        renderer->render(start, glm::vec2(1, size.y));
        
        renderer->render(start + size, -glm::vec2(size.x, 1));
        renderer->render(start + size, -glm::vec2(1, size.y));
    }
    
    renderer->setTexture(nullptr);
    const int n = m_map->getNumObjects();
    for (int a = 0; a < n; a++){
        auto* child = m_map->getObject(a);

        Unit* fig = dynamic_cast<Unit*>(child);

        if (fig){
            glm::vec2 pos = project(fig->getPosWorld());

            if (fig->isSelected()){
                renderer->render(pos - glm::vec2(5, 5), glm::vec2(10, 10));
            }
        }
    }

    std::cout.flush();
}

void WidgetGameView::onKeyDown(Key key) {

}

void WidgetGameView::onKeyUp(Key key) {

}


void WidgetGameView::onMouseDown(int mouseButton) {
    if (m_debugControls){
        if (mouseButton == 1){
            requestInputFocus();
            m_debugRotate = true;
        }
    }
    
    if (mouseButton == 0){
        if (m_build){
            m_building = true;
            m_selectionStart = m_cursorPos;
        }else{
            requestInputFocus();
            m_selecting = true;
            m_selectionStart = m_cursorPos;
            m_selectionEnd   = m_cursorPos;
        }
    }
    
    if (mouseButton == 1){
        if (m_build){
            clearBuildingMarks();
            m_build = nullptr;
            m_building = false;
        }
    }
}

void WidgetGameView::onMouseUp(int mouseButton) {
    if (m_debugControls){
        if (mouseButton == 1){
            releaseInputFocus();
            m_debugRotate = false;
        }
    }
    
    if (mouseButton == 0){
        if (m_build){
            if (m_building){
                glm::vec3 pickPos = pickRay(m_selectionStart);
                glm::ivec2 baseTile = GameMap::posToTile(glm::vec2(pickPos.x, pickPos.z));
                glm::vec2 pos = GameMap::tileToPos(baseTile);
                
                Building* building = new Building(m_build->activity, glm::vec3(pos.x, 0, pos.y));
                building->setState(Building::STATE_TELEPORT);
                m_game->getGameMap()->addObject(building);
                
                for (int a = 0; a < m_build->getNumTiles(); a++){
                    glm::ivec2 tile = baseTile + m_build->getTilePos(a);
                    switch(m_build->getTileType(a)){
                        case ObjectInfo::TILE_BUILDING:
                            m_game->getGameMap()->setTileType(tile.x, tile.y, GameMap::TILE_BUILD);
                            m_game->getGameMap()->update(tile.x, tile.y);
                            break;
                        case ObjectInfo::TILE_POWER:
                            m_game->getGameMap()->setTileType(tile.x, tile.y, GameMap::TILE_BUILDWALK);
                            m_game->getGameMap()->update(tile.x, tile.y);
                            break;
                        case ObjectInfo::TILE_WATER:
                            break;
                        case ObjectInfo::TILE_EMPTY:
                            break;
                    }
                }
                
                clearBuildingMarks();
                
                m_building = false;
                m_build    = nullptr;
            }
        }else{
            releaseInputFocus();

            m_game->deselectTile();

            const int n = m_map->getNumObjects();
            int count = 0;
            Unit* fig;
            for (int a = 0; a < n; a++){
                if (fig = dynamic_cast<Unit*>(m_map->getObject(a))){
                    glm::vec2 pos = project(fig->getPosWorld());

                    glm::vec2 p1 = m_selectionStart;
                    glm::vec2 p2 = m_selectionEnd;

                    glm::vec2 s = glm::min(p1, p2);
                    glm::vec2 e = glm::max(p1, p2);
                    bool select = pos.x > s.x && pos.y > s.y && pos.x < e.x && pos.y < e.y;
                    fig->select(select);

                    if (select) count++;
                }
            }

            if (count == 0 && glm::distance(m_selectionStart, m_selectionEnd) < 1.0f){
                glm::vec3 normal = getPickRayDir(m_selectionEnd);
                glm::vec3 cPos   = m_engine->getCameraPos();

                glm::vec3 pos = cPos + cPos.y * normal / (-normal.y);
                glm::ivec2 tile = m_map->posToTile(glm::vec2(pos.x, pos.z));

                m_game->selectTile(tile);
            }

            m_selecting = false;
        }
    }
}

void WidgetGameView::onMouseMove(glm::vec2 relPos) {
    m_cursorPos = relPos;
    
    glm::vec2 cursorDiff = relPos - m_lastCursor;
    
    if (m_selecting){
        m_selectionEnd = m_cursorPos;
    }
    
    if (m_debugRotate){
        float factor = 0.01f;
        
        setCameraRot(getCameraRot() + glm::vec3(cursorDiff.y * factor, -cursorDiff.x * factor, 0));
    }
    
    if (m_build){
        clearBuildingMarks();
        setBuildingMarks();
    }
    
    m_lastCursor = relPos;
    
    m_engine->setCursorLightPos(pickRay(relPos) + glm::vec3(0, 50, 0));
    m_engine->setCursorLightColor(glm::vec3(1.0f, 1.0f, 1.0f));
}

glm::vec3 WidgetGameView::getPickRayDir(glm::vec2 cursorPos) {
    glm::vec3 cursor = glm::vec3(cursorPos, 0.98f);
    glm::vec4 viewport = glm::vec4(getAbsPos(), getSize());
    
    engineSetCamera();
    
    glm::vec3 posWorld = glm::unProject(cursor, glm::mat4(), m_engine->getCameraMatrix(), viewport);
    
    return glm::normalize(posWorld - m_engine->getCameraPos()); //TODO: replace with getCameraPos()
}

glm::vec3 WidgetGameView::pickRay(glm::vec2 cursorPos) {
    glm::vec3 normal = getPickRayDir(cursorPos);
    glm::vec3 cPos   = m_engine->getCameraPos();
    
    glm::vec3 pos(0);
    
    m_rendererMap->rayTest(cPos, normal, pos);
    
    return pos;
    //return cPos + cPos.y * normal / (-normal.y);
}


glm::vec2 WidgetGameView::project(glm::vec3 point) {
    glm::vec4 viewport = glm::vec4(getAbsPos(), getSize());
    
    glm::vec3 pos3D = glm::project(point, glm::mat4(), m_engine->getCameraMatrix(), viewport);
    
    return glm::vec2(pos3D.x, pos3D.y);
}

void WidgetGameView::build(ObjectInfo* building) {
    m_build = building;
}

void WidgetGameView::setBuildingMarks() {
    if (m_build){
        glm::vec3 pickPos = pickRay(m_cursorPos);
        glm::ivec2 baseTile = GameMap::posToTile(glm::vec2(pickPos.x, pickPos.z));

        m_lastBuildPos = baseTile;

        GameMap* map = m_game->getGameMap();

        for (int a = 0; a < m_build->getNumTiles(); a++){
            glm::ivec2 tile = baseTile + m_build->getTilePos(a);
            if (m_build->getTileType(a) == ObjectInfo::TILE_BUILDING){
                map->setTileMark(tile.x, tile.y, GameMap::MARK_BUILD, true);
            }else if (m_build->getTileType(a) != ObjectInfo::TILE_EMPTY){
                map->setTileMark(tile.x, tile.y, GameMap::MARK_BUILDPATH, true);
            }
        }
    }
}

void WidgetGameView::clearBuildingMarks() {
    if (m_build){
        GameMap* map = m_game->getGameMap();

        for (int a = 0; a < m_build->getNumTiles(); a++){
            glm::ivec2 tile = m_lastBuildPos + m_build->getTilePos(a);
            map->setTileMark(tile.x, tile.y, GameMap::MARK_BUILD    , false);
            map->setTileMark(tile.x, tile.y, GameMap::MARK_BUILDPATH, false);
        }
    }
}

