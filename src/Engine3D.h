/* 
 * File:   Engine3D.h
 * Author: Kevin
 *
 * Created on 14. Mai 2015, 14:10
 */

#ifndef ENGINE3D_H
#define	ENGINE3D_H

#include "Shape.h"
#include "Mesh.h"
#include "glVertexArray.h"
#include "glProgram.h"
#include "glVertexArray.h"

#include <glm/glm.hpp>
#include <map>
#include <unordered_map>
#include <sigc++/sigc++.h>

class EngineObject;
class EngineShape;

class Engine3D {
public:
    Engine3D();
    Engine3D(const Engine3D& orig) = delete;
    virtual ~Engine3D();
    
    void addShape(Shape* shape);
    void addMesh(Mesh* mesh);
    void renderMesh(Mesh* mesh, float time, const glm::mat4& mat = glm::mat4());
    void renderShape(Shape* shape, const glm::mat4& mat = glm::mat4());
    
    glm::vec3 getCameraPos();
    glm::vec3 getCameraRot();
    
    void setCameraPos(const glm::vec3& pos);
    void setCameraRot(const glm::vec3& rot);
    void setAspectRatio(float aspect);
    
    void setCursorLightPos(const glm::vec3& pos);
    void setCursorLightColor(const glm::vec3& color);
    
    glm::vec3 getCursorLightPos();
    glm::vec3 getCursorLightColor();
    glm::vec3 getAmbientLightColor();
    
    void  setGamma(float gamma);
    float getGamma();
    
    void setRenderArea(const glm::vec2 start, const glm::vec2 end);
    
    float getBufferUsage();
    
    glm::mat4 getCameraMatrix();
    
    void render();
    
    int getNumRenderedShapes();
    int getNumUpdatedObjects();
    
    sigc::signal<void, Engine3D*>& signalRender();
private:
    friend class EngineObject;
    void registerObject(EngineObject* obj);
    void removeObject(EngineObject* obj);
    
    void renderPass(bool trans);
    
    std::map<Shape*, std::vector<EngineShape*>> m_shapes;
    std::vector<EngineObject*> m_objects;
    
    int BUFFERSIZE = 1024 * 1024 * 32;
    
    typedef Shape::Vertex Vertex;
    /*struct Vertex{ 
        glm::vec3 pos;
        glm::vec2 uv;
        glm::vec3 normal;
    };*/
    
    glBuffer m_vertexBuffer = {GL_ARRAY_BUFFER};
    int      m_bufferOffset = 0;
    
    glVertexArray m_vertexArray;
    glProgram     m_shader;
    
    glUniform m_uniCameraMatrix;
    glUniform m_uniWorldMatrix;
    glUniform m_uniNormalMatrix;
    glUniform m_uniColorTexture;
    glUniform m_uniColorFactor;
    glUniform m_uniColorTrans;
    glUniform m_uniCursorLightPos;
    glUniform m_uniCursorLightColor;
    glUniform m_uniAmbientLightColor;
    glUniform m_uniCameraPos;
    glUniform m_uniGamma;
    
    glm::vec3 m_cameraPos = {0, 10, 60};
    glm::vec3 m_cameraRot = {0, 0, 0};
    
    glm::vec2 m_viewStart;
    glm::vec2 m_viewEnd;
    
    float m_aspectRatio = 1.0f;
    
    float m_gamma = 1.0f / 2.2f;
    
    int m_numRenderedShapes = 0;
    int m_numUpdatedObjects = 0;
    
    glm::vec3 m_cursorLightPos;
    glm::vec3 m_cursorLightColor;
    
    sigc::signal<void, Engine3D*> m_signalRender;
};

#endif	/* ENGINE3D_H */

