/* 
 * File:   GameWindow.cpp
 * Author: Kevin
 * 
 * Created on 1. Mai 2015, 12:00
 */

#include "GameWindow.h"

#include <algorithm>
#include <iostream>

std::vector<GameWindow*> GameWindow::s_windowList;



GameWindow::GameWindow(const std::string& title, int width, int height, bool fullscreen) {
    sf::ContextSettings settings;
    
    settings.majorVersion = 3;
    settings.minorVersion = 1;
    settings.depthBits = 24;
    
    uint32_t style = sf::Style::Default;
    
    if (fullscreen){
        style = sf::Style::Fullscreen;
    }
    
    m_window.create(sf::VideoMode(width, height), title, style, settings);
    
    if (!m_window.isOpen()){
        std::cout << "couldn't create window" << std::endl;
        std::cout.flush();
    }
    
    m_window.setVerticalSyncEnabled(true);
    
    s_windowList.push_back(this);
}

GameWindow::~GameWindow() {
    s_windowList.erase(std::remove(s_windowList.begin(), s_windowList.end(), this), s_windowList.end());
}



int GameWindow::getWidth() const {
    return m_window.getSize().x;
}

int GameWindow::getHeight() const {
    return m_window.getSize().y;
}

bool GameWindow::shouldClose() const {
    return m_shouldClose;
}



bool GameWindow::getKey(sf::Keyboard::Key id) const {
    return sf::Keyboard::isKeyPressed(id);
}

glm::vec2 GameWindow::getCursorPos() const {
    auto pos = sf::Mouse::getPosition(m_window);
    
    return glm::vec2(pos.x, getHeight() - pos.y);
}

bool GameWindow::getMouseButton(int id) const {
    sf::Mouse::Button button;
    
    switch(id){
        case 0: button = sf::Mouse::Left; break;
        case 1: button = sf::Mouse::Right; break;
        case 2: button = sf::Mouse::Middle; break;
        case 3: button = sf::Mouse::XButton1; break;
        case 4: button = sf::Mouse::XButton2; break;
    }
    
    return sf::Mouse::isButtonPressed(button);
}



void GameWindow::makeCurrent() {
    m_window.setActive(true);
}

void GameWindow::setFullscreen(bool fullscreen) {
    
}



void GameWindow::update() {
    for (GameWindow* window: s_windowList){
        window->onUpdate();
    }
}

void GameWindow::onUpdate() {
    sf::Event event;
    
    while (m_window.pollEvent(event)){
        if (event.type == sf::Event::Closed){
            m_shouldClose = true;
        }
    }
}

void GameWindow::swapBuffers() {
    m_window.display();
}



