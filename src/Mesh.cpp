/* 
 * File:   Mesh.cpp
 * Author: Kevin
 * 
 * Created on 17. Mai 2015, 16:04
 */

#include "Mesh.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/euler_angles.hpp>

#include "util.h"
#include "App.h"

Mesh::Mesh() {
    
}

Mesh::~Mesh() {
    
}

void Mesh::SetAnimTime(float startTime, float endTime) {
    m_startTime = startTime;
    m_endTime   = endTime;
}

void Mesh::setLoopAnim(bool state) {
    m_loop = state;
}


int Mesh::addGroup() {
    m_groups.push_back(Group());
    
    int id = m_groups.size() - 1;
    
    m_groups[id].parentGroup = -1;
    m_groups[id].pivotPoint = glm::vec3(0);
    
    return id;
}

void Mesh::setGroupParent(int groupId, int parentId) {
    if (groupId < m_groups.size()){
        m_groups[groupId].parentGroup = parentId;
    }
}

void Mesh::setGroupPivotPoint(int groupId, const glm::vec3& point) {
    if (groupId < m_groups.size()){
        m_groups[groupId].pivotPoint = point;
    }
}

void Mesh::setGroupName(int groupId, const std::string& name) {
    std::string lName = name;
    std::transform(lName.begin(), lName.end(), lName.begin(), ::tolower);
    
    if (groupId < m_groups.size()){
        m_groups[groupId].name = lName;
    }
}




int Mesh::addKeyframe(int groupId) {
    if (groupId < m_groups.size()){
        m_groups[groupId].keyframes.push_back(KeyFrame());
        
        int frameId = m_groups[groupId].keyframes.size() - 1;
        
        auto& frame = m_groups[groupId].keyframes[frameId];
        
        frame.matrixBaked = false;
        frame.pos   = glm::vec3(0);
        frame.rot   = glm::vec3(0);
        frame.scale = glm::vec3(1);
        frame.time  = 0;
        
        return frameId;
    }
    
    return -1;
}

void Mesh::setKeyFramePos(int groupId, int frameId, const glm::vec3& pos) {
    if (groupId < m_groups.size() && frameId < m_groups[groupId].keyframes.size()){
        m_groups[groupId].keyframes[frameId].pos = pos;
    }
}

void Mesh::setKeyFrameRot(int groupId, int frameId, const glm::vec3& rot) {
    if (groupId < m_groups.size() && frameId < m_groups[groupId].keyframes.size()){
        m_groups[groupId].keyframes[frameId].rot = rot;
    }
}

void Mesh::setKeyFrameScale(int groupId, int frameId, const glm::vec3& scale) {
    if (groupId < m_groups.size() && frameId < m_groups[groupId].keyframes.size()){
        m_groups[groupId].keyframes[frameId].scale = scale;
    }
}

void Mesh::setKeyFrameTime(int groupId, int frameId, float time) {
    if (groupId < m_groups.size() && frameId < m_groups[groupId].keyframes.size()){
        m_groups[groupId].keyframes[frameId].time = time;
    }
}


int Mesh::addShape(int groupId, Shape* shape) {
    if (groupId < m_groups.size()){
        m_groups[groupId].shapes.push_back(shape);
        
        return m_groups[groupId].shapes.size() - 1;
    }
    
    return -1;
}


int Mesh::getNextKeyFrameId(int groupId, float time) {
    int frameId;
    
    if (groupId < m_groups.size()){
        auto& group = m_groups[groupId];
        
        int numFrames = group.keyframes.size();
        
        for (frameId = 0; frameId < numFrames; frameId++){
            if (group.keyframes[frameId].time > time){
                return frameId;
            }
        }
        
        return numFrames - 1;
    }
    
    return 0;
}

float Mesh::getBlendFactor(int groupId, float time) {
    KeyFrame* prevFrame = getPrevKeyFrame(groupId, time);
    KeyFrame* nextFrame = getNextKeyFrame(groupId, time);
    
    float diff = nextFrame->time - prevFrame->time;
    float off  = time - prevFrame->time;
    
    return clamp(off / diff, 0.0f, 1.0f);
}

Mesh::KeyFrame* Mesh::getNextKeyFrame(int groupId, float time) {
    return &(m_groups[groupId].keyframes[getNextKeyFrameId(groupId, time)]);
}

Mesh::KeyFrame* Mesh::getPrevKeyFrame(int groupId, float time) {
    int frameId = getNextKeyFrameId(groupId, time) - 1;
    
    if (frameId < 0){
        frameId = 0;
    }
    
    return &(m_groups[groupId].keyframes[frameId]);
}

glm::mat4 Mesh::getLocalMatrix(int groupId, float time) {
    glm::mat4 model = glm::mat4();
    
    glm::vec3 pos   = getPos(groupId, time);
    glm::vec3 rot   = getRot(groupId, time);
    glm::vec3 scale = getScale(groupId, time);
    glm::vec3 pivotPoint = m_groups[groupId].pivotPoint;
    
    pos.z = -pos.z;
    pivotPoint.z = -pivotPoint.z;
    
    rot.y = -rot.y;
    rot.x = -rot.x;
    //rot.z = -rot.z;
    
    model = glm::translate(model, pos);
    model = glm::translate(model, -pivotPoint);
    model = model * glm::eulerAngleYXZ(rot.x, rot.y, rot.z);
    model = glm::scale(model, scale);
    
    return model;
}

glm::mat4 Mesh::getMatrix(int groupId, float time) {
    glm::mat4 model;
    
    if (m_groups[groupId].keyframes.size() == 0){
        model = glm::mat4();
    }else if (m_groups[groupId].keyframes.size() == 1){ 
        auto& frame = m_groups[groupId].keyframes[0];
        
        if (!frame.matrixBaked){
            frame.matrix = getLocalMatrix(groupId, time);
            frame.matrixBaked;
        }
        
        model = frame.matrix;
    }else{
        model = getLocalMatrix(groupId, time);
    }
    
    int parent = m_groups[groupId].parentGroup;
    
    if (parent >= 0){
        model =  getMatrix(parent, time) * model;
    }
    
    return model;
}

glm::mat4 Mesh::getBakedMatrix(int groupId, float time) {
    KeyFrame* nextFrame = getPrevKeyFrame(groupId, time);
    KeyFrame* prevFrame = getNextKeyFrame(groupId, time);
    
    float blendFactor = getBlendFactor(groupId, time);
    
    if (nextFrame->matrixBaked == false){
        nextFrame->matrix = getMatrix(groupId, nextFrame->time);
        nextFrame->matrixBaked = true;
    }
    if (prevFrame->matrixBaked == false){
        prevFrame->matrix = getMatrix(groupId, prevFrame->time);
        prevFrame->matrixBaked = true;
    }
    
    return glm::mix(nextFrame->matrix, prevFrame->matrix, blendFactor);
}


glm::vec3 Mesh::getPos(int groupId, float time) {
    KeyFrame* prevFrame = getPrevKeyFrame(groupId, time);
    KeyFrame* nextFrame = getNextKeyFrame(groupId, time);
    
    float blendFactor = getBlendFactor(groupId, time);
    
    return glm::mix(prevFrame->pos, nextFrame->pos, blendFactor);
}

glm::vec3 Mesh::getRot(int groupId, float time) {
    KeyFrame* prevFrame = getPrevKeyFrame(groupId, time);
    KeyFrame* nextFrame = getNextKeyFrame(groupId, time);
    
    float blendFactor = getBlendFactor(groupId, time);
    
    return glm::mix(prevFrame->rot, nextFrame->rot, blendFactor);
}

glm::vec3 Mesh::getScale(int groupId, float time) {
    KeyFrame* prevFrame = getPrevKeyFrame(groupId, time);
    KeyFrame* nextFrame = getNextKeyFrame(groupId, time);
    
    float blendFactor = getBlendFactor(groupId, time);
    
    //return glm::vec3(1);
    return glm::mix(prevFrame->scale, nextFrame->scale, blendFactor);
}


int Mesh::getGroup(const std::string& name, int index) {
    std::string lName = name;
    std::transform(lName.begin(), lName.end(), lName.begin(), ::tolower);
    
    //std::cout << "Start" << std::endl;
    for (int a = 0; a < m_groups.size(); a++){
        if (m_groups[a].name == lName){
            //std::cout << index << ": " << a << std::endl;
            if (index <= 0){
                return a;
            }else{
                index--;
            }
        }
    }
    
    return -1;
}

int Mesh::countGroup(const std::string& name) {
    int count = 0;
    
    for (int a = 0; a < m_groups.size(); a++){
        if (m_groups[a].name == name){
            count++;
        }
    }
    
    return count;
}


int Mesh::getNumGroups() {
    return m_groups.size();
}

int Mesh::getNumShapes(int groupId) {
    if (groupId < m_groups.size()){
        return m_groups[groupId].shapes.size();
    }
    
    return 0;
}

Shape* Mesh::getShape(int groupId, int shapeId) {
    if (groupId < m_groups.size() && shapeId < m_groups[groupId].shapes.size()){
        return m_groups[groupId].shapes[shapeId];
    }
    
    return nullptr;
}

float Mesh::getAnimTime() {
    return m_endTime - m_startTime;
}












