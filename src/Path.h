/* 
 * File:   Path.h
 * Author: Kevin
 *
 * Created on 8. Juni 2015, 14:46
 */

#ifndef PATH_H
#define	PATH_H

#include <glm/vec2.hpp>
#include <vector>

class Path {
public:
    Path();
    Path(const Path& orig);
    Path(const Path&& orig);
    virtual ~Path();
    
    Path& operator=(const Path&);
    Path& operator=(Path&&);
    
    void clear();
    
    int getNumNodes() const;
    glm::ivec2 getNodePos(int id) const;
    
    glm::ivec2 popNextNode();
    
    void addNode(const glm::ivec2& pos);
private:
    struct Node{
        glm::ivec2 pos;
    };
    
    std::vector<Node> m_nodes;
};

#endif	/* PATH_H */

