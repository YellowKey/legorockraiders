/* 
 * File:   WidgetButton.cpp
 * Author: Kevin
 * 
 * Created on 21. Juni 2015, 16:16
 */

#include "WidgetButton.h"
#include "ButtonListener.h"
#include "Action.h"

#include <algorithm>
#include <iostream>

WidgetButton::WidgetButton() {
}

WidgetButton::WidgetButton(const InterfaceRessources::MenuIcon& textures) {
    setTextures(textures);
}


WidgetButton::~WidgetButton() {
    for (Listener listener: m_listener){
        listener.listener->removeButton(this);
    }
}

WidgetButton* WidgetButton::addListener(ButtonListener* listener, int id) {
    m_listener.push_back({listener, id});
    
    listener->addButton(this);
    
    return this;
}

void WidgetButton::removeListener(ButtonListener* listener) {
    auto iter = m_listener.begin();
    
    while (iter != m_listener.end()){
        if (iter->listener == listener){
            m_listener.erase(iter);
        }else{
            iter++;
        }
    }
    
    listener->removeButton(this);
}



void WidgetButton::setTexture(const Texture* texture) {
    m_textureNormal = texture;
    
    if (texture){
        setBaseSize(texture->getSize());
    }
}

void WidgetButton::setTexturePushed(const Texture* texture) {
    m_texturePushed = texture;
    
    if (texture){
        setBaseSize(texture->getSize());
    }
}

void WidgetButton::setTextureDisabled(const Texture* texture) {
    m_textureDisable = texture;
    
    if (texture){
        setBaseSize(texture->getSize());
    }
}

void WidgetButton::setTextureHover(const Texture* texture) {
    m_textureHover = texture;
    
    if (texture){
        setBaseSize(texture->getSize());
    }
}


void WidgetButton::setTextures(const InterfaceRessources::MenuIcon& textures) {
    setTexture(textures.normal);
    setTexturePushed(textures.pushed);
    setTextureDisabled(textures.disabled);
}

void WidgetButton::disable(bool state) {
    m_enabled = !state;
}


void WidgetButton::onPaint(Renderer2D* renderer) {
    const Texture* texture = getCurrentTexture();
    
    if (texture){
        renderer->renderTexture(texture, getAbsPos(), getSize());
    }
}

void WidgetButton::onMouseDown(int mouseButton) {
    if (mouseButton == 0){
        m_clicked = true;
        
        requestInputFocus();
    }
    
    for (Listener listener: m_listener){
        listener.listener->onPush(listener.id, mouseButton);
    }
}

void WidgetButton::onMouseUp(int mouseButton) {
    for (Listener listener: m_listener){
        listener.listener->onRelease(listener.id, mouseButton);
        
        if (mouseButton == 0 && m_clicked && isBeeingHovered()){
            listener.listener->onClick(listener.id, 0);
        }
    }
    
    if (mouseButton == 0){
        m_clicked = false;
        
        releaseInputFocus();
    }
}

void WidgetButton::onMouseHover(bool state) {
    for (Listener listener: m_listener){
        listener.listener->onHover(listener.id, state);
    }
}


bool WidgetButton::isBeeingClicked() {
    return m_clicked;
}

const Texture* WidgetButton::getCurrentTexture() {
    const Texture* texture = nullptr;
    
    if (m_enabled){
        if (isBeeingClicked()){
            texture = m_texturePushed;
        }else if (isBeeingHovered()){
            texture = m_textureHover;
        }else {
            texture = m_textureNormal;
        }
    }else{
        texture = m_textureDisable;
    }
    
    //In case a texture hasn't been selected
    if(!texture){
        texture = m_textureNormal;
    }
    
    return texture;
}




