/* 
 * File:   WidgetIconPanel.h
 * Author: Kevin
 *
 * Created on 22. Juni 2015, 17:25
 */

#ifndef WIDGETICONPANEL_H
#define	WIDGETICONPANEL_H

#include "Widget.h"

class WidgetIconPanel: public Widget{
public:
    WidgetIconPanel();
    virtual ~WidgetIconPanel();
    
    void setPanelTexture(Texture* texture);
    void setIconOffset(const glm::vec2& offset);
    void enableBackButton();
protected:
    virtual void onPaint(Renderer2D* renderer) override;
    virtual void onUpdate() override;
private:
    bool m_hasBack = false;
    Texture* m_texture = nullptr;
    glm::vec2 m_offset = {0.0f, 0.0f};
};

#endif	/* WIDGETICONPANEL_H */

