/* 
 * File:   LoaderUIController.cpp
 * Author: Kevin
 * 
 * Created on 1. Oktober 2015, 12:15
 */

#include "LoaderUIController.h"

#include "RessourceContainer.h"

LoaderUIController::LoaderUIController(LoaderRessources& ress):m_ress(ress){
    m_widgetText.setRelPos(glm::vec2(0,0));
    m_widgetBackground.setTexture(m_ress.background);
    
    m_widgetBar.setRelPos(glm::vec2(142,21));
    m_widgetBar.setTexture(m_ress.bar);
    
    m_widgetText.setRelPos(glm::vec2(142 + 175,17));
    m_widgetText.setFont(m_ress.font);
    m_widgetText.setText("Loading");
    
    m_widgetScreen.addChild(&m_widgetBackground);
    m_widgetScreen.addChild(&m_widgetBar);
    m_widgetScreen.addChild(&m_widgetText);
    
    m_widgetScreen.setBaseSize(glm::vec2(640, 480));
    
    m_barSize = m_widgetBar.getBaseSize();
}


LoaderUIController::~LoaderUIController() {
}

void LoaderUIController::scale(glm::vec2 size) {
    float scale = size.y / 480.0f;
    m_widgetScreen.setScale(scale);
    
    float x = (size.x - m_widgetScreen.getSize().x);
    m_widgetScreen.setRelPos(glm::vec2(x, 0) * 0.5f);
}

void LoaderUIController::update() {
    if (m_count == 0){
        m_count = RessourceContainer::getCount();
    }
    
    if (RessourceContainer::getCount() == 0){
        m_done = true;
    }else{
        RessourceContainer::load(m_loadCount);
        m_current+=m_loadCount;
    }
    
    m_widgetBar.setBaseSize(glm::vec2(m_barSize.x * getProgress(), m_barSize.y));
}

void LoaderUIController::render(Renderer2D* renderer) {
    m_widgetScreen.render(renderer);
}

bool LoaderUIController::isDone() {
    return m_done;
}

float LoaderUIController::getProgress() {
    float p = (float) m_current / m_count;
    
    if (p > 1.0f) p = 1.0f;
    
    return p;
}
