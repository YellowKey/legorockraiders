/* 
 * File:   WidgetCameraControl.cpp
 * Author: kevin
 * 
 * Created on June 24, 2015, 12:12 PM
 */

#include "WidgetCameraControl.h"

WidgetCameraControl::WidgetCameraControl() {
    m_widgetZoomIn.setRelPos(glm::vec2(13,13));
    m_widgetZoomOut.setRelPos(glm::vec2(103, 14));
    m_widgetNextUnit.setRelPos(glm::vec2(140, 18));
    
    addChild(&m_widgetZoomIn);
    addChild(&m_widgetZoomOut);
    addChild(&m_widgetNextUnit);
}

WidgetCameraControl::~WidgetCameraControl() {
}

void WidgetCameraControl::setTexture(InterfaceRessources* ress) {
    m_textureMain = ress->cameraControl;
    
    m_textureDown  = ress->navDown;
    m_textureUp    = ress->navUp;
    m_textureLeft  = ress->navLeft;
    m_textureRight = ress->navRight;
    
    m_widgetZoomIn.setTextureHover(ress->navZoomInHigh);
    m_widgetZoomIn.setTexturePushed(ress->navZoomInPush);
    m_widgetZoomOut.setTextureHover(ress->navZoomOutHigh);
    m_widgetZoomOut.setTexturePushed(ress->navZoomOutPush);
    m_widgetNextUnit.setTextureHover(ress->navHomeHigh);
    m_widgetNextUnit.setTexturePushed(ress->navHomePush);
    
    setBaseSize(m_textureMain->getSize());
}

void WidgetCameraControl::onPaint(Renderer2D* renderer) {
    renderer->renderTexture(m_textureMain, getAbsPos(), getSize());
}



