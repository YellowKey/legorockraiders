/* 
 * File:   glBuffer.cpp
 * Author: Kevin
 * 
 * Created on 17. November 2014, 20:20
 */


//TODO: add DSA
#include "glBuffer.h"

#include <string.h>

map<GLenum, glBuffer*> glBuffer::s_boundBuffer;

glBuffer::glBuffer(GLenum type):m_type(type){
    glGenBuffers(1, &m_handle);
}

glBuffer::glBuffer(glBuffer&& orig){
    memcpy(this, &orig, sizeof(glBuffer));
    
    orig.m_movedAway = true;
}

glBuffer::glBuffer(GLenum type, GLenum usage):glBuffer(type) {
    setUsage(usage);
}

glBuffer::~glBuffer() {
    if (!m_movedAway){
        glDeleteBuffers(1, &m_handle);
    }
}

void glBuffer::bindBuffer() {
    if (getBoundBuffer(m_type) != this){
        glBindBuffer(m_type, m_handle);
        
        setBoundBuffer(m_type, this);
    }
}

void glBuffer::clearBufferSlot(GLenum type) {
    glBindBuffer(type, 0);
    setBoundBuffer(type, nullptr);
}


void glBuffer::setUsage(GLenum usage) {
    m_usage = usage;
}


void glBuffer::BufferData(int size, const void* data) {
    setMemory(size);
    
    bindBuffer();
    
    glBufferData(m_type, size, data, m_usage);
}

void glBuffer::BufferSubData(int offset, int size, const void* data) {
    bindBuffer();
    
    glBufferSubData(m_type, offset, size, data);
}

size_t glBuffer::getSize() {
    return getMemory();
}

void* glBuffer::mapBuffer(GLenum access) {
    bindBuffer();
    
    return m_mappedTo = glMapBuffer(m_type, access);
}

void* glBuffer::mapBufferRange(int offset, int size, GLbitfield access) {
    bindBuffer();
    
    return m_mappedTo = glMapBufferRange(m_type, offset, size, access);
}

void glBuffer::unmapBuffer() {
    bindBuffer();
    
    glUnmapBuffer(m_type);
    
    m_mappedTo = nullptr;
}

void* glBuffer::getMapping() {
    return m_mappedTo;
}


glBuffer* glBuffer::getBoundBuffer(GLenum type) {
    return s_boundBuffer[type];
}

void glBuffer::setBoundBuffer(GLenum type, glBuffer* buffer) {
    s_boundBuffer[type] = buffer;
}

glObject::Type glBuffer::getObjectType() {
    return TYPE_BUFFER;
}

GLuint glBuffer::getHandle() {
    return m_handle;
}
