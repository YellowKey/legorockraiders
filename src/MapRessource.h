/* 
 * File:   MapRessource.h
 * Author: Kevin
 *
 * Created on 28. Mai 2015, 12:13
 */

#ifndef MAPRESSOURCE_H
#define	MAPRESSOURCE_H

#include <string>
#include "Mesh.h"

#include "glTexture.h"

class MapRessource {
public:
    MapRessource(const std::string& biome);
    MapRessource(const MapRessource& orig) = delete;
    virtual ~MapRessource();
    
    void load();
    
    Mesh* getShape(int type);
    Texture* m_texture;
    glTexture* m_textureTiles;
    glTexture* m_textureNoise;
private:
    void loadShape(int id, int heightField = 0, bool swap = false, bool light = false);
    
    void loadTextureTiles();
    
    bool m_loaded = false;
    Mesh* m_meshes[100];
    std::string m_biome;
};

#endif	/* MAPRESSOURCE_H */

