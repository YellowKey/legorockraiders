/* 
 * File:   glTexture.cpp
 * Author: Kevin
 * 
 * Created on 17. November 2014, 21:02
 */

#include "glTexture.h"
//TODO: add DSA

//map <GLenum, SlotManager> glTexture::s_slotManager;
int   glTexture::s_maxTextureSlots = 0;
GLint glTexture::s_activeSlot = 0;

glTexture::glTexture(GLenum type):m_type(type) {
    glGenTextures(1, &m_handle);
}

glTexture::~glTexture() {
    glDeleteTextures(1, &m_handle);
}

GLint glTexture::bindTexture(GLint id) {
    activeTexture(id);
    glBindTexture(m_type, m_handle);
    
    return id;
    /*int slot = s_slotManager[m_type].reserveSlot(this);
    
    if (slot == SlotManager::SLOT_OLD){
        return m_boundSlot;
    }else if(slot == SlotManager::SLOT_ERROR){
        //TODO: error
        return -1;
    }else{
        m_boundSlot = slot;
        m_bound     = true;
        activeTexture(slot);
        glBindTexture(m_type, m_handle);
        return slot;
    }*/
}

void glTexture::bindCurrent() {
    glBindTexture(m_type, m_handle);
}


/*void glTexture::unbindTexture() {
    s_slotManager[m_type].freeSlot(this);
    
    m_bound = false;
}

bool glTexture::isTextureBound() {
    return m_bound;
}*/

/*GLint glTexture::getTextureSlot() {
    if (m_bound)
        return m_boundSlot;
    return -1;
}*/

GLuint glTexture::getTextureHandle() {
    return m_handle;
}

GLenum glTexture::getGlTextureType() {
    return m_type;
}


void glTexture::textureParameters(GLenum target, GLfloat param) {
    if (glTextureParameterfEXT){
        glTextureParameterfEXT(m_handle, m_type, target, param);
    }else{
        bindCurrent();
        
        glTexParameterf(m_type, target, param);
    }
    
    /*TempBinder temp(this, true);
    
    glTexParameterf(m_type, target, param);*/
}

void glTexture::textureParameters(GLenum target, GLint param) {
    if (glTextureParameteriEXT){
        glTextureParameteriEXT(m_handle, m_type, target, param);
    }else{
        bindCurrent();
        
        glTexParameteri(m_type, target, param);
    }
    /*TempBinder temp(this, true);
    
    glTexParameteri(m_type, target, param);*/
}

void glTexture::genMipmaps(int levels) {
    //TempBinder binder(this, true);
    
    textureParameters(GL_TEXTURE_BASE_LEVEL, 0);
    textureParameters(GL_TEXTURE_MAX_LEVEL,  levels);
    
    if (glGenerateTextureMipmapEXT){
        glGenerateTextureMipmapEXT(m_handle, m_type);
    }else{
        bindCurrent();
        glGenerateMipmap(m_type);
    }
    //glGenerateMipmap(m_type);
}

bool glTexture::isImmutable() {
    return  m_immutable;
}

void glTexture::markImmutable() {
    m_immutable = true;
}

void glTexture::activeTexture(int id) {
    if (id != s_activeSlot){
        glActiveTexture(GL_TEXTURE0 + id);
        s_activeSlot = id;
    }
}


/*glTexture::TempBinder::TempBinder(glTexture* texture, bool active):m_texture(texture) {
    if (texture->isTextureBound()){
        m_wasBound = true;
        
        int slot = texture->getTextureSlot();
    
        if (active && s_activeSlot != slot){
            activeTexture(slot);
        }
    }else{
        texture->bindTexture();
        m_wasBound = false;
    }
}

glTexture::TempBinder::~TempBinder() {
    if (!m_wasBound){
        m_texture->unbindTexture();
    }
}*/


void glTexture::texStorage1D(GLint levels, GLint internalFormat, GLsizei width) {
    markImmutable();
    setMemory(width * getFormatSize(internalFormat));
    
    if (glTextureStorage1DEXT){
        glTextureStorage1DEXT(m_handle, m_type, levels, internalFormat, width);
    }else{
        bindCurrent();
        glTexStorage1D(m_type, levels, internalFormat, width);
    }
    
    /*TempBinder binder(this, true);
    
    glTexStorage1D(m_type, levels, internalFormat, width);*/
}

void glTexture::texStorage2D(GLint levels, GLint internalFormat, GLsizei width, GLsizei height) {
    markImmutable();
    setMemory(width * height * getFormatSize(internalFormat));
    
    if (glTextureStorage2DEXT){
        glTextureStorage2DEXT(m_handle, m_type, levels, internalFormat, width, height);
    }else{
        bindCurrent();
        glTexStorage2D(m_type, levels, internalFormat, width, height);
    }
    
    /*TempBinder binder(this, true);
    
    glTexStorage2D(m_type, levels, internalFormat, width, height);*/
}

void glTexture::texStorage3D(GLint levels, GLint internalFormat, GLsizei width, GLsizei height, GLsizei depth) {
    markImmutable();
    setMemory(width * height * depth * getFormatSize(internalFormat));
    
    if (glTextureStorage3DEXT){
        glTextureStorage3DEXT(m_handle, m_type, levels, internalFormat, width, height, depth);
    }else{
        bindCurrent();
        glTexStorage3D(m_type, levels, internalFormat, width, height, depth);
    }
    
    /*TempBinder binder(this, true);
    
    glTexStorage3D(m_type, levels, internalFormat, width, height, depth);*/
}


void glTexture::texImage1D(GLint level, GLint internalFormat, GLsizei width, GLenum format, GLenum type, const void* data) {
    setMemory(width * getFormatSize(internalFormat));
    
    if (glTextureImage1DEXT){
        glTextureImage1DEXT(m_handle, m_type, level, internalFormat, width, 0, format, type, data);
    }else{
        bindCurrent();
        glTexImage1D(m_type, level, internalFormat, width, 0, format, type, data);
    }
    
    /*TempBinder binder(this, true);
    
    glTexImage1D(m_type, level, internalFormat, width, 0, format, type, data);*/
}

void glTexture::texImage2D(GLint level, GLint internalFormat, GLsizei width, GLsizei height, GLenum format, GLenum type, const void* data) {
    setMemory(width * height * getFormatSize(internalFormat));
    
    if (glTextureImage2DEXT){
        glTextureImage2DEXT(m_handle, m_type, level, internalFormat, width, height, 0, format, type, data);
    }else{
        bindCurrent();
        glTexImage2D(m_type, level, internalFormat, width, height, 0, format, type, data);
    }
}

void glTexture::texImage3D(GLint level, GLint internalFormat, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLenum type, const void* data) {
    setMemory(width * height * depth * getFormatSize(internalFormat));
    
    if (glTextureImage3DEXT){
        glTextureImage3DEXT(m_handle, m_type, level, internalFormat, width, height, depth, 0, format, type, data);
    }else{
        bindCurrent();
        glTexImage3D(m_type, level, internalFormat, width, height, depth, 0, format, type, data);
    }
    
    /*TempBinder binder(this, true);
    
    glTexImage3D(m_type, level, internalFormat, width, height, depth, 0, format, type, data);*/
}


void glTexture::texSubImage1D(GLint level, GLint xOffset, GLsizei width, GLenum format, GLenum type, const GLvoid* data) {
    
    if (glTextureSubImage1DEXT){
        glTextureSubImage1DEXT(m_handle, m_type, level, xOffset, width, format, type, data);
    }else{
        bindCurrent();
        glTexSubImage1D(m_type, level, xOffset, width, format, type, data);
    }
    
    /*TempBinder binder(this, true);
    
    glTexSubImage1D(m_type, level, xOffset, width, format, type, data);*/
}

void glTexture::texSubImage2D(GLint level, GLint xOffset, GLint yOffset, GLsizei width, GLsizei height, GLenum format, GLenum type, const GLvoid* data) {
    if (glTextureSubImage2DEXT){
        glTextureSubImage2DEXT(m_handle, m_type, level, xOffset, yOffset, width, height, format, type, data);
    }else{
        bindCurrent();
        glTexSubImage2D(m_type, level, xOffset, yOffset, width, height, format, type, data);
    }
    
    /*TempBinder binder(this, true);
    
    glTexSubImage2D(m_type, level, xOffset, yOffset, width, height, format, type, data);*/
}

void glTexture::texSubImage3D(GLint level, GLint xOffset, GLint yOffset, GLint zOffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLenum type, const GLvoid* data) {
    if (glTextureSubImage3DEXT){
        glTextureSubImage3DEXT(m_handle, m_type, level, xOffset, yOffset, zOffset, width, height, depth, format, type, data);
    }else{
        bindCurrent();
        glTexSubImage3D(m_type, level, xOffset, yOffset, zOffset, width, height, depth, format, type, data);
    }
    
    /*TempBinder binder(this, true);
    
    glTexSubImage3D(m_type, level, xOffset, yOffset, zOffset, width, height, depth, format, type, data);*/
}

glObject::Type glTexture::getObjectType() {
    return glObject::Type::TYPE_TEXTURE;
}

int glTexture::getFormatSize(GLint format) {
    switch(format){
        case GL_R8: return 1;
        case GL_R8_SNORM: return 1;
        case GL_R16: return 2;
        case GL_R16_SNORM: return 2;
        
        case GL_RG8: return 2;
        case GL_RG8_SNORM: return 2;
        case GL_RG16: return 4;
        case GL_RG16_SNORM: return 4;
            
        case GL_RGB8: return 3;
        case GL_RGB8_SNORM: return 3;
        case GL_RGB16: return 6;
        case GL_RGB16_SNORM: return 6;
        
        case GL_RGBA8: return 4;
        case GL_RGBA8_SNORM: return 4;
        case GL_RGBA16: return 8;
        case GL_RGBA16_SNORM: return 8;
    }
    
    return 0;
}










