/* 
 * File:   Action.cpp
 * Author: kevin
 * 
 * Created on July 9, 2015, 1:29 PM
 */

#include "Action.h"

Action::Action(){
}

Action::~Action() {
    if (getTask()){
        getTask()->unitCanceled();
    }
}

void Action::start() {

}

void Action::updateUnit() {

}

void Action::cancel() {
    
}

void Action::setUnit(Unit* unit) {
    m_unit = unit;
}

Unit* Action::getUnit() {
    return m_unit;
}



void Action::finish() {
    m_finished = true;
    
    if (getTask()){
        getTask()->unitFinished();
    }
}

bool Action::isFinished() {
    return m_finished;
}

void Action::markLocked() {
    m_locked = true;
}

bool Action::isLocked() {
    return m_locked;
}

void Action::setTask(Task* task) {
    m_task = task;
}

Task* Action::getTask() {
    return m_task;
}


