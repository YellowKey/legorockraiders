/* 
 * File:   glVertexArray.cpp
 * Author: Kevin
 * 
 * Created on 22. November 2014, 13:45
 */

#include "glVertexArray.h"

#include "glBuffer.h"

glVertexArray* glVertexArray::s_boundVertexArray = nullptr;

glVertexArray::glVertexArray() {
    glGenVertexArrays(1, &m_handle);
}

glVertexArray::~glVertexArray() {
    glDeleteVertexArrays(1, &m_handle);
}

void glVertexArray::bind() {
    if (s_boundVertexArray != this){
        if (s_boundVertexArray){
            getBoundVertexArray()->onUnbind();
        }
        
        glBindVertexArray(m_handle);
        s_boundVertexArray = this;
        
        onBind();
    }
}

glVertexArray* glVertexArray::getBoundVertexArray() {
    return s_boundVertexArray;
}

void glVertexArray::bindVertexBuffer(GLuint bindingIndex, glBuffer* buffer, void* offset, GLintptr stride) {
    bind();
    glBindVertexBuffer(bindingIndex, buffer->getHandle(), (GLintptr)offset, stride);
}

void glVertexArray::vertexBindingDivisor(GLuint bindingIndex, GLuint divisor) {
    bind();
    glVertexBindingDivisor(bindingIndex, divisor);
}

void glVertexArray::vertexAttribBinding(GLuint attribIndex, GLuint bindingIndex) {
    bind();
    glVertexAttribBinding(attribIndex, bindingIndex);
}

void glVertexArray::enableVertexAttribArray(GLuint index) {
    bind();
    glEnableVertexAttribArray(index);
}

void glVertexArray::disableVertexAttribArray(GLuint index) {
    bind();
    glDisableVertexAttribArray(index);
}

void glVertexArray::vertexAttribFormat(GLuint attribIndex, GLint size, GLenum type, GLboolean normalized, void* offset) {
    bind();
    glVertexAttribFormat(attribIndex, size, type, normalized, (GLuint)((intptr_t)offset));
}

void glVertexArray::vertexAttribIFormat(GLuint attribIndex, GLint size, GLenum type, void* offset) {
    bind();
    glVertexAttribIFormat(attribIndex, size, type, (GLuint)((intptr_t)offset));
}

void glVertexArray::vertexAttribLFormat(GLuint attribIndex, GLint size, GLenum type, void* offset) {
    bind();
    glVertexAttribLFormat(attribIndex, size, type, (GLuint)((intptr_t)offset));
}


void glVertexArray::onBind() {
    m_elementArrayBuffer = glBuffer::getBoundBuffer(GL_ELEMENT_ARRAY_BUFFER);
}

void glVertexArray::onUnbind() {
    glBuffer::setBoundBuffer(GL_ELEMENT_ARRAY_BUFFER, m_elementArrayBuffer);
}
