/* 
 * File:   WidgetText.cpp
 * Author: Kevin
 * 
 * Created on 21. Juni 2015, 16:09
 */

#include "WidgetText.h"

WidgetText::WidgetText() {
}

WidgetText::~WidgetText() {
}

void WidgetText::setText(const std::string& text) {
    m_text = text;
}

void WidgetText::setFont(Font* font) {
    m_fontMain = font;
}

void WidgetText::setHoverFont(Font* font) {
    m_fontHover = font;
}

void WidgetText::setClickFont(Font* font) {
    m_fontClick = font;
}

Font* WidgetText::getCurrentFont() {
    //TODO: return fontHover and fontClick
    
    return m_fontMain;
}

void WidgetText::setMode(Mode mode) {
    m_mode = mode;
}

#include <iostream>

void WidgetText::onPaint(Renderer2D* renderer) {
    float offset = 0;
    Renderer2D::Align align;
    
    switch(m_mode){
        case MODE_LEFT:
            offset = 0;
            align  = Renderer2D::LEFT;
            break;
        case MODE_CENTER:
            offset = getSize().x / 2;
            align  = Renderer2D::CENTER;
            break;
        case MODE_RIGHT:
            offset = getSize().x;
            align  = Renderer2D::RIGHT;
            break;
    }
    
    renderer->setFont(getCurrentFont());
    renderer->setFontScale(getScale());
    renderer->renderText(m_text, getAbsPos() + glm::vec2(offset, 0), align);
}





