/* 
 * File:   ObjectInfo.h
 * Author: Kevin
 *
 * Created on 8. September 2015, 12:08
 */

#ifndef MYOBJECTINFO_H
#define	MYOBJECTINFO_H

#include <glm/vec2.hpp>

#include "ObjectInfo.h"
#include "ObjectInfoData.h"
#include "Activity.h"
#include "Mesh.h"
#include "Sample.h"
#include "AnimInfo.h"

#include <vector>
#include <bitset>

class ObjectInfo {
public:
    enum TileType{
        TILE_EMPTY,
        TILE_BUILDING,
        TILE_POWER,
        TILE_WATER,
    };
    
    enum ObjType{
        TYPE_BUILDING,
        TYPE_MONSTER,
        TYPE_FIGURE,
        TYPE_VEHICLE,
    };
    
    struct ActivityNew{
        std::string name;
        Mesh*   mesh;
        float   transCoef;
        Sample* sample; //TODO implement audio
        int     trigger;
    };
    
    struct Upgrade{
        Mesh* mesh;
        std::string null;
        int slot;
        std::bitset<4> upgrades;
    };
    
    ObjectInfo();
    ObjectInfo(const ObjectInfo& orig) = delete;
    virtual ~ObjectInfo();
    
    ObjType type;
    
    //movement
    ObjectInfoData<float> routeSpeed;
    ObjectInfoData<float> rubbleCoef;
    ObjectInfoData<float> pathCoef;
    ObjectInfoData<bool> routeAvoidance;
    ObjectInfoData<bool> crossLand;
    ObjectInfoData<bool> crossWater;
    ObjectInfoData<bool> crossLava;
    ObjectInfoData<bool> randomMove;
    ObjectInfoData<bool> randomEnterWall;
    
    //drill Speed
    ObjectInfoData<float> soilDrillTime;
    ObjectInfoData<float> looseDrillTime;
    ObjectInfoData<float> medDrillTime;
    ObjectInfoData<float> hardDrillTime;
    ObjectInfoData<float> seamDrillTime;
    
    //Upgrade
    ObjectInfoData<float> upgradeTime;
    
    //Size
    ObjectInfoData<float> collRadius;
    ObjectInfoData<float> collHeight;
    ObjectInfoData<float> pickSphere;
    ObjectInfoData<float> alertRadius;
    
    //View
    ObjectInfoData<float> trackDistance;
    ObjectInfoData<float> surveyRadius;
    ObjectInfoData<float> AwarenessRange;
    ObjectInfoData<float> wakeRadius;
    
    //Energy
    ObjectInfoData<float> restPercent;
    ObjectInfoData<float> energyDecayRate;
    ObjectInfoData<float> oxygenCoef;
    ObjectInfoData<float> HealthDecayRate;
    ObjectInfoData<float> carryMinHelath;
    ObjectInfoData<float> PainThreshold;
    
    //Skills
    ObjectInfoData<bool> canClearRubble;
    ObjectInfoData<bool> canScare;
    ObjectInfoData<bool> useLegoManTeleporter;
    ObjectInfoData<bool> canStrafe;
    ObjectInfoData<bool> enterToolStore;
    ObjectInfoData<bool> showHealthBar;
    ObjectInfoData<bool> canSteal;
    ObjectInfoData<bool> canGraphMinifigure;
    ObjectInfoData<bool> bumpDamage;
    ObjectInfoData<bool> splitOnZeroHealth;
    ObjectInfoData<bool> scareByBigBangs;
    ObjectInfoData<bool> removeReinforcement;
    ObjectInfoData<bool> causeSlip;
    ObjectInfoData<bool> showDamage;
    ObjectInfoData<bool> showOnRadar;
    ObjectInfoData<int>  numTools;
    ObjectInfoData<int>  maxCarry;
    
    //Attack
    ObjectInfoData<float> stampRadius;
    ObjectInfoData<float> attackRadius;
    ObjectInfoData<bool> attackPaths;
    
    //Damage
    ObjectInfoData<bool> canBeHitByFence;
    ObjectInfoData<bool> canBeShotAt;
    ObjectInfoData<bool> canFreeze;
    ObjectInfoData<float> freezerTime;
    ObjectInfoData<float> freezerDamage;
    ObjectInfoData<bool> canLaser;
    ObjectInfoData<float> laserDamage;
    ObjectInfoData<bool> canPush;
    ObjectInfoData<float> pusherDistance;
    ObjectInfoData<float> pushserDamage;
    
    ObjectInfoData<bool> signleWidthDig;
    ObjectInfoData<float> RepairValue;
    ObjectInfoData<int> capacity;
    ObjectInfoData<float> functionCoef;
    
    Activity* activity;
    
    //Nulls
    std::string wheelNull;
    std::string itemNull;
    std::string driverNull;
    std::string depositNull;
    std::string cameraNull;
    std::string carryNull;
    std::string drillNull;
    std::string toolNull;
    
    std::string name;
    std::string nameInternal;
    int id;
    
    bool isBuilding();
    
    int getNumTiles();
    TileType getTileType(int id);
    glm::ivec2 getTilePos(int id);
    void addTile(const glm::ivec2& pos, TileType type);
    
    void addActivity(ActivityNew* activity);
    ActivityNew* getActivity(int id);
    ActivityNew* getActivity(const std::string& name);
    int countActivity();
    
    void addUpgrade(Upgrade* upgrade);
    Upgrade* getUpgrade(std::bitset<4> upgrades, int id);
    int countUpgrades(std::bitset<4> upgrades);
    
    int getNumAnimInfo();
    void addAnimInfo(AnimInfo* info);
    AnimInfo* getAnimInfo(int id);
    
    //Sounds
    //drillSound
    //drillSoundFadeout
private:
    struct Tile{
        glm::ivec2 pos;
        TileType type;
    };
    
    int m_levels = 0;
    
    std::vector<Tile> m_tiles;
    std::vector<ActivityNew*> m_activities;
    std::vector<Upgrade*> m_upgrades;
    std::vector<AnimInfo*> m_animInfos;
};

#endif	/* MYOBJECTINFO_H */

