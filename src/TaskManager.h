/* 
 * File:   TaskManager.h
 * Author: Kevin
 *
 * Created on 19. August 2015, 13:24
 */

#ifndef TASKMANAGER_H
#define	TASKMANAGER_H

#include "Unit.h"
#include "Task.h"

#include <vector>
#include <list>

class TaskManager {
public:
    TaskManager();
    TaskManager(const TaskManager& orig) = delete;
    virtual ~TaskManager();
    
    void findTaskFor(Unit* unit);
    bool addTask(Task* task);
    void removeTask(Task* task);
    
    Task::TaskType getTaskTypeByPriority(uint32_t prio);
    uint32_t getTaskTypPrio(Task::TaskType type);
    void setTaskTypePriority(Task::TaskType task, uint32_t prio);
private:
    std::list<Task*> m_tasks;
    std::vector<Task::TaskType> m_taskPrio;
};

#endif	/* TASKMANAGER_H */

