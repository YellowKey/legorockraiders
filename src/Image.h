/* 
 * File:   Image.h
 * Author: Kevin
 *
 * Created on 12. Juni 2015, 15:55
 */

#ifndef IMAGE_H
#define	IMAGE_H

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

class Image {
public:
    #pragma pack(1)
    class Color {
    public:
        Color();
        Color(uint8_t red, uint8_t green, uint8_t blue);
        Color(uint8_t red, uint8_t green, uint8_t blue, uint8_t alpha);
        ~Color() = default;
        
        inline uint8_t getRed() const{
            return m_red;
        }
        inline uint8_t getBlue() const{
            return m_blue;
        }
        inline uint8_t getGreen() const{
            return m_green;
        }
        inline uint8_t getAlpha() const{
            return m_alpha;
        }
        
        inline void setRed(uint8_t red){
            m_red = red;
        }
        inline void setBlue(uint8_t blue){
            m_blue = blue;
        }
        inline void setGreen(uint8_t green){
            m_green = green;
        }
        inline void setAlpha(uint8_t alpha){
            m_alpha = alpha;
        }
        
        inline void setRGB(uint8_t red, uint8_t green, uint8_t blue){
            setRed(red);
            setGreen(green);
            setBlue(blue);
        }
        inline void setRGBA(uint8_t red, uint8_t green, uint8_t blue, uint8_t alpha){
            setRed(red);
            setGreen(green);
            setBlue(blue);
            setAlpha(alpha);
        }
        
        glm::vec3 toVec3() const;
        glm::vec4 toVec4() const;
        
    private:
        uint8_t m_red;
        uint8_t m_green;
        uint8_t m_blue;
        uint8_t m_alpha;
    };
    
    enum ColorIndex{
        COLOR_RED   = 0,
        COLOR_GREEN = 1,
        COLOR_BLUE  = 2,
        COLOR_ALPHA = 3,
    };
    
    Image();
    Image(int pixelBits, int rowAlign = 0);
    Image(int width, int height, int pixelBits, int rowAlign = 0);
    Image(const Image& orig);
    Image(Image&& orig);
    virtual ~Image();
    
    Image& operator= (const Image& img);
    Image& operator= (Image&& img);
    
    void import(const Image& image);
    void swapBlueRed(); //used to convert from BGR to RGB
    
    void setColor(int x, int y, Color color);
    void setColor(const glm::ivec2& pos, Color color);
    Color getColor(int x, int y) const;
    Color getColor(const glm::ivec2& pos) const;
    
    int getWidth() const;
    int getHeight() const;
    int getPixelBits() const;
    int getPixelBytes() const;
    int getRowAlign() const;
    int getRowBytes() const;
    
    void setColor(int id, Color color);
    Color getColor(int id) const;
    
    bool hasPalett() const;
    int  getPalettSize() const;
    int  getNumColors() const;
    
    int getDataSize() const;
    int getColorTableSize() const;
    
    void* getData();
    void* getColorTable();
private:
    uint8_t* m_data = nullptr;
    Color*   m_colorTable = nullptr;
    
    int m_width  = 0;
    int m_height = 0;
    int m_pixelBits = 0;
    int m_rowAlign = 0;
    
    void deleteData();
    void newBuffers();
    
    int getColorIndex(int x, int y) const;
    bool equalFormat(const Image& image) const;
    inline uint8_t* pixel(int x, int y) const{
        return m_data + x * getPixelBytes() + y * getRowBytes();
    }
};

#endif	/* IMAGE_H */

