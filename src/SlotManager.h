/* 
 * File:   SlotManager.h
 * Author: Kevin
 *
 * Created on 17. November 2014, 21:03
 */

#ifndef SLOTMANAGER_H
#define	SLOTMANAGER_H

#include <vector>
#include <climits>

using std::vector;

class SlotManager {
public:
    enum SlotState{
        SLOT_OLD = -1,
        SLOT_ERROR = -2,
    };
    
    SlotManager() = default;
    SlotManager(const SlotManager& orig) = delete;
    virtual ~SlotManager() = default;
    
    int  reserveSlot(void* obj);
    void freeSlot(void* obj);
    int  getSlot(void* obj);
    void clearSlot(int id);
    
    //int reserveSlots(void* obj[], int num);
    //void freeSlots(void* obj[], int num);
    
    bool isTrueBound(void* obj);
    
    void setMaxSlots(int id);
    int  getMaxSlots();
    
    int  getUsedSlot();
private:
    int m_maxSlots = INT_MAX;
    
    struct Slot{
        void* obj;
        bool  trueBound;
    };
    
    vector <Slot> m_slots;
};

#endif	/* SLOTMANAGER_H */

