/* 
 * File:   AnimInfo.cpp
 * Author: Kevin
 * 
 * Created on 16. Oktober 2015, 15:26
 */

#include "AnimInfo.h"

#include "util.h"

AnimInfo::AnimInfo() {
}

AnimInfo::AnimInfo(const AnimInfo& orig) {
}

AnimInfo::~AnimInfo() {
}


void AnimInfo::addActivity(Activity* activity) {
    m_activities.push_back(activity);
}

AnimInfo::Activity* AnimInfo::getDefaultActivity() {
    if (m_activities.size() == 0){
        return nullptr;
    }
    
    return m_activities[0]; //TODO: impement
}

AnimInfo::Activity* AnimInfo::getActivity(const std::string& name, bool fallback) {
    Activity* activity = nullptr;
    
    for (int a = 0; a < m_activities.size(); a++){
        if (lowerEqual(m_activities[a]->name, name)){
            activity = m_activities[a];
            break;
        }
    }
    
    if (activity == nullptr && fallback){
        activity = getDefaultActivity();
    }
    
    return activity;
}


int AnimInfo::getNumTiles() {
    return m_tiles.size();
}

glm::ivec2 AnimInfo::getTilePos(int id) {
    return m_tiles[id].pos;
}

AnimInfo::TileType AnimInfo::getTileType(int id) {
    return m_tiles[id].type;
}

void AnimInfo::addTile(const glm::ivec2& pos, TileType type) {
    m_tiles.push_back(Tile{pos, type});
}


void AnimInfo::addUpgrade(Upgrade* upgrade) {
    m_upgrades.push_back(upgrade);
}

int AnimInfo::countUpgrades(std::bitset<4> upgrades) {
    int count = 0;
    
    for (auto up: m_upgrades){
        if (up->upgrades == upgrades){
            count++;
        }
    }
    
    return count;
}

AnimInfo::Upgrade* AnimInfo::getUpgrade(std::bitset<4> upgrades, int id) {
    for (auto up: m_upgrades){
        if (up->upgrades == upgrades){
            if (id-- <= 0){
                return up;
            }
        }
    }
    
    return nullptr;
}




