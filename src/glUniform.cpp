/* 
 * File:   glUniform.cpp
 * Author: Kevin
 * 
 * Created on 22. November 2014, 13:52
 */

#include "glUniform.h"
#include "glProgram.h"


glUniform::glUniform(){
}

glUniform::glUniform(glProgram* program, GLuint id):m_program(program),m_handle(id){
}

glUniform glUniform::operator=(glUniform handle){
    m_program = handle.m_program;
    m_handle  = handle.m_handle;
    
    return handle;
}


//-------------------------------------------------------------
//                      Basic variable
//-------------------------------------------------------------
GLfloat glUniform::operator =(GLfloat param) {
    glProgramUniform1f(m_program->getHandle(), m_handle, param);
    
    return param;
}

GLint glUniform::operator =(GLint param) {
    glProgramUniform1i(m_program->getHandle(), m_handle, param);
    
    return param;
}

GLuint glUniform::operator =(GLuint param) {
    glProgramUniform1ui(m_program->getHandle(), m_handle, param);
    
    return param;
}

uint64_t glUniform::operator =(uint64_t param) {
    glProgramUniformHandleui64ARB(m_program->getHandle(), m_handle, param);
    
    return param;
}


//-------------------------------------------------------------
//                      float vectors
//-------------------------------------------------------------

glm::vec2 glUniform::operator=(const glm::vec2& param){
    glProgramUniform2fv(m_program->getHandle(), m_handle, 1, &param[0]);
    
    return param;
}

glm::vec3 glUniform::operator=(const glm::vec3& param){
    glProgramUniform3fv(m_program->getHandle(), m_handle, 1, &param[0]);
    
    return param;
}

glm::vec4 glUniform::operator=(const glm::vec4& param){
    glProgramUniform4fv(m_program->getHandle(), m_handle, 1, &param[0]);
    
    return param;
}

//-------------------------------------------------------------
//                        int vectors
//-------------------------------------------------------------
glm::ivec2 glUniform::operator=(const glm::ivec2& param){
    glProgramUniform2iv(m_program->getHandle(), m_handle, 1, &param[0]);
    
    return param;
}

glm::ivec3 glUniform::operator=(const glm::ivec3& param){
    glProgramUniform3iv(m_program->getHandle(), m_handle, 1, &param[0]);
    
    return param;
}

glm::ivec4 glUniform::operator=(const glm::ivec4& param){
    glProgramUniform4iv(m_program->getHandle(), m_handle, 1, &param[0]);
    
    return param;
}

//-------------------------------------------------------------
//                  unsinged int vectors
//-------------------------------------------------------------

glm::uvec2 glUniform::operator=(const glm::uvec2& param){
    glProgramUniform2uiv(m_program->getHandle(), m_handle, 1, &param[0]);
    
    return param;
}

glm::uvec3 glUniform::operator=(const glm::uvec3& param){
    glProgramUniform3uiv(m_program->getHandle(), m_handle, 1, &param[0]);
    
    return param;
}

glm::uvec4 glUniform::operator=(const glm::uvec4& param){
    glProgramUniform4uiv(m_program->getHandle(), m_handle, 1, &param[0]);
    
    return param;
}


//-------------------------------------------------------------
//                        Matrices
//-------------------------------------------------------------

glm::mat2 glUniform::operator=(const glm::mat2& param){
    glProgramUniformMatrix2fv(m_program->getHandle(), m_handle, 1, false, &param[0][0]);
    
    return param;
}

glm::mat3 glUniform::operator=(const glm::mat3& param){
    glProgramUniformMatrix3fv(m_program->getHandle(), m_handle, 1, false, &param[0][0]);
    
    return param;
}

glm::mat4 glUniform::operator=(const glm::mat4& param){
    glProgramUniformMatrix4fv(m_program->getHandle(), m_handle, 1, false, &param[0][0]);
    
    return param;
}


glm::mat2x3 glUniform::operator=(const glm::mat2x3& param){
    glProgramUniformMatrix2x3fv(m_program->getHandle(), m_handle, 1, false, &param[0][0]);
    
    return param;
}

glm::mat3x2 glUniform::operator=(const glm::mat3x2& param){
    glProgramUniformMatrix3x2fv(m_program->getHandle(), m_handle, 1, false, &param[0][0]);
    
    return param;
}

glm::mat2x4 glUniform::operator=(const glm::mat2x4& param){
    glProgramUniformMatrix2x4fv(m_program->getHandle(), m_handle, 1, false, &param[0][0]);
    
    return param;
}

glm::mat4x2 glUniform::operator=(const glm::mat4x2& param){
    glProgramUniformMatrix4x2fv(m_program->getHandle(), m_handle, 1, false, &param[0][0]);
    
    return param;
}

glm::mat3x4 glUniform::operator=(const glm::mat3x4& param){
    glProgramUniformMatrix3x4fv(m_program->getHandle(), m_handle, 1, false, &param[0][0]);
    
    return param;
}

glm::mat4x3 glUniform::operator=(const glm::mat4x3& param){
    glProgramUniformMatrix4x3fv(m_program->getHandle(), m_handle, 1, false, &param[0][0]);
    
    return param;
}

