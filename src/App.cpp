/* 
 * File:   App.cpp
 * Author: Kevin
 * 
 * Created on 1. Mai 2015, 12:00
 */

#include "App.h"

#include "LoaderBmp.h"
#include "LoaderLzo.h"
#include "LoaderLws.h"
#include "LoaderCfg.h"
#include "LoaderAe.h"

#include "WidgetImage.h"
#include "WidgetText.h"
#include "WidgetButton.h"
#include "WidgetIconPanel.h"
#include "WidgetRightPanel.h"
#include "WidgetTopPanel.h"
#include "WidgetCameraControl.h"
#include "WidgetMesssagePanel.h"

#include "Ressource.h"
#include "EngineObject.h"
#include "Building.h"
#include "UnitFigure.h"

#include "util.h"

#include "ActionWalk.h"
#include "ActionDrill.h"
#include "ActionClear.h"
#include "ActionCollect.h"
#include "ActionPlace.h"
#include "TaskCollect.h"
#include "UnitVehicle.h"
#include "ObjectAnim.h"

#include "Renderer2D.h"
#include "Engine3D.h"
#include "RendererMap.h"
#include "StateMap.h"
#include "LoaderLevel.h"

#include <iostream>
#include <iomanip>
#include <GL/glext.h>
#include <cmath>
#include <glm/gtc/matrix_transform.hpp>
#include <GL/glew.h>
#include <cstdlib>

const std::string App::NAME = "Lego Rock Raiders";
App* App::s_app = nullptr;

static Activity* testActivi;

App::App() {
    
}

App::~App() {
}

int App::startApp() {
    if (s_app){
        delete s_app;
    }
    
    s_app = new App();
    
    int returnValue = s_app->run();
    
    delete s_app;
    
    s_app = nullptr;
    
    return returnValue;
}

App* App::getApp() {
    return s_app;
}

const std::string& App::getName() {
    return NAME;
}

int App::getWidth() {
    return m_window->getWidth();
}

int App::getHeight() {
    return m_window->getHeight();
}

float App::getTime() {
    return m_clock.getElapsedTime().asSeconds();
}

float App::getGameTime() {
    return m_gameTime;
}


int App::run() {
    m_quit = false;
    m_window = new GameWindow(getName(), 640, 480, false); 
    m_window->makeCurrent();
    
    
    m_messureStartTime = m_clock.getElapsedTime().asSeconds();
    
    init();
    
    while (!m_window->shouldClose() && !m_quit){
        
        glViewport(0, 0, m_window->getWidth(), m_window->getHeight());
        
        mainLoop();
        
        m_window->update();
        m_window->swapBuffers();
        
        m_gameTime = m_clock.getElapsedTime().asSeconds();
        
        const float fac = 0.2;
        const float facFps = 0.1;
        m_messureCurrentFrames += 1;
        float time = m_clock.getElapsedTime().asSeconds() - m_messureStartTime;
        float frameTime = time * 60.0f;
        float fps = 1.0f / time;
        m_messureStartTime += time;
        m_gameSpeed = m_gameSpeed * (1 - fac) + frameTime * (fac);
        m_fps = m_fps * (1 - facFps) + fps * facFps;
        
        /*if (time >= m_messureTime){
            
            m_gameSpeed = time * 60 / m_messureCurrentFrames;
            m_fps = (float)m_messureCurrentFrames / time;
            
            m_messureCurrentFrames = 0;
            m_messureStartTime = glfwGetTime();
        }*/
    }
    
    cleanup();
    
    delete m_window;
    m_window = nullptr;
    
    return 0;
}

#define TEST_EXTENSION(name) \
    if (GLEW_ ## name){ \
        std::cout << "[X] " << #name << std::endl; \
    }else{ \
        std::cout << "[ ] " << #name << std::endl; \
    }

#define TEST_INT_LIMIT(name) \
    GLint store_ ## name ; \
    glGetIntegerv(name, &store_ ## name); \
    std::cout << std::setw(36) << std::left << #name << " = " << store_ ## name << std::endl;

static void glDebug(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam) {
    std::string typeString;
    switch (type){
        case GL_DEBUG_TYPE_ERROR:
            typeString = "ERROR";
            break;
        case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
            typeString = "DEPRECATED_BEHAVIOR";
            break;
        case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
            typeString = "UNDEFINDED_BEHAVIOR";
            break;
        case GL_DEBUG_TYPE_PERFORMANCE:
            typeString = "PERFORMANCE";
            break;
        case GL_DEBUG_TYPE_PORTABILITY:
            typeString = "PORTABILITY";
            break;
        case GL_DEBUG_TYPE_OTHER: //disabled
            //return;
            typeString = "OTHER";
            break;
        case GL_DEBUG_TYPE_MARKER:
            typeString = "MARKER";
            break;
        case GL_DEBUG_TYPE_PUSH_GROUP:
            typeString = "PUSH_GROUP";
            break;
        case GL_DEBUG_TYPE_POP_GROUP:
            typeString = "POP_GROUP";
            break;
    }
    printf("[OPENGL|%s]: %s\n", typeString.c_str(), message);
    fflush(stdout);
}

void App::init() {
    glewExperimental = true;
    glewInit();
    
    /*if (1){
        glDebugMessageCallback(glDebug, nullptr);
        glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, 0, GL_TRUE);
        glEnable(GL_DEBUG_OUTPUT);
    }*/
    
    m_engine3D = new Engine3D();
    m_renderer2D = new Renderer2D();
    auto ress = new MapRessource("Rock");
    ress->load();
    m_rendererMap = new RendererMap(m_engine3D, ress);
    LoaderLevel loaderLevel;
    m_stateMap = loaderLevel.loadMap("23");
    m_stateMap->updateAll();
    m_rendererMap->setMap(m_stateMap);

    //OpenGL - Tests
    
    std::cout << std::endl;
    std::cout << glGetString(GL_VERSION) << std::endl;
    
    TEST_EXTENSION(ARB_texture_storage);
    TEST_EXTENSION(ARB_vertex_attrib_binding);
    TEST_EXTENSION(ARB_explicit_uniform_location);
    TEST_EXTENSION(ARB_explicit_attrib_location);
    TEST_EXTENSION(ARB_gpu_shader5);
    TEST_EXTENSION(ARB_separate_shader_objects);
    TEST_EXTENSION(ARB_vertex_attrib_binding);
    TEST_EXTENSION(EXT_direct_state_access);
    
    std::cout << std::endl;
    
    TEST_INT_LIMIT(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS);
    TEST_INT_LIMIT(GL_MAX_COMPUTE_TEXTURE_IMAGE_UNITS);
    TEST_INT_LIMIT(GL_MAX_GEOMETRY_TEXTURE_IMAGE_UNITS);
    TEST_INT_LIMIT(GL_MAX_DRAW_BUFFERS);
    TEST_INT_LIMIT(GL_MAX_ARRAY_TEXTURE_LAYERS);
    TEST_INT_LIMIT(GL_MAX_VIEWPORTS);
    
    std::cout << std::endl;
    
    TEST_INT_LIMIT(GL_MAX_VERTEX_ATTRIBS);
    TEST_INT_LIMIT(GL_MAX_VERTEX_OUTPUT_COMPONENTS);
    TEST_INT_LIMIT(GL_MAX_VERTEX_UNIFORM_VECTORS);
    TEST_INT_LIMIT(GL_MAX_VERTEX_UNIFORM_COMPONENTS);
    TEST_INT_LIMIT(GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS);
    
    std::cout << std::endl;
    
    TEST_INT_LIMIT(GL_MAX_FRAGMENT_INPUT_COMPONENTS);
    TEST_INT_LIMIT(GL_MAX_FRAGMENT_UNIFORM_VECTORS);
    TEST_INT_LIMIT(GL_MAX_FRAGMENT_UNIFORM_COMPONENTS);
    TEST_INT_LIMIT(GL_MAX_TEXTURE_IMAGE_UNITS);
    
    std::cout << std::endl;
    
    testActivi = getFileLoader()->loadActivity("Buildings\\ToolStation\\ToolStation.ae");
    
    m_gameMap = new GameMap();
    m_game = new Game();
    
    m_fontBrief3 = m_fileLoader.loadFont("Interface\\Fonts\\MBriefFONT3.bmp");
    
    uint32_t white = UINT32_MAX;
    m_textureDefault = new Texture(&white, 1, 1, Texture::RGBA8);
    
    m_loaderRessources.load();
    
    RessourceContainer::loadAll();
    
    m_gameRessource.load();
    m_interfaceRessources.load();
    
    //m_gameUIController = new GameUIController();
    m_loaderUIController = new LoaderUIController(m_loaderRessources);
    
    std::cout.flush();
}


void App::mainLoop() {
    glClearColor(0,0,0,1);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    
    static int mouseKey[5] = {0, 0, 0, 0, 0};
    
    /*m_renderer2D->setWindowSize(getWidth(), getHeight());
    m_renderer2D->setScreenSize(640, 480);

    glDisable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);
    m_renderer2D->renderTexture(m_textureDDI, glm::vec2(0, 0), glm::vec2(640,480));
    
    std::string names[5] = {"Quit Game", "Credits", "Training Missions", "Load A Saved Game", "Start Game"};
    
    for (int a = 0; a < 5; a++){
        int height = 43;
        
        glm::vec2 pos = glm::vec2(320, 77 + height * a);
        
        int width = m_fontMain->getWidth(names[a]);
        glm::vec2 start = pos - glm::vec2(width / 2, 0);
        glm::vec2 end   = pos + glm::vec2(width / 2, height);
        glm::vec2 mouse = getCursor();
        
        if (mouse.x > start.x && mouse.x < end.x && mouse.y > start.y && mouse.y < end.y){
            m_renderer2D->setFont(m_fontMainH);
            if (m_window->getMouseButton(0) && a == 0){
                quit();
            }
        }else{
            m_renderer2D->setFont(m_fontMain);
        }
        
        m_renderer2D->renderText(names[a], pos, Renderer2D::CENTER);
    }
    
    m_renderer2D->flush();
    
    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);*/
    
    m_renderer2D->setWindowSize(getWidth(), getHeight());
    m_renderer2D->setScreenSize(getWidth(), getHeight());
        
    if (m_loaderUIController){
        m_loaderUIController->scale(glm::vec2(getWidth(), getHeight()));
        m_loaderUIController->update();
        m_loaderUIController->render(m_renderer2D);
        
        if (m_loaderUIController->isDone()){
            delete m_loaderUIController;
            m_loaderUIController = nullptr;
            
            m_gameRessource.postLoad();
            
            m_gameUIController = new GameUIController();
            
            m_gameMap->loadGameLevel(24);
            
            m_game->setGameMap(m_gameMap);
            m_game->createTestObjects();
            
            std::cout << "EngineBuffer: " << 100 * m_engine3D->getBufferUsage() << " %" << std::endl;
            std::cout << std::flush;
        }
    }
    
    if (m_gameUIController){
        static ObjectAnim* testAnim = nullptr;
    
        if (!testAnim){
            testAnim = new ObjectAnim();
            auto info = m_game->getObjectInfo(Game::OBJ_CHROMECRUSHER);
            testAnim->setAnimInfo(info->getAnimInfo(0));
            testAnim->setAnim("Drill");
            testAnim->playAnimation(true);
        }

        for (int a = 0; a < 5; a++){
            m_gameUIController->GetScreenWidget()->sendMouseMove(getCursor());
            if (m_window->getMouseButton(a) && mouseKey[a] == 0){
                mouseKey[a] = 1;
                m_gameUIController->GetScreenWidget()->sendMouseDown(a);
            }else if (!m_window->getMouseButton(a) && mouseKey[a] == 1){
                mouseKey[a] = 0;
                m_gameUIController->GetScreenWidget()->sendMouseUp(a);
            }
        }

        static bool doRot = false;
        static glm::vec2 startPos;
        static glm::vec3 startRot;

        if (!doRot && m_window->getMouseButton(1)){
            startPos = getCursor();
            startRot = m_engine3D->getCameraRot();

            doRot = true;
        }else if (doRot && m_window->getMouseButton(1)){
            glm::vec2 diff = getCursor() - startPos;

            float factor = 0.01f;

            //m_engine3D->setCameraRot(startRot + glm::vec3(diff.y * factor, -diff.x * factor, 0));
        }else{
            doRot = false;
        }

        glm::vec3 move(0);
        float speed = 0.8f * getGameSpeed();
        glm::vec3 cameraRot = m_engine3D->getCameraRot();

        float r = cameraRot.y;
        glm::mat3 rotMat = glm::mat3(
                cos(r) , 0, sin(r),
                0      , 1, 0     ,
                -sin(r), 0, cos(r));
        if (m_window->getKey(sf::Keyboard::LShift)){
            speed *= 5;
        }
        if (m_window->getKey(sf::Keyboard::LControl)){
            speed /= 5;
        }
        if (m_window->getKey(sf::Keyboard::W)){
            move += glm::vec3(0, 0, -speed) * rotMat;
        }
        if (m_window->getKey(sf::Keyboard::S)){
            move += glm::vec3(0, 0, speed) * rotMat;
        }
        if (m_window->getKey(sf::Keyboard::A)){
            move += glm::vec3(-speed, 0, 0) * rotMat;
        }
        if (m_window->getKey(sf::Keyboard::D)){
            move += glm::vec3(speed, 0, 0) * rotMat;
        }
        if (m_window->getKey(sf::Keyboard::Q)){
            move += glm::vec3(0, -speed, 0);
        }
        if (m_window->getKey(sf::Keyboard::E)){
            move += glm::vec3(0, speed, 0);
        }

        m_engine3D->setCameraPos(m_engine3D->getCameraPos() + move);

        //gameMap.render();
        m_gameMap->updateObjects();

        /*glm::vec3 cursor = glm::vec3(m_window->getCursorPos(), 0.98f);
        glm::vec4 viewport = glm::vec4(0,0, getWidth(), getHeight());

        glm::vec3 pos = glm::unProject(cursor, glm::mat4(), m_engine3D->getCameraMatrix(), viewport);*/

        glm::vec3 normal = m_gameUIController->getGameWidget()->getPickRayDir(m_window->getCursorPos());
        glm::vec3 cPos   = m_engine3D->getCameraPos();

        
        glm::vec3 tempPos = glm::vec3(0, 0, 0);
        m_rendererMap->rayTest(cPos, normal, tempPos);
        
        glm::vec3 pos = tempPos;

        int x = (pos.x + 20) / 40;
        int y = (pos.z + 20) / 40;

        if (m_window->getKey(sf::Keyboard::V)){
            if (m_stateMap->getTileType(x, y) != m_stateMap->TILE_SOLID){
                m_stateMap->destroy(x, y);
            }
            /*if (m_gameMap->getTileType(x, y) != m_gameMap->TILE_SOLID){
                m_gameMap->destroy(x, y);
            }*/
        }

        if(m_window->getKey(sf::Keyboard::R)){
            m_gameMap->setTileType(x, y, GameMap::TILE_PATH);
            m_gameMap->update(x, y);
        }

        static int key_t = 0;
        if (key_t == 0 && m_window->getKey(sf::Keyboard::T)){
            key_t = 1;

            glm::vec2 posBuild = m_gameMap->tileToPos(glm::ivec2(x, y));

            Building* building = new Building(m_gameRessource.toolstation, glm::vec3(posBuild.x, 0, posBuild.y));
            building->setState(Building::STATE_TELEPORT);

            m_gameMap->addObject(building);

            m_gameMap->setTileType(x, y, GameMap::TILE_BUILD);
            m_gameMap->setTileType(x, y - 1, GameMap::TILE_BUILDWALK);
            m_gameMap->update(x, y);
            m_gameMap->update(x, y - 1);

            m_game->setRessDest(GameMap::tileToPos(glm::ivec2(x, y-1)));
        }else if (!m_window->getKey(sf::Keyboard::T)){
            key_t = 0;
        }

        static int key_c = 0;
        if (key_c == 0 && m_window->getKey(sf::Keyboard::C)){
            key_c = 1;

            UnitFigure* fig = new UnitFigure();
            //Ressource* ress = new Ressource(Ressource::TYPE_ORE);

            fig->setPosAndStay(glm::vec2(pos.x, pos.z));
            //fig->addAction(new ActionCollect(ress));

            m_gameMap->addObject(fig);
        }else if (!m_window->getKey(sf::Keyboard::C)){
            key_c = 0;
        }

        static int key_1 = 0;
        if (key_1 == 0 && m_window->getKey(sf::Keyboard::Num1)){
            key_1 = 1;

            UnitVehicle* veh = new UnitVehicle(m_game->getObjectInfo(Game::OBJ_SMALLTRANSPORTER));

            veh->setPosAndStay(glm::vec2(pos.x, pos.z));

            m_gameMap->addObject(veh);
        }else if(!m_window->getKey(sf::Keyboard::Num1)){
            key_1 = 0;
        }
        
        static int key_2 = 0;
        if (key_2 == 0 && m_window->getKey(sf::Keyboard::Num2)){
            key_2 = 1;

            UnitVehicle* veh = new UnitVehicle(m_game->getObjectInfo(Game::OBJ_HOVERSCOUT));

            veh->setPosAndStay(glm::vec2(pos.x, pos.z));

            m_gameMap->addObject(veh);
        }else if(!m_window->getKey(sf::Keyboard::Num2)){
            key_2 = 0;
        }
        
        static int key_3 = 0;
        if (key_3 == 0 && m_window->getKey(sf::Keyboard::Num3)){
            key_3 = 1;

            UnitVehicle* veh = new UnitVehicle(m_game->getObjectInfo(Game::OBJ_CHROMECRUSHER));

            veh->setPosAndStay(glm::vec2(pos.x, pos.z));

            m_gameMap->addObject(veh);
        }else if(!m_window->getKey(sf::Keyboard::Num3)){
            key_3 = 0;
        }
        
        static int key_4 = 0;
        if (key_4 == 0 && m_window->getKey(sf::Keyboard::Num4)){
            key_4 = 1;

            UnitVehicle* veh = new UnitVehicle(m_game->getObjectInfo(Game::OBJ_SMALLDRILL));

            veh->setPosAndStay(glm::vec2(pos.x, pos.z));

            m_gameMap->addObject(veh);
        }else if(!m_window->getKey(sf::Keyboard::Num4)){
            key_4 = 0;
        }

        static int key_f = 0;
        if (key_f == 0 && m_window->getKey(sf::Keyboard::F)){
            key_f = 1;

            Ressource* ress = new Ressource(Ressource::TYPE_CRYSTAL);

            ress->setPos(pos);

            //m_game->getTaskManager()->addTask(new TaskCollect(ress));
        }else if (!m_window->getKey(sf::Keyboard::F)){
            key_f = 0;
        }

        static int key_y = 0;
        if (key_y == 0 && m_window->getKey(sf::Keyboard::Y)){
            key_y = 1;

            for (int a = 0; a < m_gameMap->getNumObjects(); a++){
                MapObject* obj = m_gameMap->getObject(a);
                Unit* unit = dynamic_cast<Unit*>(obj);

                if (unit){
                    if (unit->isCarring()){
                        unit->addAction(new ActionPlace());
                    }
                }
            }
        }else if(!m_window->getKey(sf::Keyboard::Y)){
            key_y = 0;
        }

        static int key_g = 0;
        if (key_g == 0 && m_window->getKey(sf::Keyboard::G)){
            key_g = 1;

            if (m_gameMap->isTrueWall(x, y) && m_gameMap->getTileType(x, y) != GameMap::TILE_SOLID){
                std::array<glm::ivec2, 4> offsets = {{
                    {glm::ivec2(0,1)}, 
                    {glm::ivec2(0, -1)}, 
                    {glm::ivec2(1, 0)}, 
                    {glm::ivec2(-1, 0)}}};

                glm::ivec2 pos = glm::ivec2(x, y);

                for (auto off: offsets){
                    glm::ivec2 p = pos + off;
                    if (!m_gameMap->isWall(p.x, p.y)){
                        for (int a = 0; a < m_gameMap->getNumObjects(); a++){
                            Unit* unit = dynamic_cast<Unit*>(m_gameMap->getObject(a));
                            if (unit && unit->isSelected()){
                                glm::vec2 unitPos = m_gameMap->tileToPos(pos) + glm::vec2(off) * 28.0f;
                                unit->cancelAllActions();
                                unit->addAction(new ActionWalk(unitPos));
                                unit->addAction(new ActionDrill(pos));
                            }
                        }

                        break;
                    }
                }
            }else{
                int count = 0;
                for (int a = 0; a < m_gameMap->getNumObjects(); a++){
                    Unit* unit = dynamic_cast<Unit*>(m_gameMap->getObject(a));

                    if (unit && unit->isSelected()){
                        ++count;
                    }
                }

                int maxOffset = sqrt(count * 100);

                for (int a = 0; a < m_gameMap->getNumObjects(); a++){
                    Unit* unit = dynamic_cast<Unit*>(m_gameMap->getObject(a));

                    if (unit && unit->isSelected()){
                        int offX = rand() % maxOffset - maxOffset / 2;
                        int offY = rand() % maxOffset - maxOffset / 2;

                        if (count == 1){
                            offX = offY = 0;
                        }

                        glm::vec2 goal = glm::vec2(pos.x + offX, pos.z + offY);
                        //unit->walkTo(glm::vec2(pos.x + offX, pos.z + offY));
                        unit->cancelAllActions();
                        unit->addAction(new ActionWalk(goal));

                        glm::ivec2 tilePos = m_gameMap->posToTile(goal);
                        if (m_gameMap->getTileType(tilePos.x, tilePos.y) == GameMap::TileType::TILE_GRAVEL){
                            unit->addAction(new ActionClear(tilePos));
                        }
                    }
                }
            }
        }else if (!m_window->getKey(sf::Keyboard::G)){
            key_g = 0;
        }


        float scale = (float)getHeight() / 480;

        m_game->update();

        m_gameUIController->scale(glm::vec2(getWidth(), getHeight()));
        m_gameUIController->update();
        m_gameUIController->render(m_renderer2D);

        //Debug Information

        //m_renderer2D->setWindowSize(getWidth(), getHeight());
        //m_renderer2D->setScreenSize(getWidth(), getHeight());

        m_renderer2D->setFont(m_fontBrief3);

        int h = 12;
        glm::vec2 textPos = glm::vec2(10,getHeight() - 10 - h);

        m_renderer2D->renderText("fps:     " + std::to_string(getFPS()), textPos, Renderer2D::LEFT);
        textPos.y -= h;
        m_renderer2D->renderText("time:   " + std::to_string(1000.0f / getFPS()), textPos, Renderer2D::LEFT);
        textPos.y -= h;
        m_renderer2D->renderText("speed:  " + std::to_string(getGameSpeed()), textPos, Renderer2D::LEFT);
        textPos.y -= h;
        m_renderer2D->renderText("shapes: " + std::to_string(m_engine3D->getNumRenderedShapes()), textPos, Renderer2D::LEFT);
        textPos.y -= h;
        m_renderer2D->renderText("updat:  " + std::to_string(m_engine3D->getNumUpdatedObjects()), textPos, Renderer2D::LEFT);
        textPos.y -= h;
        m_renderer2D->renderText("pos:     " + std::to_string(x) + ", " + std::to_string(y), textPos, Renderer2D::LEFT);
        textPos.y -= h;
        m_renderer2D->renderText("pos:     " + std::to_string(pos.x) + ", " + std::to_string(pos.z), textPos, Renderer2D::LEFT);
        textPos.y -= h;
        m_renderer2D->renderText("type:   " + std::to_string(m_stateMap->getTileType(x, y)), textPos, Renderer2D::LEFT);
        textPos.y -= h;
        m_renderer2D->renderText("visib:  " +std::to_string(m_stateMap->getTileVisib(x, y)), textPos, Renderer2D::LEFT);
    }
    
    m_renderer2D->flush();
}

void App::cleanup() {
    
}

glm::vec2 App::getCursor() {
    glm::vec2 windowSize(m_window->getWidth(), m_window-> getHeight());
    return m_window->getCursorPos();
}

void App::quit() {
    m_quit = true;
}

LoaderWad* App::getDataLoader0() {
    return &m_wadLoader0;
}

LoaderWad* App::getDataLoader1() {
    return &m_wadLoader1;
}

LoaderFile* App::getFileLoader() {
    return &m_fileLoader;
}


GameRessource* App::getGameRessource() {
    return &m_gameRessource;
}

Texture* App::getDefaultTexture() {
    return m_textureDefault;
}

GameMap* App::getGameMap() {
    return m_gameMap;
}

Game* App::getGame() {
    return m_game;
}


/*Widget* App::getScreenWidget() {
    return &m_screenWidget;
}*/

InterfaceRessources* App::getInterfaceRessources() {
    return &m_interfaceRessources;
}





Engine3D* App::getEngine3D() {
    return m_engine3D;
}

float App::getFPS() {
    return m_fps;
}

float App::getGameSpeed() {
    return m_gameSpeed;
}

RendererMap* App::getRendererMap() {
    return m_rendererMap;
}
