/* 
 * File:   Texture.cpp
 * Author: Kevin
 * 
 * Created on 26. April 2015, 15:33
 */

#include "Texture.h"

#include <memory>

Texture::Texture(){
    
}

Texture::Texture(const void* data, int width, int height, Format format){
    load(data, width, height, format);
}


Texture::~Texture() {
    delete m_texture;
}

void Texture::load(const void* data, int width, int height, Format format) {
    m_width = width;
    m_height = height;
    m_texture = createGlTexture(data, width, height, format);
}

bool Texture::isAnimated() const{
    return false;
}

bool Texture::isAntiAliased() const{
    return m_antiAliasing > 0;
}

glTexture* Texture::getGlTexture() const{
    return m_texture;
}

glTexture* Texture::createGlTexture(const void* data, int width, int height, Format format) {
    glTexture* texture = new glTexture(GL_TEXTURE_2D);
    
    if (format == RGB8){
        texture->texImage2D(0, GL_RGB8, width, height, GL_BGR, GL_UNSIGNED_BYTE, data);
    }else if (format == RGBA8){
        texture->texImage2D(0, GL_RGBA8, width, height, GL_BGRA, GL_UNSIGNED_BYTE, data);
    }else{
        //TODO: report error
    }
    
    texture->textureParameters(GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    texture->textureParameters(GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    texture->textureParameters(GL_TEXTURE_BASE_LEVEL, 0);
    texture->textureParameters(GL_TEXTURE_MAX_LEVEL,  0);
    
    return texture;
}

void Texture::setTransColor(const glm::vec4& color) {
    m_transColor = color;
}

glm::vec4 Texture::getTransColor() const{
    return m_transColor;
}

int Texture::getHeight() const {
    return m_height;
}

int Texture::getWidth() const {
    return m_width;
}

glm::ivec2 Texture::getSize() const {
    return glm::ivec2(m_width, m_height);
}


