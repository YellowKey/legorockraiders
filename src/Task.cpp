/* 
 * File:   task.cpp
 * Author: Kevin
 * 
 * Created on 19. August 2015, 13:13
 */

#include "Task.h"
#include "Action.h"
#include "TaskManager.h"

#include <iostream>

Task::Task() {
    
}

Task::Task(const Task& orig) {
}

Task::~Task() {
}

bool Task::assignUnit(Unit* unit) {
    return false;
}

int Task::getLocalPriority() {
    return 0;
}

glm::vec2 Task::getPos() {
    return glm::vec2(0);
}

Task::TaskType Task::getTaskType() {
    return TASK_UNKOWN;
}

bool Task::isAssigned() {
    return false;
}

void Task::unitCanceled() {
    
}

bool Task::operator==(const Task& task) {
    return false;
}

void Task::unitFinished() {
    finish();
}

void Task::finish() {
    for (auto action: m_actions){
        action->setTask(nullptr);
    }
    
    m_taskManager->removeTask(this);
    
    delete this;
}

Action* Task::addAction(Action* action) {
    m_actions.push_back(action);
    action->setTask(this);
    
    return action;
}

void Task::setTaskManager(TaskManager* manager) {
    m_taskManager = manager;
}
