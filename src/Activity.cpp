/* 
 * File:   Activity.cpp
 * Author: Kevin
 * 
 * Created on 13. Juni 2015, 14:45
 */

#include "Activity.h"

std::array<std::string, Activity::NUMANIMATIONS> Activity::m_animationNames;

Activity::Activity() {
}

Activity::Activity(const Activity& orig) {
}

Activity::~Activity() {
}


bool Activity::isIdValid(int anim) {
    if (anim < NUMANIMATIONS && anim >= 0){
        return true;
    }
    
    return false;
}

bool Activity::hasAnim(int anim) {
    if (isIdValid(anim)){
        return m_animations[anim].mesh != nullptr;
    }

    return false;
}

Mesh* Activity::getAnimMesh(int anim) {
    if (isIdValid(anim)){
        return m_animations[anim].mesh;
    }

    return nullptr;
}

float Activity::getAnimTransCoef(int anim) {
    if (isIdValid(anim)){
        return m_animations[anim].transCoeff;
    }

    return 0;
}

std::string Activity::getAnimationName(int anim) {
    static bool loaded = false;
    
    if (!loaded){
        loaded = true;
        initNames();
    }
    
    return m_animationNames[anim];
}

int Activity::getNumAnimations() {
    return NUMANIMATIONS;
}

void Activity::initNames() {
    m_animationNames[ANIM_STAND]      = "Activity_Stand";
    m_animationNames[ANIM_MPSTAND]    = "Activity_MPStand";
    m_animationNames[ANIM_ROUTE]      = "Activity_Route";
    m_animationNames[ANIM_MPROUTE]    = "Activity_MPRoute";
    m_animationNames[ANIM_DRILL]      = "Activity_Drill";
    m_animationNames[ANIM_MPDRILL]    = "Activity_MPDrill";
    m_animationNames[ANIM_TELEPORT]   = "Activity_Teleport";
    m_animationNames[ANIM_TELEPORTIN] = "Activity_TeleportIn";
    m_animationNames[ANIM_WALK]       = "Activity_Walk";
    m_animationNames[ANIM_RUNPANIC]   = "Activity_RunPanic";
    m_animationNames[ANIM_REINFORCE]  = "Activity_Reinforce";
    m_animationNames[ANIM_REVERSE]    = "Activity_Reverse";
    m_animationNames[ANIM_TURNLEFT]   = "Activity_TurnLeft";
    m_animationNames[ANIM_TURNRIGHT]  = "Activity_TurnRight";
    m_animationNames[ANIM_CANTDO]     = "Activity_CantDo";
    m_animationNames[ANIM_EMERGE]     = "Activity_Emerge";
    m_animationNames[ANIM_COLLECT]    = "Activity_Collect";
    m_animationNames[ANIM_CLEAR]      = "Activity_Clear";
    m_animationNames[ANIM_CARRY]      = "Activity_Carry";
    m_animationNames[ANIM_THROW]      = "Activity_Throw";
    m_animationNames[ANIM_CARRYTURNLEFT]  = "Activity_CarryTurnLeft";
    m_animationNames[ANIM_CARRYTURNRIGHT] = "Activity_CarryTurnRight";
    m_animationNames[ANIM_CARRYSTAND] = "Activity_CarryStand";
    m_animationNames[ANIM_EXPLODE]    = "Activity_Explode";
    m_animationNames[ANIM_UNPOWERED]  = "Activity_Unpowered";
    m_animationNames[ANIM_DEPOSITE]   = "Activity_Deposit";
    m_animationNames[ANIM_DYNAITE]    = "Activity_Dynamite";
    m_animationNames[ANIM_PLACE]      = "Activity_Place";
    m_animationNames[ANIM_REPAIR]     = "Activity_Repair";
    m_animationNames[ANIM_REST]       = "Activity_rest";
    m_animationNames[ANIM_ROUTERUBBLE]= "Activity_routeRubble";
    m_animationNames[ANIM_CARRYROUTE] = "Activity_CarryRubble";
    m_animationNames[ANIM_EAT]        = "Activity_Eat";
    m_animationNames[ANIM_FIRELASER]  = "Activity_FireLaser";
    m_animationNames[ANIM_GETUP]      = "Activity_GetUp";
    m_animationNames[ANIM_THROWNBYROCKMONSTER] = "Activity_ThrownByRockMonster";
    m_animationNames[ANIM_SLIP]       = "Activity_Slip";
    m_animationNames[ANIM_TRAIN]      = "Activity_Train";
    m_animationNames[ANIM_RECHARGE]   = "Activity_Recharge";
    m_animationNames[ANIM_WAITING1]   = "Activity_Waiting1";
    m_animationNames[ANIM_WAITING2]   = "Activity_Waiting2";
    m_animationNames[ANIM_WAITING3]   = "Activity_Waiting3";
    m_animationNames[ANIM_WAITING4]   = "Activity_Waiting4";
}

void Activity::setAnimMesh(int anim, Mesh* mesh) {
    if (isIdValid(anim)){
        m_animations[anim].mesh = mesh;
    }
}

void Activity::setAnimTransCoef(int anim, float coeff) {
    if (isIdValid(anim)){
        m_animations[anim].transCoeff = coeff;
    }
}

