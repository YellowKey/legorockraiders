/* 
 * File:   Container.h
 * Author: Kevin
 *
 * Created on 26. September 2015, 12:23
 */

#ifndef CONTAINER_H
#define	CONTAINER_H

#include <deque>

#include "ContainerItem.h"

class ContainerBase{
public:
    typedef typename std::deque<ContainerItem*>::iterator baseIterator;
    
    ContainerBase();
    ContainerBase(const ContainerBase& orig) = delete;
    virtual ~ContainerBase();
    
    void addContainerItem(ContainerItem* item);
    void removeContainerItem(ContainerItem* item);
    
    baseIterator ContainerBaseBegin();
    baseIterator ContainerBaseEnd();
    
    ContainerItem*& get(int index);
    int size();
private:
    std::deque<ContainerItem*> m_list;
};

template <class T>
class Container: protected ContainerBase{
public:
    class iterator{
    public:
        iterator(Container& container, int pos):m_container(container), m_pos(pos){};
        
        bool operator !=(const iterator& other){
            return m_pos != other.m_pos;
        }
        
        T* operator*(){
            return m_container[m_pos];
        }
        
        const iterator& operator++(){
            ++m_pos;
            
            return *this;
        }
    private:
        int m_pos;
        Container& m_container;
    };
    Container(){};
    Container(const Container& orig) = delete;
    virtual ~Container(){};
    
    void addItem(T* item){addContainerItem(static_cast<ContainerItem*>(item));};
    void removeItem(T* item){removeContainerItem(static_cast<ContainerItem*>(item));};
    
    iterator begin(){return iterator(*this, 0);};
    iterator end(){return iterator(*this, size());};
    
    T*& operator[](int index){static_cast<T*>(get(index));};
    int size(){ContainerBase::size();};
private:
    
};

#endif	/* CONTAINER_H */

