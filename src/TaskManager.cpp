/* 
 * File:   TaskManager.cpp
 * Author: Kevin
 * 
 * Created on 19. August 2015, 13:24
 */

#include "TaskManager.h"
#include <algorithm>

TaskManager::TaskManager() {
    /*m_taskPrio.push_back(Task::TASK_BUILD);
    m_taskPrio.push_back(Task::TASK_DRILL);
    m_taskPrio.push_back(Task::TASK_CLEAR);
    m_taskPrio.push_back(Task::TASK_ORE);
    m_taskPrio.push_back(Task::TASK_CRYSTAL);
    m_taskPrio.push_back(Task::TASK_REPAIR);
    m_taskPrio.push_back(Task::TASK_DRIVE);
    m_taskPrio.push_back(Task::TASK_REINFORCE);
    m_taskPrio.push_back(Task::TASK_RECHARGE);*/
}

TaskManager::~TaskManager() {
}

bool TaskManager::addTask(Task* task) {
    for (auto locTask: m_tasks){
        if (*locTask == *task){
            return false; //TODO: Could leak memory
        }
    }
    
    task->setTaskManager(this);
    
    m_tasks.push_back(task);
    
    return true;
}

void TaskManager::removeTask(Task* task) {
    m_tasks.erase(std::remove(m_tasks.begin(), m_tasks.end(), task), m_tasks.end());
}


void TaskManager::findTaskFor(Unit* unit) {
    for (auto task: m_tasks){
        if (!task->isAssigned() && task->assignUnit(unit)){
            return;
        }
    }
}

Task::TaskType TaskManager::getTaskTypeByPriority(uint32_t prio) {

}

void TaskManager::setTaskTypePriority(Task::TaskType task, uint32_t prio) {

}



