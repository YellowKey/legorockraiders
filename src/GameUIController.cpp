/* 
 * File:   UIController.cpp
 * Author: Kevin
 * 
 * Created on 14. August 2015, 11:11
 */

#include "GameUIController.h"

#include "App.h"

#include "WidgetImage.h"
#include "WidgetText.h"
#include "WidgetButton.h"
#include "WidgetIconPanel.h"
#include "WidgetRightPanel.h"
#include "WidgetTopPanel.h"
#include "WidgetCameraControl.h"
#include "WidgetMesssagePanel.h"
#include "Action.h"
#include "Game.h"


#include "TaskDrill.h"
#include "TaskClear.h"

#include <iostream>

GameUIController::GameUIController() {
    m_app = App::getApp();
    auto ress = m_app->getInterfaceRessources();
    
    m_gameMap = App::getApp()->getGameMap();
    m_game = App::getApp()->getGame();
    
    m_game->addListener(this);
    
    m_widgetRight.setBaseSize(glm::vec2(640, 480));
    m_sideMenuContainer.setBaseSize(glm::vec2(640, 480));
    
    //m_widgetGame.setEngine3D(m_engine3D);
    //m_widgetGame.setGameMap(m_gameMap);
    m_widgetGame.enableDebugControl(true);
    
    m_screenWidget.addChild(&m_widgetGame);
    m_screenWidget.addChild(&m_widgetRight);
    m_screenWidget.addChild(&m_widgetLeft);
    
    WidgetMesssagePanel* messagePanel = new WidgetMesssagePanel();
    messagePanel->setRelPos(glm::vec2(42, -129));
    messagePanel->setTextures(ress);
    m_widgetRight.addChild(messagePanel);
    
    WidgetTopPanel* topPanel = new WidgetTopPanel();
    topPanel->setTextures(ress);
   
    topPanel->setRelPos(m_widgetRight.getSize() - topPanel->getSize());
    m_widgetRight.addChild(topPanel);
    
    m_widgetRight.addChild(&m_sideMenuContainer);
    openMainSideMenu();
    
    WidgetCameraControl* cameraPanel = new WidgetCameraControl();
    cameraPanel->setTexture(ress);
    cameraPanel->setRelPos(glm::vec2(411, 0));
    m_widgetRight.addChild(cameraPanel);
    
    m_rightPanel = new WidgetRightPanel();
    m_rightPanel->setTextures(ress);
    m_rightPanel->setCrystalGoal(25);
    m_rightPanel->setCrystalCount(m_game->getCrystal());
    m_rightPanel->setCrystalUsedCount(0);
    m_rightPanel->setOreCount(m_game->getOre());
    m_rightPanel->setRelPos(glm::vec2(100, 0));
    m_rightPanel->setRelPos(glm::vec2(m_widgetRight.getSize() .x - m_rightPanel->getSize().x, 0));
    m_widgetRight.addChild(m_rightPanel);
    
    m_widgetGame.setEngine3D(App::getApp()->getEngine3D());
    m_widgetGame.setGame(m_game);
    m_widgetGame.setGameMap(m_gameMap);
    m_widgetGame.setRendererMap(App::getApp()->getRendererMap());
}

GameUIController::~GameUIController() {
}

Widget* GameUIController::GetScreenWidget() {
    return &m_screenWidget;
}

WidgetGameView* GameUIController::getGameWidget() {
    return &m_widgetGame;
}

/*void GameUIController::attachToGame(Game* game) {

}*/

void GameUIController::scale(glm::vec2 size) {
    float scale = size.y / 480;
    
    m_widgetRight.setScale(scale);
    m_widgetLeft.setScale(scale);
    m_widgetRight.setRelPos(glm::vec2(size.x - m_widgetRight.getSize().x, 0));
    m_widgetGame.setBaseSize(size);
}

void GameUIController::update() {
    if (m_currentSideMenu != m_nextSideMenu){
        if (m_nextSideMenu == SIDE_MAIN)
            openMainSideMenu();
        if (m_nextSideMenu == SIDE_UNIT)
            openUnitMenu();
        if (m_nextSideMenu == SIDE_BUILDINGS)
            openBuildingsSideMenu();
        if (m_nextSideMenu == SIDE_SMALLVEHICLES)
            openSmallVehiclesSideMenu();
        if (m_nextSideMenu == SIDE_LARGEVEHICLES)
            openLargeVehiclesSideMenu();
        if (m_nextSideMenu == SIDE_TRAIN)
            openTrainMenu();
        if (m_nextSideMenu == SIDE_TOOLS)
            openToolMenu();
        if (m_nextSideMenu == SIDE_WALL)
            openWallMenu();
        if (m_nextSideMenu == SIDE_GROUND)
            openGroundMenu();
        
        m_currentSideMenu = m_nextSideMenu;
    }
}


void GameUIController::render(Renderer2D* renderer) {
m_screenWidget.render(renderer);
}


void GameUIController::onClick(int buttonId, int mouseButton) {
    switch(buttonId){
        case BUT_BACK_MAIN:
            m_nextSideMenu = SIDE_MAIN;
            break;
        case BUT_BACK_UNIT:
            m_nextSideMenu = SIDE_UNIT;
            break;
        case BUT_BACK_DESELTILE:
            //m_nextSideMenu = SIDE_MAIN;
            m_game->deselectTile();
            break;
            
        case BUT_BUILDINGS:
            m_nextSideMenu = SIDE_BUILDINGS;
            break;
        case BUT_SMALLVEHICLES:
            m_nextSideMenu = SIDE_SMALLVEHICLES;
            break;
        case BUT_LARGEVEHICLES:
            m_nextSideMenu = SIDE_LARGEVEHICLES;
            break;
        case BUT_TRAIN:
            m_nextSideMenu = SIDE_TRAIN;
            break;
        case BUT_TOOLS:
            m_nextSideMenu = SIDE_TOOLS;
            break;
        
            
        case BUT_CLEARRUBBLE:
            m_game->getTaskManager()->addTask(new TaskClear(m_game->getSelectedTile()));
            break;
        case BUT_DRILL:
            m_game->getTaskManager()->addTask(new TaskDrill(m_game->getSelectedTile()));
            break;
        
        case BUT_BUILD_SMALLTELEPORT:
            m_widgetGame.build(m_game->getObjectInfo(Game::OBJ_SMALLTELPORTER));
            break;
        case BUT_BUILD_SUPPORTSTATION:
            m_widgetGame.build(m_game->getObjectInfo(Game::OBJ_SUPPLY));
            break;
        case BUT_BUILD_POWERPLANT:
            m_widgetGame.build(m_game->getObjectInfo(Game::OBJ_POWERSTATION));
            break;
    }
}

WidgetIconPanel* GameUIController::createPanel(int count, ButtonId backButton) {
    float scale = m_sideMenuContainer.getScale();
    if (m_sideMenu){
        m_sideMenu->deleteChildren();
        delete m_sideMenu;
    }
    
    auto ress = m_app->getInterfaceRessources();
    
    WidgetIconPanel* panel = new WidgetIconPanel();
    
    if (backButton == BUT_NONE){
        panel->setPanelTexture(ress->iconPanelNB[count]);
        panel->setIconOffset(glm::vec2(11, 15));
    }else{
        WidgetButton* button = new WidgetButton();
        button->setTextureHover(ress->iconPanelBackHigh);
        button->setTexturePushed(ress->iconPanelBackPush);
        button->setRelPos(glm::vec2(4, 22) + (float)(count - 1) * glm::vec2(0, 40));
        button->addListener(this, backButton);
        
        panel->enableBackButton();
        panel->addChild(button);
        panel->setPanelTexture(ress->iconPanel[count]);
        panel->setIconOffset(glm::vec2(36, 15));
    }
    
    panel->setRelPos(m_widgetRight.getBaseSize() - panel->getBaseSize() - glm::vec2(16, 9));
    
    m_sideMenuContainer.addChild(panel);
    
    m_sideMenu = panel;
    
    return panel;
}


void GameUIController::openMainSideMenu() {
    auto ress = m_app->getInterfaceRessources();
    
    WidgetIconPanel* panel = createPanel(4, BUT_NONE);
            
    panel->addChild((new WidgetButton(ress->menuBigVehicles))->addListener(this, BUT_LARGEVEHICLES));
    panel->addChild((new WidgetButton(ress->menuSmallVehicles))->addListener(this, BUT_SMALLVEHICLES));
    panel->addChild((new WidgetButton(ress->menuBuildings))->addListener(this, BUT_BUILDINGS));
    panel->addChild((new WidgetButton(ress->menuMinifigures))->addListener(this, BUT_TELEPORT));
    
}

void GameUIController::openBuildingsSideMenu() {
    auto ress = m_app->getInterfaceRessources();
    
    WidgetIconPanel* panel = createPanel(10, BUT_BACK_MAIN);
    
    panel->addChild((new WidgetButton(ress->menuLargeTeleport))->addListener(this, BUT_BUILD_LARGETELEPORT));
    panel->addChild((new WidgetButton(ress->menuLaser))->addListener(this, BUT_BUILD_LASER));
    panel->addChild((new WidgetButton(ress->menuOreRefinery))->addListener(this, BUT_BUILD_OREREFINERY));
    panel->addChild((new WidgetButton(ress->menuGeologicCenter))->addListener(this, BUT_BUILD_GEOLOGICCENTER));
    panel->addChild((new WidgetButton(ress->menuUpgradeStation))->addListener(this, BUT_BUILD_UPGRADESTATION));
    panel->addChild((new WidgetButton(ress->menuSupportStation))->addListener(this, BUT_BUILD_SUPPORTSTATION));
    panel->addChild((new WidgetButton(ress->menuPowerplant))->addListener(this, BUT_BUILD_POWERPLANT));
    panel->addChild((new WidgetButton(ress->menuDock))->addListener(this, BUT_BUILD_DOCKS));
    panel->addChild((new WidgetButton(ress->menuSmallTeleport))->addListener(this, BUT_BUILD_SMALLTELEPORT));
    panel->addChild((new WidgetButton(ress->menuToolstore))->addListener(this, BUT_BUILD_TOOLSTORE));
}

void GameUIController::openTrainMenu() {
    auto ress = m_app->getInterfaceRessources();
    
    WidgetIconPanel* panel = createPanel(6, BUT_BACK_UNIT);
    
    panel->addChild((new WidgetButton(ress->menuTrainExplosives))->addListener(this, BUT_TRAIN_DYNAMITE));
    panel->addChild((new WidgetButton(ress->menuTrainSalor))->addListener(this, BUT_TRAIN_SAILOR));
    panel->addChild((new WidgetButton(ress->menuTrainPilot))->addListener(this, BUT_TRAIN_PILOT));
    panel->addChild((new WidgetButton(ress->menuTrainGeologist))->addListener(this, BUT_TRAIN_GEOLOGIST));
    panel->addChild((new WidgetButton(ress->menuTrainEngineer))->addListener(this, BUT_TRAIN_REPAIR));
    panel->addChild((new WidgetButton(ress->menuTrainDriver))->addListener(this, BUT_TRAIN_DRIVER));
}

void GameUIController::openUnitMenu() {
    auto ress = m_app->getInterfaceRessources();
    
    WidgetIconPanel* panel = createPanel(10, BUT_BACK_MAIN);
    
    panel->addChild((new WidgetButton(ress->menuTeleportback))->addListener(this, BUT_TELEPORTBACK));
    panel->addChild((new WidgetButton(ress->menuShoulder))->addListener(this, BUT_SHOULDER));
    panel->addChild((new WidgetButton(ress->menuFirstPerson))->addListener(this, BUT_FIRSTPERSON));
    panel->addChild((new WidgetButton(ress->menuTrain))->addListener(this, BUT_TRAIN));
    panel->addChild((new WidgetButton(ress->menuUpgrade))->addListener(this, BUT_UPGRADE));
    panel->addChild((new WidgetButton(ress->menuSonicBlaster))->addListener(this, BUT_SONICPLASTER));
    panel->addChild((new WidgetButton(ress->menuTools))->addListener(this, BUT_TOOLS));
    panel->addChild((new WidgetButton(ress->menuPickup))->addListener(this, BUT_PICKUP));
    panel->addChild((new WidgetButton(ress->menuDrop))->addListener(this, BUT_DROP));
    panel->addChild((new WidgetButton(ress->menuEat))->addListener(this, BUT_EAT));
}


void GameUIController::openLargeVehiclesSideMenu() {
    auto ress = m_app->getInterfaceRessources();
    
    WidgetIconPanel* panel = createPanel(6, BUT_BACK_MAIN);
    
    panel->addChild((new WidgetButton(ress->menuTunnelTransport))->addListener(this, BUT_BUILD_TUNNELTRANSPORT));
    panel->addChild((new WidgetButton(ress->menuChromeCrusher))->addListener(this, BUT_BUILD_CHROMECRUSHER));
    panel->addChild((new WidgetButton(ress->menuCargoCarrier))->addListener(this, BUT_BUILD_CARGOCARRIER));
    panel->addChild((new WidgetButton(ress->menuLargeMobileLaser))->addListener(this, BUT_BUILD_LARGEMOBILELASER));
    panel->addChild((new WidgetButton(ress->menuGraniteGrinder))->addListener(this, BUT_BUILD_GRANITEGRINDER));
    panel->addChild((new WidgetButton(ress->menuLoaderDozer))->addListener(this, BUT_BUILD_LOADERDOZER));
}

void GameUIController::openSmallVehiclesSideMenu() {
    auto ress = m_app->getInterfaceRessources();
    
    WidgetIconPanel* panel = createPanel(6, BUT_BACK_MAIN);
    
    panel->addChild((new WidgetButton(ress->menuTunnelscout))->addListener(this, BUT_BUILD_TUNNELSCOUT));
    panel->addChild((new WidgetButton(ress->menuSmalllaser))->addListener(this, BUT_BUILD_SMALLLASER));
    panel->addChild((new WidgetButton(ress->menuRapidRider))->addListener(this, BUT_BUILD_RAPIDRIDER));
    panel->addChild((new WidgetButton(ress->menuTransportvehicle))->addListener(this, BUT_BUILD_TRANSPORTVEHICLE));
    panel->addChild((new WidgetButton(ress->menuSmalldigger))->addListener(this, BUT_BUILD_SMALLDIGGER));
    panel->addChild((new WidgetButton(ress->menuHoverscout))->addListener(this, BUT_BUILD_HOVERSCOUT));
}

void GameUIController::openToolMenu() {
    auto ress = m_app->getInterfaceRessources();
    
    WidgetIconPanel* panel = createPanel(8, BUT_BACK_UNIT);
    
    panel->addChild((new WidgetButton(ress->menuTakeSonic))->addListener(this, BUT_TAKE_SONIC));
    panel->addChild((new WidgetButton(ress->menuTakePush))->addListener(this, BUT_TAKE_PUSH));
    panel->addChild((new WidgetButton(ress->menuTakeFreeze))->addListener(this, BUT_TAKE_FREEZE));
    panel->addChild((new WidgetButton(ress->menuTakeLaser))->addListener(this, BUT_TAKE_LASER));
    panel->addChild((new WidgetButton(ress->menuTakeSpanner))->addListener(this, BUT_TAKE_SPANNER));
    panel->addChild((new WidgetButton(ress->menuTakeHammer))->addListener(this, BUT_TAKE_HAMMER));
    panel->addChild((new WidgetButton(ress->menuTakeShapde))->addListener(this, BUT_TAKE_SPADE));
    panel->addChild((new WidgetButton(ress->menuTakeDrill))->addListener(this, BUT_TAKE_DRILL));
}

void GameUIController::openWallMenu() {
    auto ress = m_app->getInterfaceRessources();
    
    WidgetIconPanel* panel = createPanel(3, BUT_BACK_DESELTILE);
    
    panel->addChild((new WidgetButton(ress->menuDynamite))->addListener(this, BUT_DYNAMITE));
    panel->addChild((new WidgetButton(ress->menuReinforce))->addListener(this, BUT_REINFORCE));
    panel->addChild((new WidgetButton(ress->menuDrill))->addListener(this, BUT_DRILL));
}

void GameUIController::openGroundMenu() {
    auto ress = m_app->getInterfaceRessources();
    
    WidgetIconPanel* panel = createPanel(3, BUT_BACK_DESELTILE);
    
    panel->addChild((new WidgetButton(ress->menuFence))->addListener(this, BUT_FENCE));
    panel->addChild((new WidgetButton(ress->menuClearRubble))->addListener(this, BUT_CLEARRUBBLE));
    panel->addChild((new WidgetButton(ress->menuPath))->addListener(this, BUT_PATH));
}

GameMap* GameUIController::getGameMap() const{
    return m_gameMap;
}

void GameUIController::onTileDeselect() {
    m_nextSideMenu = SIDE_MAIN;
}

void GameUIController::onTileSelect(glm::ivec2 pos) {
    if (m_gameMap->isTrueWall(pos.x, pos.y)){
        m_nextSideMenu = SIDE_WALL;
    }else{
        m_nextSideMenu = SIDE_GROUND;
    }
}

void GameUIController::onGameRessChange(int crystal, int ore, int bricks) {
    m_rightPanel->setOreCount(ore + bricks * 5);
    m_rightPanel->setCrystalCount(crystal);
}
