/* 
 * File:   ContainerItem.cpp
 * Author: Kevin
 * 
 * Created on 26. September 2015, 12:23
 */

#include "ContainerItem.h"
#include "Container.h"

#include <algorithm>

ContainerItem::ContainerItem() {
}

ContainerItem::ContainerItem(const ContainerItem& orig) {
}

ContainerItem::~ContainerItem() {
    for (ContainerBase* base: m_container){
        base->removeContainerItem(this);
    }
}

void ContainerItem::addToContainer(ContainerBase* container) {
    m_container.push_back(container);
}

void ContainerItem::removeFromContainer(ContainerBase* container) {
    m_container.erase(std::remove(m_container.begin(), m_container.end(), container), m_container.end());
}

