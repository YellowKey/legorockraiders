/* 
 * File:   glShader.cpp
 * Author: Kevin
 * 
 * Created on 22. November 2014, 14:46
 */

#include "glShader.h"

#include <fstream>
#include <iostream>

using std::getline;

string glShader::s_shaderFolder = "";

glShader::glShader(Type type):m_type(type) {
    m_handle = glCreateShader(getOpenGlType());
}

glShader::glShader(Type type, const string& file):glShader(type){
    load(file);
}

glShader::~glShader() {
    glDeleteShader(m_handle);
}

GLuint glShader::getHandle() const {
    return m_handle;
}

int glShader::getOpenGlType() const {
    switch(m_type){
        case Type::FRAGMENT:
            return GL_FRAGMENT_SHADER;
        case Type::VERTEX:
            return GL_VERTEX_SHADER;
        case Type::COMPUTE:
            return GL_COMPUTE_SHADER;
        case Type::GEOMETRY:
            return GL_GEOMETRY_SHADER;
        case Type::TESS_CONTROL:
            return GL_TESS_CONTROL_SHADER;
        case Type::TESS_EVALUATION:
            return GL_TESS_EVALUATION_SHADER;
        default:
            return 0;
    }
}

glShader::Type glShader::getType() const {
        return m_type;
}

bool glShader::load(const string& file) {
    string fileName = s_shaderFolder + file;
    std::fstream stream(fileName, std::ios_base::in);

    std::string line;
    std::string code;
    
    
    if(stream.fail()){
        std::cerr << "couldn't find " + fileName << std::endl;
        return false;
    }

    while ((getline(stream, line))){
        code += line + "\n";
    }

    const char * codePointer = code.c_str();
    glShaderSource(m_handle, 1, &codePointer, NULL);
    glCompileShader(m_handle);

    GLint result = GL_FALSE;
    int logLength;
    
    glGetShaderiv(m_handle, GL_COMPILE_STATUS, &result);
    glGetShaderiv(m_handle, GL_INFO_LOG_LENGTH, &logLength);
    char message[logLength];
    glGetShaderInfoLog(m_handle, logLength, NULL, message);
    
    if (result == 0){
        std::cerr << "couldn't compile: " + fileName << std::endl;
        std::cerr << message << std::endl;
        return false;
    }else{
        string output ="compiled: " + fileName;
        if(logLength > 1){
            output += string("\n") + message;
        }
        std::cout << output << std::endl;
    }
    return true;
}

string glShader::getShaderFolder() {
    return s_shaderFolder;
}

const string& glShader::setShaderFolder(const string& folder) {
    return s_shaderFolder = folder;
}

