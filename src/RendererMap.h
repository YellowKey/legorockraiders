/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   RendererMap.h
 * Author: Kevin
 *
 * Created on 30. Dezember 2015, 11:52
 */

#ifndef RENDERERMAP_H
#define RENDERERMAP_H

#include "Engine3D.h"
#include "StateMap.h"
#include "MapRessource.h"

#include "glBuffer.h"
#include "glVertexArray.h"
#include "glUniform.h"
#include "glProgram.h"

#include <sigc++/sigc++.h>
#include <array>
#include <vector>

class RendererMap: public sigc::trackable{
public:
    RendererMap();
    RendererMap(Engine3D* engine, MapRessource* ressource);
    ~RendererMap();
    
    void setEngine(Engine3D* engine);
    void setMap(StateMap* map);
    void setRessources(MapRessource* ressource);
    
    bool rayTest(const glm::vec3& orig, const glm::vec3& dir, glm::vec3& coll);
private:
    enum Type{
        TYPE_GROUND,
        TYPE_WALL,
        TYPE_CORNERIN,
        TYPE_CORNEROUT,
        TYPE_EDGE,
        TYPE_ROOF,
    };
    
    static constexpr int TILESIZE = 8;
    
    struct Vertex{
        glm::vec3 pos;
        glm::vec3 uv;
        glm::vec3 normal;
        glm::vec2 flow;
    };
    
    struct Tile{
        bool dirty;
        int  bufferOffset;
        std::array<Vertex, TILESIZE * TILESIZE * 6> vertices;
    };
    
    void onRender(Engine3D* engine);
    void onTileChange(const glm::ivec2& pos);
    void onMapSizeChange(int width, int height);
    
    void updateTile(const glm::ivec2& pos);
    void updateAll();
    
    int getVertexCount();
    
    int getTexture(const glm::ivec2& pos, Type type);
    
    bool isWall(int x, int y);
    
    Vertex* addVertices(Vertex* vertex, int x, int y);
    
    inline Tile& tile(int x, int y){
        return m_tiles[x + y * m_width];
    }
    
    inline Tile& tile(const glm::ivec2& pos){
        return tile(pos.x, pos.y);
    }
    
    int m_width  = 0;
    int m_height = 0;
    
    std::vector<Tile> m_tiles;
    
    StateMap* m_map;
    MapRessource* m_ressource;
    
    glVertexArray m_vertexArray;
    glProgram     m_shader;
    glBuffer      m_vertexBuffer = {GL_ARRAY_BUFFER};
    
    glUniform m_uniCameraMatrix;
    glUniform m_uniColorTexture;
    glUniform m_uniNoiseTexture;
    glUniform m_uniColorFactor;
    glUniform m_uniCursorLightPos;
    glUniform m_uniCursorLightColor;
    glUniform m_uniAmbientLightColor;
    glUniform m_uniCameraPos;
    glUniform m_uniGamma;
    glUniform m_uniFlowScale;
    glUniform m_uniFlowTime;
};

#endif /* RENDERERMAP_H */

