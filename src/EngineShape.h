/* 
 * File:   EngineShape.h
 * Author: Kevin
 *
 * Created on 4. Juni 2015, 16:04
 */

#ifndef ENGINESHAPE_H
#define	ENGINESHAPE_H

#include "Shape.h"

class EngineObject;

class EngineShape {
public:
    EngineObject* getParent();
    
    void setMatrix(const glm::mat4& matrix);
    glm::mat4 getMatrix();
    int getGroup();
private:
    friend class Engine3D;
    
    EngineShape(EngineObject* parent, int group);
    virtual ~EngineShape();
    
    EngineObject* m_parent;
    
    int m_group;
    
    glm::mat4 m_matrix;
};

#endif	/* ENGINESHAPE_H */

