/* 
 * File:   glBuffer.h
 * Author: Kevin
 *
 * Created on 17. November 2014, 20:20
 */

#ifndef GLBUFFER_H
#define	GLBUFFER_H

#include "glObject.h"

#include <GL/glew.h>

#include <map>

using std::map;

class glBuffer:public glObject{
public:
    glBuffer(GLenum type);
    glBuffer(GLenum type, GLenum usage);
    glBuffer(const glBuffer& orig) = delete;
    glBuffer(glBuffer&& orig);
    virtual ~glBuffer();
    
    void bindBuffer();
    
    void setUsage(GLenum usage);
    
    void BufferData(int size, const void* data);
    void BufferSubData(int offset, int size, const void* data);
    
    void* mapBuffer(GLenum access);
    void* mapBufferRange(int offset, int size, GLbitfield access);
    void  unmapBuffer();
    void* getMapping();
    
    size_t getSize();
    
    static glBuffer* getBoundBuffer(GLenum type);
    static void      setBoundBuffer(GLenum type, glBuffer* buffer); //use with care
    static void      clearBufferSlot(GLenum type);
    
    GLuint getHandle();
    
    virtual glObject::Type getObjectType() override;
protected:
private:
    static map<GLenum, glBuffer*> s_boundBuffer;
    
    void*  m_mappedTo = nullptr;
    GLenum m_type;
    GLenum m_usage = GL_STATIC_DRAW;
    GLuint m_handle;
    bool   m_movedAway = false;
};

#endif	/* GLBUFFER_H */

