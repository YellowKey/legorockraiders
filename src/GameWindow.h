/* 
 * File:   GameWindow.h
 * Author: Kevin
 *
 * Created on 1. Mai 2015, 12:00
 */

#ifndef GAMEWINDOW_H
#define	GAMEWINDOW_H

#include <glm/vec2.hpp>
#include <GL/glew.h>
#include <SFML/Window.hpp>

#include <vector>
#include <string>

class GameWindow {
public:
    GameWindow(const std::string& title, int width, int height, bool fullscreen = false);
    GameWindow(const GameWindow& orig) = delete;
    ~GameWindow();
    
    int getWidth() const;
    int getHeight() const;
    bool shouldClose() const;
    
    void setFullscreen(bool fullscreen);
    void makeCurrent();
    
    glm::vec2 getCursorPos() const;
    bool getKey(sf::Keyboard::Key id) const;
    bool getMouseButton(int id) const;
    
    static void update();
    
    void swapBuffers();
private:
    void onUpdate();
    
    static std::vector<GameWindow*> s_windowList;
    
    sf::Window m_window;
    
    bool m_shouldClose = false;
};

#endif	/* GAMEWINDOW_H */

