/* 
 * File:   LoaderAe.h
 * Author: kevin
 *
 * Created on June 18, 2015, 10:00 AM
 */

#ifndef LOADERAE_H
#define	LOADERAE_H

#include "LoaderCfg.h"
#include "Activity.h"

class LoaderAe {
public:
    LoaderAe();
    LoaderAe(const LoaderAe& orig);
    virtual ~LoaderAe();
    
    void load(void* mem, int size);
    void setPath(const std::string& path);
    
    Activity* getActivity();
private:
    struct Anim{
        std::string fileName;
        float transCoef;
    };
    LoaderCfg m_loaderCfg;
    
    std::array<Anim, Activity::NUMANIMATIONS> m_animations;
    std::string m_path;
    
    bool isDebugging();
};

#endif	/* LOADERAE_H */

