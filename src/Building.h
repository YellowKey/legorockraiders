/* 
 * File:   Building.h
 * Author: Kevin
 *
 * Created on 13. Juni 2015, 14:43
 */

#ifndef BUILDING_H
#define	BUILDING_H

#include <glm/vec3.hpp>

#include "MapObject.h"

#include "EngineObject.h"
#include "Activity.h"

class Building : public MapObject{
public:
    enum State{
        STATE_UNKOWN,
        STATE_BUILDING,
        STATE_TELEPORT,
        STATE_NORMAL,
        STATE_EXPLODE,
        STATE_TELEPORTOUT,
        STATE_UNPOWERED,
    };
    
    Building(Activity* activity, glm::vec3 pos);
    Building(const Building& orig);
    virtual ~Building();
    
    void setState(State state);
private:
    virtual void update() override;

    State m_state = STATE_UNKOWN;
    EngineObject m_engineObject;
    Activity* m_activity;
    glm::vec3 m_pos;
};

#endif	/* BUILDING_H */

