/* 
 * File:   main.cpp
 * Author: Kevin
 *
 * Created on 26. April 2015, 15:31
 */

#include "App.h"

int main(int argc, char** argv) {
    return App::startApp();
    
}

