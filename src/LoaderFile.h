/* 
 * File:   LoaderFile.h
 * Author: Kevin
 *
 * Created on 4. Juni 2015, 10:25
 */

#ifndef FILELOADER_H
#define	FILELOADER_H

#include "Mesh.h"
#include "Texture.h"
#include "Shape.h"
#include "Font.h"
#include "Activity.h"

#include <map>
#include <glm/vec4.hpp>

class LoaderFile {
public:
    LoaderFile();
    LoaderFile(const LoaderFile& orig) = delete;
    ~LoaderFile();
    
    Mesh* loadMesh(const std::string& file);
    Shape* loadShape(const std::string& file);
    Activity* loadActivity(const std::string& file);
    Texture* loadTexture(const std::string& file);
    Font* loadFont(const std::string& file);
    
    bool isFile(const std::string& file);
    
    static std::string getPath(const std::string& filePath);
private:
    class TextureInfo{
    public:
        TextureInfo() = default;
        ~TextureInfo() = default;
        
        glm::vec4 transColor = glm::vec4(0);
    };
    
    bool isDebugging();
    
    //std::map<std::string,TextureInfo> m_textureInfo;
    
    std::map<std::string, Texture*> m_textureCache;
    std::map<std::string, Shape*> m_shapeCache;
};

#endif	/* FILELOADER_H */

