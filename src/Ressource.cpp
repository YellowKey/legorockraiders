/* 
 * File:   Ressource.cpp
 * Author: Kevin
 * 
 * Created on 2. Juni 2015, 22:10
 */

#include "Ressource.h"

#include <glm/gtc/matrix_transform.hpp>

#include "App.h"
#include "TaskCollect.h"

Ressource::Ressource(Type type){
    m_type = type;
    m_rot = (float)(rand() % 5000) / 5000 * 6.281f;
    
    if (type == TYPE_CRYSTAL){
        m_engineObject.setMesh(App::getApp()->getGameRessource()->crystal);
        m_y = 1.0f;
    }else{
        m_engineObject.setMesh(App::getApp()->getGameRessource()->ore);
        m_y = 0.0f;
    }
    
    App::getApp()->getGame()->getTaskManager()->addTask(new TaskCollect(this));
}

Ressource::~Ressource() {
}

void Ressource::update() {
    if (m_type == TYPE_CRYSTAL && !m_attached){
        m_rot += 0.02 * App::getApp()->getGameSpeed();
    
        m_engineObject.setRot(glm::vec3(m_rot, 0, 0));
    }
}

void Ressource::setPosMap(const glm::vec2& pos) {
    setPos(glm::vec3(pos.x, 0, pos.y));
    /*m_pos = pos;
    m_engineObject.setPos(glm::vec3(pos.x, m_y, pos.y));*/
}

void Ressource::onAttachTo(MapObject* target, int slotId) {
    EngineObject* obj = target->getSlotEngineObject(slotId);
    int         group = target->getSlotGroupId(slotId);
    
    setPosMap(glm::vec2(0, 0));
    m_engineObject.setRot(glm::vec3(0));
    m_engineObject.attachTo(obj, group);
    
    m_attached = true;
}

void Ressource::onRemoved() {
    m_engineObject.remove();
    
    m_attached = false;
}

void Ressource::onRTSChange() {
    m_engineObject.setPos(getPos() + glm::vec3(0, m_y, 0));
    m_engineObject.setRot(getRot());
    m_engineObject.setScale(getScale());
}

Ressource::Type Ressource::GetType() {
    return m_type;
}
