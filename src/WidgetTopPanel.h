/* 
 * File:   WidgetTopPanel.h
 * Author: Kevin
 *
 * Created on 23. Juni 2015, 16:51
 */

#ifndef WIDGETTOPPANEL_H
#define	WIDGETTOPPANEL_H

#include "Widget.h"
#include "WidgetButton.h"
#include "WidgetImage.h"

#include "InterfaceRessorces.h"


class WidgetTopPanel: public WidgetImage{
public:
    WidgetTopPanel();
    WidgetTopPanel(const WidgetTopPanel& orig) = delete;
    virtual ~WidgetTopPanel();
    
    void setTextures(InterfaceRessources* ress);
protected:
    virtual void onUpdate() override;
private:
    WidgetButton m_buttonArms;
    WidgetButton m_buttonOptions;
    WidgetButton m_buttonPrio;
};

#endif	/* WIDGETTOPPANEL_H */

