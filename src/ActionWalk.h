/* 
 * File:   ActionWalk.h
 * Author: kevin
 *
 * Created on July 9, 2015, 1:29 PM
 */

#ifndef ACTIONWALK_H
#define	ACTIONWALK_H

#include "Action.h"

#include "GameMap.h"

class ActionWalk: public Action{
public:
    ActionWalk(const glm::vec2& goal);
    ActionWalk(const ActionWalk& orig) = delete;
    virtual ~ActionWalk();
    
    virtual void start() override;
    virtual void updateUnit() override;
    virtual void cancel() override;
    
protected:
    void createPath(const Path& path, const glm::vec2& start, const glm::vec2& end);
    bool isWalking();
    void findPath(const glm::vec2& goal);
    void update();
    void nextPathNode();
private:
    struct pathNode{
        glm::vec2 pos;
        float time;
    };
    
    glm::vec2 m_goal;
    
    float m_startTime;
    
    int m_currentPathId = 0;
    
    pathNode m_lastNode;
    pathNode m_nextNode;
    
    std::vector<pathNode> m_path;
    
    GameMap* m_gameMap;
};

#endif	/* ACTIONWALK_H */

