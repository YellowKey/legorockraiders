/* 
 * File:   ObjectAnim.h
 * Author: Kevin
 *
 * Created on 19. Oktober 2015, 09:26
 */

#ifndef OBJECTANIM_H
#define	OBJECTANIM_H

#include "AnimInfo.h"
#include "EngineObject.h"

#include <bitset>

class ObjectAnim {
public:
    ObjectAnim();
    ObjectAnim(AnimInfo* info, std::string& anim);
    ObjectAnim(const ObjectAnim& orig) = delete;
    virtual ~ObjectAnim();
    
    void setAnimInfo(AnimInfo* info);
    AnimInfo* getAnimInfo();
    
    void setPos(const glm::vec3& pos);
    void setRot(const glm::vec3& rot);
    void setScale(const glm::vec3& scale);
    void setUpgrade(std::bitset<4> upgrades);
    
    glm::vec3 getPos();
    glm::vec3 getRot();
    glm::vec3 getScale();
    std::bitset<4> getUpgrades();
    
    AnimInfo::Activity* setAnim(const std::string& name);
    
    void stopAnimation();
    void pauseAnimation();
    void playAnimation(bool loop = true);
    
    bool  isAnimRunning();
    float getAnimTime();
    
    void attachTo(ObjectAnim* target, std::string slotName, int slotId);
    void remove();
private:
    void clearEngineObjects();
    void createEngineObjects(std::string name);
    
    void attachToObject(EngineObject* target, int groupId);
    void removeFromObject();
    
    void updateChilds();
    
    EngineObject* getMainObject();
    
    AnimInfo* m_anim = nullptr;
    AnimInfo::Activity* m_activity = nullptr;
    
    ObjectAnim* m_target = nullptr;
    std::string m_targetSlot = "";
    int32_t     m_targetSlotId = 0;
    std::vector<ObjectAnim*> m_childs;
    
    glm::vec3 m_pos;
    glm::vec3 m_rot;
    glm::vec3 m_scale;
    
    //std::vector<EngineObject*> m_engineObjects;
    EngineObject m_object;
    std::vector<ObjectAnim*> m_upgrades;
    std::vector<ObjectAnim*> m_wheels;
    
    std::bitset<4> m_upgradesInstalled = 0;
};

#endif	/* OBJECTANIM_H */

