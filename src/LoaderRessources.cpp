/* 
 * File:   LoaderRessources.cpp
 * Author: Kevin
 * 
 * Created on 1. Oktober 2015, 12:14
 */

#include "LoaderRessources.h"

LoaderRessources::LoaderRessources() {
}

LoaderRessources::~LoaderRessources() {
}

void LoaderRessources::load() {
    loadTexture(&background, "Languages\\Loading.bmp");
    loadFont(&font, "Interface\\Fonts\\Font5_Hi.bmp");
    loadTexture(&bar, "Interface\\FrontEnd\\gradient.bmp");
}
