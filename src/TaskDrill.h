/* 
 * File:   TaskDrill.h
 * Author: Kevin
 *
 * Created on 19. August 2015, 14:36
 */

#ifndef TASKDRILL_H
#define	TASKDRILL_H

#include "Task.h"

class TaskDrill: public Task{
public:
    TaskDrill(const glm::vec2 &pos);
    TaskDrill(const TaskDrill& orig) = delete;
    virtual ~TaskDrill();
    

    virtual glm::vec2 getPos() override;
    virtual Task::TaskType getTaskType() override;
    

    virtual bool assignUnit(Unit* unit);
    virtual bool isAssigned();
    

    virtual void unitCanceled();


    virtual bool operator==(const Task& task) override;

private:
    glm::vec2 m_tile;
    bool m_assigned = false;
};

#endif	/* TASKDRILL_H */

