/* 
 * File:   CfgLoader.h
 * Author: Kevin
 *
 * Created on 13. Juni 2015, 15:06
 */

#ifndef CFGLOADER_H
#define	CFGLOADER_H

#include <string>
#include <vector>

class LoaderCfg {
public:
    class Group{
        friend class LoaderCfg;
    public:
        Group(const std::string& name);
        ~Group();
        
        std::string getName() const;
        const Group* getGroup(const std::string& name) const;
        
        std::string getString() const;
        std::string getString(const std::string& name) const;
        int getInt() const;
        int getInt(const std::string& name) const;
        float getFloat() const;
        float getFloat(const std::string& name) const;
        
        std::string getFieldString(char seperator, int index) const;
        std::string getFieldString(const std::string& name, char seperator, int index) const;
        int getFieldInt(char seperator, int index) const;
        int getFieldInt(const std::string& name, char seperator, int index) const;
        float getFieldFloat(char seperator, int index) const;
        float getFieldFloat(const std::string& name, char seperator, int index) const;
        
        int getNumChildren() const;
        const Group* getChild(int id) const;
    private:
        void setValue(const std::string value);
        void addGroup(const Group* group);
        
        std::vector<const Group*> m_children;
        std::string m_name;
        std::string m_value;
    };
    
    LoaderCfg();
    LoaderCfg(const LoaderCfg& orig) = delete;
    virtual ~LoaderCfg();
    
    void load(void* mem, int size);
    
    Group* getMainGroup();
    void   disableAutoDelete();
private:
    std::string readString();
    char pollChar();
    char peekChar();
    void nextLine();
    
    bool eof();
    
    int getFilePos();
    void setFilePos(int pos);
    
    bool isWhitespace(char character);
    bool isNewline(char character);
    bool isControl(char character);
    bool isLetter(char character);
    
    Group* loadGroup();
    void   deleteMainGroup();
    
    char* m_fileData = nullptr;
    char* m_filePos  = nullptr;
    int   m_fileSize = 0;
    
    Group* m_mainGroup = nullptr;
    bool   m_autoDelete  = true;
};

#endif	/* CFGLOADER_H */

