/* 
 * File:   Font.h
 * Author: Kevin
 *
 * Created on 5. Mai 2015, 16:49
 */

#ifndef FONT_H
#define	FONT_H

#include "Texture.h"

#include <glm/vec2.hpp>

class Font : public Texture{
public:
    Font(const void* data, int width, int height, Format format);
    Font(const Font& orig) = delete;
    virtual ~Font();
    
    int getFontSize() const;
    glm::vec2 getCharStart(int id) const;
    glm::vec2 getCharEnd(int id) const;
    glm::vec2 getCharSize(int id) const;
    int getWidth(const std::string& text) const;
private:
    void    scanFont(const void* data, int widht, int height, Format format);
    int32_t getColor(const glm::ivec2& pos, const void* data, const glm::ivec2& size, Format format);
    
    void upload(const void* data, int width, int height, Format format);
    
    glm::vec2 toFloat(const glm::ivec2& pos) const;
    glm::ivec2 getCharPos(int id) const;
    
    static constexpr int CHARSVERT = 10;
    static constexpr int CHARSHORI = 19;
    static constexpr int NUMCHARS = CHARSVERT * CHARSHORI;
    
    int32_t transColor;
    int32_t noFontColor;
    
    int m_charWidth[CHARSVERT * CHARSHORI];
    glm::ivec2 m_charSize;
    glm::vec2 m_imageSize;
};

#endif	/* FONT_H */

