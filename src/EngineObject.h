/* 
 * File:   EngineObject.h
 * Author: Kevin
 *
 * Created on 4. Juni 2015, 14:44
 */

#ifndef ENGINEOBJECT_H
#define	ENGINEOBJECT_H

#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include <vector>

#include "Mesh.h"

class EngineObject {
public:
    static const glm::vec4 DEFAULTCOLOR;
    
    EngineObject();
    EngineObject(Mesh*);
    virtual ~EngineObject();
    
    void setPos(const glm::vec3& pos);
    void setRot(const glm::vec3& rot);
    void setScale(const glm::vec3& scale);
    
    glm::vec3 getPos();
    glm::vec3 getRot();
    glm::vec3 getScale();
    
    glm::mat4 getMatrix();
    
    void setMesh(Mesh* mesh);
    Mesh* getMesh();
    
    void playAnimation(bool loop);
    void stopAnimation();
    void pauseAnimation();
    
    bool  isAnimRunning();
    float getAnimTime();
    
    bool hasChanged();
    void markUnchanged();
    void markChanged();
    
    void attachTo(EngineObject* target, int groupId);
    void remove();
    
    void setColor(const glm::vec4& color);
    glm::vec4 getColor();
private:
    Mesh* m_mesh = nullptr;
    
    bool m_changed = false;
    
    float m_animStartTime;
    float m_animTime = 0.0f;
    bool  m_animLooping;
    bool  m_animRunning = false;
    
    glm::vec3 m_rot;
    glm::vec3 m_pos;
    glm::vec3 m_scale;
    glm::vec4 m_color;
    
    EngineObject* m_targetObject = nullptr;
    int           m_targetGroup;
    
    std::vector<EngineObject*> m_children;
};

#endif	/* ENGINEOBJECT_H */

