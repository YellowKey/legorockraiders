/* 
 * File:   GameRessource.cpp
 * Author: kevin
 * 
 * Created on June 3, 2015, 10:07 AM
 */

#include "GameRessource.h"
#include "LoaderCfg.h"
#include "App.h"

#include <iostream> //TODO: REMOVE

GameRessource::GameRessource() {
}

GameRessource::~GameRessource() {
}

void GameRessource::load() {
    loadMesh(&manStand, "Mini-Figures\\Pilot\\VLPstand.lws");
    loadMesh(&manWalk, "Mini-Figures\\Pilot\\VLPwalk.lws");
    loadMesh(&manRun, "Mini-Figures\\Pilot\\VLPrun.lws");
    
    loadShape(&crystal, "MiscAnims\\Crystal\\VLP_Greencrystal.lwo");
    loadShape(&ore, "MiscAnims\\Ore\\Ore1st.lwo");
    
    LoadActivity(&pilot, "Mini-Figures\\Pilot\\pilot.ae");
    
    LoadActivity(&barracks, "Buildings\\Barracks\\Barracks.ae");
    LoadActivity(&bigTeleport, "Buildings\\BigTeleport\\BigTeleport.ae");
    LoadActivity(&smallTeleport, "Buildings\\Teleports\\Teleports.ae");
    LoadActivity(&docks, "Buildings\\Docks\\Docks.ae");
    //LoadActivity(&fence, "Buildings\\E-Fence\\E-Fence.ae"); //Not an AE-File
    LoadActivity(&geodome, "Buildings\\Geo-Dome\\Geo-Dome.ae");
    LoadActivity(&gunstation, "Buildings\\Gunstation\\Gunstation.ae");
    LoadActivity(&orerefinery, "Buildings\\OreRefinery\\Orerefinery.ae");
    LoadActivity(&powerstation, "Buildings\\Powerstation\\Powerstation.ae");
    LoadActivity(&toolstation, "Buildings\\Toolstation\\toolstation.ae");
    LoadActivity(&upgrade, "Buildings\\Upgrade\\Upgrade.ae");
    
    LoadActivity(&smallTruck, "Vehicles\\SmallTruck\\SmallTruck.ae");
    
    loadShape(&smallWheel, "Vehicles\\SmallTruck\\LP_SmallWheel.lwo");
    /*loadShape(&smallBucket, "World\\Shared\\sbucket.lwo");
    loadShape(&smallDash, "Vehicles\\SmallTruck\\standard_dashboard.lwo");
    loadShape(&standardRear, "Vehicles\\SmallTruck\\standard_rear.lwo");*/
    
    loadShape(&smallBucket, "World\\shared\\sbucket.lwo");
    loadShape(&smallDash, "Vehicles\\SmallTruck\\standard_dashboard.lwo");
    loadShape(&standardRear, "Vehicles\\SmallTruck\\standard_rear.lwo");
    
    
    LoaderWad* loader1 = App::getApp()->getDataLoader1();
    int fileId = loader1->getFileId("Lego.cfg");
    void* mem = loader1->loadFileData(fileId);
    m_cfgLoader.load(mem, loader1->getFileSize(fileId));
    loader1->freeFileData(mem);
    
    
    loadUpgrades();
    
    loadActivities("VehicleTypes", ObjectInfo::TYPE_VEHICLE);
    loadActivities("MiniFigureTypes", ObjectInfo::TYPE_FIGURE);
    loadActivities("RockMonsterTypes", ObjectInfo::TYPE_MONSTER);
    loadActivities("BuildingTypes", ObjectInfo::TYPE_BUILDING);
    
    loadStats();
    
    addInfos(App::getApp()->getGame());
}

void GameRessource::postLoad() {
    //Color Crystal
    for (int a = 0; a < crystal->getNumShapes(0); a++){
        crystal->getShape(0, a)->setColor(glm::vec3(0,1,0));
    }
}

void GameRessource::loadUpgrades() {
    auto upgradeGroup = m_cfgLoader.getMainGroup()->getGroup("UpgradeTypes");
    
    for (int a = 0; a < upgradeGroup->getNumChildren(); a++){
        auto group = upgradeGroup->getChild(a);
        
        std::string lName = group->getName();
        std::transform(lName.begin(), lName.end(), lName.begin(), ::tolower);
        
        m_upgrades[lName] = group->getString() + ".lwo";
        m_upgradesAI[lName] = loadAnimInfo(group->getString());
        //std::cout << group->getName() << " - " << group->getString() << std::endl;
    }
}

void GameRessource::loadActivities(const std::string& groupName, ObjectInfo::ObjType type) {
    auto groupList = m_cfgLoader.getMainGroup()->getGroup(groupName);
    
    for (int a = 0; a < groupList->getNumChildren(); a++){
        auto group = groupList->getChild(a);
        
        std::string file = group->getString();
        file = file.substr(0, file.rfind(',')); //TODO: Support Walker (two AE-Files)
                
        loadActivity(getInfo(group->getName()), file);
        getInfo(group->getName())->addAnimInfo(loadAnimInfo(file));
    }
}

void GameRessource::loadActivity(ObjectInfo* obj, const std::string& path) {
    std::string file =  path + "\\" + path.substr(path.rfind('\\') + 1, std::string::npos) + ".ae";
    
    LoaderCfg aeFile;
    LoaderWad* loader1 = App::getApp()->getDataLoader0();
    int fileId = loader1->getFileId(file);
    void* mem = loader1->loadFileData(fileId);
    aeFile.load(mem, loader1->getFileSize(fileId));
    loader1->freeFileData(mem);
    auto mainGroup = aeFile.getMainGroup();
    
    //Load Nulls
    obj->cameraNull  = mainGroup->getString("CameraNullName");
    obj->carryNull   = mainGroup->getString("CarryNullName");
    obj->drillNull   = mainGroup->getString("DrillNullName");
    obj->wheelNull   = mainGroup->getString("WheelNullName");
    obj->depositNull = mainGroup->getString("DepositNullName");
    obj->driverNull  = mainGroup->getString("DriverNullName");
    obj->toolNull    = mainGroup->getString("ToolNullName");
    
    //Upgrades
    auto upgradeGroup = mainGroup->getGroup("Upgrades");
    if (upgradeGroup){
        //std::cout << std::endl << "[" << obj->nameInternal << "]" << std::endl;
        for (int a = 0; a < 16; a++){
            std::bitset<4> upgrades(a);
            auto levelGroup = upgradeGroup->getGroup("Level" + upgrades.to_string());
            
            if (levelGroup){
                for (int a = 0; a < levelGroup->getNumChildren(); a++){
                    auto group = levelGroup->getChild(a);
                    
                    ObjectInfo::Upgrade* upgrade = new ObjectInfo::Upgrade();
                    upgrade->null = group->getFieldString(',',  0);
                    upgrade->slot = group->getFieldInt(',',  1) - 1; //LRR starts slots with 1
                    upgrade->upgrades = upgrades;
                    
                    std::string mesh = group->getName();
                    std::transform(mesh.begin(), mesh.end(), mesh.begin(), ::tolower);
                    mesh = m_upgrades[mesh];
                    loadShape(&(upgrade->mesh), mesh);
                    
                    obj->addUpgrade(upgrade);
                    //std::cout << mesh << " (" << upgrade->mesh << ") at " << upgrade->null << "(" << upgrade->slot << ")" << std::endl;
                }
            }
        }
    }
    
    //Load Activities
    auto activiGroup = mainGroup->getGroup("Activities");
    for (int a = 0; a < activiGroup->getNumChildren(); a++){
        auto group = activiGroup->getChild(a);
        auto aGroup = mainGroup->getGroup(group->getString());
        
        std::string name = rEM(group->getName());
        std::string file = path + "\\" + aGroup->getString("file") + ".lws";
        
        ObjectInfo::ActivityNew* activity = new ObjectInfo::ActivityNew();
        activity->transCoef = aGroup->getFloat("transcoef");
        activity->trigger   = aGroup->getInt("trigger");
        activity->name      = name;
        
        loadMesh(&(activity->mesh), file);
        
        obj->addActivity(activity);
    }
}

void GameRessource::loadStats() {

}

ObjectInfo* GameRessource::getInfo(const std::string& internalName) {
    std::string lName = internalName;
    
     std::transform(lName.begin(), lName.end(), lName.begin(), ::tolower);
    
    for (auto& obj: m_objInfo){
        if (obj->nameInternal == lName){
            return obj;
        }
    }
    
    m_objInfo.push_back(new ObjectInfo());
    m_objInfo.back()->nameInternal = lName;
    return m_objInfo.back();
}

std::string GameRessource::rEM(const std::string& text) {
    if (text[0] != '!'){
        return text;
    }else{
        return text.substr(1, std::string::npos);
    }
}

void GameRessource::addInfos(Game* game) {
    addInfo(game, "SmallTruck", Game::OBJ_SMALLTRANSPORTER);
    addInfo(game, "Hoverboard", Game::OBJ_HOVERSCOUT);
    addInfo(game, "LargeDigger", Game::OBJ_CHROMECRUSHER);
    addInfo(game, "SmallDigger", Game::OBJ_SMALLDRILL);
}

void GameRessource::addInfo(Game* game, const std::string& name, Game::ObjectType id) {
    ObjectInfo* info = getInfo(name);
    
    info->id = id;
    
    game->addObjectInfo(info);
}


AnimInfo* GameRessource::loadAnimInfo(const std::string& path) {
    AnimInfo* info = nullptr;
    LoaderFile* loader = App::getApp()->getFileLoader();
    
    if (loader->isFile(path + ".lws")){//Load Lightwave Scene
        std::cout << "[ERROR] Can't load lws as AnimInfo (use AE file instead)" << std::endl;
    }else if (loader->isFile(path + ".lwo")){ //Load Lgihtwave Object
        //std::cout << "[INFO] load AnimInfo: " + path + ".lwo" << std::endl;
        info = new AnimInfo();
        
        AnimInfo::Activity* activity = new AnimInfo::Activity();
        
        activity->name = ""; //Default
        loadShape(&(activity->mesh), path + ".lwo");
        
        info->addActivity(activity);
    }else{
        std::string file =  path + "\\" + path.substr(path.rfind('\\') + 1, std::string::npos) + ".ae";
    
        //std::cout << "[INFO] load AnimInfo: " + file << std::endl;
        
        info = new AnimInfo();
        
        LoaderCfg aeFile;
        LoaderWad* loader1 = App::getApp()->getDataLoader0();
        int fileId = loader1->getFileId(file);
        void* mem = loader1->loadFileData(fileId);
        aeFile.load(mem, loader1->getFileSize(fileId));
        loader1->freeFileData(mem);
        auto mainGroup = aeFile.getMainGroup();

        //Load Nulls
        info->cameraNull  = mainGroup->getString("CameraNullName");
        info->carryNull   = mainGroup->getString("CarryNullName");
        info->drillNull   = mainGroup->getString("DrillNullName");
        info->wheelNull   = mainGroup->getString("WheelNullName");
        info->depositNull = mainGroup->getString("DepositNullName");
        info->driverNull  = mainGroup->getString("DriverNullName");
        info->toolNull    = mainGroup->getString("ToolNullName");

        if (mainGroup->getGroup("WheelMesh")){
            std::string mesh = mainGroup->getString("WheelMesh");
            std::transform(mesh.begin(), mesh.end(), mesh.begin(), ::tolower);
            /*for (auto& up: m_upgradesAI){
                std::cout << up.first << " = " << up.second << std::endl;
            }*/
            if (mesh!="null_object"){
                info->wheel = loadAnimInfo(path + "\\" + mesh);
            }
        }
        
        //Upgrades
        auto upgradeGroup = mainGroup->getGroup("Upgrades");
        if (upgradeGroup){
            //std::cout << std::endl << "[" << obj->nameInternal << "]" << std::endl;
            for (int a = 0; a < 16; a++){
                std::bitset<4> upgrades(a);
                auto levelGroup = upgradeGroup->getGroup("Level" + upgrades.to_string());

                if (levelGroup){
                    for (int a = 0; a < levelGroup->getNumChildren(); a++){
                        auto group = levelGroup->getChild(a);

                        AnimInfo::Upgrade* upgrade = new AnimInfo::Upgrade();
                        upgrade->null = group->getFieldString(',',  0);
                        upgrade->slot = group->getFieldInt(',',  1) - 1; //LRR starts slots with 1
                        upgrade->upgrades = upgrades;

                        std::string mesh = group->getName();
                        std::transform(mesh.begin(), mesh.end(), mesh.begin(), ::tolower);
                        //mesh = m_upgrades[mesh];
                        upgrade->mesh = m_upgradesAI[mesh];
                        //loadShape(&(upgrade->mesh), mesh);

                        info->addUpgrade(upgrade);
                        //std::cout << mesh << " (" << upgrade->mesh << ") at " << upgrade->null << "(" << upgrade->slot << ")" << std::endl;
                    }
                }
            }
        }

        //Load Activities
        auto activiGroup = mainGroup->getGroup("Activities");
        for (int a = 0; a < activiGroup->getNumChildren(); a++){
            auto group = activiGroup->getChild(a);
            auto aGroup = mainGroup->getGroup(group->getString());

            std::string name = rEM(group->getName());
            std::string file = path + "\\" + aGroup->getString("file") + ".lws";

            AnimInfo::Activity* activity = new AnimInfo::Activity();
            activity->transCoef = aGroup->getFloat("transcoef");
            activity->trigger   = aGroup->getInt("trigger");
            activity->name      = name;

            loadMesh(&(activity->mesh), file);

            info->addActivity(activity);
        }
    }
    
    
    return info;
}
