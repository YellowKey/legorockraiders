/* 
 * File:   ActionPickup.cpp
 * Author: Kevin
 * 
 * Created on 28. August 2015, 14:15
 */

#include "ActionCollect.h"
#include "Ressource.h"
#include "App.h"

ActionCollect::ActionCollect(Ressource* ressource):m_ressource(ressource){
    
}

ActionCollect::~ActionCollect() {
    
}

void ActionCollect::start() {
    m_startTime = App::getApp()->getGameTime();
    getUnit()->setStatus(Unit::COLLECT);
    getUnit()->pickup(m_ressource);
}

void ActionCollect::updateUnit() {
    if (App::getApp()->getGameTime() - m_startTime > m_time){
        finish();
    }
}

void ActionCollect::cancel() {
    getUnit()->drop();
}




