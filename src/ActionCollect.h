/* 
 * File:   ActionPickup.h
 * Author: Kevin
 *
 * Created on 28. August 2015, 14:15
 */

#ifndef ACTIONPICKUP_H
#define	ACTIONPICKUP_H

#include "Action.h"

class Ressource;

class ActionCollect: public Action{
public:
    ActionCollect(Ressource* ressoure);
    ActionCollect(const ActionCollect& orig) = delete;
    virtual ~ActionCollect();
    
    virtual void start() override;
    virtual void updateUnit() override;
    virtual void cancel() override;
    
private:
    Ressource* m_ressource = nullptr;
    float m_time = 1.2f;
    float m_startTime;
};

#endif	/* ACTIONPICKUP_H */

