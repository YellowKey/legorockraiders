/* 
 * File:   WidgetIconPanel.cpp
 * Author: Kevin
 * 
 * Created on 22. Juni 2015, 17:25
 */

#include "WidgetIconPanel.h"

WidgetIconPanel::WidgetIconPanel() {
}

WidgetIconPanel::~WidgetIconPanel() {
}

void WidgetIconPanel::enableBackButton() {
    m_hasBack = true;
}

void WidgetIconPanel::setPanelTexture(Texture* texture) {
    m_texture = texture;
    
    setBaseSize(texture->getSize());
}

void WidgetIconPanel::setIconOffset(const glm::vec2& offset) {
    m_offset = offset;
}


void WidgetIconPanel::onPaint(Renderer2D* renderer) {
    renderer->renderTexture(m_texture, getAbsPos(), getSize());
}

void WidgetIconPanel::onUpdate() {
    const int count = getNumChildren();
    
    glm::vec2 pos = m_offset;
    
    int off = (m_hasBack)?(1):(0);
    
    for (int a = off; a < count; a++){
        Widget* widget = getChild(a);
        
        widget->setRelPos(pos);
        pos += glm::vec2(0, widget->getBaseSize().y);
    }
}
