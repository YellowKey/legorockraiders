/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   LoaderMap.h
 * Author: Kevin
 *
 * Created on 30. Dezember 2015, 17:24
 */

#ifndef LOADERMAP_H
#define LOADERMAP_H

#include <vector>
#include <string>

class LoaderMap {
public:
    LoaderMap();
    LoaderMap(const LoaderMap& orig) = delete;
    virtual ~LoaderMap();
    
    bool load(const std::string& file);
    
    uint16_t getData(int x, int y) const;
    int getWidth() const;
    int getHeight() const;
private:
    std::vector<uint16_t> m_data;
    int m_width  = 0;
    int m_height = 0;
    bool m_loaded = false;
};

#endif /* LOADERMAP_H */

