/* 
 * File:   Activity.h
 * Author: Kevin
 *
 * Created on 13. Juni 2015, 14:45
 */

#ifndef ACTIVITY_H
#define	ACTIVITY_H

#include "Mesh.h"

class Activity {
public:
    enum AnimationId{
        ANIM_STAND,
        ANIM_MPSTAND,
        ANIM_ROUTE,
        ANIM_MPROUTE,
        ANIM_DRILL,
        ANIM_MPDRILL,
        ANIM_TELEPORT,
        ANIM_TELEPORTIN,
        ANIM_WALK,
        ANIM_RUNPANIC,
        ANIM_REINFORCE,
        ANIM_REVERSE,
        ANIM_TURNLEFT,
        ANIM_TURNRIGHT,
        ANIM_CANTDO,
        ANIM_EMERGE,
        ANIM_COLLECT,
        ANIM_CLEAR,
        ANIM_CARRY,
        ANIM_THROW,
        ANIM_CARRYTURNLEFT,
        ANIM_CARRYTURNRIGHT,
        ANIM_CARRYSTAND,
        ANIM_EXPLODE,
        ANIM_UNPOWERED,
        ANIM_DEPOSITE,
        ANIM_DYNAITE,
        ANIM_PLACE,
        ANIM_REPAIR,
        ANIM_REST,
        ANIM_ROUTERUBBLE,
        ANIM_CARRYROUTE,
        ANIM_EAT,
        ANIM_FIRELASER,
        ANIM_GETUP,
        ANIM_THROWNBYROCKMONSTER,
        ANIM_SLIP,
        ANIM_TRAIN, 
        ANIM_RECHARGE,
        ANIM_WAITING1,
        ANIM_WAITING2,
        ANIM_WAITING3,
        ANIM_WAITING4,
        
        NUMANIMATIONS,
    };
    Activity();
    Activity(const Activity& orig);
    virtual ~Activity();
    
    bool  hasAnim(int anim);
    Mesh* getAnimMesh(int anim);
    float getAnimTransCoef(int anim);
    
    void setAnimMesh(int anim, Mesh* mesh);
    void setAnimTransCoef(int anim, float coeff);
    
    static int getNumAnimations();
    static std::string getAnimationName(int anim);
    
    bool isIdValid(int anim);
private:
    struct Animation{
        Mesh* mesh       = nullptr;
        float transCoeff = 0;
    };
    
    std::array<Animation, NUMANIMATIONS> m_animations;
    
    static std::array<std::string, NUMANIMATIONS> m_animationNames;
    
    static void initNames();
};

#endif	/* ACTIVITY_H */

