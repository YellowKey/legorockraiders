/* 
 * File:   Shape.h
 * Author: Kevin
 *
 * Created on 13. Mai 2015, 18:17
 */

#ifndef SHAPE_H
#define	SHAPE_H

#include <glm/vec3.hpp>
#include <glm/vec2.hpp>

#include "Texture.h"
#include "glBuffer.h"

class Shape {
public:
    enum TextureSlot{
        TEXTURE_COLOR,
        NUM_TEXTURE_SLOTS,
    };
    Shape();
    Shape(const Shape& orig) = delete;
    ~Shape();
    
    //Local Data
    void addVertex(const glm::vec3& pos, const glm::vec2& uv, const glm::vec3& normal = glm::vec3(0, 1, 0));
    
    int getNumVertices();
    int getVertexSize();
    int getDataSize();
    
    void setAdditiveBlending(bool state);
    bool getAdditiveBlending();
    
    void setLuminance(float lumi);
    float getLuminance();
    
    void clearLocalData();
    
    void writeDataTo(void* data);
    const void* getData();
    
    //GPU data
    int getBufferOffset();
    glBuffer* getBuffer();
    void setBuffer(glBuffer* buffer, int offset);
    
    void setTexture(TextureSlot slot, Texture* texture);
    Texture* getTexture(TextureSlot slot);
    void setColor(const glm::vec3& color);
    glm::vec3 getColor();
private:
    friend class Engine3D;
    
    float m_luminance = 0.0f;
    float m_diffuse;
    float m_specular;
    bool  m_additiveBlending = false;
    glm::vec3 m_color = {1,1,1};
    
    struct Vertex{
        glm::vec3 pos;
        glm::vec2 uv;
        glm::vec3 normal;
    };
    struct Surface{
        Texture* texture;
        bool additive;
    };
    
    std::vector<Vertex> m_vertices;
    
    Surface m_surfaces[NUM_TEXTURE_SLOTS];
    
    glBuffer* m_buffer;
    int       m_bufferOffset;
};

#endif	/* SHAPE_H */

