/* 
 * File:   WidgetMesssagePanel.h
 * Author: kevin
 *
 * Created on June 25, 2015, 12:16 PM
 */

#ifndef WIDGETMESSSAGEPANEL_H
#define	WIDGETMESSSAGEPANEL_H

#include "Widget.h"
#include "WidgetText.h"
#include "InterfaceRessorces.h"

class WidgetMesssagePanel:public Widget{
public:
    WidgetMesssagePanel();
    WidgetMesssagePanel(const WidgetMesssagePanel& orig) = delete;
    virtual ~WidgetMesssagePanel();
    
    void setTextureMain(Texture* texture);
    void setTextureAirBar(Texture* texture);
    void setTextureNoAir(Texture* texture);
    void setTextureInfo(Texture* texture);
    
    void setTextures(InterfaceRessources* ress);
    
    void setAir(float value);
    
    void setFont(Font* font);
    void setText(const std::string text);
public:
    virtual void onPaint(Renderer2D* renderer) override;
private:
    const glm::vec2 m_infoOffset = {274, 129};
    const glm::vec2 m_noAirOffset = {21, 177};
    const glm::vec2 m_airOffset = {85, 186};
    
    const glm::vec2 m_airSize = {236, 8};
    
    WidgetText m_widgetText;
    
    float m_air = 1.0f;
    
    Texture* m_textureMain   = nullptr;
    Texture* m_textureAirBar = nullptr;
    Texture* m_textureNoAir  = nullptr;
    Texture* m_textureInfo   = nullptr;
};

#endif	/* WIDGETMESSSAGEPANEL_H */

