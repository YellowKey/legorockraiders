/* 
 * File:   ActionClear.cpp
 * Author: Kevin
 * 
 * Created on 18. August 2015, 15:50
 */

#include "ActionClear.h"
#include "App.h"
#include "Ressource.h"

#include <iostream>

ActionClear::ActionClear(const glm::ivec2& tilePos):m_tilePos(tilePos){
}

ActionClear::~ActionClear() {
}

void ActionClear::start() {
    m_startTime = App::getApp()->getGameTime();
    getUnit()->setStatus(Unit::CLEAR);
}

void ActionClear::updateUnit() {
    if (App::getApp()->getGameTime() - m_startTime > m_cleanTime){
        GameMap* map = getUnit()->getGameMap();
        //map->destroy(m_wallPos.x, m_wallPos.y);
        map->setTileType(m_tilePos.x, m_tilePos.y, GameMap::TILE_GROUND);
        map->update(m_tilePos.x, m_tilePos.y);
        
        for (int a = 0; a < 3; a++){
            Ressource* res = new Ressource(Ressource::TYPE_ORE);
            
            int s = App::getApp()->getGameMap()->getTileSize();
            int px = m_tilePos.x * s + rand() % s - s/2;
            int py = m_tilePos.y * s + rand() % s - s/2;

            res->setPosMap(glm::vec2(px, py));
        }
        
        finish();
    }
}

void ActionClear::cancel() {
    std::cout << "Cancel" << std::endl;
    Action::cancel();
}



