/* 
 * File:   Object.h
 * Author: Kevin
 *
 * Created on 26. April 2015, 15:32
 */

#ifndef OBJECT_H
#define	OBJECT_H

class Object {
public:
    Object();
    Object(const Object& orig);
    virtual ~Object();
private:

};

#endif	/* OBJECT_H */

