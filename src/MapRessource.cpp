/* 
 * File:   MapRessource.cpp
 * Author: Kevin
 * 
 * Created on 28. Mai 2015, 12:13
 */

#include "MapRessource.h"

#include "App.h"
#include "LoaderBmp.h"
#include "GameMap.h"
#include "Shape.h"

#include <iostream>
#include <bitset>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/noise.hpp>
#include <string.h>


MapRessource::MapRessource(const std::string& biome) {
    m_biome = biome;
}

MapRessource::~MapRessource() {
    delete m_textureTiles;
    delete m_textureNoise;
}

void MapRessource::load() {
    if (!m_loaded){
        loadShape( 0, 0b0000); //Ground
        loadShape(10, 0b0000); 
        loadShape(11, 0b0000); 
        loadShape(12, 0b0000); 
        loadShape(13, 0b0000); 

        loadShape( 1, 0b1100); //Sides
        loadShape( 2, 0b1100);
        loadShape( 3, 0b1100);
        loadShape( 4, 0b1100);
        loadShape( 5, 0b1100);

        loadShape(31, 0b1101); //EdgeIn
        loadShape(32, 0b1101); 
        loadShape(33, 0b1101); 
        loadShape(34, 0b1101); 
        loadShape(35, 0b1101); 

        loadShape(20, 0b1100); //Special
        loadShape(40, 0b1100);
        loadShape(67, 0b1100);

        loadShape(51, 0b1000); //EdgeOut
        loadShape(52, 0b1000); 
        loadShape(53, 0b1000); 
        loadShape(54, 0b1000); 
        loadShape(55, 0b1000); 

        loadShape(77, 0b01010, true); //EdgeInner

        /*loadShape( 6, 0b0000); //Lava
        loadShape(16, 0b0000);
        loadShape(26, 0b0000);
        loadShape(36, 0b0000);*/
        loadShape(46, 0b0000, false, true);
        
        loadShape(45, 0b0000); //Water

        loadShape(61, 0b0000); //PowerPath build

        loadShape(60, 0b0000); //PowerPath Off
        loadShape(62, 0b0000);
        loadShape(63, 0b0000);
        loadShape(64, 0b0000);
        loadShape(65, 0b0000);

        loadShape(71, 0b0000); //PowerPath On
        loadShape(72, 0b0000);
        loadShape(73, 0b0000);
        loadShape(74, 0b0000);
        loadShape(75, 0b0000);

        loadShape(66, 0b00000);//BuildingBase
        loadShape(76, 0b00000);

        loadShape(70, 0b11111); //Top
        
        loadShape(30, 0b0000); //SlimySlugHole
        
        loadTextureTiles();
        
        m_loaded = true;
    }
}

Mesh* MapRessource::getShape(int type) {
    return m_meshes[type];
}

void MapRessource::loadShape(int id, int heightField, bool swap, bool light) {
    std::bitset<4> hs(heightField);
    auto* loader = App::getApp()->getDataLoader0();
    LoaderBmp bmpLoader;
    
    std::string name = "";
    std::string file;
    
    if ( id < 10){
        name += "0";
    }
    name += std::to_string(id);
    
    file = "World\\WorldTextures\\" + m_biome + "Split\\" + m_biome + name + ".bmp";
    
    if (id == 70){
        //file = "World\\WorldTextures\\" + m_biome + "Roof.bmp";
    }
    
    //std::cout << file << std::endl;
    
    int fileId = loader->getFileId(file);
    void * data = loader->loadFileData(fileId);
    bmpLoader.load(data, loader->getFileSize(fileId));
    bmpLoader.convertToRGB();
    Texture* texture = new Texture(bmpLoader.getData(), bmpLoader.getWidth(), bmpLoader.getHeight(), Texture::RGB8);
    loader->freeFileData(data);
    
    if (id == 0){
        m_texture = texture;
    }
    
    Shape* shape  = new Shape();
    
    shape->setTexture(Shape::TEXTURE_COLOR, texture);
    
    if (light){
        shape->setLuminance(0.5f);
    }
    
    float s = 0.5f * GameMap::getTileSize();
    float h = GameMap::getTileHeight();
    float t = 1;
    
    glm::vec3 v0 = glm::vec3(-s, h * hs[0], -s);
    glm::vec3 v1 = glm::vec3( s, h * hs[1], -s);
    glm::vec3 v2 = glm::vec3( s, h * hs[2],  s);
    glm::vec3 v3 = glm::vec3(-s, h * hs[3],  s);
    
    if (!swap){
        glm::vec3 n0 = glm::normalize(glm::cross(v1 - v3, v0 - v3));
        glm::vec3 n1 = glm::normalize(glm::cross(v3 - v1, v2 - v1));
        
        shape->addVertex(v3, glm::vec2( 0,  t), n0);
        shape->addVertex(v0, glm::vec2( 0,  0), n0);
        shape->addVertex(v1, glm::vec2( t,  0), n0);

        shape->addVertex(v1, glm::vec2( t,  0), n1);
        shape->addVertex(v2, glm::vec2( t,  t), n1);
        shape->addVertex(v3, glm::vec2( 0,  t), n1);
    }else{
        glm::vec3 n0 = glm::normalize(glm::cross(v0 - v2, v3 - v2));
        glm::vec3 n1 = glm::normalize(glm::cross(v2 - v0, v1 - v0));
        
        shape->addVertex(v2, glm::vec2( t,  t), n0);
        shape->addVertex(v3, glm::vec2( 0,  t), n0);
        shape->addVertex(v0, glm::vec2( 0,  0), n0);
        
        shape->addVertex(v0, glm::vec2( 0,  0), n1);
        shape->addVertex(v1, glm::vec2( t,  0), n1);
        shape->addVertex(v2, glm::vec2( t,  t), n1);
    }
    
    Mesh* mesh = new Mesh();
    
    int groupId = mesh->addGroup();
    
    mesh->addShape(groupId, shape);
    
    App::getApp()->getEngine3D()->addMesh(mesh);
    
    m_meshes[id] = mesh;
}

void MapRessource::loadTextureTiles() {
    auto* loader = App::getApp()->getDataLoader0();
    LoaderBmp bmpLoader;
    
    const int size = 128;
    const int layer = 100;
    
    const int imgSize = size * size * 3;
    
    std::vector<uint8_t> m_data(imgSize * layer);
    
    for (int a = 0; a < layer; a++){
        std::string index = std::to_string(a);
        if (a < 10){
            index = "0" + index;
        }
        std::string file = "World\\WorldTextures\\" + m_biome + "Split\\" + m_biome + index + ".bmp";

        int fileId = loader->getFileId(file);
        if (fileId >= 0){
            void * data = loader->loadFileData(fileId);
            bmpLoader.load(data, loader->getFileSize(fileId));
            bmpLoader.convertToRGB();

            uint8_t* texture = m_data.data() + imgSize * a;
            if (bmpLoader.getWidth() == size && bmpLoader.getHeight() == size){
                memcpy(texture, bmpLoader.getData(), imgSize);
            }else{
                std::cout << "Scale: " << a << std::endl;
                uint8_t* bmpData = (uint8_t*)bmpLoader.getData();
                for (int y = 0; y < size; y++){
                    for (int x = 0; x < size; x++){
                        int lx = x * bmpLoader.getWidth() / size;
                        int ly = y * bmpLoader.getHeight() / size;
                        int offset = (lx + ly * bmpLoader.getWidth()) * 3; 
                        *(texture++) = *(bmpData + offset + 0);
                        *(texture++) = *(bmpData + offset + 1);
                        *(texture++) = *(bmpData + offset + 2);
                    }
                }
                //memcpy(texture, bmpLoader.getData(), imgSize / 4);
            }

            loader->freeFileData(data);
        }
    }
    
    m_textureTiles = new glTexture(GL_TEXTURE_2D_ARRAY);
    
    m_textureTiles->texImage3D(0, GL_RGB8, size, size, layer, GL_BGR, GL_UNSIGNED_BYTE, m_data.data());

    m_textureTiles->textureParameters(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    m_textureTiles->textureParameters(GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    m_textureTiles->textureParameters(GL_TEXTURE_BASE_LEVEL, 0);
    m_textureTiles->textureParameters(GL_TEXTURE_MAX_LEVEL,  0);
    
    
    int sizeNoise = 1024;
    std::vector<uint32_t> dataNoise(sizeNoise * sizeNoise);
    
    for(int y = 0; y < sizeNoise; y++){
        for (int x = 0; x < sizeNoise; x++){
            float noise = glm::simplex(glm::vec2(0.05f * x, 0.05f * (y+100)));
            uint32_t noiseI = (uint32_t)(noise * (255.0f * 0.5f) + (255.0f * 0.5f)) & 0xFF;
            //uint32_t noiseI = (uint32_t)(noise * (255.0f)) & 0xFF;
            dataNoise[x + y * sizeNoise] = (0xFF << 24) + (noiseI << 16) + (noiseI << 8) + (noiseI << 0); 
            //std::cout << noise << " => " << (noise * (255.0f * 0.5f) + (255.0f * 0.5f)) << " => "<< std::hex << noiseI << "->" << dataNoise[x + y * sizeNoise] << std::dec << std::endl;
        }
    }
    
    m_textureNoise = new glTexture(GL_TEXTURE_2D);
    
    m_textureNoise->texImage2D(0, GL_RGBA8, sizeNoise, sizeNoise, GL_RGBA, GL_UNSIGNED_BYTE, dataNoise.data());
    
    m_textureNoise->textureParameters(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    m_textureNoise->textureParameters(GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    m_textureNoise->textureParameters(GL_TEXTURE_BASE_LEVEL, 0);
    m_textureNoise->textureParameters(GL_TEXTURE_MAX_LEVEL,  0);
}
