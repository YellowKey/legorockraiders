
/* 
 * File:   LoaderWad.h
 * Author: Kevin
 *
 * Created on 27. April 2015, 19:13
 */

#ifndef WADLOADER_H
#define	WADLOADER_H

#include <string>
#include <stdio.h>
#include <vector>

class LoaderWad {
public:
    LoaderWad(const std::string& filename);
    LoaderWad(const LoaderWad& orig) = delete;
    ~LoaderWad();
    
    void readHeader();
    
    int getNumFiles();
    std::string getFileName(int id);
    int getFileSize(int id);
    int getFileId(const std::string& name);
    
    void* loadFileData(int id);
    void  freeFileData(void* data);
private:
    int getFileOffset(int id);
    
    std::string readString(int maxLength = 0);
    int readInt();
    
    
    struct File{
        std::string absDir; //absDir not required
        std::string relName;
        
        int version;
        int size;
        int thing; //seems to be the same as size
        int offset;
    };
    
    std::vector<File> m_files;
    FILE * m_file;
    std::string m_filename;
};

#endif	/* WADLOADER_H */

