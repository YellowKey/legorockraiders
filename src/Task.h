/* 
 * File:   task.h
 * Author: Kevin
 *
 * Created on 19. August 2015, 13:13
 */

#ifndef TASK_H
#define	TASK_H

#include <glm/vec2.hpp>
#include <vector>


class Action;
class TaskManager;
class Unit;

class Task {
public:
    enum TaskType{
        TASK_BUILD,
        TASK_DRILL,
        TASK_CLEAR,
        TASK_ORE,
        TASK_CRYSTAL,
        TASK_REPAIR,
        TASK_DRIVE,
        TASK_REINFORCE,
        TASK_RECHARGE,
        TASK_UNKOWN,
    };
    
    Task();
    Task(const Task& orig);
    virtual ~Task();
    
    virtual glm::vec2 getPos();
    virtual int getLocalPriority(); //used to order tasks of the same type
    virtual TaskType getTaskType();
    
    virtual bool assignUnit(Unit* unit);
    virtual bool isAssigned();
    virtual void unitCanceled();
    virtual void unitFinished();
    
    virtual bool operator==(const Task& task);
    
protected:
    void finish();
    Action* addAction(Action* action);
private:
    friend class TaskManager;
    void setTaskManager(TaskManager* manager);
    
    TaskManager* m_taskManager;
    
    std::vector<Action*> m_actions;
};

#endif	/* TASK_H */

