/* 
 * File:   TaskDrill.cpp
 * Author: Kevin
 * 
 * Created on 19. August 2015, 14:36
 */

#include "TaskDrill.h"
#include "GameMap.h"

#include "App.h"

#include "ActionWalk.h"
#include "ActionDrill.h"

TaskDrill::TaskDrill(const glm::vec2 &tile):m_tile(tile) {
    App::getApp()->getGameMap()->setTileMark(m_tile.x, m_tile.y, GameMap::MARK_DRILL, true);
}

TaskDrill::~TaskDrill() {
    App::getApp()->getGameMap()->setTileMark(m_tile.x, m_tile.y, GameMap::MARK_DRILL, false);
}

glm::vec2 TaskDrill::getPos() {
    return GameMap::tileToPos(m_tile);
}

Task::TaskType TaskDrill::getTaskType() {
    return TASK_DRILL;
}

bool TaskDrill::assignUnit(Unit* unit) {
    if (unit->canDrill()){
        std::array<glm::ivec2, 4> offsets = {{
            {glm::ivec2(0,1)}, 
            {glm::ivec2(0, -1)}, 
            {glm::ivec2(1, 0)}, 
            {glm::ivec2(-1, 0)}}};

        GameMap* map = App::getApp()->getGameMap(); //TODO: could cause problems later

        glm::ivec2 pos = m_tile;

        for (auto off: offsets){
            glm::ivec2 p = pos + off;
            if (!map->isWall(p.x, p.y)){
                glm::vec2 unitPos = map->tileToPos(pos) + glm::vec2(off) * 28.0f;
                //unit->cancelAllActions();
                unit->addAction(new ActionWalk(unitPos));
                unit->addAction(addAction(new ActionDrill(pos)));

                m_assigned = true;

                break;
            }
        }

        return true;
    }
    
    return false;
}

bool TaskDrill::isAssigned() {
    return m_assigned;
}

void TaskDrill::unitCanceled() {
    m_assigned = false;
}


bool TaskDrill::operator==(const Task& task) {
    const TaskDrill* taskDrill = dynamic_cast<const TaskDrill*>(&task);
    
    if (taskDrill && taskDrill->m_tile == m_tile){
        return true;
    }
    
    return false;
}
