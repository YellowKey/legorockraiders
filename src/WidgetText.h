/* 
 * File:   WidgetText.h
 * Author: Kevin
 *
 * Created on 21. Juni 2015, 16:09
 */

#ifndef WIDGETTEXT_H
#define	WIDGETTEXT_H

#include "Widget.h"
#include "Font.h"
#include <string>

class WidgetText: public Widget{
public:
    enum Mode{
        MODE_LEFT,
        MODE_RIGHT,
        MODE_CENTER,
    };
    WidgetText();
    virtual ~WidgetText();
    
    void setFont(Font* font);
    void setHoverFont(Font* font);
    void setClickFont(Font* font);
    
    void setMode(Mode mode);
    
    void setText(const std::string& text);
    
protected:
    virtual void onPaint(Renderer2D* renderer) override;
    
private:
    Font* getCurrentFont();
    
    Font* m_fontMain  = nullptr;
    Font* m_fontHover = nullptr;
    Font* m_fontClick = nullptr;
    
    Mode m_mode = MODE_CENTER;
    
    std::string m_text = "";
};

#endif	/* WIDGETTEXT_H */

