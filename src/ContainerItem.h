/* 
 * File:   ContainerItem.h
 * Author: Kevin
 *
 * Created on 26. September 2015, 12:23
 */

#ifndef CONTAINERITEM_H
#define	CONTAINERITEM_H

#include <vector>

class ContainerBase;

class ContainerItem {
public:
    ContainerItem();
    ContainerItem(const ContainerItem& orig);
    virtual ~ContainerItem();
private:
    friend class ContainerBase;
    
    void addToContainer(ContainerBase* container);
    void removeFromContainer(ContainerBase* container);
    
    std::vector<ContainerBase*> m_container;
};

#endif	/* CONTAINERITEM_H */

