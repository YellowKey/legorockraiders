/* 
 * File:   ActionWalk.cpp
 * Author: kevin
 * 
 * Created on July 9, 2015, 1:29 PM
 */

#include "ActionWalk.h"

#include "Pathfinder.h"
#include "App.h"

#include "util.h"

ActionWalk::ActionWalk(const glm::vec2& goal):m_goal(goal) {
    
}

ActionWalk::~ActionWalk() {
}

void ActionWalk::start() {
    m_gameMap = getUnit()->getGameMap();
    
    findPath(m_goal);
    
    getUnit()->setStatus(Unit::WALK);
}

void ActionWalk::updateUnit() {
    if (isWalking()){
        float time = App::getApp()->getGameTime() - m_startTime;
        
        while (time > m_nextNode.time && isWalking()){
            nextPathNode();
        }
        
        glm::vec2 diff = m_nextNode.pos - m_lastNode.pos;
        
        float rot = -atan2(diff.y, diff.x) - M_PI_2;
        
        float blend = (time - m_lastNode.time) / (m_nextNode.time - m_lastNode.time);
        blend = glm::clamp(blend, 0.0f, 1.0f);
        
        getUnit()->setRotf(rot);
        getUnit()->setPosMap(glm::mix(m_lastNode.pos, m_nextNode.pos, blend));
    }else{
        finish();
    }
}

void ActionWalk::cancel() {
    Action::cancel();
}


void ActionWalk::createPath(const Path& path, const glm::vec2& start, const glm::vec2& end) {
    m_path.clear();
    
    const glm::ivec2 startPoint = m_gameMap->posToTile(start);
    const glm::ivec2 endPoint   = m_gameMap->posToTile(end);
    
    const glm::vec2 startOff = start - m_gameMap->tileToPos(startPoint);
    const glm::vec2 endOff   = end  - m_gameMap->tileToPos(endPoint);
    
    pathNode currentNode;
    currentNode.pos = start;
    currentNode.time = 0.0f;
    
    m_lastNode = currentNode;
    
    const int numNodes = path.getNumNodes();
    
    for (int a = 0; a < numNodes; a++){
        pathNode prevNode = currentNode;
        
        float blend = (float(a + 1) / (numNodes));
        
        currentNode.pos = m_gameMap->tileToPos(path.getNodePos(numNodes - a - 1));
        currentNode.pos += glm::mix(startOff, endOff, blend);
        
        currentNode.time = prevNode.time + glm::distance(currentNode.pos, prevNode.pos) * 0.032f ;
        
        m_path.push_back(currentNode);
    }
    
    m_currentPathId = 0;
    m_startTime = App::getApp()->getGameTime();
    
    if (m_path.size()){
        m_nextNode = m_path[0];
    }
}

void ActionWalk::findPath(const glm::vec2& goal) {
    glm::vec2 start = getUnit()->getPosMap();
    glm::vec2 end   = goal;
    
    glm::ivec2 startPoint = m_gameMap->posToTile(start);
    glm::ivec2 endPoint   = m_gameMap->posToTile(end);
    
    Pathfinder pathfinder;
    
    pathfinder.setGroundSpeed(1.0f);
    pathfinder.setGravelSpeed(1.0f);
    pathfinder.setPathSpeed(1.0f);
    pathfinder.setMap(m_gameMap);
    pathfinder.setStartPoint(startPoint);
    pathfinder.setEndPoint(endPoint);
    pathfinder.pathfind();
    
    Path path = pathfinder.getPath();
    
    createPath(path, start, end);
}

bool ActionWalk::isWalking() {
    if (m_currentPathId < m_path.size()){
        return true;
    }
    
    return false;
}

void ActionWalk::nextPathNode() {
    m_currentPathId += 1;
    
    if (m_currentPathId >= m_path.size()){
        return;
    }
    
    m_lastNode = m_nextNode;
    m_nextNode = m_path[m_currentPathId];
}