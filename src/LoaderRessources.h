/* 
 * File:   LoaderRessources.h
 * Author: Kevin
 *
 * Created on 1. Oktober 2015, 12:14
 */

#ifndef LOADERRESSOURCES_H
#define	LOADERRESSOURCES_H

#include "RessourceContainer.h"

class LoaderRessources: public RessourceContainer{
public:
    LoaderRessources();
    LoaderRessources(const LoaderRessources& orig) = delete;
    virtual ~LoaderRessources();
    
    void load();
    
    Texture* background;
    Font*    font;
    Texture* bar;
private:

};

#endif	/* LOADERRESSOURCES_H */

