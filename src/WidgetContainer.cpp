/* 
 * File:   WigetContainer.cpp
 * Author: kevin
 * 
 * Created on July 1, 2015, 1:58 PM
 */

#include "WidgetContainer.h"

WidgetContainer::WidgetContainer() {
}

WidgetContainer::~WidgetContainer() {
}

bool WidgetContainer::isOver(const glm::vec2 relPos) {
    bool over = false;
    
    const int num = getNumChildren();
    
    for (int a = 0; a < num; a++){
        Widget* child = getChild(a);
        
        over = over || child->isOver(relPos - child->getRelPos());
    }
    
    return over;
}
