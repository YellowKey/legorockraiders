/* 
 * File:   WidgetCameraControl.h
 * Author: kevin
 *
 * Created on June 24, 2015, 12:12 PM
 */

#ifndef WIDGETCAMERACONTROL_H
#define	WIDGETCAMERACONTROL_H

#include "Widget.h"
#include "WidgetButton.h"
#include "InterfaceRessorces.h"

class WidgetCameraControl: public Widget{
public:
    WidgetCameraControl();
    WidgetCameraControl(const WidgetCameraControl& orig) = delete;
    virtual ~WidgetCameraControl();
    
    void setTexture(InterfaceRessources* ress);
    
private:
    virtual void onPaint(Renderer2D* renderer) override;

    Texture* m_textureMain  = nullptr;
    Texture* m_textureUp    = nullptr;
    Texture* m_textureDown  = nullptr;
    Texture* m_textureLeft  = nullptr;
    Texture* m_textureRight = nullptr;
    
    WidgetButton m_widgetZoomIn;
    WidgetButton m_widgetZoomOut;
    WidgetButton m_widgetNextUnit;
    
    const glm::vec2 m_arrowUpPos    = {67, 38};
    const glm::vec2 m_arrowDownPos  = {67, 10};
    const glm::vec2 m_arrowLeftPos  = {50, 25};
    const glm::vec2 m_arrowRightPos = {81, 25};
};

#endif	/* WIDGETCAMERACONTROL_H */

