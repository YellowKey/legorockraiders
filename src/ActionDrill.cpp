/* 
 * File:   ActionDrill.cpp
 * Author: kevin
 * 
 * Created on July 9, 2015, 1:29 PM
 */

#include "ActionDrill.h"

#include "App.h"
#include "util.h"

ActionDrill::ActionDrill(const glm::ivec2& wallPos):m_wallPos(wallPos){
}

ActionDrill::~ActionDrill() {
}

void ActionDrill::start() {
    m_startTime = App::getApp()->getGameTime();
    getUnit()->setStatus(Unit::DRILL);
    
    glm::vec2 delta = getUnit()->getGameMap()->tileToPos(m_wallPos) - getUnit()->getPosMap();
    
    float rot;
    if (glm::abs(delta.x) > glm::abs(delta.y)){
        if (delta.x < 0){
            rot = M_PI_2;
        }else{
            rot = M_PI + M_PI_2;
        }
    }else{
        if (delta.y < 0){
            rot = 0;
        }else{
            rot = M_PI;
        }
    }
    getUnit()->setRotf(rot);
}

void ActionDrill::updateUnit() {
    if (App::getApp()->getGameTime() - m_startTime > m_drillTime){
        GameMap* map = getUnit()->getGameMap();
        map->destroy(m_wallPos.x, m_wallPos.y);
    
        finish();
    }
}

void ActionDrill::cancel() {
    Action::cancel();
}


