/* 
 * File:   UnitFigure.h
 * Author: Kevin
 *
 * Created on 2. Juni 2015, 21:59
 */

#ifndef UNITFIGURE_H
#define	UNITFIGURE_H

#include "Unit.h"
#include "Activity.h"

class UnitFigure: public Unit{
public:
    UnitFigure();
    UnitFigure(const UnitFigure& orig) = delete;
    virtual ~UnitFigure();

    virtual void update() override;
    
    virtual int getSlotGroupId(int slotId) override;
    virtual EngineObject* getSlotEngineObject(int slotId) override;

protected:
    virtual void onStatusChange(Status status) override;
private:
    Activity* m_activity;
};

#endif	/* UNITFIGURE_H */

