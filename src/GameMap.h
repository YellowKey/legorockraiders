/* 
 * File:   GameMap.h
 * Author: Kevin
 *
 * Created on 25. Mai 2015, 17:16
 */

#ifndef GAMEMAP_H
#define	GAMEMAP_H

#include <string>
#include <vector>
#include <bitset>

#include "MapRessource.h"
#include "MapObject.h"

#include "EngineObject.h"

class GameMap {
public:
    enum TileMark{
        MARK_SELECTED  = 1,
        MARK_DRILL     = 2,
        MARK_DYNAMITE  = 3,
        MARK_CLEANUP   = 4,
        MARK_BUILD     = 5,
        MARK_BUILDPATH = 6,
    };
    enum TileType{
        //Defined by Lego Rock Raiders
        TILE_SOLID = 1,
        TILE_HARD = 2,
        TILE_LOOSE = 3,
        TILE_DIRT = 4,
        TILE_GROUND = 5,
        TILE_LAVA = 6,
        TILE_GRAVEL = 7,
        TILE_ORESEAM = 8,
        TILE_WATER = 9,
        TILE_CRYSTALSEAM = 10,
        TILE_RECHARGE = 11,
        TILE_SLUGHOLE = 12,
        
        //My Tiles
        TILE_BUILD,
        TILE_BUILDWALK,
        TILE_PATH,
    };
    
    enum TileRess{
        RESS_NONE = 0x00,
        
        RESS_CRYSTAL_1  = 0x01,
        RESS_CRYSTAL_3  = 0x05,
        RESS_CRYSTAL_5  = 0x09,
        RESS_CRYSTAL_11 = 0x0D,
        RESS_CRYSTAL_25 = 0x11,
        
        RESS_ORE_1  = 0x02,
        RESS_ORE_3  = 0x06,
        RESS_ORE_5  = 0x0A,
        RESS_ORE_11 = 0x0E,
        RESS_ORE_25 = 0x12,
    };
    
    enum TileVisib{
        VISIB_WALL = 0,
        VISIB_TRUE = 1,
        VISIB_FALSE = 2,
    };
    GameMap();
    GameMap(const GameMap& orig) = delete;
    virtual ~GameMap();
    
    int getWidth();
    int getHeight();
    
    void loadGameLevel(int id);
    void load(const std::string& path, const std::string& id);
    uint16_t* loadMapData(const std::string& name);
    
    void debugMap();
    
    //void render();
    //void renderTile(int type, int x, int y);
    
    int getHeightmap(int x, int y);
    
    TileType  getTileType (int x, int y);
    TileType  getTileType (const glm::ivec2& pos);
    TileRess  getTileRess (int x, int y);
    TileRess  getTileRess (const glm::ivec2& pos);
    TileVisib getTileVisib(int x, int y);
    TileVisib getTileVisib(const glm::ivec2& pos);
    bool      getTileMark (const glm::ivec2& pos, TileMark mark);
    bool      getTileMark (int x, int y, TileMark mark);
    
    void setTileType (int x, int y, TileType  type);
    void setTileVisib(int x, int y, TileVisib visible);
    void setTileRess (int x, int y, TileRess  ress);
    void setTileMark (int x, int y, TileMark mark, bool state);
    
    void update(int x, int y);
    void updateNeighbours(int x, int y);
    void updateAll();
    void onUpdate(int x, int y);
    
    void destroy(int x, int y);
    
    bool isWall(int x, int y);
    bool isTrueWall(int x, int y);
    bool isTrueWall(TileType type);
    
    void addObject(MapObject* object);
    void removeObject(MapObject* object);
    void updateObjects();
    MapObject* getObject(int id);
    int getNumObjects();
    
    void spawnRessources(int x, int y);
    
    static int getTileSize();
    static int getTileHeight();
    
    void updateEngineObject(int x, int y);
    
    static glm::ivec2 posToTile(const glm::vec2& pos);
    static glm::vec2  tileToPos(const glm::ivec2& pos);
private:
    struct Tile{
        TileType  type;
        TileVisib visible;
        TileRess  ress;
        
        std::bitset<16> marks;
        
        EngineObject obj;
    };
    
    Tile& tile(int x, int y);
    
    void setTileColor(int x, int y, const glm::vec4& color);
    void onTileMarkChange(int x, int y);
    
    bool onMap(int x, int y);
    
    std::vector<MapObject*> m_objects;
    
    Tile* m_tiles = nullptr;
    
    int m_width;
    int m_height;
    
    MapRessource* m_ressource = nullptr;
};

#endif	/* GAMEMAP_H */

