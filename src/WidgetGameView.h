/* 
 * File:   WidgetGameView.h
 * Author: kevin
 *
 * Created on July 1, 2015, 10:20 AM
 */

#ifndef WIDGETGAMEVIEW_H
#define	WIDGETGAMEVIEW_H

#include "Widget.h"

#include "Engine3D.h"
#include "GameMap.h"
#include "RendererMap.h"

class ObjectInfo;
class Game;

class WidgetGameView: public Widget{
public:
    WidgetGameView();
    WidgetGameView(const WidgetGameView& orig) = delete;
    virtual ~WidgetGameView();
    
    void setEngine3D(Engine3D* engine);
    void setGameMap(GameMap* map);
    void setGame(Game* game);
    void setRendererMap(RendererMap* renderer);
    
    void setCameraPos(const glm::vec3 pos);
    void setCameraRot(const glm::vec3 rot);
    void setCameraFov(float fov);
    
    glm::vec3 getCameraPos();
    glm::vec3 getCameraRot();
    float     getCameraFov();
    
    void enableDebugControl(bool state);
    
    glm::vec3 getPickRayDir(glm::vec2 cursorPos);
    glm::vec3 pickRay(glm::vec2 cursorPos);
    glm::vec2 project(glm::vec3 point);
    
    void build(ObjectInfo* building);
protected:
    virtual void onPaint(Renderer2D* renderer) override;

    virtual void onKeyDown(Key key) override;
    virtual void onKeyUp(Key key) override;

    virtual void onMouseDown(int mouseButton) override;
    virtual void onMouseUp(int mouseButton) override;
    virtual void onMouseMove(glm::vec2 relPos) override;

    void engineSetCamera();
    
    void clearBuildingMarks();
    void setBuildingMarks();
private:
    RendererMap* m_rendererMap = nullptr;
    Engine3D*    m_engine      = nullptr;
    GameMap*     m_map         = nullptr;
    Game*        m_game        = nullptr;
    
    ObjectInfo* m_build = nullptr;
    
    glm::vec3 m_cameraPos;
    glm::vec3 m_cameraRot;
    
    glm::vec2 m_cursorPos;
    glm::vec2 m_lastCursor;
    
    glm::ivec2 m_lastBuildPos;
    
    bool      m_selecting = false;
    bool      m_building  = false;
    glm::vec2 m_selectionStart;
    glm::vec2 m_selectionEnd;
    
    float m_cameraFov = 60.0f;
    
    bool m_debugControls = false;
    bool m_debugRotate   = false;
};

#endif	/* WIDGETGAMEVIEW_H */

