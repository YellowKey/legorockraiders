/* 
 * File:   ActionPlace.h
 * Author: Kevin
 *
 * Created on 28. August 2015, 14:15
 */

#ifndef ACTIONPLACE_H
#define	ACTIONPLACE_H

#include "Action.h"

class ActionPlace: public Action{
public:
    enum Place{
        PLACE_GROUND,
        PLACE_STORE,
    };
    ActionPlace(Place place = PLACE_GROUND);
    ActionPlace(const ActionPlace& orig) = delete;
    virtual ~ActionPlace();
    
    virtual void start() override;
    virtual void updateUnit() override;
    virtual void cancel() override;
private:
    Place m_place;
    float m_time = 1.16f;
    float m_startTime;
};

#endif	/* ACTIONPLACE_H */

