/* 
 * File:   LoaderFile.cpp
 * Author: Kevin
 * 
 * Created on 4. Juni 2015, 10:25
 */

#include "LoaderFile.h"

#include "LoaderLws.h"
#include "LoaderLzo.h"
#include "LoaderBmp.h"
#include "LoaderAe.h"
#include "App.h"

#include <algorithm>
#include <iostream>

#include "util.h"

LoaderFile::LoaderFile() {
}

LoaderFile::~LoaderFile() {
    
}

Mesh* LoaderFile::loadMesh(const std::string& file) {
    LoaderLws lwsLoader;
    
    lwsLoader.load(file);
    return lwsLoader.createMesh();
}

Shape* LoaderFile::loadShape(const std::string& file) {
    return nullptr; //TODO: implement
}

Activity* LoaderFile::loadActivity(const std::string& filename) {
    LoaderAe aeLoader;
    
    auto* wadLoader = App::getApp()->getDataLoader0();
    
    auto fileId = wadLoader->getFileId(filename);
    void* data = wadLoader->loadFileData(fileId);
    aeLoader.setPath(LoaderFile::getPath(filename));
    aeLoader.load(data, wadLoader->getFileSize(fileId));
    wadLoader->freeFileData(data);
    
    return aeLoader.getActivity();
}


Texture* LoaderFile::loadTexture(const std::string& file) {
    std::string name = file;
    Texture* texture;
    
    std::transform(name.begin(), name.end(), name.begin(), ::tolower);
    
    auto old = m_textureCache.find(name);
    
    if (old != m_textureCache.end()){
        static int cacheCount = 0;
        texture = old->second;
    }else{
        LoaderBmp bmpLoader;
        auto* wadLoader = App::getApp()->getDataLoader0();

        int fileId = wadLoader->getFileId(file);
        if (fileId == -1){
            if (isDebugging()){
                std::cout << "Texture not found:" << file << std::endl;
            }

            return nullptr;
        }

        void* data = wadLoader->loadFileData(fileId);
        bmpLoader.load(data, wadLoader->getFileSize(fileId));
        bmpLoader.convertToRGB();
        texture = new Texture(bmpLoader.getData(), bmpLoader.getWidth(), bmpLoader.getHeight(), Texture::RGB8);
        wadLoader->freeFileData(data);

        glm::vec4 transColor = glm::vec4(0);
        std::string fileName = name.substr(name.rfind("\\") + 1, std::string::npos);
        if (fileName.size() > 9 && fileName[0] == 'a' && fileName[4] == '_'){
            int index = std::stoi(fileName.substr(1, 3));
            //std::cout << name << " = " << index << std::endl;
            auto color = bmpLoader.getPalet()[index];
            
            transColor = glm::vec4((color >> 16) & 0xFF, (color >> 8) & 0xFF, color & 0xFF, 255);
            transColor *= glm::vec4(1.0f / 255.0f);
        }else if (name == "interface\\cameracontrol\\camcontrol.bmp"){ //I don't know why this one is different
            transColor = glm::vec4(0, 1.0f/255.0f, 1.0f/255.0f, 1.0f);
        }else if (name.substr(0, 9) == "interface"){
            transColor = glm::vec4(0, 0, 0, 1);
        }
        
        texture->setTransColor(transColor);
        //texture->setTransColor(m_textureInfo[name].transColor);

        if (isDebugging()){
            std::cout << "Load Texture: " << file << std::endl;
        }
        
        m_textureCache[name] = texture;
    }
    
    return texture;
}

Font* LoaderFile::loadFont(const std::string& file) {
    LoaderBmp bmpLoader;
    std::string name = file;
    
    auto* wadLoader = App::getApp()->getDataLoader0();
    
    int fileId = wadLoader->getFileId(file);
    if (fileId == -1){
        if (isDebugging()){
            std::cout << "couln't load Font: " << file << std::endl;
        }
        return nullptr;
    }
    void* data = wadLoader->loadFileData(fileId);
    bmpLoader.load(data, wadLoader->getFileSize(fileId));
    bmpLoader.convertToRGBA();
    Font* font = new Font(bmpLoader.getData(), bmpLoader.getWidth(), bmpLoader.getHeight(), Texture::RGBA8);
    wadLoader->freeFileData(data);
    
    if (isDebugging()){
        std::cout << "Load Font: " << file  << std::endl;
    }
    
    return font;
}

bool LoaderFile::isDebugging() {
    return false;
}

bool LoaderFile::isFile(const std::string& file) {
    int fileId = App::getApp()->getDataLoader0()->getFileId(file);
    
    if (fileId >= 0){
        return true;
    }else{
        return false;
    }
}


std::string LoaderFile::getPath(const std::string& filePath) {
    return filePath.substr(0, filePath.rfind('\\') + 1);
}
