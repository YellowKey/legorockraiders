/* 
 * File:   EngineShape.cpp
 * Author: Kevin
 * 
 * Created on 4. Juni 2015, 16:04
 */

#include "EngineShape.h"

EngineShape::EngineShape(EngineObject* parent, int group){
    m_parent = parent;
    m_group  = group;
}


EngineShape::~EngineShape() {
}


EngineObject* EngineShape::getParent() {
    return m_parent;
}


void EngineShape::setMatrix(const glm::mat4& matrix) {
    m_matrix = matrix;
}

glm::mat4 EngineShape::getMatrix() {
    return m_matrix;
}

int EngineShape::getGroup() {
    return m_group;
}
