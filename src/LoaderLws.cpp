/* 
 * File:   LoaderLws.cpp
 * Author: Kevin
 * 
 * Created on 16. Mai 2015, 17:51
 */

#include "LoaderLws.h"

#include <cstring>
#include <iostream>
#include <algorithm>

#ifndef M_PI
#define M_PI           3.14159265358979323846
#endif

#include "util.h"
#include "App.h"
#include "LoaderLzo.h"

LoaderLws::LoaderLws() {
}

LoaderLws::LoaderLws(const LoaderLws& orig) {
}

LoaderLws::~LoaderLws() {
}

std::string globName = "";

void LoaderLws::load(const std::string& name) {
    globName = name;
    auto* loader = App::getApp()->getDataLoader0();
    
    int lastSlash = name.rfind('\\') + 1;
    m_path = name.substr(0, lastSlash);
    
    int fileId = loader->getFileId(name);
    void* data = loader->loadFileData(fileId);
    parseData(data, loader->getFileSize(fileId));
    loader->freeFileData(data);
}

void LoaderLws::parseData(const void* data, int size) {
    clearData();
    
    m_data = (const char*)data;
    m_dataPos = m_data;
    m_dataSize = size;
    m_eof = false;
    
    m_ignoreNewLines = false;
    
    parseCommands();
}

void LoaderLws::clearData() {
    m_objects.clear();
    m_firstFrame = 0;
    m_lastFrame = 0;
    m_frameRate = 0;
}

char LoaderLws::readChar() {
    if (m_dataPos < m_data + m_dataSize){
        if (m_dataPos + 1 >= m_data + m_dataSize){
            markEof();
        }
        return *(m_dataPos++);
    }else{
        markEof();
        
        return 0;
    }
}

std::string LoaderLws::readString() {
    std::string text;
    
    while (isWhitespace() || (m_ignoreNewLines && isNewLine())){
        readChar();
    }
    
    while (!isWhitespace() && !isNewLine()){
        text += readChar();
    }
    
    return text;
}

int LoaderLws::readInt() {
    return atoi(readString().c_str());
}

float LoaderLws::readFloat() {
    return atof(readString().c_str());
}

void LoaderLws::nextLine() {
    while(!isNewLine()){
        readChar();
    }
    
    //TODO: Improve for Unix line encoding
    readChar();
    readChar();
}

void LoaderLws::ignoreNewLines(bool status) {
    m_ignoreNewLines = status;
}

void LoaderLws::markEof() {
    m_eof = true;
}

bool LoaderLws::eof() {
    return m_eof;
}



bool LoaderLws::isNewLine() {
    if (!eof()){
        if (*m_dataPos != 13 && *m_dataPos != 10){
            return false;
        }else{
            return true;
        }
    }else{
        return true;
    }
}

bool LoaderLws::isWhitespace() {
    if(!eof()){
        if (*m_dataPos != 32 && *m_dataPos != 9){
            return false;
        }else{
            return true;
        }
    }else{
        return false;
    }
}

void LoaderLws::parseCommands() {
    while (!eof()){
        std::string cmd = readString();
        
        if (isDebugging()){
            std::cout << "Command: " << cmd << std::endl;
        }
        
        if (cmd == "FirstFrame"){
            m_firstFrame = readInt();
        }else if (cmd == "LastFrame"){
            m_lastFrame = readInt();
        }else if (cmd == "FramesPerSecond"){
            m_frameRate = readFloat();
        }else if (cmd == "LoadObject"){
            addObject(readString());
        }else if (cmd == "AddNullObject"){
            addNullObject(readString());
        }else if (cmd == "AddLight"){
            addNullObject("_LIGHT");
        }else if (cmd == "ObjectMotion"){
            parseMotion();
        }else if (cmd == "ParentObject"){
            setParent(readInt() - 1);
        }else if (cmd == "EndBehavior"){
            setEndBehavior(readInt());
        }else if (cmd == "PivotPoint"){
            setPivotPoint(glm::vec3(readFloat(), readFloat(), readFloat()));
        }else if (isDebugging()){
            std::cout << " - unkown" << std::endl;
        }
        
        
        nextLine();
    }
}

void LoaderLws::parseMotion() {
    nextLine();
    ignoreNewLines(true);
    
    int freedomDegrees = readInt();
    int keyFrames = readInt();
    
    if (isDebugging()){
        std::cout << " - Keyframes: " << keyFrames << std::endl;
    }
    
    for (int a = 0; a < keyFrames; a++){
        KeyFrame frame;
        
        frame.pos.x = readFloat();
        frame.pos.y = readFloat();
        frame.pos.z = readFloat();
        
        frame.rot.x = readFloat() * M_PI / 180;
        frame.rot.y = readFloat() * M_PI / 180;
        frame.rot.z = readFloat() * M_PI / 180;
        
        frame.scale.x = readFloat();
        frame.scale.y = readFloat();
        frame.scale.z = readFloat();
        
        frame.frame = readInt();
        readFloat(); // Curved/Linear
        readFloat(); // Tension
        readFloat(); // Continuity
        readFloat(); // Bias
        
        
        addKeyFrame(frame);
        
        if (isDebugging()){
            std::cout << " - KeyFrame: " << a << std::endl;
            std::cout << "   - Pos:   " << frame.pos << std::endl; 
            std::cout << "   - Rot:   " << frame.rot << std::endl;
            std::cout << "   - Scale: " << frame.scale << std::endl;
            std::cout << "   - Frame: " << frame.frame << std::endl;
        }
    }
    
    ignoreNewLines(false);
}

void LoaderLws::addNullObject(const std::string& name) {
    std::string lname = name;
    std::transform(lname.begin(), lname.end(), lname.begin(), ::tolower);
    addObject(lname, true);
}

void LoaderLws::addObject(const std::string& name, bool nullObject) {
    Object object;
    
    object.name = name;
    object.nullObject = nullObject;
    object.parent = -1;
    
    m_objects.push_back(object);
    
    if (isDebugging()){
        std::cout << " - ID  : " << m_objects.size() <<std::endl;
        std::cout << " - Name: " << name << std::endl;
    }
}

void LoaderLws::addKeyFrame(const KeyFrame& frame) {
    m_objects[m_objects.size() - 1].motion.push_back(frame);
}


void LoaderLws::setParent(int parentId) {
    if (isDebugging()){
        std::cout << " - ID    : " << m_objects.size();
        std::cout << " - Parent: " << parentId + 1 << std::endl;
    }
    m_objects[m_objects.size() - 1].parent = parentId;
}

void LoaderLws::setEndBehavior(int behavior) {
    m_objects[m_objects.size() - 1].endBehavior = behavior;
}

void LoaderLws::setPivotPoint(const glm::vec3& pivotPoint) {
    m_objects[m_objects.size() - 1].pivotPoint = pivotPoint;
}



Mesh* LoaderLws::createMesh() {
    /*for (int a = 0; a < m_objects.size(); a++){
        auto& obj = m_objects[a];
        
        std::cout << "[" << a << "] " << obj.name << " -> " << obj.parent << std::endl;
        
        for (int b = 0; b < obj.motion.size(); b++){
            //std::cout << obj.motion[b].frame <<  obj.motion[b].pos << std::endl;
        }
    }*/
    Mesh* mesh = new Mesh();
    
    mesh->SetAnimTime((float)m_firstFrame / m_frameRate, (float)m_lastFrame / m_frameRate);
    
    for (int a = 0; a < m_objects.size(); a++){
        auto& obj = m_objects[a];
        int   group = mesh->addGroup();
        
        mesh->setGroupParent(group, obj.parent);
        mesh->setGroupPivotPoint(group, obj.pivotPoint);
        
        
        if (isDebugging()){
            std::cout << "Add Group: " << group;
            std::cout << "(Frames: " << obj.motion.size();
            std::cout << ", Parent: " << obj.parent << ")" << std::endl;
        }
        
        LoaderLzo lzoLoader;
        
        
        if (!obj.nullObject){
            size_t begin = obj.name.rfind('\\') + 1;
            
            std::string objName = obj.name.substr(begin, std::string::npos);
            std::string objPath = m_path;
            
            std::transform(objName.begin(), objName.end(), objName.begin(), ::tolower);
            
            if (App::getApp()->getApp()->getDataLoader0()->getFileId(objPath + objName) == -1){
                objPath = "world\\shared\\";
            }
            
            //Testcode to load Pilot
            //if (objName.find("vlp") == 0){           
                if (objName == "vlphead.lwo") objName = "hphead.lwo";
                if (objName == "vlphandl.lwo") objName = "hphand.lwo";
                if (objName == "vlphandr.lwo") objName = "hphand.lwo";
                if (objName == "vlpbicepl.lwo") objName = "hpbicepl.lwo";
                if (objName == "vlpbicepr.lwo") objName = "hpbicepr.lwo";
                if (objName == "vlptorso.lwo") objName = "lptorso.lwo";
                if (objName == "vlpthighl.lwo") objName = "mpthighl.lwo";
                if (objName == "vlpthighr.lwo") objName = "mpthighr.lwo";
                if (objName == "vlpfootr.lwo") objName = "mpfootr.lwo";
                if (objName == "vlpfootl.lwo") objName = "mpfootl.lwo";
                //if (objName == "l_hammer.lwo") objName = "hammer.lwo";
                //if (objName == "l_clipboard.lwo") objName = "clipboard.lwo";
                if (objName == "vl_smalltruckbody.lwo") objName = "smalltruckbody.lwo";
                
                /*if (App::getApp()->getApp()->getDataLoader0()->getFileId(objPath +  "mp" + objName.substr(3)) != -1){
                    objName = "mp" + objName.substr(3);
                }
                if (objName == "vlpbicepr.lwo"){
                    objName = "mparmr.lwo";
                }else if (objName == "vlpbicepl.lwo"){
                    objName = "mparml.lwo";
                }else if (objName == "vlphandl.lwo"){
                    objName = "lphandl.lwo";
                }else if (objName == "vlphandr.lwo"){
                    objName = "lphandr.lwo";
                }*/
            //}
            
            lzoLoader.load(objPath + objName);
            
            for (int a = 1; a < lzoLoader.getNumShapes(); a++){
                Shape* shape = lzoLoader.getShape(a);
                
                mesh->addShape(group, lzoLoader.getShape(a));
            }
        }else{
            mesh->setGroupName(group, obj.name);
        }
        
        for (int b = 0; b < obj.motion.size(); b++){
            //std::cout << "Add Frame " << b << std::endl;
            auto& frame = obj.motion[b];
            
            int id = mesh->addKeyframe(group);
            
            mesh->setKeyFramePos(group, id, frame.pos);
            mesh->setKeyFrameRot(group, id, frame.rot);
            mesh->setKeyFrameScale(group, id, frame.scale);
            mesh->setKeyFrameTime(group, id, (float)frame.frame / m_frameRate);
        }
        
    }
    
    return mesh;
    
}

bool LoaderLws::isDebugging() {
    return false;
}
