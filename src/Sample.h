/* 
 * File:   Sample.h
 * Author: Kevin
 *
 * Created on 2. Oktober 2015, 16:31
 */

#ifndef SAMPLE_H
#define	SAMPLE_H

class Sample {
public:
    Sample();
    Sample(const Sample& orig) = delete;
    virtual ~Sample();
private:

};

#endif	/* SAMPLE_H */

