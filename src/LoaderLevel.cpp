/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   LoaderLevel.cpp
 * Author: Kevin
 * 
 * Created on 30. Dezember 2015, 16:49
 */

#include "LoaderLevel.h"

#include "App.h"
#include "LoaderMap.h"
#include "StateGame.h"

#include <iostream>

LoaderLevel::LoaderLevel() {
}


LoaderLevel::~LoaderLevel() {
}

StateMap* LoaderLevel::loadMap(const std::string& name) {
    std::string path = "Levels\\GameLevels\\Level" + name + "\\";
    StateMap* map = new StateMap;
    
    
    if (!loadSurf(map, path + "Surf_"+name +".map")){
        std::cerr << "Couln't load level " << name << std::endl;
    }
    
    loadDugg(map, path + "Dugg_"+name +".map");
    loadCror(map, path + "Cror_"+name +".map");
    loadHigh(map, path + "High_"+name +".map");
    
    return map;
}

bool LoaderLevel::loadSurf(StateMap* map, const std::string& file) {
    LoaderMap loader;
    
    if (loader.load(file)){
        map->setSize(loader.getWidth(), loader.getHeight());
        
        for (int y = 0; y < loader.getHeight(); y++){
            for (int x = 0; x < loader.getWidth(); x++){
                map->setTileType(x, y, (StateMap::TileType)loader.getData(x, y));
            }
        }
    }else{
        return false;
    }
    
    return true;
}

bool LoaderLevel::loadCror(StateMap* map, const std::string& file) {
    LoaderMap loader;
    
    if (loader.load(file)){
        for (int y = 0; y < loader.getHeight(); y++){
            for (int x = 0; x < loader.getWidth(); x++){
                switch(loader.getData(x, y)){
                    case 1:
                    case 3:
                        map->setTileCrystal(x, y, 1);
                        break;
                    case 2:
                    case 4:
                        map->setTileOre(x, y, 1);
                        break;
                    case 5:
                    case 7:
                        map->setTileCrystal(x, y, 3);
                        break;
                    case 6:
                    case 8:
                        map->setTileOre(x, y, 3);
                        break;
                    case 9:
                    case 11:
                        map->setTileCrystal(x, y, 5);
                        break;
                    case 10:
                    case 12:
                        map->setTileOre(x, y, 5);
                        break;
                    case 13:
                    case 15:
                        map->setTileCrystal(x, y, 11);
                        break;
                    case 14:
                    case 16:
                        map->setTileOre(x, y, 11);
                        break;
                    case 17:
                    case 19:
                        map->setTileCrystal(x, y, 25);
                        break;
                    case 18:
                    case 20:
                        map->setTileOre(x, y, 25);
                        
                }
            }
        }
    }else{
        return false;
    }
    
    return true;
}

bool LoaderLevel::loadDugg(StateMap* map, const std::string& file) {
    LoaderMap loader;
    
    if (loader.load(file)){
        for (int y = 0; y < loader.getHeight(); y++){
            for (int x = 0; x < loader.getWidth(); x++){
                int visib = loader.getData(x, y);
                auto type = map->getTileType(x, y);
                
                if (visib == 4){
                    visib = 2;
                    type = StateMap::TILE_SLUGHOLE;
                }else if (visib == 3){
                    visib = 1;
                    type = StateMap::TILE_SLUGHOLE;
                }
                
                if (visib == 0 && !map->isTileWall(x, y)){
                    type = StateMap::TILE_DIRT;
                }
                if (visib && map->isTileWall(x, y)){
                    type = StateMap::TILE_GROUND;
                }
                
                map->setTileVisib(x, y, (StateMap::TileVisib)visib);
                map->setTileType(x, y, type);
            }
        }
    }else{
        return false;
    }
    
    return true;
}

bool LoaderLevel::loadHigh(StateMap* map, const std::string& file) {
    LoaderMap loader;
    
    if (loader.load(file)){
        for (int y = 0; y < loader.getHeight(); y++){
            for (int x = 0; x < loader.getWidth(); x++){
                float h = 0;
                
                h += loader.getData(x  , y  );
                h += loader.getData(x+1, y  );
                h += loader.getData(x+1, y+1);
                h += loader.getData(x  , y+1);
                
                // Tile Height  = 60
                // Height Steps = 6
                // Count        = 4
                h *= 40.0f / 4.0f / 6.0f;
                
                map->setHeight(x, y, h);
            }
        }
    }
}


