/* 
 * File:   ObjectInfoData.cpp
 * Author: Kevin
 * 
 * Created on 8. September 2015, 12:15
 */

#include "ObjectInfoData.h"

template<class T>
ObjectInfoData<T>::ObjectInfoData() {

}

template<class T>
ObjectInfoData<T>::ObjectInfoData(const ObjectInfoData& orig) {
    m_parent = orig.m_parent;
    m_data = orig.m_data;
}

template<class T>
ObjectInfoData<T>::~ObjectInfoData() {
}

template<class T>
T ObjectInfoData<T>::get(int level) {

}

template<class T>
void ObjectInfoData<T>::set(int level, T data) {

}

template class ObjectInfoData<float>;
template class ObjectInfoData<bool>;
template class ObjectInfoData<int>;