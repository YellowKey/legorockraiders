/* 
 * File:   UnitVehicle.h
 * Author: Kevin
 *
 * Created on 25. September 2015, 13:13
 */

#ifndef UNITVEHICLE_H
#define	UNITVEHICLE_H

#include "Unit.h"
#include "Activity.h"
#include "ObjectInfo.h"

class UnitVehicle: public Unit{
public:
    UnitVehicle(ObjectInfo* obj);
    UnitVehicle(const UnitVehicle& orig) = delete;
    virtual ~UnitVehicle();
    
    virtual void update() override;
    
    virtual int getSlotGroupId(int slotId) override;
    virtual EngineObject* getSlotEngineObject(int slotId) override;


    virtual bool canMove() override;
    virtual int canCarry() override;
    virtual bool canDrill() override;
    virtual bool canClean() override;


protected:
    virtual void onStatusChange(Status status) override;
private:
    struct Attachment{
        EngineObject* obj;
        std::string null;
        int index;
        bool update;
    };
    
    std::vector<Attachment> m_attachments;
    ObjectInfo* m_info;
    
    void addWheels(Mesh* mesh, const std::string& null);
    void addAttachment(Mesh* mesh, const std::string& null, int index, bool update = true);
    void clearUpdateAttachments();
    void updateAttachments();
};

#endif	/* UNITVEHICLE_H */

