/* 
 * File:   ActionDrill.h
 * Author: kevin
 *
 * Created on July 9, 2015, 1:29 PM
 */

#ifndef ACTIONDRILL_H
#define	ACTIONDRILL_H

#include "Action.h"

class ActionDrill: public Action{
public:
    ActionDrill(const glm::ivec2& wallPos);
    ActionDrill(const ActionDrill& orig) = delete;
    virtual ~ActionDrill();
    
    virtual void start() override;
    virtual void updateUnit() override;
    virtual void cancel() override;
private:
    glm::ivec2 m_wallPos;
    
    float m_drillTime = 1.0f;
    float m_startTime;
};

#endif	/* ACTIONDRILL_H */

