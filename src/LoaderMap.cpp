/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   LoaderMap.cpp
 * Author: Kevin
 * 
 * Created on 30. Dezember 2015, 17:24
 */

#include "LoaderMap.h"

#include "App.h"

#include <iostream>
#include <cstring>

LoaderMap::LoaderMap() {
}

LoaderMap::~LoaderMap() {
}

bool LoaderMap::load(const std::string& file) {
    auto* loader = App::getApp()->getDataLoader0();
    
    int fileId = loader->getFileId(file);
    
    if (fileId == -1){
        m_width  = 0;
        m_height = 0;
        
        return false;
    }
    
    char* data = (char*)loader->loadFileData(fileId);
    
    std::string header = std::string(data, 4);
    
    int fileSize = *((int*)(data + 4));
    m_width      = *((int*)(data + 8));
    m_height     = *((int*)(data + 12));
    
    if (m_width == 0 || m_height == 0){
        std::cerr << "width or height of map is zero (" << file << ")" << std::endl;
        exit(1);
    }
    
    m_data.resize(m_width * m_height);
    
    memcpy(m_data.data(), data + 16, m_width * m_height * 2);
    
    loader->freeFileData(data);
    
    return true;
}

int LoaderMap::getWidth() const {
    return m_width;
}

int LoaderMap::getHeight() const {
    return m_height;
}

uint16_t LoaderMap::getData(int x, int y) const {
    if (x < 0){
        x = 0;
    }else if (x >= m_width){
        x = m_width - 1;
    }
    
    if (y < 0){
        y = 0;
    }else if (y >= m_height){
        y = m_height - 1;
    }
    
    return m_data[x + y * m_width];
}


