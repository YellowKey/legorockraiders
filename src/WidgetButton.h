/* 
 * File:   WidgetButton.h
 * Author: Kevin
 *
 * Created on 21. Juni 2015, 16:16
 */

#ifndef WIDGETBUTTON_H
#define	WIDGETBUTTON_H

#include "Widget.h"

#include "InterfaceRessorces.h"

#include <vector>

class ButtonListener;

class WidgetButton: public Widget{
public:
    WidgetButton();
    WidgetButton(const InterfaceRessources::MenuIcon& textures);
    virtual ~WidgetButton();

    WidgetButton* addListener(ButtonListener* listener, int id);    
    void removeListener(ButtonListener* listener);
    
    void setTexture(const Texture* texture);
    void setTexturePushed(const Texture* texture);
    void setTextureDisabled(const Texture* texture);
    void setTextureHover(const Texture* texture);
    
    void setTextures(const InterfaceRessources::MenuIcon& textures);
    
    void disable(bool state);
protected:
    virtual void onPaint(Renderer2D* renderer) override;

    virtual void onMouseDown(int mouseButton) override;
    virtual void onMouseUp(int mouseButton) override;
    virtual void onMouseHover(bool state) override;

    
    virtual bool isBeeingClicked();

private:
    struct Listener{
        ButtonListener* listener;
        int id;
    };
    const Texture* getCurrentTexture();
    
    const Texture* m_textureNormal  = nullptr;
    const Texture* m_texturePushed  = nullptr;
    const Texture* m_textureDisable = nullptr;
    const Texture* m_textureHover = nullptr;
    
    bool m_enabled = true;
    bool m_clicked = false;
    
    std::vector<Listener> m_listener;
};

#endif	/* WIDGETBUTTON_H */

