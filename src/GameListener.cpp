/* 
 * File:   GameListener.cpp
 * Author: Kevin
 * 
 * Created on 13. August 2015, 15:56
 */

#include "GameListener.h"

GameListener::~GameListener() {
    if (getGame()){
        getGame()->removeListener(this);
    }
}


void GameListener::onNewObject(uint32_t objId, Game::ObjectType type){}
void GameListener::onObjectSelect(uint32_t objId, bool state){}
void GameListener::onObjectRotate(uint32_t objId, glm::vec3 rotation){}
void GameListener::onObjectMove(uint32_t objId, glm::vec3 pos){}
void GameListener::onObjectAttach(uint32_t objId, uint32_t targetId, uint32_t slotId){}
void GameListener::onObjectRemove(uint32_t objId, uint32_t targetId, uint32_t slotId){}
void GameListener::onObjectAnim(uint32_t objId, uint32_t animId, float time){}


void GameListener::onOrderCancel(uint32_t objId){}
void GameListener::onOrderSend(uint32_t objId, glm::vec2 pos){}
void GameListener::onOrderPickup(uint32_t objId, uint32_t targetId){}
void GameListener::onOrderEnter(uint32_t objId, uint32_t targetId){}
void GameListener::onOrderLeave(uint32_t objId){}
void GameListener::onOrderDrop(uint32_t objId, uint32_t slotId){}
void GameListener::onOrderDrill(uint32_t objId, glm::ivec2 pos){}
void GameListener::onOrderCleanup(uint32_t objId, glm::ivec2 pos){}
void GameListener::onOrderDestroy(uint32_t objId){}

void GameListener::onAddBuildingBuild(uint32_t objId, Game::ObjectType type){}

void GameListener::onTileSelect(glm::ivec2 pos){}
void GameListener::onTileDeselect() {}


void GameListener::onTileMarkDrill(glm::ivec2 pos, bool state){}
void GameListener::onTileMarkCleanup(glm::ivec2 pos, bool state){}
void GameListener::onTileChange(glm::uint32_t tileId){}
void GameListener::onPrioChange(Game::PriotyType, uint32_t priority) {}


void GameListener::onGameSpeedChange(float speed) {}
void GameListener::onGamePause(bool state){}
void GameListener::onGameRessChange(int crystal, int ore, int bricks) {}



Game* GameListener::getGame(){
    return m_game;
}

void GameListener::setGame(Game* game) {
    if (m_game != game && m_game){
        m_game->removeListener(this);
    }
    
    m_game = game;
}
