/* 
 * File:   InterfaceRessorces.cpp
 * Author: Kevin
 * 
 * Created on 10. Juni 2015, 17:43
 */

#include "InterfaceRessorces.h"

#include "App.h"

InterfaceRessources::InterfaceRessources() {
}

InterfaceRessources::~InterfaceRessources() {
}

void InterfaceRessources::load() {
    loadTexture(&crystalBar, "Interface\\RightPanel\\CrystalSideBar.bmp");
    loadTexture(&crystalIcon, "Interface\\RightPanel\\SmallCrystal.bmp");
    loadTexture(&crystalIconUsed, "Interface\\RightPanel\\UsedCrystal.bmp");
    loadTexture(&crystalIconEmpty, "Interface\\RightPanel\\NoSmallCrystal.bmp");
    loadTexture(&oreBar, "Interface\\RightPanel\\CrystalSideBar_Ore.bmp");
    
    loadTexture(&topPanel, "Interface\\TopPanel\\TopPanel.bmp");
    loadTexture(&tpArmsHigh, "Interface\\TopPanel\\TP_Arms_HL.bmp");
    loadTexture(&tpArmsPush, "Interface\\TopPanel\\TP_Arms_PR.bmp");
    loadTexture(&tpOptionsHigh, "Interface\\TopPanel\\TP_options_HL.bmp");
    loadTexture(&tpOptionsPush, "Interface\\TopPanel\\TP_options_PR.bmp");
    loadTexture(&tpPrioHigh, "Interface\\TopPanel\\TP_prio_HL.bmp");
    loadTexture(&tpPrioPush, "Interface\\TopPanel\\TP_prio_PR.bmp");
    
    loadTexture(&cameraControl, "Interface\\CameraControl\\CamControl.bmp");
    loadTexture(&navHomeHigh, "Interface\\CameraControl\\NAV_home_HL.bmp");
    loadTexture(&navHomePush, "Interface\\CameraControl\\NAV_home_PR.bmp");
    loadTexture(&navZoomInHigh, "Interface\\CameraControl\\Nav_zoomIn_HL.bmp");
    loadTexture(&navZoomInPush, "Interface\\CameraControl\\NAV_zoomIN_PR.bmp");
    loadTexture(&navZoomOutHigh, "Interface\\CameraControl\\NAV_zoomOut_HL.bmp");
    loadTexture(&navZoomOutPush, "Interface\\CameraControl\\NAV_zoomOut_PR.bmp");
    loadTexture(&navDown, "Interface\\CameraControl\\NAV_down.bmp");
    loadTexture(&navUp, "Interface\\CameraControl\\NAV_up.bmp");
    loadTexture(&navLeft, "Interface\\CameraControl\\NAV_left.bmp");
    loadTexture(&navRight, "Interface\\CameraControl\\NAV_right.bmp");
    
    loadTexture(&messagePanel, "Interface\\MessagePanel\\MsgPanel.bmp");
    loadTexture(&mpAirBar, "Interface\\AirMeter\\MsgPanel_air_juice.bmp");
    loadTexture(&mpNoAir, "Interface\\AirMeter\\MsgPanel_noair.bmp");
    
    loadTexture(&mpAirLow, "Interface\\InfoImages\\AirLow.bmp");
    loadTexture(&mpAirWarning, "Interface\\InfoImages\\AirWarning.bmp");
    loadTexture(&mpCavernIce, "Interface\\InfoImages\\CavernICE.bmp");
    loadTexture(&mpCavernLava, "Interface\\InfoImages\\CavernLAVA.bmp");
    loadTexture(&mpCavernRock, "Interface\\InfoImages\\CavernROCK.bmp");
    loadTexture(&mpCrystal, "Interface\\InfoImages\\Crystal.bmp");
    loadTexture(&mpCrystalFound, "Interface\\InfoImages\\CrystalFound.bmp");
    loadTexture(&mpOreFound, "Interface\\InfoImages\\Orefound.bmp");
    loadTexture(&mpPressSpace, "Interface\\InfoImages\\PressSpace.bmp");
    
    loadMenuIcon(&menuDrill, "drill");
    loadMenuIcon(&menuDynamite, "dynamite");
    loadMenuIcon(&menuReinforce, "reinforce");
    loadMenuIcon(&menuCancelDrill, "stopDrill");
    
    loadMenuIcon(&menuPath, "BuildPath");
    loadMenuIcon(&menuClearRubble, "ClearRubble");
    loadMenuIcon(&menuDestroyPath, "DigupPath");
    loadMenuIcon(&menuRemoveLava, "Dam");
    loadIcon(&menuFence, "eFence");
    
    loadMenuIcon(&menuSmallVehicles, "SMvehicle");
    loadMenuIcon(&menuBigVehicles, "BIGvehicle");
    loadMenuIcon(&menuBuildings, "building");
    
    loadMenuIcon(&menuTrainDriver, "Train_Driver");
    loadMenuIcon(&menuTrainEngineer, "Train_Engineer");
    loadMenuIcon(&menuTrainGeologist, "Train_Geologist");
    loadMenuIcon(&menuTrainPilot, "Train_Pilot");
    loadMenuIcon(&menuTrainSalor, "Train_Sailor");
    loadMenuIcon(&menuTrainExplosives, "Train_Explosives");
    
    loadMenuIcon(&menuTakeDrill, "get_Drill");
    loadMenuIcon(&menuTakeShapde, "get_Spade");
    loadMenuIcon(&menuTakeHammer, "get_Hammer");
    loadMenuIcon(&menuTakeSpanner, "get_Spanner");
    loadMenuIcon(&menuTakeLaser, "gun_Lazer");
    loadMenuIcon(&menuTakeFreeze, "gun_Pusher");
    loadMenuIcon(&menuTakePush, "gun_Freeze");
    loadMenuIcon(&menuTakeSonic, "get_BirdScarer");
    
    loadTexture(&(menuTools.normal), "Interface\\Priorities\\getTool.bmp");
    loadTexture(&(menuTools.pushed), "Interface\\Priorities\\PgetTool.bmp");
    loadTexture(&(menuTools.disabled), "Interface\\Priorities\\NgetTool.bmp");
    
    loadMenuIcon(&menuEat, "Sandwich");
    loadMenuIcon(&menuDrop, "UnloadMinifigure");
    loadMenuIcon(&menuPickup, "MF_Pickup");
    //loadMenuIcon(&menuTools, "get_Drill"); //TODO: Replace with right file
    loadMenuIcon(&menuSonicBlaster, "PlaceBirdScarer");
    loadMenuIcon(&menuUpgrade, "Upgrade");
    loadMenuIcon(&menuTrain, "TrainAs");
    loadMenuIcon(&menuFirstPerson, "1stPerson");
    loadMenuIcon(&menuShoulder, "2ndPerson");
    loadMenuIcon(&menuThirdPerson, "3rdPerson");
    loadMenuIcon(&menuTeleportback, "delete");
    
    loadIcon(&menuToolstore, "ToolStation");
    loadIcon(&menuSmallTeleport, "SmallTeleporter");
    loadIcon(&menuDock, "dock");
    loadIcon(&menuPowerplant, "PowerStation");
    loadIcon(&menuSupportStation, "barracks");
    loadIcon(&menuUpgradeStation, "upgrade");
    loadIcon(&menuGeologicCenter, "geo");
    loadIcon(&menuOreRefinery, "OreRefinery");
    loadIcon(&menuLaser, "Gunstation");
    loadIcon(&menuLargeTeleport, "LargeTeleporter");
    
    loadIcon(&menuHoverscout, "hoverboard");
    loadIcon(&menuSmalldigger, "SmallDigger");
    loadIcon(&menuTransportvehicle, "SmallTruck");
    loadIcon(&menuRapidRider, "SmallCat");
    loadIcon(&menuSmalllaser, "SmallMWP");
    loadIcon(&menuTunnelscout, "SmallHeli");
    
    loadIcon(&menuLoaderDozer, "Bulldozer");
    loadIcon(&menuGraniteGrinder, "WalkerDigger");
    loadIcon(&menuLargeMobileLaser, "LargeMWP");
    loadIcon(&menuCargoCarrier, "LargeCatamaran");
    loadIcon(&menuChromeCrusher, "LargeDigger");
    loadIcon(&menuTunnelTransport, "LargeHeli");
    
    loadIcon(&menuMinifigures, "minifigures");
    
    loadTexture(&iconPanelNB[3], "Interface\\IconPanel\\IconPanel3WOBack.bmp");
    loadTexture(&iconPanelNB[4], "Interface\\IconPanel\\IconPanel4WOBack.bmp");
    loadTexture(&iconPanelBackHigh, "Interface\\IconPanel\\Back_HL.bmp");
    loadTexture(&iconPanelBackPush, "Interface\\IconPanel\\Back_PR.bmp");
    
    for (int a = 1; a < 12; a++){
        std::string n = std::to_string(a);
        loadTexture(&iconPanel[a], "Interface\\IconPanel\\IconPanel" + n + ".bmp");
    }
    
    loadFont(&fontMenuSmall, "Interface\\Fonts\\MenuLoFont.bmp");
}