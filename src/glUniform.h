/* 
 * File:   glUniform.h
 * Author: Kevin
 *
 * Created on 22. November 2014, 13:52
 */

#ifndef GLUNIFORM_H
#define	GLUNIFORM_H

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

#include <glm/mat2x2.hpp>
#include <glm/mat3x3.hpp>
#include <glm/mat4x4.hpp>

#include <glm/mat2x3.hpp>
#include <glm/mat3x2.hpp>
#include <glm/mat2x4.hpp>
#include <glm/mat4x2.hpp>
#include <glm/mat3x4.hpp>
#include <glm/mat4x3.hpp>

#include <GL/glew.h>

class glProgram;

class glUniform {
public:
    glUniform();
    glUniform(glProgram* program, GLuint id);
    glUniform(const glUniform& orig) = default;
    virtual ~glUniform() = default;
    
    glUniform operator=(glUniform handle);
    
    //require ARB_separate_shader_objects
    
    //basic variables
    GLfloat  operator=(GLfloat  param);
    GLint    operator=(GLint    param);
    GLuint   operator=(GLuint   param);
    //int64_t  operator=(int64_t  param);
    uint64_t operator=(uint64_t param); //requires GL_ARB_bindless_texture
    
    //float vectors
    glm::vec2 operator=(const glm::vec2& param);
    glm::vec3 operator=(const glm::vec3& param);
    glm::vec4 operator=(const glm::vec4& param);
    
    //int vectors
    glm::ivec2 operator=(const glm::ivec2& param);
    glm::ivec3 operator=(const glm::ivec3& param);
    glm::ivec4 operator=(const glm::ivec4& param);
    
    //unsigned int vectors
    glm::uvec2 operator=(const glm::uvec2& param);
    glm::uvec3 operator=(const glm::uvec3& param);
    glm::uvec4 operator=(const glm::uvec4& param);
    
    //matrices
    glm::mat2 operator=(const glm::mat2& param);
    glm::mat3 operator=(const glm::mat3& param);
    glm::mat4 operator=(const glm::mat4& param);
    
    glm::mat2x3 operator=(const glm::mat2x3& param);
    glm::mat3x2 operator=(const glm::mat3x2& param);
    glm::mat2x4 operator=(const glm::mat2x4& param);
    glm::mat4x2 operator=(const glm::mat4x2& param);
    glm::mat3x4 operator=(const glm::mat3x4& param);
    glm::mat4x3 operator=(const glm::mat4x3& param);
private:
    glProgram* m_program = nullptr;
    GLuint     m_handle  = 0;
};

#endif	/* GLUNIFORM_H */

