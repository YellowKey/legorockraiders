/* 
 * File:   glVertexArray.h
 * Author: Kevin
 *
 * Created on 22. November 2014, 13:45
 */

#ifndef GLVERTEXARRAY_H
#define	GLVERTEXARRAY_H

#include <GL/glew.h>

class glBuffer;

class glVertexArray {
public:
    glVertexArray();
    glVertexArray(const glVertexArray& orig) = delete;
    virtual ~glVertexArray();
    
    void bind();
    
    glVertexArray* getBoundVertexArray();
    
    void enableVertexAttribArray(GLuint index);
    void disableVertexAttribArray(GLuint index);
    
    //require GL_ARB_vertex_attrib_binding
    void bindVertexBuffer(GLuint bindingIndex, glBuffer* buffer, void* offset, GLintptr stride);
    void vertexBindingDivisor(GLuint bindingIndex, GLuint divisor); //require ARB_instanced_arrays
    void vertexAttribFormat(GLuint attribIndex, GLint size, GLenum type, GLboolean normalized, void* offset);
    void vertexAttribIFormat(GLuint attribIndex, GLint size, GLenum type, void* offset);
    void vertexAttribLFormat(GLuint attribIndex, GLint size, GLenum type, void* offset);
    void vertexAttribBinding(GLuint attribIndex, GLuint bindingIndex);
private:
    void onUnbind();
    void onBind();
    
    static glVertexArray* s_boundVertexArray;
    
    glBuffer* m_elementArrayBuffer = nullptr;
    GLuint m_handle;
};

#endif	/* GLVERTEXARRAY_H */

