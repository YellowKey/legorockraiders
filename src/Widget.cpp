/* 
 * File:   Widget.cpp
 * Author: Kevin
 * 
 * Created on 21. Juni 2015, 13:27
 */

#include "Widget.h"
#include "Activity.h"
#include "Action.h"

#include <cassert>
#include <algorithm>

Widget::Widget() {
}

Widget::~Widget() {
    if (m_parent){
        m_parent->removeChild(this);
    }
}

void Widget::addChild(Widget* widget) {
    if (widget->m_parent){
        m_parent->removeChild(widget);
    }
    
    m_children.push_back(widget);
    
    widget->m_parent = this;
    
    sendUpdate();
}

void Widget::removeChild(Widget* widget) {
    widget->m_parent = nullptr;
    m_children.erase(std::remove(m_children.begin(), m_children.end(), widget), m_children.end());
    
    sendUpdate();
}

void Widget::deleteChildren() {
    auto iter  = m_children.begin();
    while ((iter = m_children.begin()) != m_children.end()){
        (*iter)->deleteChildren();
        delete *iter;
    }
    /*for (Widget* child: m_children){
        delete child;
    }*/
}


void Widget::setRelPos(const glm::vec2& pos) {
    m_pos = pos;
    
    sendUpdate();
}

void Widget::setBaseSize(const glm::vec2& size) {
    m_size = size;
    
    sendUpdate();
}

void Widget::setScale(const float scale) {
    m_scale = scale;
    
    sendUpdate();
}



glm::vec2 Widget::getAbsPos() {
    glm::vec2 pos = getRelPos();
    
    if (m_parent){
        pos += m_parent->getAbsPos();
    }
    
    return pos;
}

glm::vec2 Widget::getRelPos() {
    float scale = 1.0f;
    
    if (m_parent){
        scale = m_parent->getScale();
    }
    return m_pos * scale;
}

glm::vec2 Widget::getBaseSize() {
    return m_size;
}

float Widget::getScale() {
    float parentScale = 1.0f;
    
    if (m_parent){
        parentScale = m_parent->getScale();
    }
    
    return m_scale * parentScale;
}

glm::vec2 Widget::getSize() {
    return getBaseSize() * getScale();
}

int Widget::getNumChildren() {
    return m_children.size();
}

Widget* Widget::getChild(int id) {
    assert(id < getNumChildren());
    
    return m_children[id];
}




void Widget::render(Renderer2D* renderer) {
    onPaint(renderer);
    
    for (auto child:m_children){
        child->render(renderer);
    }
}


void Widget::sendKeyDown(Key key) {
    Widget* widget = getFocusedWidget();
    
    if (widget){
        widget->sendKeyDown(key);
    }else{
        onKeyDown(key);
    }
}

void Widget::sendKeyUp(Key key) {
    Widget* widget = getFocusedWidget();
    
    if (widget){
        widget->sendKeyUp(key);
    }else{
        onKeyUp(key);
    }
}

void Widget::sendMouseDown(int mouseButton) {
    Widget* widget = getFocusedWidget();
    
    if (widget){
        widget->sendMouseDown(mouseButton);
    }else{
        onMouseDown(mouseButton);
    }
}

void Widget::sendMouseUp(int mouseButton) {
    Widget* widget = getFocusedWidget();
    
    if (widget){
        widget->sendMouseUp(mouseButton);
    }else{
        onMouseUp(mouseButton);
    }
}

bool Widget::isOver(const glm::vec2 relPos) {
    return (relPos.x < getSize().x && relPos.x > 0 && relPos.y < getSize().y && relPos.y>0);
}


void Widget::sendMouseMove(const glm::vec2& relPos) {
    bool wasHovered = m_hovered;
    m_hovered = isOver(relPos);
    
    if (m_hovered != wasHovered){
        onMouseHover(m_hovered);
    }
    
    onMouseMove(relPos);
    
    for (auto child: m_children){
        child->sendMouseMove(relPos - child->getRelPos());
    }
}

void Widget::sendUpdate() {
    onUpdate();
}

bool Widget::isBeeingHovered() {
    return m_hovered;
}

Widget* Widget::getFocusedWidget() {
    if (m_focused){
        return m_focused;
    }
    
    Widget* widget = nullptr;
    for (Widget* child: m_children){
        if (child->isBeeingHovered()){
            widget = child;
        }
    }
    
    return widget;
}

void Widget::requestInputFocus() {
    if (m_parent){
        m_parent->requestInputFocus();
        m_parent->m_focused = this;
    }else{
        m_focused = this;
    }
}

void Widget::releaseInputFocus() {
    if (m_parent){
        if (m_parent->m_focused == this){
            m_parent->releaseInputFocus();
            m_parent->m_focused = nullptr;
        }
    }else if (m_focused == this){
        m_focused = nullptr ;
    }
}
