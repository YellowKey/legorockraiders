/* 
 * File:   App.h
 * Author: Kevin
 *
 * Created on 1. Mai 2015, 12:00
 */

#ifndef APP_H
#define	APP_H

#include "GameWindow.h"

//#include "Renderer2D.h"
//#include "Engine3D.h"

#include "Texture.h"
#include "Font.h"
#include "Mesh.h"

#include "LoaderWad.h"
#include "LoaderFile.h"

#include "GameRessource.h"
#include "InterfaceRessorces.h"
#include "LoaderRessources.h"

#include "Game.h"
#include "GameMap.h"
#include "GameUIController.h"

//#include "RendererMap.h"

#include "LoaderUIController.h"

#include <SFML/System.hpp>

class Renderer2D;
class Engine3D;
class RendererMap;
class StateMap;

class App {
public:
    static int startApp();
    
    static App* getApp();
    
    const std::string& getName();
    
    LoaderWad* getDataLoader0();
    LoaderWad* getDataLoader1();
    LoaderFile* getFileLoader();
    
    //Widget* getScreenWidget();
    
    GameMap* getGameMap();
    Game* getGame();
    
    float getGameSpeed();
    float getFPS();
    
    float getGameTime();
    float getTime();
    
    GameRessource* getGameRessource();
    InterfaceRessources* getInterfaceRessources();
    
    Engine3D* getEngine3D();
    RendererMap* getRendererMap();
    
    Texture* getDefaultTexture();
private:
    App();
    App(const App& orig) = delete;
    ~App();
    
    int run();
    
    void init();
    void mainLoop();
    void cleanup();
    
    void quit();
    
    int getWidth();
    int getHeight();
    
    glm::vec2 getCursor();
    
    GameWindow* m_window = nullptr;
    
    static App* s_app;
    const static std::string NAME;
    
    LoaderWad  m_wadLoader0 = {"LegoRR0.wad"};
    LoaderWad  m_wadLoader1 = {"LegoRR1.wad"};
    LoaderFile m_fileLoader;
    
    GameRessource m_gameRessource;
    InterfaceRessources m_interfaceRessources;
    LoaderRessources m_loaderRessources;
    
    Font*    m_fontBrief3;
    Texture* m_textureDefault;
    GameMap* m_gameMap;
    
    Renderer2D* m_renderer2D;
    Engine3D* m_engine3D;
    RendererMap* m_rendererMap;
    StateMap* m_stateMap;
    
    GameUIController* m_gameUIController = nullptr;
    LoaderUIController* m_loaderUIController = nullptr;
    
    Game* m_game;
    
    float m_messureStartTime;
    int   m_messureCurrentFrames = 0;
    float m_messureTime = 0.3f;
    
    float m_gameSpeed = 1.0f;
    float m_fps       = 0.0f;
    
    float m_time;
    float m_gameTime;
    bool  m_quit;
    
    sf::Clock m_clock;
};

#endif	/* APP_H */

