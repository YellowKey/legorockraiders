/* 
 * File:   RessourceContainer.h
 * Author: Kevin
 *
 * Created on 1. Oktober 2015, 11:58
 */

#ifndef RESSOURCECONTAINER_H
#define	RESSOURCECONTAINER_H

#include "Activity.h"
#include "Font.h"
#include "Texture.h"
#include "Mesh.h"

#include <deque>

class RessourceContainer {
public:
    struct MenuIcon{
        Texture* normal;
        Texture* disabled;
        Texture* pushed;
    };
    
    RessourceContainer();
    RessourceContainer(const RessourceContainer& orig) = delete;
    virtual ~RessourceContainer();
    
    static bool loadNext();
    static void loadAll();
    static void load(int count);
    
    static int getCount();
protected:
    enum LoadType{
        TYPE_MESH,
        TYPE_TEXTURE,
        TYPE_ACTIVITY,
        TYPE_FONT,
        TYPE_SHAPE,
    };
    struct Load{
        std::string file;
        LoadType type;
        union{
            Mesh** ptrMesh;
            Texture** ptrTexture;
            Activity** ptrActivity;
            Font** ptrFont;
        };
    };
    void loadMesh(Mesh** pointer, const std::string& name);
    void loadShape(Mesh** pointer, const std::string& name);
    void LoadActivity(Activity** pointer, const std::string& name);
    
    void loadTexture(Texture** pointer, const std::string& file);
    void loadMenuIcon(MenuIcon* pointer, const std::string& name);
    void loadIcon(MenuIcon* pointer, const std::string& name);
    
    void loadFont(Font** pointer, const std::string& file);
    
    
    static void loadTexture(Load& load);
    static void loadMesh(Load& load);
    static void loadActivity(Load& load);
    static void loadFont(Load& load);
    static void loadShape(Load& load);
    
    static std::deque<Load> s_loadList;
};

#endif	/* RESSOURCECONTAINER_H */

