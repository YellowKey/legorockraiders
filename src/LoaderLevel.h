/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   LoaderLevel.h
 * Author: Kevin
 *
 * Created on 30. Dezember 2015, 16:49
 */

#ifndef LOADERLEVEL_H
#define LOADERLEVEL_H

#include "StateMap.h"

#include <string>

class LoaderLevel {
public:
    LoaderLevel();
    LoaderLevel(const LoaderLevel& orig) = delete;
    ~LoaderLevel();
    
    StateMap* loadMap(const std::string& name);
private:
    bool loadSurf(StateMap* map, const std::string& file);
    bool loadDugg(StateMap* map, const std::string& file);
    bool loadCror(StateMap* map, const std::string& file);
    bool loadHigh(StateMap* map, const std::string& file);
};

#endif /* LOADERLEVEL_H */

