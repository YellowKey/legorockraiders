/* 
 * File:   Ressource.h
 * Author: Kevin
 *
 * Created on 2. Juni 2015, 22:10
 */

#ifndef RESSOURCE_H
#define	RESSOURCE_H

#include <glm/vec2.hpp>

#include "MapObject.h"
#include "EngineObject.h"

class Ressource: public MapObject{
public:
    enum Type{
        TYPE_CRYSTAL,
        TYPE_ORE,
        TYPE_BRICK,
    };
    Ressource(Type type);
    Ressource(const Ressource& orig) = delete;
    ~Ressource();
    
    virtual void setPosMap(const glm::vec2& pos);
    virtual void update() override;
    
    Type GetType();
protected:
    virtual void onRTSChange() override;

    virtual void onAttachTo(MapObject* target, int slotId) override;
    virtual void onRemoved() override;
private:
    Type m_type;
    EngineObject m_engineObject;
    float m_y;
    float m_rot;
    
    bool m_attached = false;
};

#endif	/* RESSOURCE_H */

