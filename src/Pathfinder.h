/* 
 * File:   Pathfinder.h
 * Author: Kevin
 *
 * Created on 8. Juni 2015, 14:46
 */

#ifndef PATHFINDER_H
#define	PATHFINDER_H

#include "GameMap.h"
#include "Path.h"

#include <cmath>

#include <list>

class Pathfinder {
public:
    Pathfinder();
    Pathfinder(const Pathfinder& orig) = delete;
    virtual ~Pathfinder();
    
    void setMap(GameMap* map);
    
    void setGroundSpeed(float speed);
    void setGravelSpeed(float speed);
    void setPathSpeed(float speed);
    void setWaterSpeed(float speed);
    
    void setStartPoint(const glm::ivec2& pos);
    void setEndPoint(const glm::ivec2& end);
    
    void clear();
    
    bool isGoal();
    
    Path getPath();
    
    void pathfind();
private:
    struct Test{
        const glm::ivec2 offset;
        float distance;
    };
    struct Node{
        float dist;
        glm::ivec2 pos;
    };
    struct TileData{
        float distance;
    };
    
    float getTileSpeed(const glm::ivec2& pos);
    
    void initPathfinding();
    void mainPathfinding();
    void checkNode(const Node& data);
    void addNode(const Node& data);
    Node getNextNode();
    bool isFinished();
    
    inline TileData& tile(int x, int y){
        return m_tileData[x + y * m_width];
    }
    inline TileData& tile(const glm::ivec2& pos){
        return tile(pos.x, pos.y);
    }
    
    glm::ivec2 m_startPos;
    glm::ivec2 m_endPos;

    int m_width;
    int m_height;
    
    float m_groundSpeed;
    float m_gravelSpeed;
    float m_pathSpeed;
    float m_waterSpeed;
    
    GameMap*  m_map = nullptr;
    TileData* m_tileData = nullptr;
    
    std::list<Node> m_nodes;
    
    std::array<Test, 8> m_tests = {{
        {glm::ivec2( 1,  0), 1},
        {glm::ivec2(-1,  0), 1},
        {glm::ivec2( 0,  1), 1},
        {glm::ivec2( 0, -1), 1},
        {glm::ivec2( 1,  1), sqrt(2)},
        {glm::ivec2(-1,  1), sqrt(2)},
        {glm::ivec2(-1, -1), sqrt(2)},
        {glm::ivec2( 1, -1), sqrt(2)}}};
};

#endif	/* PATHFINDER_H */

