/* 
 * File:   WidgetMesssagePanel.cpp
 * Author: kevin
 * 
 * Created on June 25, 2015, 12:16 PM
 */

#include "WidgetMesssagePanel.h"

WidgetMesssagePanel::WidgetMesssagePanel() {
    m_widgetText.setRelPos(glm::vec2(8, 0));
    m_widgetText.setBaseSize(glm::vec2(353, 177));
    
    addChild(&m_widgetText);
}


WidgetMesssagePanel::~WidgetMesssagePanel() {
}

void WidgetMesssagePanel::setTextures(InterfaceRessources* ress) {
    setTextureMain(ress->messagePanel);
    setTextureAirBar(ress->mpAirBar);
    setTextureNoAir(ress->mpNoAir);
}

void WidgetMesssagePanel::setTextureMain(Texture* texture) {
    m_textureMain = texture;
    
    setBaseSize(texture->getSize());
}

void WidgetMesssagePanel::setTextureAirBar(Texture* texture) {
    m_textureAirBar = texture;
}

void WidgetMesssagePanel::setTextureNoAir(Texture* texture) {
    m_textureNoAir = texture;
}

void WidgetMesssagePanel::setTextureInfo(Texture* texture) {
    m_textureInfo = texture;
}

void WidgetMesssagePanel::setFont(Font* font) {
    m_widgetText.setFont(font);
}


void WidgetMesssagePanel::setText(const std::string text) {
    m_widgetText.setText(text);
}

void WidgetMesssagePanel::setAir(float value) {
    m_air = value;
}

void WidgetMesssagePanel::onPaint(Renderer2D* renderer) {
    renderer->renderTexture(m_textureMain, getAbsPos(), getSize());
    {
        const glm::vec2 pos  = getAbsPos() + m_airOffset * getScale();
        const glm::vec2 size = m_airSize * glm::vec2(m_air, 1) * getScale();
        renderer->renderTexture(m_textureAirBar, pos, size);
    }
    
    if (m_air <= 0.0001f){
        const glm::vec2 pos = getAbsPos() + m_noAirOffset * getScale();
        const glm::vec2 size = glm::vec2(m_textureNoAir->getSize()) * getScale();
        renderer->renderTexture(m_textureNoAir, pos, size);
    }
    
    if (m_textureInfo){
        const glm::vec2 pos =  getAbsPos() + m_infoOffset * getScale();
        const glm::vec2 size = glm::vec2(m_textureInfo->getSize()) * getScale();
        renderer->renderTexture(m_textureInfo, pos, size);
    }
}




