/* 
 * File:   LoaderWad.cpp
 * Author: Kevin
 * 
 * Created on 27. April 2015, 19:13
 */

#include "LoaderWad.h"
#include "App.h"

#include <iostream>
#include <algorithm>

#if defined(MSDOS) || defined(OS2) || defined(WIN32) || defined(__CYGWIN__)
#  include <fcntl.h>
#  include <io.h>
#  define SET_BINARY_MODE(file) setmode(fileno(file), O_BINARY)
#else
#  define SET_BINARY_MODE(file)
#endif

LoaderWad::LoaderWad(const std::string& filename):m_filename(filename){
    m_file = fopen(filename.c_str(), "rb");
    
    if (!m_file){
        std::cout << "couldn't load: " << filename << std::endl;
        std::cout.flush();
    }else{
        std::cout << "loaded WAD : " << filename << std::endl;
        std::cout.flush();
    }
    
    SET_BINARY_MODE(m_file);
    
    readHeader();
}

LoaderWad::~LoaderWad() {
}


void LoaderWad::readHeader() {
    const int BUFFERSIZE = 200;
    std::string header;
    char buffer[BUFFERSIZE];
    int numFiles;
    
    header   = readString(4);
    numFiles = readInt();
    
    //std::cout << "fileHeader: " << header << std::endl;
    //std::cout << "num Files: " << numFiles << std::endl;
    
    m_files.resize(numFiles);
    
    for (int a = 0; a < numFiles; a++){
        std::string name = readString();
        
        std::transform(name.begin(), name.end(), name.begin(), ::tolower);
        
        m_files[a].relName = name;
    }
    for (int a = 0; a < numFiles; a++){
        m_files[a].absDir = readString();
        //readString(); //absDir not required for anything
        
    }
    for (int a = 0; a < numFiles; a++){
        m_files[a].version = readInt();
        m_files[a].size    = readInt();
        m_files[a].thing   = readInt();
        m_files[a].offset  = readInt();
    }
    
    /*for (int a = 0; a < numFiles; a++){
        const File& f = m_files[a];
        std::cout << "------------------------" << std::endl;
        std::cout << f.relName << std::endl;
        std::cout << f.absDir << std::endl;
        std::cout << " v: " <<f.version<< std::endl;
        std::cout << " o: " << f.offset<< std::endl;
        std::cout << " ?: " << f.thing << std::endl;
        std::cout << " s: " << f.size << std::endl;
    }*/
}

int LoaderWad::readInt() {
    int value, r;
    r = fread(&value, 4, 1, m_file);
    
    if (ferror(m_file)){
        std::cout << "ERROR" << std::endl;
    }
    if (feof(m_file)){
        std::cout << ftell(m_file) << std::endl;
        std::cout << "EOF" << std::endl;
    }
    
    return value;
}

std::string LoaderWad::readString(int maxLength) {
    const int BUFFERSIZE = 200;
    char buffer[BUFFERSIZE];
    
    if (maxLength == 0){
        maxLength = BUFFERSIZE - 1;
    }
    
    int length = 0;
    char lChar;
    
    do{
        buffer[length] = lChar = fgetc(m_file);
        
        ++length;
    }while(length < maxLength && lChar != 0);
    
    buffer[length] = 0;
    
    return std::string(buffer);
}

std::string LoaderWad::getFileName(int id) {
    return m_files[id].relName;
}

int LoaderWad::getFileSize(int id) {
    return m_files[id].size;
}

int LoaderWad::getFileOffset(int id) {
    return m_files[id].offset;
}



int LoaderWad::getNumFiles() {
    return m_files.size();
}

int LoaderWad::getFileId(const std::string& name){
    std::string lname = name;
    
    //std::cout << "Load: " << name << std::endl;
    
    std::transform(lname.begin(), lname.end(), lname.begin(), ::tolower);
    
    for (int a = 0; a < getNumFiles(); a++){
        if (getFileName(a) == lname){
            return a;
        }
    }
    
    //std::cout << "FILE NOT FOUND: " << name << std::endl;
    
    return -1;
}

void* LoaderWad::loadFileData(int id) {
    void* data = new char[getFileSize(id)];
    
    fseek(m_file, getFileOffset(id), SEEK_SET);
    
    fread(data, 1, getFileSize(id), m_file);
    
    return data;
    
}

void LoaderWad::freeFileData(void* data) {
    delete[] (char*)data;
}


