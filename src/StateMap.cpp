/* 
 * File:   StateMap.cpp
 * Author: Kevin
 * 
 * Created on 29. Dezember 2015, 20:42
 */

#include "StateMap.h"


StateMap::StateMap() {
}

StateMap::StateMap(int width, int height) {
    setSize(width, height);
}


StateMap::~StateMap() {
}




void StateMap::setSize(int width, int height) {
    m_width = width;
    m_height = height;
    
    m_tiles.resize(width * height);
    m_heightmap.resize((width + 1) * (height + 1));
}



int StateMap::getTileCrystal(const glm::ivec2& pos) const {
    getTileCrystal(pos.x, pos.y);
}

int StateMap::getTileCrystal(int x, int y) const {
    return tile(x, y).crystalCount;
}


int StateMap::getTileOre(int x, int y) const {
    return tile(x, y).oreCount;
}

int StateMap::getTileOre(const glm::ivec2& pos) const {
    return getTileOre(pos.x, pos.y);
}


bool StateMap::getTileMark(int x, int y, TileMark mark) const {
    return tile(x, y).marks[mark];
}

bool StateMap::getTileMark(const glm::ivec2& pos, TileMark mark) const {
    return getTileMark(pos.x, pos.y, mark);
}


StateMap::TileType StateMap::getTileType(int x, int y) const {
    return tile(x, y).type;
}

StateMap::TileType StateMap::getTileType(const glm::ivec2& pos) const {
    return getTileType(pos.x, pos.y);
}


StateMap::TileVisib StateMap::getTileVisib(int x, int y) const {
    return tile(x, y).visib;
}

StateMap::TileVisib StateMap::getTileVisib(const glm::ivec2& pos) const {
    return getTileVisib(pos.x, pos.y);
}


bool StateMap::isTileWall(int x, int y) const {
    switch(getTileType(x, y)){
        case TILE_DIRT:
        case TILE_LOOSE:
        case TILE_HARD: 
        case TILE_SOLID:
        case TILE_ORESEAM: 
        case TILE_CRYSTALSEAM:
        case TILE_RECHARGE:
            return true;
        default:
            return false;
    }
}

bool StateMap::isTileWall(const glm::ivec2& pos) const {
    return isTileWall(pos.x, pos.y);
}




int StateMap::setTileCrystal(int x, int y, int count) {
    return tile(x, y).crystalCount = count;
}

int StateMap::setTileCrystal(const glm::ivec2& pos, int count) {
    return setTileCrystal(pos.x, pos.y, count);
}


int StateMap::setTileOre(int x, int y, int count) {
    return tile(x, y).oreCount = count;
}

int StateMap::setTileOre(const glm::ivec2& pos, int count) {
    return setTileOre(pos.x, pos.y, count);
}

bool StateMap::setTileMark(int x, int y, TileMark mark, bool state) {
    return tile(x, y).marks[mark] = state;
}

bool StateMap::setTileMark(const glm::ivec2& pos, TileMark mark, bool state) {
    return setTileMark(pos.x, pos.y, mark, state);
}

StateMap::TileType StateMap::setTileType(int x, int y, TileType type) {
    return tile(x, y).type = type;
}

StateMap::TileType StateMap::setTileType(const glm::ivec2& pos, TileType type) {
    return setTileType(pos.x, pos.y, type);
}

StateMap::TileVisib StateMap::setTileVisib(int x, int y, TileVisib visible) {
    return tile(x, y).visib = visible;
}

StateMap::TileVisib StateMap::setTileVisib(const glm::ivec2& pos, TileVisib visible) {
    return setTileVisib(pos.x, pos.y, visible);
}

void StateMap::setHeight(int x, int y, float height) {
    heightmap(x, y) = height;
}

void StateMap::setHeight(const glm::ivec2& pos, float height) {
    setHeight(pos.x, pos.y, height);
}

void StateMap::setMapHeight(float height) {
    m_mapHeight = height;
}

void StateMap::setTileHeight(float height) {
    m_tileHeight = height;
}

void StateMap::setTileWidht(float width) {
    m_tileWidth = width;
}

float StateMap::getHeight(int x, int y) const {
    return heightmap(x, y);
}

float StateMap::getHeight(const glm::vec2& pos) const {
    //TODO: Implement
}

float StateMap::getMapHeight() const {
    return m_mapHeight;
}

float StateMap::getTileWidth() const {
    return m_tileWidth;
}

float StateMap::getTileHeight() const {
    return m_tileHeight;
}

glm::ivec2 StateMap::getSize() const {
    return glm::ivec2(m_width, m_height);
}




sigc::signal<void, const glm::ivec2&>& StateMap::signalTileChange() {
    return m_signalTileChange;
}

sigc::signal<void, int, int>& StateMap::signalSizeChange() {
    return m_signalSizeChange;
}




void StateMap::update(int x, int y) {
    if (x > 0 && x < m_width && y > 0 && y < m_height){
        m_signalTileChange(glm::ivec2(x, y));
        
        if (isTileWall(x, y)){
            if (!canTileBeWall(x, y)){
                destroy(x, y);
            }
        }else if (getTileVisib(x,y) == 2){
            if (getTileVisib(x + 1, y) == VISIB_TRUE ||
                    getTileVisib(x - 1, y) == VISIB_TRUE ||
                    getTileVisib(x, y + 1) == VISIB_TRUE ||
                    getTileVisib(x, y - 1) == VISIB_TRUE){
                    setTileVisib(x, y, VISIB_TRUE);
                updateNeighbours(x, y);
            }
        }
    }
}

void StateMap::updateNeighbours(int x, int y) {
    update(x + 1, y);
    update(x - 1, y);
    update(x, y + 1);
    update(x, y - 1);
    
    update(x + 1, y + 1);
    update(x + 1, y - 1);
    update(x - 1, y - 1);
    update(x - 1, y + 1);
}

void StateMap::updateAll() {
    for (int y = 0; y < m_width; y++){
        for (int x = 0; x < m_height; x++){
            update(x, y);
        }
    }
}

void StateMap::spawnRessources(int x, int y) {
    //TODO: Implement
}

void StateMap::destroy(int x, int y) {
    if (isTileWall(x, y)){
        setTileType(x, y, TILE_GRAVEL);
        setTileVisib(x, y, VISIB_TRUE);
        spawnRessources(x, y);
        update(x, y);
        updateNeighbours(x, y);
    }
}

bool StateMap::canTileBeWall(int x, int y) {
    int count = 0;
    
    bool pz = getTileVisib(x + 1, y    ) != VISIB_TRUE; //A wall or hidden
    bool pp = getTileVisib(x + 1, y + 1) != VISIB_TRUE;
    bool pn = getTileVisib(x + 1, y - 1) != VISIB_TRUE;
    bool nz = getTileVisib(x - 1, y    ) != VISIB_TRUE;
    bool np = getTileVisib(x - 1, y + 1) != VISIB_TRUE;
    bool nn = getTileVisib(x - 1, y - 1) != VISIB_TRUE;
    bool zp = getTileVisib(x    , y + 1) != VISIB_TRUE;
    bool zn = getTileVisib(x    , y - 1) != VISIB_TRUE;
    
    if (zp && pp && pz){
        count += 1;
    }
    if (pz && pn && zn){
        count += 1;
    }
    if (zn && nn && nz){
        count += 1;
    }
    if (nz && np && zp){
        count += 1;
    }
    
    return count > 0;
}
