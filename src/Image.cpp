/* 
 * File:   Image.cpp
 * Author: Kevin
 * 
 * Created on 12. Juni 2015, 15:55
 */

/*
 *              ,,~~--___---,
 *             /            .~,
 *       /  _,~             )
 *      (_-(~)   ~, ),,,(  /'
 *       Z6  .~`' ||     \ |
 *       /_,/     ||      ||
 * ~~~~~~~~~~~~~~~W`~~~~~~W`~~~~~~~~~~~~~~~
 * Evil goats cursed this file. Do not use.
 */
#include "Image.h"

#include <assert.h>
#include <string.h>

Image::Image() {
    int a = 0;
}

Image::Image(int pixelBits, int rowAlign):
    Image(0, 0, pixelBits, rowAlign)
{
}

Image::Image(int width, int height, int pixelBits, int rowAlign):
    m_width(width), m_height(height), m_pixelBits(pixelBits), m_rowAlign(rowAlign)
{
    newBuffers();
    
    assert(m_pixelBits == 8 || m_pixelBits == 24 || m_pixelBits == 32);
}

Image::Image(const Image& orig)
{
    *this = orig;
}

Image::Image(Image&& orig)
{
    *this = orig;
}


Image::~Image() {
    deleteData();
}


Image::Color Image::getColor(int x, int y) const {
    Color color;
    
    if (hasPalett()){
        color = getColor(getColorIndex(x, y));
    }else{
        color.setAlpha(255);
        
        //24 bit mode doesn't have the last 8 Bits;
        
        memcpy(&color, pixel(x, y), getPixelBytes());
    }
    
    return color;
}

Image::Color Image::getColor(const glm::ivec2& pos) const {
    return getColor(pos.x, pos.y);
}

void Image::setColor(int x, int y, Color color) {
    if(!hasPalett()){
        // pixelsize can only be 24 or 32 bits without a pallet
        // in 24 bit mode the last 8 bit are ignored
        // that's why only a single memcpy is needed
        memcpy(pixel(x,y), &color, getPixelBytes());
    }
}

void Image::setColor(const glm::ivec2& pos, Color color) {
    setColor(pos.x, pos.y, color);
}


int Image::getWidth() const {
    return m_width;
}

int Image::getHeight() const {
    return m_height;
}

int Image::getPixelBits() const {
    return m_pixelBits;
}

int Image::getPixelBytes() const {
    return m_pixelBits / 8;
}

int Image::getRowAlign() const {
    return m_rowAlign;
}

int Image::getRowBytes() const {
    int rowBytes = getWidth() * getPixelBytes();
    
    if (rowBytes % m_rowAlign){
        rowBytes += m_rowAlign - rowBytes % m_rowAlign;
    }
    
    return rowBytes;
}



void* Image::getData() {
    return m_data;
}

int Image::getDataSize() const {
    return getRowBytes() * getHeight();
}

int Image::getColorTableSize() const {
    return sizeof(Color[getNumColors()]);
}


void Image::import(const Image& image) {
    if (image.m_pixelBits == m_pixelBits && image.m_rowAlign == m_rowAlign){
        *this = image;
    }else{
        m_width = image.m_width;
        m_height = image.m_height;
        
        newBuffers();
        
        for (int y = 0; y < m_width; y++){
            for(int x = 0; x < m_height; x++){
                setColor(x, y, image.getColor(x, y));
            }
        }
    }
}

void Image::swapBlueRed() {
    for (int y = 0; y < m_width; y++){
        for(int x = 0; x < m_height; x++){
            Color color = getColor(x, y);
            
            auto temp = color.getBlue();
            color.setBlue(color.getRed());
            color.setRed(temp);
            
            setColor(x, y, color);
        }
    }
}




//Color Table functions
Image::Color Image::getColor(int id) const{
    if (hasPalett() && id < getNumColors()){
        return m_colorTable[id];
    }
    
    return Color();
    
}

void Image::setColor(int id, Color color) {
    if (hasPalett() && id < getNumColors()){
        m_colorTable[id] = color;
    }
}

int Image::getNumColors() const {
   return (1 << m_pixelBits); 
}

int Image::getPalettSize() const {
    return sizeof(Color[getNumColors()]);
}

void* Image::getColorTable() {
    return m_colorTable;
}

bool Image::hasPalett() const {
    return getPixelBits() <= 8;
}

int Image::getColorIndex(int x, int y) const {
    return *pixel(x, y);
}



bool Image::equalFormat(const Image& image) const {
    return getWidth() == image.getWidth() &&
            getHeight() == image.getHeight() &&
            getPixelBits() == image.getPixelBits() &&
            getRowAlign() == image.getRowAlign();
}

Image& Image::operator=(const Image& img) {
    deleteData();
    
    m_width     = img.m_width;
    m_height    = img.m_height;
    m_pixelBits = img.m_pixelBits;
    m_rowAlign  = img.m_rowAlign;
    
    newBuffers();
    
    memcpy(m_data, img.m_data, getDataSize());
    if (hasPalett()){
        memcpy(m_colorTable, img.m_colorTable, getPalettSize());
    }
    
    return *this;
}

Image& Image::operator=(Image&& img) {
    //copy all data
    memcpy(this, &img, sizeof(Image));
    
    //make sure no data is deleted
    img.m_data       = nullptr;
    img.m_colorTable = nullptr;
    
    return *this;
}

void Image::deleteData() {
    delete[] m_data;
    delete[] m_colorTable;
    
    m_data = nullptr;
    m_colorTable = nullptr;
}

void Image::newBuffers() {
    deleteData();
    
    if (m_width > 0 && m_height > 0){
        m_data = new uint8_t[getDataSize()];
        
        if (hasPalett()){
            m_colorTable = new Color[getNumColors()];
        }
    }
}


//Color Class

Image::Color::Color():
    Color(255, 255, 255, 255){

}

Image::Color::Color(uint8_t red, uint8_t green, uint8_t blue):
    Color(red, green, blue, 255){

}

Image::Color::Color(uint8_t red, uint8_t green, uint8_t blue, uint8_t alpha):
    m_red(red), m_green(green), m_blue(blue), m_alpha(alpha){

}

//Channel to Float
static constexpr float ctf(const uint8_t value) {
    return (float)value * (1.0f / 256.0f);
}

glm::vec3 Image::Color::toVec3() const {
    return glm::vec3(ctf(getRed()), ctf(getGreen()), ctf(getBlue()));
}

glm::vec4 Image::Color::toVec4() const {
    return glm::vec4(ctf(getRed()), ctf(getGreen()), ctf(getBlue()), ctf(getAlpha()));
}

