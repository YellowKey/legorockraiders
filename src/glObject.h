/* 
 * File:   glObject.h
 * Author: Kevin
 *
 * Created on 17. November 2014, 20:06
 */

#ifndef GLOBJECT_H
#define	GLOBJECT_H

#include <string>

using std::string;

class glObject {
public:
    enum Type{
        TYPE_BUFFER,
        TYPE_TEXTURE,
        TYPE_OTHER,
        
        TYPE_NUM,
        
        TYPE_ALL,
    };
    
    glObject() = default;
    glObject(const glObject& orig) = default ;
    virtual ~glObject();
    
    virtual Type getObjectType();
    
    static size_t getMemoryUsage(Type type);
    static int    getNumObjectTypes();
    static string getObjectTypeName(Type type);
    
    bool dsaExtAvailable();
    bool dsaAvailable();
protected:
    virtual void setMemory(int size);
    virtual int  getMemory();
private:
    int m_memorySize = 0;
    
    static int s_totalMemory[TYPE_NUM];
    static void addMemoryToObjectType(Type type, int mem);
};

#endif	/* GLOBJECT_H */

