/* 
 * File:   Path.cpp
 * Author: Kevin
 * 
 * Created on 8. Juni 2015, 14:46
 */

#include "Path.h"

#include "util.h"

Path::Path() {
}

Path::Path(const Path& orig) {
    *this = orig;
}

Path::Path(const Path&& orig) {
    *this = orig;
}

Path::~Path() {
}

void Path::addNode(const glm::ivec2& pos) {
    m_nodes.push_back({pos});
}

glm::ivec2 Path::getNodePos(int id) const{
    return m_nodes[id].pos;
}

glm::ivec2 Path::popNextNode() {
    glm::ivec2 pos = m_nodes.back().pos;
    
    m_nodes.pop_back();
    
    return pos;
}


int Path::getNumNodes() const{
    return m_nodes.size();
}

void Path::clear() {
    m_nodes.clear();
}

Path& Path::operator=(const Path& path) {
    m_nodes = path.m_nodes;
    
    return *this;
}

Path& Path::operator=(Path&& path) {
    m_nodes = std::move(path.m_nodes);
    
    return *this;
}
