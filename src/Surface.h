/* 
 * File:   Surface.h
 * Author: Kevin
 *
 * Created on 26. April 2015, 15:33
 */

#ifndef SURFACE_H
#define	SURFACE_H

class Surface {
public:
    Surface();
    Surface(const Surface& orig);
    virtual ~Surface();
private:
    
};

#endif	/* SURFACE_H */

