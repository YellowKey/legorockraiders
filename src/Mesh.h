/* 
 * File:   Mesh.h
 * Author: Kevin
 *
 * Created on 17. Mai 2015, 16:04
 */

#ifndef MESH_H
#define	MESH_H

#include "Shape.h"
#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>

class Mesh {
public:
    Mesh();
    Mesh(const Mesh& orig) = delete;
    virtual ~Mesh();
    
    void SetAnimTime(float startTime, float endTime);
    void setLoopAnim(bool state);
    
    int  addGroup();
    void setGroupParent(int groupId, int parentId);
    void setGroupPivotPoint(int groupId, const glm::vec3& point);
    void setGroupName(int groupId, const std::string&name);
    
    int  addKeyframe(int groupId);
    void setKeyFramePos(int groupId, int frameId, const glm::vec3& pos);
    void setKeyFrameRot(int groupId, int frameId, const glm::vec3& rot);
    void setKeyFrameScale(int groupId, int frameId, const glm::vec3& scale);
    void setKeyFrameTime(int groupId, int frameId, float time);
    
    int  addShape(int group, Shape* shape);
    
    int getNumGroups();
    int getGroup(const std::string& name, int index = 0);
    int countGroup(const std::string& name);
    int getNumShapes(int groupId);
    Shape* getShape(int groupId, int shapeId);
    
    float getAnimTime();
    
    glm::mat4 getLocalMatrix(int groupId, float time);
    glm::mat4 getMatrix(int groupId, float time);
    glm::mat4 getBakedMatrix(int groupId, float time); //broken
    glm::vec3 getRot(int groupId, float time);
    glm::vec3 getScale(int groupId, float time);
    glm::vec3 getPos(int groupId, float time);
private:
    struct KeyFrame{
        glm::vec3 rot;
        glm::vec3 pos;
        glm::vec3 scale;
        
        bool matrixBaked;
        glm::mat4 matrix;
        
        float time;
    };
    struct Group{
        std::vector<Shape*> shapes;
        std::vector<KeyFrame> keyframes;
        
        glm::vec3 pivotPoint;
        
        int parentGroup;
        
        std::string name;
    };
    
    float m_startTime;
    float m_endTime;
    
    bool m_loop;
    
    std::vector<Group> m_groups;
    
    int       getNextKeyFrameId(int groupId, float time);
    KeyFrame* getPrevKeyFrame(int groupId, float time);
    KeyFrame* getNextKeyFrame(int groupId, float time);
    float     getBlendFactor(int groupId, float time);
};

#endif	/* MESH_H */

