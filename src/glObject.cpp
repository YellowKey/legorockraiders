/* 
 * File:   glObject.cpp
 * Author: Kevin
 * 
 * Created on 17. November 2014, 20:06
 */

#include "glObject.h"

int glObject::s_totalMemory[TYPE_NUM] = {0, 0};

glObject::~glObject() {
    addMemoryToObjectType(getObjectType(), -m_memorySize);
}


size_t glObject::getMemoryUsage(Type type) {
    if (type >= 0 && type < TYPE_NUM){
        return s_totalMemory[type];
    }else if (type == TYPE_ALL){
        int mem = 0;
        
        for (int a = 0; a < TYPE_NUM; a++){
            mem += s_totalMemory[a];
        }
        
        return mem;
    }else{
        //TODO: Crash with style
        return 0;
    }
}

void glObject::addMemoryToObjectType(Type type, int mem) {
#ifdef GL_DEBUG
    if (type <0 || typ >= TYPE_NUM){
        //TODO: Crash with style
    }
#endif
    
    s_totalMemory[type] += mem;
}

int glObject::getNumObjectTypes() {
    return TYPE_NUM;
}

string glObject::getObjectTypeName(Type type) {
    switch(type){
        case TYPE_BUFFER:
            return "buffer";
        case TYPE_OTHER:
            return "other";
        case TYPE_TEXTURE:
            return "texture";
    }
    
    return "undefined";
}

void glObject::setMemory(int size) {
    addMemoryToObjectType(getObjectType(), -m_memorySize + size);
    m_memorySize = size;
}

int glObject::getMemory() {
    return m_memorySize;
}

bool glObject::dsaExtAvailable() {
    //TODO: Check for DSA
    return false;
}

glObject::Type glObject::getObjectType() {
    return TYPE_OTHER;
}
