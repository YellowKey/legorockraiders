/* 
 * File:   Widget.h
 * Author: Kevin
 *
 * Created on 21. Juni 2015, 13:27
 */

#ifndef WIDGET_H
#define	WIDGET_H

#include <vector>
#include <glm/vec2.hpp>

#include "Engine3D.h"
#include "Renderer2D.h"

class Widget {
public:
    enum Key{
        KEY_LEFT,
        KEY_RIGHT,
        KEY_UP,
        KEY_DOWN,
        
        KEY_MOVEFAST,
        KEY_MOVESLOW,
        KEY_CANCEL,
    };
    
    Widget();
    virtual ~Widget();
    
    //manipulate
    virtual void setRelPos(const glm::vec2& pos);
    virtual void setBaseSize(const glm::vec2& size);
    virtual void setScale(const float scale);
    
    //Getter
    virtual glm::vec2 getRelPos();
    virtual glm::vec2 getAbsPos();
    virtual glm::vec2 getSize();
    virtual glm::vec2 getBaseSize();
    virtual float     getScale();
    
    int getNumChildren();
    Widget* getChild(int id);
    
    //send events
    void sendKeyDown(Key key);
    void sendKeyUp(Key key);
    void sendMouseDown(int mouseButton);
    void sendMouseUp(int mouseButton);
    void sendMouseMove(const glm::vec2& relPos);
    void sendUpdate();
    
    //Actions
    void addChild(Widget* widget);
    void removeChild(Widget* widget);
    void deleteChildren();
    void render(Renderer2D* renderer);
    
    //status
    virtual bool isOver(const glm::vec2 relPos);
    virtual bool isBeeingHovered();
    
    void requestInputFocus();
    void releaseInputFocus();
protected:
     //receive events
    virtual void onMouseDown(int mouseButton){};
    virtual void onMouseUp(int mouseButton){};
    virtual void onMouseHover(bool state){};
    virtual void onMouseMove(glm::vec2 relPos){};
    
    virtual void onKeyDown(Key key){};
    virtual void onKeyUp(Key key){};
    
    virtual void onPaint(Renderer2D* renderer){};
    virtual void onUpdate(){};
    
    glm::vec2 m_pos   = {0.0f, 0.0f};
    glm::vec2 m_size  = {0.0f, 0.0f};
    float     m_scale = 1.0f;
    
    bool m_hovered = false;
private:
    Widget* getFocusedWidget();
    
    std::vector<Widget*> m_children;
    Widget* m_parent = nullptr;
    
    Widget* m_focused = nullptr;
};

#endif	/* WIDGET_H */

