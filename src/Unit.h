/* 
 * File:   Unit.h
 * Author: Kevin
 *
 * Created on 2. Juni 2015, 21:58
 */

#ifndef UNIT_H
#define	UNIT_H

#include <glm/vec2.hpp>

#include "MapObject.h"
#include "EngineObject.h"
#include "Path.h"
#include "GameMap.h"

#include <vector>
#include <deque>

class Action;

class Unit: public MapObject{
public:
    enum Status{
        UNDEFINED,
        STAND,
        WALK,
        DRILL,
        IDLE,
        CLEAR,
        PLACE,
        COLLECT,
    };
    Unit();
    Unit(const Unit& orig) = delete;
    virtual ~Unit();
    
    virtual void select(bool state);
    virtual bool isSelected();
    
    virtual float getWalkSpeed(const glm::ivec2& pos);
    
    virtual void update() override;
    
    virtual void damage(float damage);
    virtual void scare(const glm::vec2& direction);
    
    virtual void setPosAndStay(const glm::vec2& pos);
    virtual void setPosMap(const glm::vec2& pos);
    virtual void setRotf(float rot);
    virtual void setStatus(Status status);
    
    virtual void pickup(MapObject* object);
    virtual MapObject* drop();
    virtual bool isCarring();
    
    virtual glm::vec3 getPosWorld();
    virtual glm::vec2 getPosMap();
    
    GameMap* getGameMap();
    
    void addAction(Action* action);
    void cancelAllActions();
    bool hasActions();
    
    bool isIdle();
    
    virtual bool canMove();
    virtual bool canDrill();
    virtual int  canCarry();
    virtual bool canClean();
protected:
    virtual void onRTSChange() override;

    virtual void onScare();
    virtual void onHurt();
    virtual void onStatusChange(Status status);
    
    bool m_selected = false;
    float m_baseSpeed = 1.0f;
    float m_health = 1.0f;
    
    //glm::vec2 m_pathStartOff;
    //glm::vec2 m_pathEndOff;
    //int m_pathNodes;
    //int m_pathCurrentNodeId;
    
    EngineObject m_engineObject;
    
    std::deque<Action*> m_actions;
    Action* m_currentAction = nullptr;
    
    Status m_status = UNDEFINED;
    
    MapObject* m_carry = nullptr;
};

#endif	/* UNIT_H */

