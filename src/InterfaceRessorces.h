/* 
 * File:   InterfaceRessorces.h
 * Author: Kevin
 *
 * Created on 10. Juni 2015, 17:43
 */

#ifndef INTERFACERESSORCES_H
#define	INTERFACERESSORCES_H

#include "Texture.h"
#include "Font.h"
#include "RessourceContainer.h"

#include <string>

class InterfaceRessources: public RessourceContainer{
public:
    InterfaceRessources();
    InterfaceRessources(const InterfaceRessources& orig) = delete;
    virtual ~InterfaceRessources();
    
    void load();
    
    Texture* crystalBar;
    Texture* oreBar;
    Texture* crystalIcon;
    Texture* crystalIconUsed;
    Texture* crystalIconEmpty;
    
    Texture* iconPanel[13];
    Texture* iconPanelNB[13];
    Texture* iconPanelBackHigh;
    Texture* iconPanelBackPush;
    
    Texture* topPanel;
    Texture* tpArmsHigh;
    Texture* tpArmsPush;
    Texture* tpOptionsHigh;
    Texture* tpOptionsPush;
    Texture* tpPrioHigh;
    Texture* tpPrioPush;
    
    Texture* cameraControl;
    Texture* navHomeHigh;
    Texture* navHomePush;
    Texture* navZoomInHigh;
    Texture* navZoomInPush;
    Texture* navZoomOutHigh;
    Texture* navZoomOutPush;
    Texture* navUp;
    Texture* navDown;
    Texture* navLeft;
    Texture* navRight;
    
    Texture* messagePanel;
    Texture* mpAirBar;
    Texture* mpNoAir;
    
    Texture* mpAirLow;
    Texture* mpAirWarning;
    Texture* mpCavernIce;
    Texture* mpCavernLava;
    Texture* mpCavernRock;
    Texture* mpCelebrate;
    Texture* mpCrystal;
    Texture* mpCrystalFound;
    Texture* mpOreFound;
    Texture* mpPressSpace;
    
    Font* fontMenuSmall;
    
    
    MenuIcon menuDrill;
    MenuIcon menuDynamite;
    MenuIcon menuReinforce;
    MenuIcon menuCancelDrill;
    
    MenuIcon menuPath;
    MenuIcon menuClearRubble;
    MenuIcon menuDestroyPath;
    MenuIcon menuRemoveLava;
    MenuIcon menuFence;
    
    MenuIcon menuMinifigures;
    MenuIcon menuBuildings;
    MenuIcon menuBigVehicles;
    MenuIcon menuSmallVehicles;
    
    MenuIcon menuToolstore;
    MenuIcon menuSmallTeleport;
    MenuIcon menuDock;
    MenuIcon menuPowerplant;
    MenuIcon menuSupportStation;
    MenuIcon menuUpgradeStation;
    MenuIcon menuGeologicCenter;
    MenuIcon menuOreRefinery;
    MenuIcon menuLaser;
    MenuIcon menuLargeTeleport;
    
    MenuIcon menuTrainDriver;
    MenuIcon menuTrainEngineer;
    MenuIcon menuTrainGeologist;
    MenuIcon menuTrainPilot;
    MenuIcon menuTrainSalor;
    MenuIcon menuTrainExplosives;
    
    MenuIcon menuTakeDrill;
    MenuIcon menuTakeShapde;
    MenuIcon menuTakeHammer;
    MenuIcon menuTakeSpanner;
    MenuIcon menuTakeLaser;
    MenuIcon menuTakeFreeze;
    MenuIcon menuTakePush;
    MenuIcon menuTakeSonic;
    
    MenuIcon menuHoverscout;
    MenuIcon menuSmalldigger;
    MenuIcon menuTransportvehicle;
    MenuIcon menuRapidRider;
    MenuIcon menuSmalllaser;
    MenuIcon menuTunnelscout;
    
    MenuIcon menuLoaderDozer;
    MenuIcon menuGraniteGrinder;
    MenuIcon menuLargeMobileLaser;
    MenuIcon menuCargoCarrier;
    MenuIcon menuChromeCrusher;
    MenuIcon menuTunnelTransport;
    
    MenuIcon menuEat;
    MenuIcon menuDrop;
    MenuIcon menuPickup;
    MenuIcon menuTools;
    MenuIcon menuSonicBlaster;
    MenuIcon menuUpgrade;
    MenuIcon menuTrain;
    MenuIcon menuFirstPerson;
    MenuIcon menuShoulder;
    MenuIcon menuThirdPerson;
    MenuIcon menuTeleportback;
private:
};

#endif	/* INTERFACERESSORCES_H */

