/* 
 * File:   Renderer2D.h
 * Author: Kevin
 *
 * Created on 2. Mai 2015, 14:36
 */

#ifndef RENDERER2D_H
#define	RENDERER2D_H

#include <glm/vec2.hpp>

#include "Texture.h"
#include "Font.h"

#include "glVertexArray.h"
#include "glBuffer.h"
#include "glProgram.h"
#include "glTexture.h"

class Renderer2D {
public:
    enum Align{
        LEFT,
        RIGHT,
        CENTER,
    };
    Renderer2D();
    Renderer2D(const Renderer2D& orig) = delete;
    ~Renderer2D();
    
    void setColor(int red, int green, int blue);
    void setColor(float red, float green, float blue);
    void setColor(const glm::vec4 color);
    
    void setFont(const Font* font);
    void setFontScale(float size);
    void setTexture(const Texture* texture);
    void setTextureClipping(const glm::vec2& start, const glm::vec2& end);
    void render(const glm::vec2& pos, const glm::vec2& size);
    
    void setScreenSize(int width, int height);
    void setWindowSize(int width, int height);
    
    void renderTexture(const Texture* texture, const glm::vec2& pos, const glm::vec2& size);
    void renderText(const std::string& text, const glm::vec2& pos, Align mode);
    glm::vec2 renderChar(int charId, const glm::vec2& pos);
    
    void setClipRegion(const glm::vec2& start, const glm::vec2& end);
    
    void flush();
private:
    void addVertex(glm::vec2 pos, glm::vec2 uv);
    void newBuffer();
    
    constexpr static int BUFFERVERTICES = 1024 * 3; //make sure this number can be divided by three
    
    struct Vertex{
        glm::vec2 pos;
        glm::vec2 uv;
        glm::vec4 color;
        int textureId;
    };
    struct TextureUniform{
        glUniform texture;
        glUniform transColor;
    };
    void setupOpenGl();
    
    glVertexArray m_vertexArray;
    glProgram     m_shader;
    glBuffer      m_vertexBuffer = {GL_ARRAY_BUFFER};
    
    std::vector<TextureUniform> m_uniTexture;
    glUniform              m_uniTransColor;
    glUniform              m_uniScaleFactor;
    glUniform              m_uniScreenOffset;
    
    std::vector<const Texture*> m_textures;
    
    glm::vec2 m_screenSize;
    glm::vec2 m_windowSize;
    glm::vec2 m_screenOffset;
    glm::vec2 m_clipStart;
    glm::vec2 m_clipEnd;
    glm::vec4 m_color;

    glm::vec2 m_textureStart;
    glm::vec2 m_textureEnd;
    
    int m_numVertices = 0;
    Vertex* m_vertex = nullptr;
    int m_currentTextureId;
    
    const Font* m_font;
    float m_fontScale = 1.0f;
};

#endif	/* RENDERER2D_H */

