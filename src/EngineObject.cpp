/* 
 * File:   EngineObject.cpp
 * Author: Kevin
 * 
 * Created on 4. Juni 2015, 14:44
 */

#include "EngineObject.h"

#include "App.h"
#include "util.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/euler_angles.hpp>

#include <algorithm>
#include <iostream>
#include <cmath>


const glm::vec4 EngineObject::DEFAULTCOLOR = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);


EngineObject::EngineObject() {
    m_pos = glm::vec3(0);
    m_rot = glm::vec3(0);
    m_scale = glm::vec3(1);
    m_color = DEFAULTCOLOR;
}

EngineObject::EngineObject(Mesh* mesh):EngineObject(){
    setMesh(mesh);
}

EngineObject::~EngineObject() {
    if (m_mesh){
        App::getApp()->getEngine3D()->removeObject(this);
    }
    
    remove();
    
    while (m_children.begin() != m_children.end()){
        (*m_children.begin())->remove();
    }
}

void EngineObject::setMesh(Mesh* mesh) {
    if (mesh != m_mesh){
        if (m_mesh){
            App::getApp()->getEngine3D()->removeObject(this);
        }

        m_mesh = mesh;
        
        stopAnimation();

        if (mesh){
            markChanged();
            App::getApp()->getEngine3D()->registerObject(this);
        }
    }
}

Mesh* EngineObject::getMesh() {
    return m_mesh;
}



void EngineObject::setPos(const glm::vec3& pos) {
    markChanged();
    m_pos = pos;
}

void EngineObject::setRot(const glm::vec3& rot) {
    markChanged();
    m_rot = rot;
}

void EngineObject::setScale(const glm::vec3& scale) {
    markChanged();
    m_scale = scale;
}


glm::vec3 EngineObject::getPos() {
    return m_pos;
}

glm::vec3 EngineObject::getRot() {
    return m_rot;
}

glm::vec3 EngineObject::getScale() {
    return m_scale;
}


glm::mat4 EngineObject::getMatrix() {
    glm::mat4 model = glm::mat4();
    
    model   = glm::translate(model, m_pos);
    
    model = model * glm::eulerAngleYXZ(m_rot.x, m_rot.y, m_rot.z);
    
    model = glm::scale(model, m_scale);
    
    if (m_targetObject){
        model = m_targetObject->getMatrix() * model;
        
        if (m_targetGroup >= 0){
            model = model * m_targetObject->getMesh()->getMatrix(m_targetGroup,  m_targetObject->getAnimTime());
        }
    }
    
    return model;
}



void EngineObject::playAnimation(bool loop) {
    markChanged();
    
    m_animRunning = true;
    m_animStartTime = App::getApp()->getGameTime();
    m_animLooping = loop;
    m_animTime = 0.0f;
}

void EngineObject::pauseAnimation() {
    //m_animTime = getAnimTime();
    m_animRunning = false;
}

void EngineObject::stopAnimation() {
    m_animTime = 0.0f;
    m_animRunning = false;
}

float EngineObject::getAnimTime() {
    if (m_animRunning){
        m_animTime = App::getApp()->getGameTime() - m_animStartTime;
        
        float time = m_mesh->getAnimTime();
        
        if (!m_animLooping && m_animTime > time){
            //m_animTime = time;
            pauseAnimation();
        }else if (m_animLooping){
            if (time < 0.001f){ //static animation;
                m_animTime = 0;
            }
            m_animTime -= floor(m_animTime / time) * time;
            
            /*while (m_animTime > time){ //TODO: Avoid loop
                m_animTime -= time;
                m_animStartTime += time;
            }*/
        }
    }
    return m_animTime;
}

bool EngineObject::isAnimRunning() {
    return m_animRunning;
}

void EngineObject::attachTo(EngineObject* target, int groupId) {
    if (m_targetObject){
        remove();
    }
    
    m_targetObject = target;
    m_targetGroup  = groupId;
    
    m_targetObject->m_children.push_back(this);
    
    markChanged();
}

void EngineObject::remove() {
    if (m_targetObject){
        auto& c = m_targetObject->m_children;
        
        c.erase(std::remove(c.begin(), c.end(), this), c.end());
        
        m_targetObject = nullptr;
    
        markChanged();
    }
}



bool EngineObject::hasChanged() {
    return m_changed;
}

void EngineObject::markChanged() {
    m_changed = true;
    
    for (auto child: m_children){
        child->markChanged();
    }
}

void EngineObject::markUnchanged() {
    if (!m_animRunning && (m_targetObject && !m_targetObject->hasChanged())){
        m_changed = false;
    }
}

void EngineObject::setColor(const glm::vec4& color) {
    m_color = color;
}

glm::vec4 EngineObject::getColor() {
    return m_color;
}
