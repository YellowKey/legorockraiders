/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   StateGame.h
 * Author: Kevin
 *
 * Created on 29. Dezember 2015, 20:41
 */

#ifndef STATEGAME_H
#define STATEGAME_H

#include "StateMap.h"

class StateGame {
public:
    StateGame();
    ~StateGame();
    
    int getOreCount() const;
    int getCrystalCount() const;
    int getBrickCount() const;
    
    void setOreCount(int count);
    void setCrystalCount(int count);
    void setBrickCount(int count);
private:
    int m_oreCount;
    int m_crystalCount;
    int m_brickCount;
    
    StateMap map;
};

#endif /* STATEGAME_H */

