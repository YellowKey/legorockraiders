/* 
 * File:   LoaderLzo.cpp
 * Author: Kevin
 * 
 * Created on 11. Mai 2015, 21:06
 */

#include "LoaderLzo.h"

#include <iostream>
#include <string.h>
#include <bitset>

#include <glm/vec3.hpp>
#include <glm/gtc/quaternion.hpp>
#include "util.h"
#include "App.h"
#include "LoaderBmp.h"

LoaderLzo::LoaderLzo() {
}

LoaderLzo::~LoaderLzo() {
}

bool LoaderLzo::load(std::string filename) {
    LoaderWad* wadLoader = App::getApp()->getDataLoader0();
    
    int   fileId = wadLoader->getFileId(filename);
    
    if (fileId < 0){
        clearData();
        return false;
    }
    
    void* data = wadLoader->loadFileData(fileId);
    
    int lastSlash = filename.rfind('\\') + 1;
    m_path = filename.substr(0, lastSlash);
    
    if (isDebugging()){
        std::cout << "Path: " << m_path << std::endl;
    }
    
    if (data){
        parseData(data, wadLoader->getFileSize(fileId), false);
        wadLoader->freeFileData(data);
        
        return true;
    }else{
        clearData();
        
        return false;
    }
}

char* LoaderLzo::createBuffer(int size) {
    return new char[size];
}

void LoaderLzo::parseData(const void* data, int size, bool autoRelease) {
    clearData();
    
    m_data = (const char*)data;
    m_dataPos = m_data;
    m_dataSize = size;
    
    std::string mainChunkName = readString(4);
    int32_t mainChunkSize = readInt32();
    
    DebugChunk(0, mainChunkName, mainChunkSize);
    
    std::string fileFormat = readString(4);
    
    if (isDebugging()){
        std::cout << "Format: " << fileFormat << std::endl;
    }
    
    while (getDataOffset() - 8 < mainChunkSize){
        std::string chunkName = readString(4);
        int chunkSize = readInt32();
        
        DebugChunk(1, chunkName, chunkSize);
        parseChunk(m_chunkHandler, chunkName, chunkSize);
    }
    
    computeNormals();
    
    if (autoRelease){
        delete[] m_data;
    }
}

void LoaderLzo::clearData() {
    m_vertices.clear();
    m_surfaces.clear();
    m_triangles.clear();
    m_shapes.clear();
    m_currentSurface = 0;
}


void LoaderLzo::DebugChunk(int layer, std::string name, int size) {
    if (isDebugging()){
        for (int a = 0; a < layer; a++){
            std::cout << "  ";
        }
    
        std::cout << name << " (" << size << ")" << std::endl;
    }
}

void LoaderLzo::parseChunk(const std::vector<chunkHandler>& handlerList, const std::string& name, int size) {
    intptr_t pos = getDataOffset();
    
    for (auto& handler: handlerList){
        if (handler.name == name){
            (this->*handler.handler)(size);
        }
    }
    
    setDataOffset(pos + size);
}

intptr_t LoaderLzo::getDataOffset() {
    return (intptr_t)(m_dataPos - m_data);
}

void LoaderLzo::setDataOffset(intptr_t offset) {
    m_dataPos = m_data + offset;
}

std::string LoaderLzo::readString() {
    std::string text;
    
    while(true){
        char character = readChar();
        
        if (character != 0){
            text += character;
        }else{
            break;
        }
    }
    
    if (text.size() % 2 == 0){ //String + null uneven
        readChar();
    }
    
    return text;
}

std::string LoaderLzo::readString(int size){
    char buffer[size];
    
    readMem(buffer, size);
    
    return std::string(buffer, size);
}

int32_t LoaderLzo::readInt32() {
    int32_t value;
    int8_t* value8 = (int8_t*)&value;
    
    value8[3] = readInt8();
    value8[2] = readInt8();
    value8[1] = readInt8();
    value8[0] = readInt8();
    
    return value;
}

uint32_t LoaderLzo::readUInt32() {
    uint32_t value;
    int8_t* value8 = (int8_t*)&value;
    
    value8[3] = readInt8();
    value8[2] = readInt8();
    value8[1] = readInt8();
    value8[0] = readInt8();
    
    return value;
}


int16_t LoaderLzo::readInt16() {
    int16_t value;
    int8_t* value8 = (int8_t*)&value;
    
    value8[1] = readInt8();
    value8[0] = readInt8();
    
    return value;
}

uint16_t LoaderLzo::readUInt16() {
    uint16_t value;
    int8_t* value8 = (int8_t*)&value;
    
    value8[1] = readInt8();
    value8[0] = readInt8();
    
    return value;
}


int8_t LoaderLzo::readInt8() {
    int8_t value;
    
    readMem(&value, 1);
    
    return value;
}

uint8_t LoaderLzo::readUInt8() {
    uint8_t value;
    
    readMem(&value, 1);
    
    return value;
}


char LoaderLzo::readChar() {
    char value;
    
    readMem(&value, 1);
    
    return value;
}


float LoaderLzo::readFloat() {
    /*float value;
    
    readMem(&value, 4);
    
    return value;*/
    
    float value;
    int8_t* value8 = (int8_t*)&value;
    
    value8[3] = readInt8();
    value8[2] = readInt8();
    value8[1] = readInt8();
    value8[0] = readInt8();
    
    return value;
}

void LoaderLzo::readMem(void* mem, int size) {
    if (getDataOffset() + size < m_dataSize){
        memcpy(mem, m_dataPos, size);
        
        m_dataPos += size;
    }
}


void LoaderLzo::readFlags(int size) {
    std::bitset<16> flags(readUInt16());
    
    if (isDebugging()){
        std::cout << "      Flags: " << flags << std::endl;
        std::cout << "      Luminous:        " << flags[ 0] << std::endl;
        std::cout << "      Outline:         " << flags[ 1] << std::endl;
        std::cout << "      Smoothing:       " << flags[ 2] << std::endl;
        std::cout << "      ColorHighlights: " << flags[ 3] << std::endl;
        std::cout << "      ColorFilter:     " << flags[ 4] << std::endl;
        std::cout << "      OpaqueEdge:      " << flags[ 5] << std::endl;
        std::cout << "      TransparentEdge: " << flags[ 6] << std::endl;
        std::cout << "      SharpTerminator: " << flags[ 7] << std::endl;
        std::cout << "      DoubleSided:     " << flags[ 8] << std::endl;
        std::cout << "      Additive:        " << flags[ 9] << std::endl;
        std::cout << "      ShadowAlpha:     " << flags[10] << std::endl;
    }
    
    //m_surfaces[m_currentSurface].luminance         = flags[0] ? (1.0f) : (0.0f);
    m_surfaces[m_currentSurface].additiveBlending = flags[9];
    m_surfaces[m_currentSurface].doubleSided      = flags[8];
    m_surfaces[m_currentSurface].smoothShading    = flags[2];
    
}

void LoaderLzo::readVertices(int size) {
    int count = size / 12;
    
    for (int a = 0; a < count; a++){
        Vertex vertex;
        
        vertex.pos.x = readFloat();
        vertex.pos.y = readFloat();
        vertex.pos.z = -readFloat();
        vertex.normal   = glm::vec3(0);
        vertex.triCount = 0;
        
        m_vertices.push_back(vertex);

        if (isDebugging()){
            std::cout << "    " << vertex.pos << std::endl;
        }
    }
}

void LoaderLzo::readPolygons(int size) {
    intptr_t offset = getDataOffset();
    
    while (getDataOffset() - offset + 4 < size){
        int vertId[4];
        
        int count = readInt16();
        
        if (isDebugging()){
            std::cout << "    V: ";
        }
        
        for (int a = 0; a < count; a++){
            int v = readInt16();
            
            if (isDebugging()){
                std::cout << v << " ";
            }
            
            if (a < 4){
                vertId[a] = v;
            }
        }
        
        int surfaceId = readInt16();
        
        if (count >= 3){
            Triangle triangle;
            
            triangle.vertex[0] = vertId[0];
            triangle.vertex[1] = vertId[1];
            triangle.vertex[2] = vertId[2];
            triangle.surfaceId = surfaceId;
            
            m_triangles.push_back(triangle);
        }
        if (count == 4){
            Triangle triangle;
            
            triangle.vertex[0] = vertId[2];
            triangle.vertex[1] = vertId[3];
            triangle.vertex[2] = vertId[0];
            triangle.surfaceId = surfaceId;
            
            m_triangles.push_back(triangle);
        }
        
        if (isDebugging()){
            std::cout << "S: " << surfaceId << std::endl;
        }
        
    }
}


void LoaderLzo::readSurface(int size) {
    intptr_t startPos = getDataOffset();
    
    std::string name = readString();
    
    for (int a = 0; a < m_surfaces.size(); a++){
        if (m_surfaces[a]. name == name){
            m_currentSurface = a;
        }
    }
    
    if (isDebugging()){
        std::cout << "    Name: " << name << std::endl;
        std::cout << "    Id:   " << m_currentSurface << std::endl;
    }
    
    while (getDataOffset() - startPos + 6 < size){
        std::string chunkName = readString(4);
        int chunkSize = readInt16();
        
        DebugChunk(2, chunkName, chunkSize);
        parseChunk(m_surfHandler, chunkName, chunkSize);
    }
}

void LoaderLzo::readSurfaceList(int size) {
    intptr_t startPos = getDataOffset();
    
    int id = 0;
    while (getDataOffset() - startPos + 2 < size){
        ++id;
        
        std::string name = readString();
        
        getSurface(id)->name = name;
        
        if (isDebugging()){
            std::cout << "    [" << id << "]" << name << std::endl;
        }
    }
}

void LoaderLzo::readTags(int size) {

}







void LoaderLzo::readColor(int size) {
    float red   = (float)readUInt8() / 255;
    float green = (float)readUInt8() / 255;
    float blue  = (float)readUInt8() / 255;
    
    glm::vec3 color(red, green, blue);
    
    getSurface(m_currentSurface)->color = color;
    
    if (isDebugging()){
        std::cout << "      Color: " << color << std::endl;
    }
    
}

void LoaderLzo::readDiffuse(int size) {
    float diffuse = readFloat();
    
    if (isDebugging()){
        std::cout << "    Diffuse: " << diffuse << std::endl;
    }
}

void LoaderLzo::readLuminance(int size) {
    float luminance = readFloat();
    
    m_surfaces[m_currentSurface].luminance = luminance;
    
    if (isDebugging()){
        std::cout << "    Luminance: " << luminance << std::endl;
    }
}



void LoaderLzo::readColorTexture(int size) {
    std::string name = readString();
    
    if (isDebugging()){
        std::cout << "      Texture: " << name << std::endl;
    }
}

void LoaderLzo::readTextureColor(int size) {
    if (isDebugging()){
        std::cout << "      Color: ";
        std::cout << (int)readUInt8() << ", ";
        std::cout << (int)readUInt8() << ", ";
        std::cout << (int)readUInt8() << ", ";
        std::cout << (int)readUInt8() << std::endl;;
    }
}

void LoaderLzo::readTextureImage(int size) {
    std::string file = readString();
    
    size_t begin = file.rfind('\\') + 1;
    size_t last = std::string::npos;
    
    size_t offset = file.find(" (sequence)");
    if (offset != std::string::npos){
        last = offset;
    }
    
    std::string name = file.substr(begin, last - begin); 
    
    LoaderWad* wadLoader = App::getApp()->getDataLoader0();
    
    if (isDebugging()){
        std::cout << "      File: \"" << file << "\" => \"" << name << "\"" << std::endl;
    }
    
    if (name == "(none)"){
        return;
    }
    
    int fileId = wadLoader->getFileId(m_path + name);
    
    LoaderFile* fileLoader = App::getApp()->getFileLoader();
    if (fileId == -1){
        getSurface(m_currentSurface)->texture = fileLoader->loadTexture("world\\shared\\" + name);
        //fileId = wadLoader->getFileId("world\\shared\\" + name);
    }else{
        getSurface(m_currentSurface)->texture = fileLoader->loadTexture(m_path + name);
    }
    
    std::cout.flush();
    
    /*void* data = wadLoader->loadFileData(fileId);
    bmpLoader.load(data,wadLoader->getFileSize(fileId));
    bmpLoader.convertToRGB();
    getSurface(m_currentSurface)->texture = new Texture(bmpLoader.getData(), bmpLoader.getWidth(), bmpLoader.getHeight(), Texture::RGB8);
    wadLoader->freeFileData(data);*/
}

void LoaderLzo::readTextureSize(int size) {
    glm::vec3 textureSize;
    
    textureSize.x = readFloat();
    textureSize.y = readFloat();
    textureSize.z = readFloat();
    
    getSurface(m_currentSurface)->size = textureSize;
    
    if (isDebugging()){
        std::cout << "      Size: " << textureSize << std::endl;
    }
}

void LoaderLzo::readTextureCenter(int size) {
    glm::vec3 textureCenter;
    
    textureCenter.x = readFloat();
    textureCenter.y = readFloat();
    textureCenter.z = readFloat();
    
    getSurface(m_currentSurface)->center = textureCenter;
    
    if (isDebugging()){
        std::cout << "      Center: " << textureCenter << std::endl;
    }
}

void LoaderLzo::readTextureFlags(int size) {
    std::bitset<16> flags(readUInt16());
    
    if (flags[0]){
        getSurface(m_currentSurface)->axis = 0;
    }else if(flags[1]){
        getSurface(m_currentSurface)->axis = 1;
    }else if(flags[2]){
        getSurface(m_currentSurface)->axis = 2;
    }
    
    if (isDebugging()){
        std::cout << "      Flags: " << flags << std::endl;
        std::cout << "      X-Axis:       " << flags[0] << std::endl;
        std::cout << "      Y-Axis:       " << flags[1] << std::endl;
        std::cout << "      Z-Axis:       " << flags[2] << std::endl;
        std::cout << "      WorldCoords:  " << flags[3] << std::endl;
        std::cout << "      NegativeImage:" << flags[4] << std::endl;
        std::cout << "      PixelBlending:" << flags[5] << std::endl;
        std::cout << "      AntiAliasing: " << flags[6] << std::endl;
    }
}

void LoaderLzo::readTextureWrap(int size) {
    int wrapX = readUInt16();
    int wrapY = readUInt16();
    
    if (isDebugging()){
        for (int a = 0; a < 2; a++){
            int v;
            if (a == 0){
                std::cout << "      WrapX: ";
                v = wrapX;
            }else{
                std::cout << "      WrapY: ";
                v = wrapY;
            }

            if (v == 0){
                std::cout << "black";
            }else if (v == 1){
                std::cout << "clamped";
            }else if (v == 2){
                std::cout << "repeated";
            }else if (v == 3){
                std::cout << "mirrored";
            }else if (v == 4){
                std::cout << "unkown";
            }
            std::cout << std::endl;
        }
    }
}


void LoaderLzo::readTextureSequence(int size) {
    int offset = readUInt16();
    std::bitset<16> flags(readUInt16());
    int length = readUInt16();
    
    if (isDebugging()){
        std::cout << "      " << offset << ", " << flags << ", " << length << std::endl;
    }
}

Shape* LoaderLzo::getShape(int id) {
    while (m_shapes.size() <= id){
        m_shapes.push_back(nullptr);
    }
    
    if (m_shapes[id]){
        return m_shapes[id];
    }else{
        return m_shapes[id] = createShape(id);
    }
}

int LoaderLzo::getNumShapes() {
    return m_surfaces.size();
}


LoaderLzo::Surface* LoaderLzo::getSurface(int id) {
    while (m_surfaces.size() <= id){
        m_surfaces.push_back(Surface());
    }
    
    return &(m_surfaces[id]);
}


Shape* LoaderLzo::createShape(int id) {
    Shape* shape = new Shape();
    Surface* surface = getSurface(id);
    
    shape->setTexture(Shape::TEXTURE_COLOR, surface->texture);
    shape->setAdditiveBlending(surface->additiveBlending);
    shape->setLuminance(surface->luminance);
    
    if (surface->texture == nullptr){
        shape->setColor(surface->color);
    }
    
    for (int a = 0; a < m_triangles.size(); a++){
        Triangle& triangle = m_triangles[a];
        
        if (triangle.surfaceId == id){
            glm::vec3 posList[3];
            glm::vec2 uvList[3];
            glm::vec3 normList[3];
            for (int b = 0; b < 3; b++){
                int vertId = triangle.vertex[b];
            
                if (vertId < m_vertices.size()){
                    Vertex& vertex = m_vertices[vertId];
                    
                    glm::vec3 pos = vertex.pos;
                    glm::vec3 norm = triangle.normal[b];
                    
                    glm::vec3 uvPos = (glm::vec3(pos.x, pos.y, -pos.z) - surface->center) / surface->size;
                    glm::vec2 uv;
                    
                    if (surface->axis == 0){
                        uv = glm::vec2(uvPos.z, uvPos.y);
                    }else if(surface->axis == 1){
                        uv = glm::vec2(uvPos.x, uvPos.z);
                    }else if(surface->axis == 2){
                        uv = glm::vec2(uvPos.x, uvPos.y);
                    }
                    
                    //uv = (uv + glm::vec2(1.0f)) * glm::vec2(0.5f);
                    uv += glm::vec2(0.5f);
                    
                    shape->addVertex(pos, uv, norm);
                    
                    posList [b] = pos;
                    uvList  [b] = uv;
                    normList[b] = norm;
                }
            }
            
            if (surface->doubleSided){
                shape->addVertex(posList[2], uvList[2], -normList[2]);
                shape->addVertex(posList[1], uvList[1], -normList[1]);
                shape->addVertex(posList[0], uvList[0], -normList[0]);
            }
        }
    }
    
    return shape;
}


bool LoaderLzo::isDebugging() {
    return false;
}

void LoaderLzo::computeNormals() {
    //Compute each triangle individually
    for (int t = 0; t < m_triangles.size(); t++){
        auto& tri = m_triangles[t];

        glm::vec3 v0 = m_vertices[tri.vertex[0]].pos;
        glm::vec3 v1 = m_vertices[tri.vertex[1]].pos;
        glm::vec3 v2 = m_vertices[tri.vertex[2]].pos;

        tri.normal[2] = tri.normal[1] = tri.normal[0] = glm::normalize(glm::cross(v2 - v0, v1 - v0));
        
        for (int v = 0; v < 3; v++){
            auto& vert = m_vertices[tri.vertex[v]];
            
            vert.normal   += tri.normal[0];
            vert.triCount += 1;
        }
    }
    
    //Smooth shading
    for (int t = 0; t < m_triangles.size(); t++){
        auto& tri = m_triangles[t];
        
        if (m_surfaces[tri.surfaceId].smoothShading){
            tri.normal[0] = m_vertices[tri.vertex[0]].normal / (float)m_vertices[tri.vertex[0]].triCount;
            tri.normal[1] = m_vertices[tri.vertex[1]].normal / (float)m_vertices[tri.vertex[1]].triCount;
            tri.normal[2] = m_vertices[tri.vertex[2]].normal / (float)m_vertices[tri.vertex[2]].triCount;
        }
    }
    
    //Testcode
    /*for (int t = 0; t < m_triangles.size(); t++){
        auto& tri = m_triangles[t];
        
        tri.normal[0] = glm::vec3(1, 0, 0);
        tri.normal[1] = glm::vec3(0, 1, 0);
        tri.normal[2] = glm::vec3(0, 0, 1);
    }*/
}
