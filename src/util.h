/* 
 * File:   util.h
 * Author: Kevin
 *
 * Created on 11. Mai 2014, 16:54
 */

#ifndef UTIL_H
#define	UTIL_H

#include <string>

template <class T>
inline constexpr T min(T a, T b){
    return a < b ? a : b;
}

template <class T>
inline constexpr T max(T a, T b){
    return a > b ? a : b;
}

template <class T>
inline constexpr T clamp(T x, T a, T b){
    return max(min(x , b), a);
}

template <class T>
inline constexpr T abs(T v){
    return v < 0 ? v : -v;
}

template <class T>
inline T range(T x, T lower, T upper){
    T off = (x - lower) / upper;
    off -= floor(off); 
    
    return x * upper + lower; 
}

inline bool lowerEqual(const std::string& s1, const std::string& s2){
    if (s1.length() != s2.length()){
        return false;
    }
    
    for (int a = 1; a < s1.length(); a++){
        if (tolower(s1[a]) != tolower(s2[a])){
            return false;
        }
    }
    
    return true;
}

#include <iostream>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

template <typename T, glm::precision P>
std::ostream& operator<< (std::ostream &stream, const glm::tvec1<T,P>& vec){
    return stream << "(" << vec.x << ")";
}

template <typename T, glm::precision P>
std::ostream& operator<< (std::ostream &stream, const glm::tvec2<T,P>& vec){
    return stream << "(" << vec.x << "," << vec.y << ")";
}

template <typename T, glm::precision P>
std::ostream& operator<< (std::ostream &stream, const glm::tvec3<T,P>& vec){
    return stream << "(" << vec.x << ", " << vec.y << ", " << vec.z << ")";
}

template <typename T, glm::precision P>
std::ostream& operator<< (std::ostream &stream, const glm::tvec4<T,P>& vec){
    return stream << "(" << vec.x << ", " << vec.y << ", " << vec.z << ", " << vec.w << ")";
}


#ifndef M_PI
    #define M_PI		3.14159265358979323846
#endif

#ifndef M_PI_2
    #define M_PI_2		1.57079632679489661923
#endif

#ifndef M_PI_4
    #define M_PI_4		0.78539816339744830962
#endif


#endif	/* UTIL_H */
