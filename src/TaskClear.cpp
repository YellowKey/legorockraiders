/* 
 * File:   TaskClear.cpp
 * Author: Kevin
 * 
 * Created on 19. August 2015, 14:35
 */

#include "TaskClear.h"
#include "GameMap.h"
#include "Unit.h"
#include "ActionWalk.h"
#include "ActionClear.h"
#include "App.h"

#include <iostream>

TaskClear::TaskClear(const glm::ivec2 &tile):m_tile(tile){
    App::getApp()->getGameMap()->setTileMark(m_tile.x, m_tile.y, GameMap::MARK_CLEANUP, true);
}

TaskClear::~TaskClear() {
    App::getApp()->getGameMap()->setTileMark(m_tile.x, m_tile.y, GameMap::MARK_CLEANUP, false);
}

glm::vec2 TaskClear::getPos() {
    return GameMap::tileToPos(m_tile);
}

Task::TaskType TaskClear::getTaskType() {
    return TASK_CLEAR;
}

bool TaskClear::assignUnit(Unit* unit) {
    if (unit->canClean()){
        unit->addAction(new ActionWalk(GameMap::tileToPos(m_tile)));
        unit->addAction(addAction(new ActionClear(m_tile)));

        m_assigned = true;

        return true;
    }
    
    return false;
}

bool TaskClear::isAssigned() {
    return m_assigned;
}

void TaskClear::unitCanceled() {
    m_assigned = false;
}

bool TaskClear::operator==(const Task& task) {
    const TaskClear* taskClear = dynamic_cast<const TaskClear*>(&task);
    
    if (taskClear && taskClear->m_tile == m_tile){
        return true;
    }
    
    return false;
}
