/* 
 * File:   ButtonListener.cpp
 * Author: Kevin
 * 
 * Created on 14. August 2015, 10:29
 */

#include "ButtonListener.h"
#include "Action.h"
#include <algorithm>

ButtonListener::ButtonListener() {
}

ButtonListener::~ButtonListener() {
    for (WidgetButton* button: m_buttons){
        button->removeListener(this);
    }
}


void ButtonListener::onClick(int buttonId, int mouseButton) {

}

void ButtonListener::onHover(int buttonId, bool state) {

}

void ButtonListener::onPush(int buttonId, int mouseButton) {

}

void ButtonListener::onRelease(int buttonId, int mouseButton) {

}


void ButtonListener::addButton(WidgetButton* button) {
    m_buttons.push_back(button);
}

void ButtonListener::removeButton(WidgetButton* button) {
    m_buttons.erase(std::remove(m_buttons.begin(), m_buttons.end(), button), m_buttons.end());
}
