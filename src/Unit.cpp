/* 
 * File:   Unit.cpp
 * Author: Kevin
 * 
 * Created on 2. Juni 2015, 21:58
 */

#include "Unit.h"

#include "App.h"
#include "Pathfinder.h"

#include <glm/gtc/epsilon.hpp>

#include <iostream>
#include "util.h"

#include <cmath>

#include "Action.h"

Unit::Unit() {
    
}

Unit::~Unit() {
    drop();
}

void Unit::select(bool state) {
    m_selected = state;
}

bool Unit::isSelected() {
    return m_selected;
}

float Unit::getWalkSpeed(const glm::ivec2& pos) {
    /* walk ~ 0.16
     * run 
     */
    return 0.6f;
}

void Unit::update() {
    if (m_currentAction == nullptr){
        if (m_actions.size() > 0){
            m_currentAction = m_actions.front();
            m_actions.pop_front();
            m_currentAction->start();
        }else{
            setStatus(STAND);
        }
    }
    
    if (m_currentAction){
        m_currentAction->updateUnit();
        
        if (m_currentAction->isFinished()){
            delete m_currentAction;
            m_currentAction = nullptr;
        }
    }
}


void Unit::damage(float damage) {

}

void Unit::scare(const glm::vec2& direction) {

}


void Unit::setRotf(float rot) {
    //m_engineObject.setRot(glm::vec3(rot, 0, 0));
    setRot(glm::vec3(rot, 0, 0));
}

void Unit::setPosMap(const glm::vec2& pos) {
    /*m_pos = pos;
    
    m_engineObject.setPos(glm::vec3(pos.x, 0, pos.y));*/
    setPos(glm::vec3(pos.x, 0, pos.y));
}

void Unit::setPosAndStay(const glm::vec2& pos) {
    setPosMap(pos);
}

void Unit::setStatus(Status status) {
    if (status != m_status){
        m_status = status;
        onStatusChange(status);
    }
}


GameMap* Unit::getGameMap() {
    return App::getApp()->getGameMap();
}

glm::vec3 Unit::getPosWorld() {
    return getPos();
}

glm::vec2 Unit::getPosMap() {
    auto pos = getPos();
    
    return glm::vec2(pos.x, pos.z);
}

void Unit::addAction(Action* action) {
    action->setUnit(this);
    
    m_actions.push_back(action);
}

void Unit::cancelAllActions() {
    if (m_currentAction){
        m_currentAction->cancel();
        delete m_currentAction;
        
        m_currentAction = nullptr;
    }
    
    for (auto action: m_actions){
        delete action;
    }
    
    m_actions.clear();
}

bool Unit::hasActions() {
    return m_actions.size() > 0;
}


void Unit::onStatusChange(Status status) {

}

void Unit::onHurt() {

}

void Unit::onScare() {

}

bool Unit::isIdle() {
    if (!hasActions() && m_currentAction == nullptr){
        return true;
    }
    
    return false;
}

void Unit::pickup(MapObject* object) {
    if (m_carry != object){
        drop();
    }
    
    object->attachTo(this, 1);
    m_carry = object;
}

MapObject* Unit::drop() {
    MapObject* obj = m_carry;
    
    if (m_carry){
        glm::vec3 pos = getSlotPos(1);
        m_carry->setPos(glm::vec3(pos.x, 0, pos.z));
        m_carry->remove();
        m_carry = nullptr;
    }
    
    return obj;
}

void Unit::onRTSChange() {
    m_engineObject.setPos(getPos());
    m_engineObject.setRot(getRot());
    m_engineObject.setScale(getScale());
}

bool Unit::isCarring() {
    if (m_carry){
        return true;
    }
    
    return false;
}

bool Unit::canMove() {
    return true;
}

int Unit::canCarry() {
    return 1;
}

bool Unit::canClean() {
    return true;
}

bool Unit::canDrill() {
    return true;
}



