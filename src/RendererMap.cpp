/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   RendererMap.cpp
 * Author: Kevin
 * 
 * Created on 30. Dezember 2015, 11:52
 */

#include "RendererMap.h"

#include <glm/gtx/intersect.hpp>

#include <cassert>
#include <array>
#include <iostream>
#include <bitset>
#include <utility>
#include <glm/gtc/quaternion.hpp>

#include "util.h"
#include "Action.h"

constexpr int RendererMap::TILESIZE;

RendererMap::RendererMap() {
    m_shader.load("shader/shaderTerrain");
    
    m_uniCameraMatrix = m_shader.getUniform("cameraMatrix");
    m_uniColorTexture = m_shader.getUniform("colorTexture");
    m_uniNoiseTexture = m_shader.getUniform("noiseTexture");
    m_uniColorFactor  = m_shader.getUniform("colorFactor");
    m_uniCameraPos    = m_shader.getUniform("cameraPos");
    m_uniGamma        = m_shader.getUniform("gamma");
    m_uniFlowScale    = m_shader.getUniform("flowScale");
    m_uniFlowTime     = m_shader.getUniform("flowOffset");
    
    m_uniCursorLightPos    = m_shader.getUniform("cursorLightPos");
    m_uniCursorLightColor  = m_shader.getUniform("cursorLightColor");
    m_uniAmbientLightColor = m_shader.getUniform("ambientLightColor");
    
    m_vertexArray.vertexAttribFormat(0, 3, GL_FLOAT, false, (void*)offsetof(Vertex, pos));
    m_vertexArray.vertexAttribFormat(1, 3, GL_FLOAT, false, (void*)offsetof(Vertex, uv));
    m_vertexArray.vertexAttribFormat(2, 3, GL_FLOAT, false, (void*)offsetof(Vertex, normal));
    m_vertexArray.vertexAttribFormat(3, 2, GL_FLOAT, false, (void*)offsetof(Vertex, flow));
    
    m_vertexArray.bindVertexBuffer(0, &m_vertexBuffer, 0, sizeof(Vertex));
    m_vertexArray.vertexAttribBinding(0, 0);
    m_vertexArray.vertexAttribBinding(1, 0);
    m_vertexArray.vertexAttribBinding(2, 0);
    m_vertexArray.vertexAttribBinding(3, 0);
    
    m_vertexArray.enableVertexAttribArray(0);
    m_vertexArray.enableVertexAttribArray(1);
    m_vertexArray.enableVertexAttribArray(2);
    m_vertexArray.enableVertexAttribArray(3);
}

RendererMap::RendererMap(Engine3D* engine, MapRessource* ressource):RendererMap(){
    setEngine(engine);
    setRessources(ressource);
}



RendererMap::~RendererMap() {
}

int RendererMap::getVertexCount() {
    return TILESIZE * TILESIZE * m_width * m_height * 6;
}


void RendererMap::onRender(Engine3D* engine) {
    static float time = 0.0f; //TODO: use real Time
    time += 0.00005f;
    
    glm::ivec2 pos;
    for (pos.y = 0; pos.y < m_height; pos.y++){
        for (pos.x = 0; pos.x < m_width; pos.x++){
            if (tile(pos).dirty){
                updateTile(pos);
            }
        }
    }
    
    m_vertexArray.bind();
    m_shader.use();
    
    m_uniGamma        = engine->getGamma();
    m_uniCameraMatrix = engine->getCameraMatrix();
    m_uniCameraPos    = engine->getCameraPos();
    m_uniColorFactor  = glm::vec4(1,1,1,1);
    m_uniColorTexture = m_ressource->m_textureTiles->bindTexture(0);
    m_uniNoiseTexture = m_ressource->m_textureNoise->bindTexture(1);
    m_uniCursorLightColor  = engine->getCursorLightColor();
    m_uniCursorLightPos    = engine->getCursorLightPos();
    m_uniAmbientLightColor = engine->getAmbientLightColor();
    m_uniFlowScale  = glm::vec2(.0002f);
    m_uniFlowTime   = glm::vec2(time);
    
    glDrawArrays(GL_TRIANGLES, 0, getVertexCount());
}

void RendererMap::onTileChange(const glm::ivec2& pos) {
    tile(pos.x / TILESIZE, pos.y / TILESIZE).dirty = true;
}

void RendererMap::onMapSizeChange(int width, int height) {
    assert(width != 0 && height != 0);
    assert(TILESIZE != 0);
    
    m_width  = width / TILESIZE  + 1;
    m_height = height / TILESIZE + 1;
    
    m_vertexBuffer.BufferData(getVertexCount() * sizeof(Vertex), nullptr);
    
    m_tiles.resize(m_width * m_height);
    
    updateAll();
}

void RendererMap::setEngine(Engine3D* engine) {
    engine->signalRender().connect(sigc::mem_fun(this, &RendererMap::onRender));
}

void RendererMap::setRessources(MapRessource* ressource) {
    m_ressource = ressource;
}

void RendererMap::setMap(StateMap* map) {
    m_map = map;
    
    map->signalTileChange().connect(sigc::mem_fun(this, &RendererMap::onTileChange));
    
    auto size = map->getSize();
    onMapSizeChange(size.x, size.y);
}

void RendererMap::updateAll() {
    for (int y = 0; y < m_height; y++){
        for (int x = 0; x < m_width; x++){
            updateTile(glm::ivec2(x, y));
        }
    }
}

void RendererMap::updateTile(const glm::ivec2& pos) {
    Vertex* const buffer = tile(pos).vertices.data();
    Vertex* offset = buffer;
    const glm::ivec2 pOffset = pos * glm::ivec2(TILESIZE);
            
    for (int y = 0; y < TILESIZE; y++){
        for (int x = 0; x < TILESIZE; x++){
            offset = addVertices(offset, x + pOffset.x, y + pOffset.y);
        }
    }
    
    int size = sizeof(Vertex) * TILESIZE * TILESIZE * 6;
    int off  = size * (pos.x + pos.y * m_width);
    m_vertexBuffer.BufferSubData(off, size, buffer);
    
    tile(pos).dirty = false;
}

RendererMap::Vertex* RendererMap::addVertices(Vertex* vertex, int x, int y) {
    Vertex V[4];
    std::bitset<4> wall;
    
    glm::vec3 posScale = glm::vec3(m_map->getTileWidth(), 1,  m_map->getTileWidth());
    
    wall[0] = isWall(x  , y  );
    wall[1] = isWall(x+1, y  );
    wall[2] = isWall(x+1, y+1);
    wall[3] = isWall(x  , y+1);
    
    float h1 = m_map->getHeight(x  , y  ) + wall[0] * m_map->getTileHeight();
    float h2 = m_map->getHeight(x+1, y  ) + wall[1] * m_map->getTileHeight();
    float h3 = m_map->getHeight(x+1, y+1) + wall[2] * m_map->getTileHeight();
    float h4 = m_map->getHeight(x  , y+1) + wall[3] * m_map->getTileHeight();
    
    Type type = TYPE_GROUND;
    int rot  = 0;
    int flip = 1;
    if (wall == std::bitset<4>(0)){        //0000
        rot = 0;
        type = TYPE_GROUND;
    }else if (wall == std::bitset<4>(1)){  //0001
        rot = 1;
        type = TYPE_CORNEROUT;
        flip = 0;
    }else if (wall == std::bitset<4>(2)){  //0010
        rot = 2;
        type = TYPE_CORNEROUT;
    }else if (wall == std::bitset<4>(3)){  //0011
        rot = 2;
        type = TYPE_WALL;
    }else if (wall == std::bitset<4>(4)){  //0100
        rot = 3;
        type = TYPE_CORNEROUT;
        flip = 0;
    }else if (wall == std::bitset<4>(5)){  //0101
        rot = 0; 
        type = TYPE_EDGE;
    }else if (wall == std::bitset<4>(6)){  //0110
        rot = 3;
        type = TYPE_WALL;
    }else if (wall == std::bitset<4>(7)){  //0111
        rot = 2;
        type = TYPE_CORNERIN;
    }else if (wall == std::bitset<4>(8)){  //1000
        rot = 0;
        type = TYPE_CORNEROUT;
    }else if (wall == std::bitset<4>(9)){  //1001
        rot = 1;
        type = TYPE_WALL;
        flip = 0;
    }else if (wall == std::bitset<4>(10)){ //1010
        rot = 1;
        type = TYPE_EDGE;
    }else if (wall == std::bitset<4>(11)){ //1011
        rot = 1;
        type = TYPE_CORNERIN;
        flip = 0;
    }else if (wall == std::bitset<4>(12)){ //1100
        rot = 0;
        type = TYPE_WALL;
    }else if (wall == std::bitset<4>(13)){ //1101
        rot = 0;
        type = TYPE_CORNERIN;
    }else if (wall == std::bitset<4>(14)){ //1110
        rot = 3;
        type = TYPE_CORNERIN;
        flip = 0;
    }else if (wall == std::bitset<4>(15)){ //1111
        rot = 0;
        type = TYPE_ROOF;
    }
    
    //float l = (1.0f / 99) * (getTexture(glm::ivec2(x, y), type));
    float l = (getTexture(glm::ivec2(x, y), type));
    
    glm::vec3 vertOffset = glm::vec3(m_map->getTileWidth() / 2, 0, m_map->getTileWidth() / 2);
    
    glm::vec2 flow = glm::vec2(0);
    if (m_map->getTileType(x, y) == m_map->TILE_WATER || 
            m_map->getTileType(x, y) == m_map->TILE_LAVA){
        flow = glm::vec2(1);
    }
    
    V[0].flow = flow;
    V[1].flow = flow;
    V[2].flow = flow;
    V[3].flow = flow;
    
    V[0].pos = glm::vec3(x    , h1, y    ) * posScale - vertOffset;
    V[1].pos = glm::vec3(x + 1, h2, y    ) * posScale - vertOffset;
    V[2].pos = glm::vec3(x + 1, h3, y + 1) * posScale - vertOffset;
    V[3].pos = glm::vec3(x    , h4, y + 1) * posScale - vertOffset;
    
    V[0].normal = glm::vec3(0, 1, 0);
    V[1].normal = glm::vec3(0, 1, 0);
    V[2].normal = glm::vec3(0, 1, 0);
    V[3].normal = glm::vec3(0, 1, 0);
    
    V[(0 + rot) % 4].uv     = glm::vec3(0, 0, l);
    V[(1 + rot) % 4].uv     = glm::vec3(1, 0, l);
    V[(2 + rot) % 4].uv     = glm::vec3(1, 1, l);
    V[(3 + rot) % 4].uv     = glm::vec3(0, 1, l);
    
    
    vertex[0] = V[(0 + flip) % 4];
    vertex[1] = V[(1 + flip) % 4];
    vertex[2] = V[(2 + flip) % 4];

    vertex[3] = V[(2 + flip) % 4];
    vertex[4] = V[(3 + flip) % 4];
    vertex[5] = V[(0 + flip) % 4];
    
    /*for (int a = 0; a < 2; a++){
        glm::vec3 d1 = vertex[a * 3].pos - vertex[a * 3 + 1].pos;
        glm::vec3 d2 = vertex[a * 3].pos - vertex[a * 3 + 2].pos;
        
        glm::vec3 normal = glm::normalize(glm::cross(d1, d2));
        
        vertex[a * 3 + 0].normal = normal;
        vertex[a * 3 + 1].normal = normal;
        vertex[a * 3 + 2].normal = normal;
    }*/
    
    return vertex + 6;
}

bool RendererMap::isWall(int x, int y) {
    return (m_map->getTileVisib(x-1, y-1) != m_map->VISIB_TRUE && 
            m_map->getTileVisib(x-1, y) != m_map->VISIB_TRUE &&
            m_map->getTileVisib(x, y-1) != m_map->VISIB_TRUE && 
            m_map->getTileVisib(x, y) != m_map->VISIB_TRUE);
    //return m_map->isTileWall(x, y);
}


int RendererMap::getTexture(const glm::ivec2& pos, Type type) {
    if (type == TYPE_ROOF){
        return 70;
    }
    int offset = 0;
    
    if (type == TYPE_CORNERIN){
        offset = 30;
    }else if (type == TYPE_CORNEROUT){
        offset = 50;
    }else if (type == TYPE_EDGE){
        return 77;
    }
    
    switch(m_map->getTileType(pos)){
        case StateMap::TILE_GROUND:
            if (type != TYPE_GROUND){
                return 1 + offset;
            }
            return 0;
        case StateMap::TILE_SOLID:
            return 5 + offset;
        case StateMap::TILE_DIRT:
            return 2 + offset;
        case StateMap::TILE_LOOSE:
            return 3 + offset;
        case StateMap::TILE_HARD:
            return 4 + offset;
        case StateMap::TILE_LAVA:
            return 46;
        case StateMap::TILE_WATER:
            return 45;
        case StateMap::TILE_CRYSTALSEAM:
            return 20;
        case StateMap::TILE_ORESEAM:
            return 40;
        case StateMap::TILE_RECHARGE:
            return 67;
        case StateMap::TILE_SLUGHOLE:
            return 30;
        case StateMap::TILE_GRAVEL:
            return 10;
        default:
            return 61;
    }
    
    return 60;
}

bool RendererMap::rayTest(const glm::vec3& orig, const glm::vec3& dir, glm::vec3& coll) {
    float minLen = std::numeric_limits<float>::infinity();
    
    for (auto& tile: m_tiles){
        for (int t = 0; t < tile.vertices.size() / 3; t++){
            glm::vec3 point;
            
            glm::vec3 V0 = tile.vertices[t * 3 + 0].pos;
            glm::vec3 V1 = tile.vertices[t * 3 + 1].pos;
            glm::vec3 V2 = tile.vertices[t * 3 + 2].pos;
            
            if (glm::intersectRayTriangle(orig, dir, V1, V0, V2, point)){
                point = orig + dir * point.z;
                float len = glm::dot(orig - point, orig - point);
                if (len < minLen){
                    coll = point;
                    minLen = len;
                }
            }
        }
    }
    
    return minLen != std::numeric_limits<float>::infinity();
}
