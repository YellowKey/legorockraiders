/* 
 * File:   Engine3D.cpp
 * Author: Kevin
 * 
 * Created on 14. Mai 2015, 14:10
 */

#include <glm/gtc/matrix_transform.hpp>

#include "Engine3D.h"

#include <iostream>
#include <algorithm>
#include "util.h"
#include "App.h"

#include "EngineObject.h"
#include "EngineShape.h"

Engine3D::Engine3D(){
    m_shader.load("shader/shader3D");
    
    m_uniCameraMatrix = m_shader.getUniform("cameraMatrix");
    m_uniWorldMatrix  = m_shader.getUniform("worldMatrix");
    m_uniNormalMatrix = m_shader.getUniform("normalMatrix");
    m_uniColorTexture = m_shader.getUniform("colorTexture");
    m_uniColorFactor  = m_shader.getUniform("colorFactor");
    m_uniColorTrans   = m_shader.getUniform("colorTrans");
    m_uniCameraPos    = m_shader.getUniform("cameraPos");
    m_uniGamma        = m_shader.getUniform("gamma");
    
    m_uniCursorLightPos    = m_shader.getUniform("cursorLightPos");
    m_uniCursorLightColor  = m_shader.getUniform("cursorLightColor");
    m_uniAmbientLightColor = m_shader.getUniform("ambientLightColor");
    
    m_vertexArray.vertexAttribFormat(0, 3, GL_FLOAT, false, (void*)offsetof(Vertex, pos));
    m_vertexArray.vertexAttribFormat(1, 2, GL_FLOAT, false, (void*)offsetof(Vertex, uv));
    m_vertexArray.vertexAttribFormat(2, 3, GL_FLOAT, false, (void*)offsetof(Vertex, normal));
    
    m_vertexArray.bindVertexBuffer(0, &m_vertexBuffer, 0, sizeof(Vertex));
    m_vertexArray.vertexAttribBinding(0, 0);
    m_vertexArray.vertexAttribBinding(1, 0);
    m_vertexArray.vertexAttribBinding(2, 0);
    
    m_vertexArray.enableVertexAttribArray(0);
    m_vertexArray.enableVertexAttribArray(1);
    m_vertexArray.enableVertexAttribArray(2);
    
    m_vertexBuffer.BufferData(BUFFERSIZE, nullptr);
}

Engine3D::~Engine3D() {
}

void Engine3D::addShape(Shape* shape) {
    int size = shape->getDataSize();
    
    m_vertexBuffer.BufferSubData(m_bufferOffset, size, shape->getData());
    //m_vertexBuffer.BufferData(size, shape->getData());
    shape->setBuffer(&m_vertexBuffer, m_bufferOffset);
    
    m_bufferOffset += size;
    
    //std::cout << "Added Shape (" << (float)m_bufferOffset / BUFFERSIZE * 100 << " %)" << std::endl;
}

void Engine3D::addMesh(Mesh* mesh) {
    for (int a = 0; a < mesh->getNumGroups(); a++){
        for (int b = 0; b < mesh->getNumShapes(a); b++){
            addShape(mesh->getShape(a, b));
        }
    }
}

void Engine3D::renderMesh(Mesh* mesh, float time, const glm::mat4& model) {
    for (int a = 0; a < mesh->getNumGroups(); a++){
        for (int b = 0; b < mesh->getNumShapes(a); b++){
            if (!mesh->getShape(a,b)->getAdditiveBlending()){
                renderShape(mesh->getShape(a, b), model * mesh->getMatrix(a, time));
            }
        }
    }
    
    
    glEnable(GL_BLEND);
    glDepthMask(GL_FALSE);
    glBlendFunc(GL_ONE, GL_ONE);
    
    for (int a = 0; a < mesh->getNumGroups(); a++){
        for (int b = 0; b < mesh->getNumShapes(a); b++){
            if (mesh->getShape(a,b)->getAdditiveBlending()){
                renderShape(mesh->getShape(a, b), model * mesh->getMatrix(a, time));
            }
        }
    }
    
    glDepthMask(GL_TRUE);
    glDisable(GL_BLEND);
}

void Engine3D::renderShape(Shape* shape, const glm::mat4& matrix) {
    m_vertexArray.bind();
    
    int startVertex = shape->getBufferOffset() / shape->getVertexSize();
    
    m_shader.use();
    
    Texture* texture = shape->getTexture(Shape::TEXTURE_COLOR);
    
    if (!texture){
        texture = App::getApp()->getDefaultTexture();
    }
    
    //GLuint textureId = texture->getGlTexture()->getTextureHandle();
    
    //glActiveTexture(GL_TEXTURE0);
    //glBindTexture(GL_TEXTURE_2D, textureId);
    
    m_uniCameraMatrix = getCameraMatrix();
    m_uniWorldMatrix  = matrix;
    m_uniColorTexture = texture->getGlTexture()->bindTexture(0);
    m_uniColorFactor  = glm::vec4(shape->getColor(), 1);
    m_uniColorTrans   = texture->getTransColor();
    m_uniCameraPos    = m_cameraPos;
    m_uniGamma        = getGamma();
    
    glDrawArrays(GL_TRIANGLES, startVertex, shape->getNumVertices());
    
   //texture->getGlTexture()->unbindTexture();
}

glm::vec3 Engine3D::getCameraPos() {
    return m_cameraPos;
}

glm::vec3 Engine3D::getCameraRot() {
    return m_cameraRot;
}

void Engine3D::setCameraPos(const glm::vec3& pos) {
    m_cameraPos = pos;
}

void Engine3D::setCameraRot(const glm::vec3& rot) {
    m_cameraRot = rot;
}

void Engine3D::setAspectRatio(float ratio) {
    m_aspectRatio = ratio;
}

void Engine3D::setCursorLightPos(const glm::vec3& pos) {
    m_cursorLightPos = pos;
}

void Engine3D::setCursorLightColor(const glm::vec3& color) {
    m_cursorLightColor = color;
}

glm::vec3 Engine3D::getAmbientLightColor() {
    return glm::vec3(0.2f);
}

glm::vec3 Engine3D::getCursorLightColor() {
    return m_cursorLightColor;
}

glm::vec3 Engine3D::getCursorLightPos() {
    return m_cursorLightPos;
}




void Engine3D::setRenderArea(const glm::vec2 start, const glm::vec2 end) {
    m_viewStart = start;
    m_viewEnd   = end;
}


glm::mat4 Engine3D::getCameraMatrix() {
    glm::mat4 project = glm::infinitePerspective(60.0f / 180.0f * 3.141f, m_aspectRatio, 0.5f);
    glm::mat4 model = glm::mat4();

    model = glm::rotate(model, -m_cameraRot.x, glm::vec3(1.0f, 0.0f, 0.0f));
    model = glm::rotate(model, -m_cameraRot.y, glm::vec3(0.0f, 1.0f, 0.0f));
    model = glm::rotate(model, -m_cameraRot.z, glm::vec3(0.0f, 0.0f, 1.0f));

    model   = glm::translate(model, -m_cameraPos);
    
    return project * model;
}

float Engine3D::getBufferUsage() {
    return (float)m_bufferOffset  / BUFFERSIZE;
}


void Engine3D::registerObject(EngineObject* obj) {
    Mesh* mesh = obj->getMesh();
    
    for (int g = 0; g < mesh->getNumGroups(); g++){
        for (int s = 0; s < mesh->getNumShapes(g); s++){
            Shape* shape = mesh->getShape(g, s);
            EngineShape* eShape = new EngineShape(obj, g);
            
            eShape->setMatrix(obj->getMatrix() * mesh->getMatrix(g, obj->getAnimTime()));
            
            m_shapes[shape].push_back(eShape);
        }
    }
    
    m_objects.push_back(obj);
}

void Engine3D::removeObject(EngineObject* obj) {
    for (auto& shapeList : m_shapes){
        auto& list = shapeList.second;
        
        auto iter = list.begin();
        
        while(iter != list.end()){
            if ((*iter)->getParent() == obj){
                list.erase(iter);
            }else{
                iter++;
            }
        }
    }
    
    m_objects.erase(std::remove(m_objects.begin(), m_objects.end(), obj), m_objects.end());
}



void Engine3D::render() {
    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_SCISSOR_TEST);
    glFrontFace(GL_CW);
    
    glViewport(m_viewStart.x, m_viewStart.y, m_viewEnd.x, m_viewEnd.y);
    glScissor(m_viewStart.x, m_viewStart.y, m_viewEnd.x, m_viewEnd.y);
    
    //External Renderers
    m_signalRender(this);
    
    m_numRenderedShapes = 0;
    m_numUpdatedObjects = 0;
    
    renderPass(false);
    
    glEnable(GL_BLEND);
    glDepthMask(GL_FALSE);
    glBlendFunc(GL_ONE, GL_ONE);
    
    renderPass(true);
    
    glDepthMask(GL_TRUE);
    glDisable(GL_BLEND);
    
    
    for (auto& object: m_objects){
        if (object->hasChanged()){
            m_numUpdatedObjects++;
        }
        object->markUnchanged();
    }
    
    std::cout.flush();
    
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);
    glDisable(GL_SCISSOR_TEST);
}

void Engine3D::renderPass(bool trans){
    m_vertexArray.bind();
    
    m_shader.use();
     
    m_uniCursorLightPos    = getCursorLightPos();
    m_uniCameraMatrix      = getCameraMatrix();
    m_uniCameraPos         = getCameraPos();
    m_uniGamma             = getGamma();
    
    for (const auto& shapeList: m_shapes){
        const auto& shape = shapeList.first;
        
        
        if (shape->getAdditiveBlending() == trans){
            
            int numShapes = 0;
            
            int startVertex = shape->getBufferOffset() / shape->getVertexSize();

            Texture* texture = shape->getTexture(Shape::TEXTURE_COLOR);

            if (!texture){
                texture = App::getApp()->getDefaultTexture();
            }

            glm::vec4 colorFac = EngineObject::DEFAULTCOLOR;
            
            float lumi = shape->getLuminance();
            
            m_uniCursorLightColor  = getCursorLightColor() * (1.0f - lumi);
            m_uniAmbientLightColor = glm::mix(getAmbientLightColor(), glm::vec3(1.0f), lumi);
           
            m_uniColorTexture = texture->getGlTexture()->bindTexture(0);
            m_uniColorFactor  = glm::vec4(shape->getColor(), 1) * colorFac;
            m_uniColorTrans   = texture->getTransColor();
            
            for (const auto& object: shapeList.second){
                if (object->getParent()->hasChanged()){
                    Mesh* mesh = object->getParent()->getMesh();
                    float time = object->getParent()->getAnimTime();
                    glm::mat4 objMatrix = mesh->getMatrix(object->getGroup(), time);
                    object->setMatrix(object->getParent()->getMatrix() * objMatrix);
                }
                
                m_uniWorldMatrix  = object->getMatrix();
                m_uniNormalMatrix = glm::transpose(glm::inverse(object->getMatrix()));

                if (object->getParent()->getColor() != colorFac){
                    colorFac = object->getParent()->getColor();
                    m_uniColorFactor = glm::vec4(shape->getColor(), 1) * colorFac;
                }
                
                glDrawArrays(GL_TRIANGLES, startVertex, shape->getNumVertices());
                
                m_numRenderedShapes += 1;
                numShapes +=1 ;
            }
        }
    }
}

int Engine3D::getNumRenderedShapes() {
    return m_numRenderedShapes;
}

int Engine3D::getNumUpdatedObjects() {
    return m_numUpdatedObjects;
}

sigc::signal<void, Engine3D*>& Engine3D::signalRender() {
    return m_signalRender;
}

float Engine3D::getGamma() {
    return m_gamma;
}

void Engine3D::setGamma(float gamma) {
    m_gamma = gamma;
}

