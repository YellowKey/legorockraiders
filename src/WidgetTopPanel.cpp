/* 
 * File:   WidgetTopPanel.cpp
 * Author: Kevin
 * 
 * Created on 23. Juni 2015, 16:51
 */

#include "WidgetTopPanel.h"

WidgetTopPanel::WidgetTopPanel() {
    m_buttonArms.setRelPos(glm::vec2(72, 17));
    m_buttonOptions.setRelPos(glm::vec2(110, 17));
    m_buttonPrio.setRelPos(glm::vec2(147, 17));
    
    addChild(&m_buttonArms);
    addChild(&m_buttonOptions);
    addChild(&m_buttonPrio);
}

WidgetTopPanel::~WidgetTopPanel() {
}

void WidgetTopPanel::setTextures(InterfaceRessources* ress) {
    setTexture(ress->topPanel);
    
    m_buttonArms.setTextureHover(ress->tpArmsHigh);
    m_buttonArms.setTexturePushed(ress->tpArmsPush);
    m_buttonOptions.setTextureHover(ress->tpOptionsHigh);
    m_buttonOptions.setTexturePushed(ress->tpOptionsPush);
    m_buttonPrio.setTextureHover(ress->tpPrioHigh);
    m_buttonPrio.setTexturePushed(ress->tpPrioPush);
}

void WidgetTopPanel::onUpdate() {

}
