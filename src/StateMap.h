/* 
 * File:   StateMap.h
 * Author: Kevin
 *
 * Created on 29. Dezember 2015, 20:42
 */

#ifndef STATEMAP_H
#define STATEMAP_H

#include <glm/vec2.hpp>

#include <sigc++/sigc++.h>

#include <vector>
#include <bitset>
#include <cassert>

class StateMap {
public:
    enum TileType{
        //Defined by Lego Rock Raiders
        TILE_SOLID = 1,
        TILE_HARD = 2,
        TILE_LOOSE = 3,
        TILE_DIRT = 4,
        TILE_GROUND = 5,
        TILE_LAVA = 6,
        TILE_GRAVEL = 7,
        TILE_ORESEAM = 8,
        TILE_WATER = 9,
        TILE_CRYSTALSEAM = 10,
        TILE_RECHARGE = 11,
        TILE_SLUGHOLE = 12,
        
        //My Tiles
        TILE_BUILD,
        TILE_BUILDWALK,
        TILE_PATH,
    };
    
    enum TileMark{
        MARK_SELECTED  = 1,
        MARK_DRILL     = 2,
        MARK_DYNAMITE  = 3,
        MARK_CLEANUP   = 4,
        MARK_BUILD     = 5,
        MARK_BUILDPATH = 6,
    };
    
    enum TileVisib{
        VISIB_WALL = 0,
        VISIB_TRUE = 1,
        VISIB_FALSE = 2,
    };
    
    StateMap();
    StateMap(int width, int height);
    ~StateMap();
    
    void setSize(int width, int height);
    
    TileType  getTileType (int x, int y) const;
    TileType  getTileType (const glm::ivec2& pos) const;
    TileVisib getTileVisib(int x, int y) const;
    TileVisib getTileVisib(const glm::ivec2& pos) const;
    bool      getTileMark (int x, int y, TileMark mark) const;
    bool      getTileMark (const glm::ivec2& pos, TileMark mark) const;
    int       getTileOre  (int x, int y) const;
    int       getTileOre  (const glm::ivec2& pos) const;
    int       getTileCrystal(int x, int y) const;
    int       getTileCrystal(const glm::ivec2& pos) const;
    
    TileType  setTileType (int x, int y, TileType  type);
    TileType  setTileType (const glm::ivec2& pos, TileType  type);
    TileVisib setTileVisib(int x, int y, TileVisib visible);
    TileVisib setTileVisib(const glm::ivec2& pos, TileVisib visible);
    bool      setTileMark (int x, int y, TileMark mark, bool state);
    bool      setTileMark (const glm::ivec2& pos, TileMark mark, bool state);
    int       setTileOre (int x, int y, int count);
    int       setTileOre (const glm::ivec2& pos, int count);
    int       setTileCrystal(int x, int y, int count);
    int       setTileCrystal(const glm::ivec2& pos, int count);
    
    
    bool isTileWall(int x, int y) const;
    bool isTileWall(const glm::ivec2& pos) const;
    
    void setHeight(int x, int y, float height);
    void setHeight(const glm::ivec2& pos, float height);
    
    float getHeight(int x, int y) const;
    float getHeight(const glm::vec2& pos) const;
    
    
    void setMapHeight(float height);
    void setTileWidht(float width);
    void setTileHeight(float height);
    
    float getMapHeight() const;
    float getTileWidth() const;
    float getTileHeight() const;
    
    glm::ivec2 getSize() const;
    
    //Needs to be called after changes
    void update(int x, int y);
    void updateNeighbours(int x, int y);
    void updateAll();
    
    void destroy(int x, int y);
    
    sigc::signal<void, const glm::ivec2&>& signalTileChange();
    sigc::signal<void, int, int >& signalSizeChange();
private:
    bool canTileBeWall(int x, int y);
    
    struct Tile{
        TileType type;
        TileVisib visib;
        int crystalCount;
        int oreCount;
        
        std::bitset<16> marks;
    };
    
    std::vector<Tile>  m_tiles;
    std::vector<float> m_heightmap;
    
    void spawnRessources(int x, int y);
    
    int m_width;
    int m_height;
    
    float m_tileHeight = 40.0f;
    float m_tileWidth  = 40.0f;
    float m_mapHeight  = 100.0f;
    
    inline void clampPos(int& x, int& y) const{
        if (y < 0){
            y = 0;
        }else if (y >= m_height){
            y = m_height - 1;
        }
        
        if (x < 0){
            x = 0;
        }else if (x >= m_width){
            x = m_width - 1;
        }
    }
    
    inline Tile& tile(int x, int y){
        clampPos(x, y);
        return m_tiles[x + y * m_width];
    }
    
    inline const Tile& tile(int x, int y) const{
        clampPos(x, y);
        return m_tiles[x + y * m_width];
    }
    
    inline float& heightmap(int x, int y){
        clampPos(x, y);
        return m_heightmap[x + y * (m_width + 1)];
    }
    
    inline const float& heightmap(int x, int y) const{
        clampPos(x, y);
        return m_heightmap[x + y * (m_width + 1)];
    }
    
    sigc::signal<void, const glm::ivec2&> m_signalTileChange;
    sigc::signal<void, int, int> m_signalSizeChange;
};

#endif /* STATEMAP_H */

