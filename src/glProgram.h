/* 
 * File:   glProgram.h
 * Author: Kevin
 *
 * Created on 22. November 2014, 14:46
 */

#ifndef GLPROGRAM_H
#define	GLPROGRAM_H

#include "glShader.h"
#include "glUniform.h"
#include <string>

using std::string;

class glProgram {
public:
    
    glProgram();
    glProgram(string name, bool tesselation = false, bool geometry = false);
    virtual ~glProgram();
    
    void attachShader(const glShader& shader);
    void load(string name, bool tesselation = false, bool geometry = false);
    void link();
    void use();
    
    GLuint getHandle() const;
    
    glUniform getUniform(const string& name);
    GLint     getAttribLocation(const string& name);
private:
    GLuint m_handle;
};

#endif	/* GLPROGRAM_H */

