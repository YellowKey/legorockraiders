/* 
 * File:   Font.cpp
 * Author: Kevin
 * 
 * Created on 5. Mai 2015, 16:49
 */

#include "Font.h"

#include "util.h"
#include "App.h"

#include <iostream>
#include <cstring>

constexpr int Font::CHARSHORI;
constexpr int Font::CHARSVERT;
constexpr int Font::NUMCHARS;

Font::Font(const void* data, int width, int height, Format format){
    scanFont(data, width, height, format);
    upload(data, width, height, format);
}


Font::~Font() {
    
}

void Font::scanFont(const void* data, int width, int height, Format format) {
    int charsHori = CHARSHORI;
    
    
    if (height < 150){
        charsHori = 13;
    }
    
    const glm::ivec2 size(width, height);
    const glm::ivec2 charSize(width / CHARSVERT, height / charsHori);
    glm::ivec2 cPos; //cursorPos
    
    m_charSize = charSize;
    m_imageSize = glm::vec2(width, height);
    
    //std::cout << "Max char size: " << charSize << std::endl;
    
    transColor = getColor(glm::ivec2(0), data, size, format);
    noFontColor = getColor(glm::ivec2(charSize.x - 1, 0), data, size, format);
    
    for (cPos.y = 0; cPos.y < charsHori; cPos.y++){
        for (cPos.x = 0; cPos.x < CHARSVERT; cPos.x++){
            int charId = cPos.x + cPos.y * CHARSVERT;
            glm::ivec2 pos = cPos * charSize;
            
            int width = 0; 
            
            while (width < charSize.x && getColor(pos, data, size, format) != noFontColor){
                ++width;
                ++pos.x;
            }
            
            m_charWidth[cPos.x + cPos.y * CHARSVERT] = width;
            //std::cout << charId << ": " << width << std::endl;
        }
    }
}

void Font::upload(const void* data, int width, int height, Format format) {
    int32_t* image = new int32_t[width * height];
    
    for (int y = 0; y < height; y++){
        for (int x = 0; x < width; x++){
            int32_t color = getColor(glm::ivec2(x, y), data, glm::ivec2(width, height), format);
            
            if (color == transColor){
                color &= 0xFFFFFF; // mask out alpha;
            }
            if (color == noFontColor){
                color = (transColor & 0xFFFFFF);
            }
            
            image[x + y * width] = color;
        }
    }
    
    load(image, width, height, RGBA8);
    
    delete[] image;
}


int32_t Font::getColor(const glm::ivec2& pos, const void* data, const glm::ivec2& size, Format format) {
    int pixelSize = 3;
    
    if (format == RGBA8){
        pixelSize = 4;
    }
    
    int lineSize = pixelSize * size.x;
    
    char* pixelPointer = ((char*)data) + (pos.x * pixelSize + (size.y - pos.y - 1) * lineSize);
    
    int color = 0xFF000000;
    memcpy(&color, pixelPointer, pixelSize);
    
    //std::cout << pos << ": " << std::hex << color << std::dec << ", " << (intptr_t) pixelPointer << std::endl;
    
    return color;
}



int Font::getFontSize() const{
    return m_charSize.y;
}

glm::vec2 Font::getCharStart(int id) const{
    return toFloat(getCharPos(id) + glm::ivec2(0, m_charSize.y)) ;
}

glm::vec2 Font::getCharEnd(int id) const{
    if ((unsigned)(id - 32) < NUMCHARS){
        return toFloat(getCharPos(id) + glm::ivec2(m_charWidth[id - 32], 0));
    }else{
        return glm::vec2(0);
    }
}

glm::vec2 Font::toFloat(const glm::ivec2& pos) const{
    return glm::vec2(pos.x, pos.y) / m_imageSize;
}

glm::ivec2 Font::getCharPos(int id) const{
    id -= 32;
    
    if ((unsigned)id < NUMCHARS){
        glm::ivec2 pos(id % CHARSVERT, id / CHARSVERT);
        return glm::ivec2(pos * m_charSize);
    }else{
        return glm::ivec2(0);
    }
}

int Font::getWidth(const std::string& text) const{
    int width = 0;
    
    for (int a = 0; a < text.size(); a++){
        int charId = text[a];
        
        if (charId < NUMCHARS){
            width += m_charWidth[charId - 32];
        }
    }
    
    return width;
}

glm::vec2 Font::getCharSize(int id) const{
    id -= 32;
    
    if ((unsigned)id < NUMCHARS){
        return glm::vec2(m_charWidth[id], m_charSize.y);
    }else{
        return glm::vec2(0);
    }
}
