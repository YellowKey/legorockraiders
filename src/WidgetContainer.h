/* 
 * File:   WigetContainer.h
 * Author: kevin
 *
 * Created on July 1, 2015, 1:58 PM
 */

#ifndef WIGETCONTAINER_H
#define	WIGETCONTAINER_H

#include "Widget.h"

class WidgetContainer: public Widget{
public:
    WidgetContainer();
    WidgetContainer(const WidgetContainer& orig) = delete;
    virtual ~WidgetContainer();
    
    virtual bool isOver(const glm::vec2 relPos);
private:

};

#endif	/* WIGETCONTAINER_H */

