/* 
 * File:   AnimInfo.h
 * Author: Kevin
 *
 * Created on 16. Oktober 2015, 15:26
 */

#ifndef ANIMINFO_H
#define	ANIMINFO_H

#include <string>
#include <bitset>

#include "Mesh.h"
#include "Sample.h"

class AnimInfo {
public:
    struct Activity{
        std::string name;
        Mesh*   mesh = nullptr;
        float   transCoef;
        Sample* sample = nullptr; //TODO implement audio
        int     trigger;
    };
    
    enum TileType{
        TILE_EMPTY,
        TILE_BUILDING,
        TILE_POWER,
        TILE_WATER,
    };
    
    struct Upgrade{
        AnimInfo* mesh = nullptr;
        std::string null;
        int slot;
        std::bitset<4> upgrades;
    };
    
    AnimInfo();
    AnimInfo(const AnimInfo& orig);
    virtual ~AnimInfo();
    
    void addActivity(Activity* activity);
    Activity* getActivity(const std::string& name, bool fallback = true);
    Activity* getDefaultActivity();
    
    int getNumTiles();
    TileType getTileType(int id);
    glm::ivec2 getTilePos(int id);
    void addTile(const glm::ivec2& pos, TileType type);
    
    void addUpgrade(Upgrade* upgrade);
    Upgrade* getUpgrade(std::bitset<4> upgrades, int id);
    int countUpgrades(std::bitset<4> upgrades);
    
    float scale = 1.0f;
    AnimInfo* wheel = nullptr;
    
    //Nulls
    std::string wheelNull;
    std::string itemNull;
    std::string driverNull;
    std::string depositNull;
    std::string cameraNull;
    std::string carryNull;
    std::string drillNull;
    std::string toolNull;
    
    bool holdMissing; //unkown (seen in WalkerLegs.ae)
    
    bool cameraFlipDir;
    int  cameraNullFrames;
private:
    struct Tile{
        glm::ivec2 pos;
        TileType type;
    };
    
    std::vector<Tile> m_tiles;
    std::vector<Activity*> m_activities;
    std::vector<Upgrade*> m_upgrades;
};

#endif	/* ANIMINFO_H */

