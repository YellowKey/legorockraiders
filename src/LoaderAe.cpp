/* 
 * File:   LoaderAe.cpp
 * Author: kevin
 * 
 * Created on June 18, 2015, 10:00 AM
 */

#include "LoaderAe.h"

#include <iostream>
#include <algorithm>
#include <iomanip>

#include "util.h"
#include "App.h"

LoaderAe::LoaderAe() {
}

LoaderAe::LoaderAe(const LoaderAe& orig) {
}

LoaderAe::~LoaderAe() {
}

void LoaderAe::load(void* mem, int size) {
    m_loaderCfg.load(mem, size);
    
    auto groupMain   = m_loaderCfg.getMainGroup();
    auto groupActivi = groupMain->getGroup("Activities");
    
    for (int a = 0; a < Activity::NUMANIMATIONS; a++){
        auto* child = groupActivi->getGroup(Activity::getAnimationName(a));
        auto& anim = m_animations[a];
        
        anim.fileName = "";
        anim.transCoef = 0;
            
        if (child){
            auto* childAnim = groupMain->getGroup(child->getString());
            
            if (childAnim){
                auto* childFile = childAnim->getGroup("FILE");
                auto* childTrans = childAnim->getGroup("TRANSCOEF");

                if (childFile){
                    anim.fileName = childFile->getString() + ".lws";
                }
                if (childTrans){
                    anim.transCoef = childFile->getFloat();
                }
            }
        }
    }
    
    if (isDebugging() ){
        std::cout << "----------------------------------------" << std::endl;
        std::cout << "                New Animation           " << std::endl;
        std::cout << "----------------------------------------" << std::endl;
        
        int w = 0;
        for (int a = 0; a < m_animations.size(); a++){
            if (m_animations[a].fileName != ""){
                w = std::max(w, (int)Activity::getAnimationName(a).size());
            }
        }
        
        for (int a = 0; a < m_animations.size(); a++){
            const auto& anim = m_animations[a];
            
            if (anim.fileName != ""){
                std::cout << std::setw(w) << std::left << Activity::getAnimationName(a);
                std::cout << " = " << anim.fileName << std::endl;
            }
        }
    }
}

void LoaderAe::setPath(const std::string& path) {
    m_path = path;
}


Activity* LoaderAe::getActivity() {
    Activity* activity = new Activity;
    LoaderFile* loader = App::getApp()->getFileLoader();
    
    for (int a = 0; a < m_animations.size(); a++){
        const auto& anim = m_animations[a];
        
        if (anim.fileName != ""){
            Mesh* mesh = loader->loadMesh(m_path + anim.fileName);
            App::getApp()->getEngine3D()->addMesh(mesh);
            
            activity->setAnimMesh(a, mesh);
            activity->setAnimTransCoef(a, anim.transCoef);
        }
    }
    
    return activity;
}



bool LoaderAe::isDebugging() {
    return false;
}
