#version 140

//#extension GL_ARB_gpu_shader5 : enable

in vec2 vert_uv;
in vec3 vert_pos;
in vec3 vert_normal;
//flat in  int  vert_texId;

uniform sampler2D colorTexture;
uniform vec4      colorFactor;
uniform vec4      colorTrans;

uniform vec3 cursorLightPos;
uniform vec3 cursorLightColor;
uniform vec3 ambientLightColor;

uniform vec3 cameraPos;

uniform float gamma;

out vec4 color;

void main(){
    //Fog
    /*float fogFac = 0;
    if (cameraPos.y > 60.0f){
        vec3 camNormal = normalize(cameraPos - vert_pos);
        fogFac = (60.0f - vert_pos.y) / abs(camNormal.y);
    }else{
        fogFac = distance(cameraPos, vert_pos);
    }
    fogFac = clamp(fogFac / 1000.0f, 0, 1);*/

    vec3 normal = normalize(vert_normal);

    color = texture(colorTexture, vert_uv);

    //Ambient light
    vec3 light = ambientLightColor;

    //CursorLight
    vec3 delta = cursorLightPos - vert_pos;
    float f = clamp(1000 / dot(delta, delta), 0.0f, 1.0f);
    f *= clamp(dot(normalize(delta), normal), 0, 1);
    light += cursorLightColor * f;

    if (distance(color, colorTrans) <= 0.00001){
        discard;
    }

    color *= colorFactor;

    color = color * pow(vec4(light, 1), vec4(gamma));

    //color += vec4(1,1,1,0) * fogFac;
    //color = vec4(abs(normal), 1);
    //color = vec4(normal * 0.5f + vec3(0.5f), 1);
}
