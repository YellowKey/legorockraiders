#version 140

#extension GL_ARB_explicit_attrib_location : require
//#extension GL_ARB_gpu_shader5 : enable

layout(location = 0) in vec2 vertexPosition;
layout(location = 1) in vec2 texturePosition;
layout(location = 2) in vec4 vertexColor;
layout(location = 3) in int  textureId;

out vec2 vert_uv;
out vec4 vert_color;
flat out int  vert_texId;

uniform vec2 scaleFactor;
uniform vec2 screenOffset;

void main(){
    vert_uv    = texturePosition;
    vert_color = vertexColor;
    vert_texId = textureId;

    vec2 pos = vertexPosition * scaleFactor + screenOffset;

    gl_Position = vec4(pos, -0.5, 1);
}
