#version 140

in vec3 vert_uv;
in vec3 vert_pos;
in vec3 vert_normal;
in vec2 vert_flow;

uniform sampler2DArray colorTexture;
uniform sampler2D noiseTexture;
uniform vec4      colorFactor;

uniform vec3 cursorLightPos;
uniform vec3 cursorLightColor;
uniform vec3 ambientLightColor;

uniform vec2 flowScale;
uniform vec2 flowOffset;

uniform vec3 cameraPos;

uniform float gamma;

out vec4 color;

void main(){
    float offFac = (texture(noiseTexture, vert_pos.xz * flowScale + flowOffset).r * 2 - 1) * 0.13f;

    vec3 normal = normalize(vert_normal);

    color = texture(colorTexture, vert_uv + vec3(offFac * vert_flow.x, offFac * vert_flow.y, 0));

    //Ambient light
    vec3 light = ambientLightColor;

    //CursorLight
    vec3 delta = cursorLightPos - vert_pos;
    float f = clamp(1000 / dot(delta, delta), 0.0f, 1.0f);
    f *= clamp(dot(normalize(delta), normal), 0, 1);
    light += cursorLightColor * f;

    color *= colorFactor;

    color = color * pow(vec4(light, 1), vec4(gamma));

    //color = vec4(vert_uv, 1);

    //color += vec4(1,1,1,0) * fogFac;
    //color = vec4(abs(normal), 1);
    //color = vec4(normal * 0.5f + vec3(0.5f), 1);
}
