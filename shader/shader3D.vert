#version 140

#extension GL_ARB_explicit_attrib_location : require

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec2 texturePosition;
layout(location = 2) in vec3 vertexNormal;

uniform mat4 cameraMatrix;
uniform mat4 worldMatrix;
uniform mat4 normalMatrix;

out vec2 vert_uv;
out vec3 vert_pos;
out vec3 vert_normal;
//flat out int  vert_texId;

void main(){
    vert_uv     = texturePosition;
    //vert_normal = (normalMatrix * vec4(vertexNormal, 1)).xyz;
    vert_normal = mat3(normalMatrix) * vertexNormal;
    //vert_normal.x = -vert_normal.x; //fix for wrong rotation around y axis

    gl_Position = cameraMatrix * worldMatrix * vec4(vertexPosition, 1);
    vert_pos = (worldMatrix * vec4(vertexPosition, 1)).xyz;
}
