#version 140

#extension GL_ARB_gpu_shader5 : enable

#ifdef GL_ARB_gpu_shader5
    #define TEXTURE_ID vert_texId
    #define NUM_TEXTURES gl_MaxTextureImageUnits
#else
    #define TEXTURE_ID 0
    #define NUM_TEXTURES 1
#endif

in vec2 vert_uv;
in vec4 vert_color;
flat in  int  vert_texId;

uniform sampler2D colorTexture[NUM_TEXTURES];
uniform vec4      transColor[NUM_TEXTURES];

out vec4 color;

void main(){
    
    color = texture(colorTexture[TEXTURE_ID], vert_uv);

    if (color.a < 0.5){
        discard;
    }

    if (distance(color, transColor[TEXTURE_ID]) <= 0.00001){
        discard;
    }

    color *= vert_color;
}
